#!/bin/csh -f
#
# Usage : new_compile_to_c_from_germ.sh
#

source `dirname $0`/../tools/SE_conf || exit 1

# ****** Temporarily disabled because of the following tcc bug <FM-23/02/2005>
# ****** Fatal Error: Value out of INTEGER_32 range.
#   exit_success_code: INTEGER is 0
#
#if (-f `which tcc`) then
#    set CC=tcc
#else
    set CC=gcc
#endif


${CC} -o ${SmartEiffelBin}/compile_to_c compile_to_c.c -lm
/bin/rm -f ${SmartEiffelHome}/install/germ/compile_to_c*.c
/bin/cp -f compile_to_c.c ${SmartEiffelHome}/install/germ/compile_to_c1.c
/bin/cp -f compile_to_c.h ${SmartEiffelHome}/install/germ


echo "Done."
