When previous compiler release is not able to compile the new compiler
(language changes), then we need to generate some new germ.
The new germ allow to regenerate compile_to_c able to compile new eiffel
sources. With this compile_to_c, you can:
- either run c_bootstrap or if it does fail,
- generate compile_to_c*.[ch] files in SmartEiffel/install/germ and
then run SmartEiffel/install.c
 
To update the germ (after incompatible compiler changes):

   compile_to_c new_germ.ace

   svn commit -m "New germ for the new ... keyword" compile_to_c.[ch]
