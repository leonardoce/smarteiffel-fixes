#!/bin/sh
#--
#-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
#-- See the Copyright notice at the end of this file.
#--
#
# Installation of the sedev tools. Currently there is only one such tool: make.
#


path="$0"
path=${path%/*}
cd $path

DIR=${SmartEiffel:-"$HOME"/.serc}
if test ! -d $DIR; then
    mkdir $DIR
fi
cat > $DIR/sedev.se <<EOF
[Tools]
make: make.sh
EOF

SE_BIN=`se -environment | grep '^SE_BIN=' | awk -F= '{print $2}'`
SMART_EIFFEL_SHORT_VERSION=1
export SMART_EIFFEL_SHORT_VERSION
VERSION=`se c2c -version`
unset SMART_EIFFEL_SHORT_VERSION
sed 's!%SE_BIN%!'"${SE_BIN%/}"'!g;s!%VERSION%!'"$VERSION"'!g' < make.sh | awk '/^%LONG_VERSION%$/ {system("se c2c -version | sed '"'"'s/compile_to_c/make.sh'"'"'/g"); next} {print}' > $SE_BIN/make.sh
chmod +x $SE_BIN/make.sh

#--
#-- ------------------------------------------------------------------------------------------------------------------------------
#-- Copyright notice below. Please read.
#--
#-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
#-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
#-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
#-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
#-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
#-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
#--
#-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
#-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
#--
#-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
#--
#-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
#-- ------------------------------------------------------------------------------------------------------------------------------
