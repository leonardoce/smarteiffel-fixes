#!/bin/bash

# This script is used to build the nightly snapshots. For security
# reasons, the copy of this script that is actually run is not copied
# from the repository. If you change it, please send an email to
# frederic.merizen@laposte.net to have the actual script updated.

FORGE_LOGIN=""
FORGE_PASS=""
RELEASE_ID=1879
GROUP_ID=184
PACKAGE_ID=355
SNAP="${HOME}/snapshot"
STAMP="${SNAP}/smarteiffel-stamp.txt"
PUBLIC_HTML="${HOME}/public_html"
SVN_LOG="${SNAP}/svn-log.txt"
OLDCOOKIES="${SNAP}/old_cookies.txt"
NEWCOOKIES="${SNAP}/new_cookies.txt"
ARCHIVE="${SNAP}/smarteiffel.tgz"
FORGE="https://gforge.inria.fr"
CURL="curl -k -b ${OLDCOOKIES} -c ${NEWCOOKIES}"

# Initialise timestamp
rm -f "${STAMP}"
echo -n "Retrieval started at " > "${STAMP}"
date >> "${STAMP}"

# Update SmartEiffel
cd "${SNAP}/SmartEiffel.svn"
START_REVISION=$(($(svn info|sed -n 's/Revision: \([0-9]*\)/\1/p') + 1))
REVISION=$(svn update|sed -r -n 's/^(Updated to|At) revision ([0-9]+).$/\2/p')
if [[ $START_REVISION > $REVISION ]]; then
    exit
fi
svn log -r "${REVISION}:${START_REVISION}" > "${SVN_LOG}"
echo "Retrieved revision ${REVISION}" >> "${STAMP}"

# Make cleaned copy (no svn additions)
cd "${SNAP}"
rm -rf SmartEiffel
cp -al SmartEiffel.svn SmartEiffel
find SmartEiffel -name ".svn" -type d -print0|xargs -0 -r rm -rf

# Patch version name
rm -f SmartEiffel/tools/smart_eiffel.e
VERSION_NAME="svn snaphsot ${REVISION}"
sed -r "s/(release_number: STRING is \".*\()[^)]*(\).*\")/\1${VERSION_NAME}\2/" SmartEiffel.svn/tools/smart_eiffel.e > SmartEiffel/tools/smart_eiffel.e

# Build archive
rm -f "${ARCHIVE}"
tar -czf "${ARCHIVE}" SmartEiffel

echo -n "Archive upload started at " >> "${STAMP}"
date >> "${STAMP}"

rm -f "${NEWCOOKIES}" "${OLDCOOKIES}"
touch "${OLDCOOKIES}"
${CURL} -d "return_to=&form_loginname=${FORGE_LOGIN}&form_pw=${FORGE_PASS}&login=Login%20with%20SSL" -e "${FORGE}/account/login.php" "${FORGE}/account/login.php" >/dev/null 2>&1
mv -f ${NEWCOOKIES} ${OLDCOOKIES}
OLD_FILE=$(${CURL} -F "step1=1" -F "release_date=$(date '+%Y-%m-%d %k:%M')" -F "release_name=Nightly Snapshot" -F status_id=1 -F uploaded_notes= -F "uploaded_changes=@${SVN_LOG}" -F "release_notes=${VERSION_NAME}" -F "release_changes=" -F preformatted=1 -F "submit=Submit/Refresh" "${FORGE}/frs/admin/editrelease.php?group_id=${GROUP_ID}&release_id=${RELEASE_ID}&package_id=${PACKAGE_ID}" 2>/dev/null|sed -n '/<form action="\/frs\/admin\/editrelease.php" method="post">/,/<\/form>/s/.*name="file_id" value="\([^"]*\)".*/\1/p')
mv -f ${NEWCOOKIES} ${OLDCOOKIES}
${CURL} -d "group_id=${GROUP_ID}&release_id=${RELEASE_ID}&package_id=${PACKAGE_ID}&file_id=${OLD_FILE}&step3=Delete%20File&submit=Delete%20File&im_sure=1" "${FORGE}/frs/admin/editrelease.php" >/dev/null 2>&1
mv -f ${NEWCOOKIES} ${OLDCOOKIES}
${CURL} -F "step2=1" -F "userfile=@${ARCHIVE}" -F type_id=3110 -F processor_id=8000 -F "submit=Add%20This%20File" "${FORGE}/frs/admin/editrelease.php?group_id=${GROUP_ID}&release_id=${RELEASE_ID}&package_id=${PACKAGE_ID}" >/dev/null 2>&1

echo -n "Finished at " >> ${STAMP}
date >> ${STAMP}
rm -f "${NEWCOOKIES}" "${OLDCOOKIES}"
mv ${STAMP} ${PUBLIC_HTML}
