class FILE
   --SZ:256: this file make short, compile_to_c crash

inherit TEXT_FILE_READ
        
feature

   is_connected: BOOLEAN is
         -- Is this file connected to some file of the operating system?
      deferred
      end

   connect_to(new_path: STRING) is
         -- Try to connect to an existing file of the operating system.
      require
         not is_connected
         not new_path.is_empty
      deferred
      end

   disconnect is
         -- Disconnect from any file.
      require
         is_connected
      deferred
      ensure
         not is_connected
      end

end
