#!/bin/csh -f
#
# Testing `compile_to_c'.
#
# Printed Output should be the same as the file
# SmartEiffel/tools/c_self_test.ok
#
source `dirname $0`/../tools/SE_conf || exit 1
set f='avl_dictionary'
set short="${SmartEiffelBin}/short $f"

echo -n "short -short $f"

set short_length=`${short} -short|wc -l`
set long_length=`${short}|wc -l`

if ($short_length < $long_length) then
    echo "."
else
    echo "Error"
endif
