#include "brown_malloc.h"

void* brown_malloc(size_t size) {
  static char *big;
  static int big_left = 0;
  size_t offset;

  /* Hack to make 32k blocks consecutive. */
  if (size == 32768) {
    if (big_left < size) {
      big_left = size * 7 + ALIGN_TO - 1;
      big = (char *) malloc (big_left + size);
      offset = ((size_t)big) % ALIGN_TO;
      if (offset)
	  offset = ALIGN_TO - offset;
      big += offset;
      big_left -= offset;
      return big;
    }
    big += size;
    big_left -= size;
    return big;
  }
  return malloc(size);
}
