class TEST_MEMORY_SUCKER
inherit EXCEPTIONS
creation make
feature
   blow_up: STRING;

   stop: BOOLEAN;

   make is
      do
         from
            blow_up := ("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx").twin
         until
            stop
         loop
            blow_up.append(blow_up.twin);
         end
      rescue
         if exception /= No_more_memory then
            std_output.put_string(once "TEST_MEMORY_SUCKER: ERROR.%N");
	 elseif not stop then
	    stop := True;
	    retry;
         end
      end
end

