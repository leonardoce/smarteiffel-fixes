#include "brown_malloc.h"

void* brown_malloc(size_t size) {
  /* If the static variables are absent, the test will sometimes fail
  *  in boost mode (alignment problems ?) */
  static char *mem;
  static int dummy;
  size_t offset;

  if (size != 32768)
    return malloc(size);

  size += ALIGN_TO;
  mem = (char *) malloc (size);
  offset = ((size_t)mem) % ALIGN_TO;
  if (offset)
    offset = ALIGN_TO - offset;
  mem += offset;
  return mem;
}
