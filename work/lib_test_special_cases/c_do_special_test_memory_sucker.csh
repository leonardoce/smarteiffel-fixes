#!/bin/csh -f
#
# Usage : c_self_test_do_it
#
source `dirname $0`/../tools/SE_conf || exit 1

###set verbose=1

cd ${SE}/work/lib_test_special_cases

set f=test_memory_sucker.e
set out_file=xx.tmp_out
set ok_error_msg=Killed
set ok_error_msg_length=7
## 6+1 for CR at end of Killed message


echo -n ${f}
foreach gc (" " -no_gc)
    foreach mode (-boost -no_check -all_check)
	compile -clean -o xx -no_split ${gc} ${mode} ${f}
	echo -n "."
	csh -c "limit vmemoryuse 150m; ./xx"  >&! ${out_file}
	if ($status) then
	    set error=0
	    set file_size=`wc -c ${out_file}`
	    set file_size=${file_size[1]}

	    if (${file_size} != ${ok_error_msg_length}) then
		set error=1
	    else
		set file_msg=`cat ${out_file}`
		if (${file_msg} != ${ok_error_msg}) then
		    set error=1
		endif
	    endif
	    if (${error} == 1) then
		echo "Error testing `pwd`/${f} ${gc} ${mode}"
		exit 1
	    endif
	endif
	clean ${f}
	/bin/rm -f xx
	/bin/rm -f ${out_file}
	echo -n "."
    end
end
echo "."
