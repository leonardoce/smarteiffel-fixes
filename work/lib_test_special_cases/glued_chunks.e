class GLUED_CHUNKS
   -- Tease out the GC inefficiency in SmartEiffel.

creation {ANY}
   make

feature {}
   make is
      local
         s: STRING
         i: INTEGER
      do
         from i := 0 until i = 40000 loop
            create s.copy("A bit of data to append")
            i := i + 1
         end
      end

end
