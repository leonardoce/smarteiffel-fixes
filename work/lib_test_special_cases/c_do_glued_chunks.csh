#!/bin/csh -f
#
# Usage : $0
#
source `dirname $0`/../tools/SE_conf || exit 1

###set verbose=1

cd ${SE}/work/lib_test_special_cases

echo -n glued_chunks.e
gcc away_malloc.c -c
gcc close_malloc.c -c
foreach mode (-boost -no_check -require_check \
	      -loop_check -all_check -debug )
    compile_to_c -no_split -gc_info ${mode} glued_chunks
    echo -n "."
    echo '#include "brown_malloc.h"' >! test_glued_chunks.c
    echo '#define malloc(x) brown_malloc(x)' >> test_glued_chunks.c
    cat glued_chunks.c >> test_glued_chunks.c
    gcc test_glued_chunks.c -c
    gcc test_glued_chunks.o away_malloc.o -o xx
    ./xx >&! away_chunks
    echo -n "."
    gcc test_glued_chunks.o close_malloc.o -o xx
    ./xx >&! close_chunks
    echo -n "."
    diff away_chunks close_chunks >& /dev/null
    if ($status) then
	echo
	echo diff close_chunks${mode} away_chunks${mode}
	/bin/mv close_chunks close_chunks${mode}
	/bin/mv away_chunks away_chunks${mode}
	break
    else
	/bin/rm close_chunks away_chunks
	echo -n "."
    endif
    clean glued_chunks
    /bin/rm -f xx test_glued_chunks.[co]
    echo -n "."
end
/bin/rm -f *.o  test_glued_chunks.c
echo "."
