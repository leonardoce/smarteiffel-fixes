#!/bin/csh -f
#
# Usage : c_self_test_do_it
#
source `dirname $0`/../tools/SE_conf || exit 1
set compile="${SmartEiffelBin}/compile "
set short="${SmartEiffelBin}/short "

###set verbose=1

cd ${SE}/work/lib_test_special_cases
${compile} same_msg -clean -boost -o same_msg >! /dev/null
cd cycle_check
echo "---- c_do_cycle_check ---"
${short} file.e >&! cycle_check.out
../same_msg cycle_check.ok cycle_check.out
