#ifndef brown_malloc_h
#define brown_malloc_h

/* *** For some unknown reason, the test fails in all_check mode
   if ALIGN_TO is a power of 2 !? <fm-02/06/2004> */

#define ALIGN_TO (3*sizeof(double))
#include <stdlib.h>
void* brown_malloc(size_t size);
#endif
