class TEST_XY_NUM1
-- From "Aleksa Todorovic <alexa@mindnever.org>"
creation make

feature {NONE}

   make is
      do
	 test_12
	 test_21
      end

   test_12 is
      do
	 io.put_string ("Test ONE-TWO%N")
	 test_one
	 test_two
      end

   test_21 is
      do
	 io.put_string ("Test TWO-ONE%N")
	 test_two
	 test_one
      end

   test_one is
      local
	 x : X_ONE
      do
	 io.put_string ("X_ONE : ")
	 !!x.make
	 test_xnum (x)
	 io.put_new_line
      end
   
   test_two is
      local
	 x : X_TWO
      do
	 io.put_string ("X_TWO : ")
	 !!x.make
	 test_xnum (x)
	 io.put_new_line
      end
   
   test_xnum(num: X_NUM) is
      require
	 num /= Void
      do
	 num.print_num
      end
   
end

