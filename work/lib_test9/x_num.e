deferred class X_NUM

feature {NONE}

   make is
      do
	 private_num := new_num
      end
   
   private_num : like new_num
   
   new_num: Y_NUM is
      deferred
      end

feature

   print_num is
      do
	 private_num.print_num
      end
   
end
