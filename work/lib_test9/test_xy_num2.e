class TEST_XY_NUM2
-- From "Aleksa Todorovic <alexa@mindnever.org>"
creation make

feature {NONE}

   make is
      do
	 test_one
	 test_two
      end

   test_one is
      local
	 x: X_ONE
      do
	 !!x.make
	 test_xnum(x)
	 io.put_new_line
      end
   
   test_two is
      local
	 x: X_TWO
      do
	 !!x.make
	 test_xnum(x)
	 io.put_new_line
      end
   
   test_xnum(num: X_NUM) is
      do
	 num.print_num
      end
   
end
