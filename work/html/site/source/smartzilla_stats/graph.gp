# $1 = date
# $2 = total created
# $3 = total created previous week
# $4 = total closed
# $5 = total closed previous week
# $6 = enhancement
# $7 = grave

set terminal fig color fontsize 14 size 9 6
set output "graph1.fig"

#show xrange, set xdata time, set format
#using est obligatoire

set xdata time

set format x "%d/%m/%Y"

set timefmt "%d/%m/%Y"

set xlabel "Weeks"

#set title "My title"
plot [*:*][0:*] "data.tmp" using 1:($2-$3) title "Created bugs" with lines linewidth 2 linetype 5, "data.tmp" using 1:($4-$5) title "Closed bugs" with lines linewidth 2 linetype 3

#pause -1 "Hit any key to continue"

set output "graph2.fig"

#set title "Open bugs"

#X11 colors
# 1 = red
# 2 = green
# 7 = orange

#Xfig colors
# 3 = green
# 5 = red
# 7 = yellow

plot [*:*][0:*] "data.tmp" using 1:($2-$4) title "Enhancement" with impulse linewidth 6 linetype 3, "data.tmp" using 1:($2-$4-$6) title "Trivial + minor + normal" with impulse linewidth 6 linetype 7, "data.tmp" using 1:($7) title "Major + critical + blocker" with impulse linewidth 6 linetype 5

#pause -1 "Hit any key to continue"
