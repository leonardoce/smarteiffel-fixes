class TEST_STATE
   --SZ:142:

creation
   make

feature

   make is
      local
	 sm: STATE_MACHINE[STATE, INTEGER, TRANSITION_RULESET[STATE, INTEGER]]
      do
	 create sm
      end
end
