-- This file is  free  software, which  comes  along  with  SmartEiffel. This
-- software  is  distributed  in the hope that it will be useful, but WITHOUT
-- ANY  WARRANTY;  without  even  the  implied warranty of MERCHANTABILITY or
-- FITNESS  FOR A PARTICULAR PURPOSE. You can modify it as you want, provided
-- this header is kept unaltered, and a notification of the changes is added.
-- You  are  allowed  to  redistribute  it and sell it, alone or as a part of
-- another product.
--          Copyright (C) 1994-98 LORIA - UHP - CRIN - INRIA - FRANCE
--            Dominique COLNET and Suzanne COLLIN - colnet@loria.fr
--                       http://SmartEiffel.loria.fr
--
class EXAMPLE1
   --
   -- To start with agents, just read this first example.
   --

creation make

feature

   make is
      local
         my_collection: COLLECTION[STRING];
      do
	 my_collection := <<"Benedicte","Lucien","Marie">>;

	 my_collection.do_all(agent print_item(?));
      end;

feature {NONE}

   print_item(item: STRING) is
      do
	 number := number + 1;
	 std_output.put_character('#');
	 std_output.put_integer(number);
	 std_output.put_character(' ');
	 std_output.put_string(item);
	 std_output.put_character('%N');
      end;

   number: INTEGER;

end -- EXAMPLE1

