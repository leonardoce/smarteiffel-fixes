#!/bin/tcsh -f
#
# Called by c_self_test in order to perform the test for one given mode of command 
# pretty.
#
#set verbose = 1
set usage = "Usage: c_basic_test <mode>"
if ($#argv != 1) then
    echo $usage
    exit 1
endif
set flag = $1
switch ($flag)
case "-zen":
    breaksw
case "-default":
    breaksw
case "-end":
    breaksw
case "-parano":
    breaksw
default
    echo $usage
    echo "      <mode> must be in {-zen -default -end -parano}"
    exit 1
endsw

source `dirname $0`/../tools/SE_conf || exit 1

set pretty = "${SmartEiffelBin}/pretty"
echo -n "Testing pretty (step 0) ."
cd ${SE}/work
/bin/rm -fr pretty_test_tmp
echo -n "."
mkdir pretty_test_tmp
echo -n "."
cd pretty_test_tmp
###### Copy of lib_test_pretty
cp -f ${SE}/work/lib_test_pretty/*.e .
cp -f ${SE}/work/lib_test_pretty/c2c.ace .
cp -f ${SE}/work/lib_test_pretty/no_loadpath_compile_to_xx .
echo "."
######
echo "Testing pretty (step 1) -----------------------------------------------------"
foreach file (test_*.e)
   if ($file == "test_pretty20.e") then
      set SPECIAL = "-no_warning"
   else
      set SPECIAL = " "
   endif
   echo -n "   ${file} :"
   se c -o xx -clean $SPECIAL ${file}
   if ($status) then
      echo "Cannot compile ${file} in test pretty ${flag} at step 1."
      exit 1
   endif
   echo -n "."
   ./xx
   /bin/rm -f xx
   echo -n "."
   ${pretty} ${flag} ${file}
   if ($status) then
      echo "Bad first pretty ${flag} at step 1 for ${file}."
      exit 1
   endif
   echo -n "."
   set bak=${file:r}.bak
   /bin/rm -f ${bak}
   ${pretty} ${flag} ${file}
   if ($status) then
      echo "Bad second pretty ${flag} at step 1 for ${file}."
      exit 1
   endif
   echo -n "."
   /bin/rm -f ${bak}
   ${pretty} ${flag} ${file}
   if ($status) then
      echo "Bad third pretty ${flag} at step 1 for ${file}."
      exit 1
   endif
   echo "."
   /bin/rm -f ${bak}
   cp ${file} ${file}.step1
end
######
echo "Testing pretty (step 2) -----------------------------------------------------"
foreach file (test_pretty*.e)
   if ($file == "test_pretty20.e") then
      set SPECIAL = "-no_warning"
   else
      set SPECIAL = " "
   endif
   echo -n "   ${file} :"
   se c -o xx -clean $SPECIAL ${file}
   if ($status) then
      echo "Cannot compile ${file} in test pretty ${flag} at step 2."
      exit 1
   endif
   echo -n "."
   ./xx
   /bin/rm -f xx
   echo -n "."
   ${pretty} ${flag} ${file}
   if ($status) then
      echo "Bad pretty_test at step 2 for ${file}."
      exit 1
   endif
   diff ${file} ${file}.step1
   if ($status) then
      echo "File ${file} is changing with option ${flag} during step 2."
      exit 1
   endif
   set bak=${file:r}.bak
   /bin/rm -f ${bak}
   echo "."
end
######
echo "Testing pretty (step 3) -----------------------------------------------------"
cd ${SE}/work/pretty_test_tmp
chmod a+rwx .
echo -n "Copying files of SmartEiffel/lib and SmartEiffel/tools here ..."
foreach f (`find ${SE}/lib -name '*.e' | grep -v CVS`)
   cp $f .
end
foreach f (`find ${SE}/tools -name '*.e' | grep -v CVS`)
   cp $f .
end
chmod a+rwx *.e
echo "."
cd ${SE}/work/pretty_test_tmp
echo -n "Apply pretty on the local copy of all classes..."
${SE}/bin/pretty ${flag} *.e
/bin/rm -f *.bak
echo "."
echo -n "Recompiling local pretty in -all_check ..."
./no_loadpath_compile_to_xx pretty
mv -f xx pretty
if ($status) then
   echo "Cannot recompile pretty at step 3 with option ${flag}."
   exit 1
endif
echo "."
foreach file (test_pretty*.e)
   echo -n "   ${file} :"
   ./pretty ${flag} ${file}
   if ($status) then
      echo "Bad pretty_test at step 3 for ${file}."
      exit 1
   endif
   diff ${file} ${file}.step1
   if ($status) then
      echo "File ${file} is changing with option ${flag} during step 3."
      exit 1
   endif
   set bak=${file:r}.bak
   /bin/rm -f ${bak}
   echo "."
end
echo "Testing pretty (step 4) -----------------------------------------------------"
echo "Recompiling local compile_to_c using lib_test_pretty/c2c.ace ..."
# It would be better to be in plain -all_check here too, but it is really too slow.
compile c2c.ace
echo "Creating ACE file for hello_world..."
echo 'system "xx" root HELLO_WORLD: main cluster "." generate no_split(yes) end' >& hello_world.ace
echo "Copying hello_world..."
cp -f ${SE}/tutorial/hello_world.e .
echo "Compiling hello_world with the local compiler..."
./c2c hello_world.ace
echo "Compiling the C code of hello_world..."
source hello_world.make
echo "Running the local hello_world..."
./xx
echo "Testing pretty done."
