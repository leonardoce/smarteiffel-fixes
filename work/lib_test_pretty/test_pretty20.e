class TEST_PRETTY20

create
	main
	
feature {ANY}
	main is
		do
		end
	
	foo: INTEGER

	bar: INTEGER is
		require
			True
		attribute
		end

	baz: STRING is
		obsolete "Whatever"
		attribute
		end

	quux: CHARACTER is
		attribute
		ensure
			True
		end

	quuux: ARRAY[INTEGER] is
		obsolete "Yeah"
		require
			1 < 2
		attribute
		ensure
			not ("abcd").is_empty
		end

	foo_c: INTEGER -- With a comment

	bar_c: INTEGER is
			-- Blah
		require
			True
		attribute
		end

	baz_c: STRING is
			-- Hum
		obsolete "Whatever"
		attribute
		end

	quux_c: CHARACTER is
			-- Di da dum
		attribute
		ensure
			True
		end

	quuux_c: ARRAY[INTEGER] is
			-- Poodle
		obsolete "42"
		require
			1 < 2
		attribute
		ensure
			not ("Getting wild").is_empty
		end

	foo_c2: INTEGER
			-- Another style

	bar_c2: INTEGER is
		require
			True
		attribute
		end -- End comments anybody?

	baz_c2: STRING is
		obsolete "or else"
		attribute
		end -- Here too

	quux_c2: CHARACTER is
		attribute
		ensure
			True
		end -- Nothing to see, move along

	quuux_c2: ARRAY[INTEGER] is
		obsolete "Yeah"
		require
			1 < 2
		attribute
		ensure
			not ("True/False/File Not Found").is_empty
		end -- Sure

end -- class TEST_PRETTY20
