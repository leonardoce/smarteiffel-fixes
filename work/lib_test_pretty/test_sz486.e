class TEST_SZ486
	-- SZ:486:

insert
	EIFFELTEST_TOOLS

creation {ANY}
	make

feature {}
	make is
		local
			dict: DICTIONARY[INTEGER, INTEGER]
		do
			dict := {HASHED_DICTIONARY[INTEGER, INTEGER] << 1, 1;
			                                                2, 2;
			                                                3, 3 >> }
			assert(dict.at(1) = 1)
			assert(dict.at(2) = 2)
			assert(dict.at(3) = 3)
		end

end -- class TEST_SZ486
