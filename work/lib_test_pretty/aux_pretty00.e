-- Originally written by Vincent CROIZIER.
class AUX_PRETTY00
   --
   -- Root class for all class named PRETTY_TEST*
   --
   -- Used to test stability of command `pretty' when it is used with expressions with
   -- infix or prefix operators.
   --

inherit
   EIFFELTEST_TOOLS

creation
   make_trace
   
feature
   
   trace: STRING
   
   make_trace(t: STRING) is
      do
	 create trace.make(0)
	 trace.append(t)
      end
   
   a: like Current is
      do
	 create Result.make_trace("a ")
      end
   
   b: like Current is
      do
	 create Result.make_trace("b ")
      end
   
   c: like Current is
      do
	 create Result.make_trace("c ")
      end
   
   d: like Current is
      do
	 create Result.make_trace("d ")
      end
    
   infix "+" (other: like Current): like Current is 
      do
	 create Result.make_trace(trace)
	 Result.trace.append(other.trace)
	 Result.trace.append("+ ")
      end
   
   infix "-" (other: like Current): like Current is   
      do
	 create Result.make_trace(trace)
	 Result.trace.append(other.trace)
	 Result.trace.append("- ")
      end
   
   infix "*" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("* ")
      end

   infix "/" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("/ ")
      end

   infix "^" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("^ ")
      end

   infix "//" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("// ")
      end

   infix "\\" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("\\ ")
      end

   infix "&+" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("&+ ")
      end

   infix "and" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("and ")
      end

   infix "and then" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("and then ")
      end

   infix "or" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("or ")
      end

   infix "or else" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("or else ")
      end

   infix "xor" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("xor ")
      end

   infix ">=" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append(">= ")
      end

   infix ">" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("> ")
      end

   infix "<=" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("<= ")
      end

   infix "<" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("< ")
      end

   infix "implies" (other: like Current): like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append(other.trace)
         Result.trace.append("implies ")
      end

   prefix "-": like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append("u- ")
      end

   prefix "+": like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append("u+ ")
      end

   prefix "not": like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append("not ")
      end

   prefix "&-": like Current is
      do
         create Result.make_trace(trace)
         Result.trace.append("u&- ")
      end

end

