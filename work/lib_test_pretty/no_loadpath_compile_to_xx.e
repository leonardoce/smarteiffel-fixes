class NO_LOADPATH_COMPILE_TO_XX
--
-- Little tool which create a simple ACE file in order to compile some system with this ACE file.
-- This make sure that local file are used and also avoids the warning about some class which 
-- is hidden by another.
--
insert
	ARGUMENTS
	
creation
	make
	
feature {}
	usage: STRING is "no_loadpath_compile_to_xx foo.e"

	make is
		local
			ace_file_name, system_root: STRING; text_file_write: TEXT_FILE_WRITE
			command: STRING; exit_status: INTEGER
		do
			-- Creation of the ACE file:
			ace_file_name := argument(1).twin
			ace_file_name.to_lower
			if ace_file_name.has_suffix(".e") then
				ace_file_name.remove_tail(2)
			end
			system_root := ace_file_name.twin
			system_root.to_upper
			ace_file_name.append(".ace")
			create text_file_write.connect_to(ace_file_name)
			text_file_write.put_string("system %"xx%"%Nroot%N%T")
			text_file_write.put_string(system_root)
	 		text_file_write.put_string(": make%N")
		 	text_file_write.put_string("{
default
    trace(no)
    collect (no)
    assertion (all)
    debug (no)
    verbose (no)
    manifest_string_trace(no)
    high_memory_compiler (yes);

cluster
	 all_is_here: "."
generate
    clean (yes)						  
end
	 
	}")			
			text_file_write.disconnect
			-- Now compiling:
			command := "se c " + ace_file_name
			exit_status := (create {SYSTEM}).execute_command(command)
			-- Now cleaning:
			(create {FILE_TOOLS}).delete(ace_file_name)
			-- And at the end:
			die_with_code(exit_status)
		end
	
end
	
