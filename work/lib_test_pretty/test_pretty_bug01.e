--
-- Written by Vincent CROIZIER.
--
class TEST_PRETTY_BUG01
	--
	-- Also used to test stability of command `pretty'
	-- This the test of a bug report by Philippe Ribet.
	--
	--   

creation {ANY}
   make

feature {ANY}
   
   make is 
      do
			cpt := 0  
         assert((-3).is_equal(f(2.5,2,3).to_integer_8))
         assert((0).is_equal(f(3.5,4,4).to_integer_8))
         assert((4).is_equal(f(4.5,5,4).to_integer_8))
      end
   
   x: REAL
   
   f (y: REAL; bb, cc: INTEGER): INTEGER is 
      local 
			b, c: INTEGER; a: REAL
      do  
         x := y
         b := bb
         c := cc
         a := (x * (b - c)).floor
         -- The bug : (x * (b - c)).floor -> (x * b - c).floor
         Result := a.force_to_integer_32
      end
	
feature {NONE}

   cpt: INTEGER
   
   assert(t: BOOLEAN) is 
      do  
         cpt := cpt + 1
         if not t then 
            std_output.put_string("TEST_PRETTY_BUG01: ERROR Test # ")
            std_output.put_integer(cpt)
            std_output.put_string("%N")
         end 
      end

end


