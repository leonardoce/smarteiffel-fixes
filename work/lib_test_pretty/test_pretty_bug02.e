class TEST_PRETTY_BUG02

inherit
	EIFFELTEST_TOOLS

creation
	make
	
feature

	make is
		do
			create callbacks.with_capacity(4)
			assert(callbacks.count = 0)
			disconnect('a')
			callbacks.add_last('a')
			assert(callbacks.count = 1)
			disconnect('a')
			assert(callbacks.count = 0)
		end
	
	callbacks: FAST_ARRAY[CHARACTER]
	
	disconnect (c: CHARACTER) is
		local
			i: INTEGER
		do
			i := callbacks.fast_first_index_of(c)
			if callbacks.valid_index(i) then
				callbacks.remove(i)
			end
		ensure
			(old (not callbacks.fast_has(c))) implies (callbacks.count = old callbacks.count)
		end

end
	
