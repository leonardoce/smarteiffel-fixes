--
-- Written by Vincent CROIZIER.
--
class TEST_PRETTY05
   --
   -- Also used to test stability of command `pretty'
   -- when it is used with expressions with
   -- infix or prefix operators.
   --
   
inherit AUX_PRETTY00   
   
creation make, make_trace
   
feature
   
   make is
      do
			-- test "\\"
         assert((a \\ b \\ c).trace.is_equal("a b \\ c \\ "));     
			assert(((a \\ b) \\ c).trace.is_equal("a b \\ c \\ "))
			assert((a \\ (b \\ c)).trace.is_equal("a b c \\ \\ "))
			-- test "//"
			assert((a // b // c).trace.is_equal("a b // c // "))
			assert(((a // b) // c).trace.is_equal("a b // c // "))
			assert((a // (b // c)).trace.is_equal("a b c // // "))
			-- test "\\" et "//"
			assert((a \\ b // c).trace.is_equal("a b \\ c // "))
			assert((a // b \\ c).trace.is_equal("a b // c \\ "))
			assert((a \\ (b // c)).trace.is_equal("a b c // \\ "))
			assert((a // (b \\ c)).trace.is_equal("a b c \\ // "))
			assert(((a \\ b) // c).trace.is_equal("a b \\ c // "))
			assert(((a // b) \\ c).trace.is_equal("a b // c \\ "))
      end
   
end

