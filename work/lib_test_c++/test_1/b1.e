class B1

inherit	A1

creation make

feature 

   bar (a: BOOLEAN) is
      do
	 cpp_bar (object_ptr, a)
      end
   
feature {NONE}

   cpp_bar (cpp_obj: POINTER; a: BOOLEAN) is
      external "C++ [B %"B.h%"] (bool)"
      alias "bar"
      end

end
