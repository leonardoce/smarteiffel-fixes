class C1

creation make

feature 
   
   make is
      do
	 object_ptr := cpp_new
      end
   
feature

   bar (a: BOOLEAN) is
      do
	 cpp_bar (object_ptr, a)
      end

feature {NONE}

   object_ptr: POINTER
   
   cpp_new: POINTER is
      external "C++ [new C %"C.h%"] ()"
      end

   cpp_bar (cpp_obj: POINTER; a: BOOLEAN) is
      external "C++ [C %"C.h%"] (bool)"
      alias "bar"
      end
   
end
