#include <iostream>
#include "A.h"
#include "B.h"
#include "C.h"

void A::foo(int a) { std::cout << "A:foo(" << a << ")\n"; }

void B::bar(bool a) { std::cout << "B:bar(" << a << ")\n"; }

void C::bar(bool a) { std::cout << "C:bar(" << a << ")\n"; }
