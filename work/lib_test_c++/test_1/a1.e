class A1

creation make

feature 

   make is
      do
	 object_ptr := cpp_new
      end
   
feature
   
   foo(a: INTEGER) is
      do
	 cpp_foo (object_ptr, a)
      end
   
feature {NONE}
   
   object_ptr: POINTER
   
   cpp_new: POINTER is
      external "C++ [new A %"A.h%"] ()"
      end
   
   cpp_foo (cpp_obj: POINTER; a: INTEGER) is
      external "C++ [A %"A.h%"] (int)"
      alias "foo"
      end
   
end
