class TEST_1

creation make

feature

   make is
      local
	 a: A1;
	 b: B1;
	 c: C1;
      do
	 !!a.make;
	 a.foo(1);
	 !!b.make;
	 b.bar(True);
	 !!c.make;
	 c.bar(False);
      end
   
end
