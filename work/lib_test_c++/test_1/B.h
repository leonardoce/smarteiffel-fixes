#ifndef B_H
#define B_H

#include "A.h"

class B : public A {
public:
	void bar(bool a);
};

#endif
