#include <iostream>
#include "A.h"
#include "B.h"
#include "C.h"

A::A() { std::cout << "A::A()\n"; }
void A::foo(int a) { std::cout << "A::foo(" << a << ")\n"; }

B::B() { std::cout << "B::B()\n"; }
void B::bar(bool a) { std::cout << "B::bar(" << a << ")\n"; }

C::C() { std::cout << "C::C()\n"; }
void C::bar(bool a) { std::cout << "C::bar(" << a << ")\n"; }
