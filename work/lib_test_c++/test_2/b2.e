class B2

inherit A2

creation make

feature -- Basic operations

   bar (a: BOOLEAN) is
      do
	 cpp_bar (object_ptr, a)
      end

feature {NONE} -- Externals

   cpp_bar (cpp_obj: POINTER; a: BOOLEAN) is
      external "C++ [B %"B.h%"] (bool)"
      alias
	 "bar"
      end

end
