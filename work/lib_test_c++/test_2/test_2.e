class TEST_2

creation make

feature -- Initialization

   make is
      local
	 a: A2;
	 b: B2;
	 c: C2;
      do
	 std_output.put_string("-- 0 --%N");
	 !!a.make;
	 std_output.put_string("-- 1 --%N");
	 a.foo(1);
	 std_output.put_string("-- 2 --%N");
	 !!b.make;
	 std_output.put_string("-- 3 --%N");
	 b.bar(True);
	 std_output.put_string("-- 4 --%N");
	 !!c.make;
	 std_output.put_string("-- 5 --%N");
	 c.bar(False);
	 std_output.put_string("-- 6 --%N");
      end

end
