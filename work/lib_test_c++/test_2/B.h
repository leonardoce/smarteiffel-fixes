#ifndef B_H
#define B_H

#include "A.h"

class B : public A {
public:
	B();
	void bar(bool a);
};

#endif
