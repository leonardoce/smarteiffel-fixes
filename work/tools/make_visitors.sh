#!/bin/csh -f
#
# Rebuild all visitors.
#
# Usage : make_visitor.sh
#
###################################################################
source `dirname $0`/SE_conf || exit 1

###################################################################
#set verbose=1
cd ${SE}/work/tools
echo Working in `pwd`.
echo "Compiling make_visitors.e"
/bin/rm -f make_visitors
se c -boost -verbose -flat_check -no_gc -split by_type -o make_visitors make_visitors
if (!(-f make_visitors)) then
   echo "Cannot compile make_visitors.e"
   exit 1
endif
echo "Making the old SmartEiffel/tools/visitor writable..."
/bin/chmod u+w ../../tools/visitor/*.e
echo "Creating the new SmartEiffel/tools/visitor..."
./make_visitors
echo "Applying pretty..."
cd ${SE}/tools/visitor
se pretty -smarteiffel_copyright_for_gnu *.e
rm -f *.bak
echo "Done."
echo "Please check manually the SVN status of files from SmartEiffel/tools/visitor/."
