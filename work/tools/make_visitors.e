class MAKE_VISITORS
--
-- To create automatically all classes of SmartEiffel/tools/visitors/*.e.
--
-- An *_VISITOR class is generated in SmartEiffel/tools/visitor only if the * class is iteself VISITABLE.
--
-- Run with the make_visitor.sh script.
--
-- (See also make_visitors.txt)
--
insert
	CLASS_CHECK
	CODE_PRINTER
	PARENT_LISTS_HANDLER
	
create {ANY}
   main

feature {}
   main is		
      do
			-- Simulation of arguments passing:
			command_arguments.add_last("-verbose")
			-- I know, it is still a little bit manual but better that the previous version.
			command_arguments.add_last("CLASS_CHECK")
			command_arguments.add_last("CLEAN")
			command_arguments.add_last("COMPILE")
			command_arguments.add_last("COMPILE_TO_C")
			command_arguments.add_last("COMPILE_TO_JVM")
			command_arguments.add_last("EIFFELDOC")			
			command_arguments.add_last("PRETTY")
			command_arguments.add_last("SE")
			command_arguments.add_last("SHORT")
			-- Now calling the `make' from CLASS_CHECK:
			make
			-- Now filling `all_classes':
			ace.for_all(agent all_classes.add_last(?))

         go_to_tools_directory

         generate_visitors
      end

	path: STRING
			-- the path of the "tools" directory

	all_classes: FAST_ARRAY[CLASS_TEXT] is
      once
         create Result.with_capacity(256)
      end
	

	visitor_counter: INTEGER
	
   visitor_file: TEXT_FILE_WRITE is
      once
         create Result.make
      end

feature {}
   go_to_tools_directory is
      local
         bd: BASIC_DIRECTORY
      do
         path := ""
         path.copy(echo.getenv(once "path_tools", Void))
         bd.change_current_working_directory(path)
      end

feature {}
   class_name (filename: STRING): CLASS_NAME is
      require
         filename.has_suffix(once ".e")
      local
         s: STRING
      do
         s := once ""
         s.copy(filename)
         s.remove_suffix(once ".e")
         s.to_upper
         create Result.unknown_position(string_aliaser.hashed_string(s))
      end

feature {}
   generate_visitors is
      local
         i, j: INTEGER
         ct, child: CLASS_TEXT
         classname, visitor_path, visitor_dir_path, visitor_classname, tmp: STRING
			my_children: FAST_ARRAY[CLASS_TEXT]
         bd: BASIC_DIRECTORY
         d: DIRECTORY
         ft: FILE_TOOLS
      do
			echo.put_string("Now generating visitors:%N")
         tmp := once ""
         bd.compute_subdirectory_with(path, once "visitor")
         visitor_dir_path := bd.last_entry.twin
			
         -- Remove all existing visitors:
         create d.scan(visitor_dir_path)
         from
            i := d.upper
         until
            i < d.lower
         loop
            tmp.copy(visitor_dir_path)
            bd.compute_file_path_with(tmp, d.item(i))
				if d.item(i).has_suffix(once ".e") then
					warn_if_deleted(class_name(d.item(i)))
					tmp.copy(bd.last_entry)
					ft.delete(tmp)
				end
            i := i - 1
         end

         -- Now create the new visitors:
         from
            i := all_classes.lower
         until
            i > all_classes.upper
         loop
            ct := all_classes.item(i)
            classname := ct.name.to_string
            if is_visitable(ct) then

               my_children := children(ct)

					visitor_classname := once ""
					visitor_classname.copy(classname)
					visitor_classname.append(once "_VISITOR")

					visitor_counter := visitor_counter + 1
					echo.put_integer(visitor_counter)
					echo.put_character('%T')
					echo.put_string(visitor_classname)
					echo.put_character('%N')
					
					visitor_path := once ""
					visitor_path.copy(visitor_classname)
					visitor_path.to_lower
					visitor_path.append(once ".e")
					bd.compute_file_path_with(visitor_dir_path, visitor_path)
					visitor_path.copy(bd.last_entry)

					visitor_file.connect_to(visitor_path)
	            if not visitor_file.is_connected then
						std_error.put_string("***** Fatal Error: could not connect to ")
						std_error.put_string(visitor_path)
						std_error.put_string(" (write-protected file?)%N")
						die_with_code(1)
					end

					if ct.is_deferred and then not my_children.is_empty then
						-- group visitor

						visitor_file.put_string(once "deferred class ")
						visitor_file.put_string(visitor_classname)
						visitor_file.put_string(once "%Ninherit%N")

	               from
							j := my_children.lower
						until
							j > my_children.upper
						loop
							child := my_children.item(j)
							classname := child.name.to_string
							visitor_file.put_character('%T')
							visitor_file.put_string(classname)
							visitor_file.put_string(once "_VISITOR%N")
							j := j + 1
						end
					else
						-- individual visitor
						 
						visitor_file.put_string(once "deferred class ")
						visitor_file.put_string(visitor_classname)
						visitor_file.put_string(once "%Ninherit%N")
						visitor_file.put_string(once "%TVISITOR%N")
						
						visitor_file.put_string(once "%Nfeature {")
						visitor_file.put_string(classname)
						visitor_file.put_string(once "}%N%Tvisit_")
						tmp.copy(classname)
						tmp.to_lower
						visitor_file.put_string(tmp)
						visitor_file.put_string(once " (visited: ")
						visitor_file.put_string(classname)
						
						visitor_file.put_string(once ") is%N")
						if not ct.is_expanded then
							visitor_file.put_string("%T%Trequire%N%T%T%Tvisited /= Void%N")
						end

						if ct.is_deferred then
							visitor_file.put_string(once "%T%Tdo%N%T%T%Tvisited.accept(Current)%N%T%Tend%N%N")
						else
							visitor_file.put_string(once "%T%Tdeferred%N%T%Tend%N%N")
						end
					end

					visitor_file.put_string(once "end -- class ")
					visitor_file.put_string(visitor_classname)
					visitor_file.put_new_line
					
					visitor_file.disconnect
				end
				i := i + 1
			end
		end

   is_visitable (class_text: CLASS_TEXT): BOOLEAN is
		local
			visitable_class_text: CLASS_TEXT
      do
			visitable_class_text := visitable_class_text_memory
			if class_text.is_any then
				check
					not Result
				end
			elseif class_text = visitable_class_text then
				check
					not Result
				end
			else
				Result := class_text.get_export_permission_of(visitable_class_text)
				--|*** May be we should check that it is not only an "insert" relationship?
				--|*** Dom. feb 19th 2006 ***
			end
      end

   visitable_class_text_memory: CLASS_TEXT is
      once
         Result := smart_eiffel.class_text(class_name("visitable.e"), True)
      end

   children (ct: CLASS_TEXT): like children_memory is
      local
         i, j: INTEGER
			child: CLASS_TEXT
			parent_edge_list: FAST_ARRAY[PARENT_EDGE]
      do
         Result := children_memory
         Result.clear_count
			from
				i := all_classes.lower
			until
				i > all_classes.upper
			loop
				child := all_classes.item(i)
				if child.parent_lists /= Void and then child.parent_lists.inherit_list /= Void then
					from
						parent_edge_list := child.parent_lists.inherit_list
						j := parent_edge_list.lower
					until
						j > parent_edge_list.upper
					loop
						if parent_edge_list.item(j).class_text = ct then
							Result.add_last(child)
						end
						j := j + 1
					end
				end
				i := i + 1
			end
		end

   children_memory: FAST_ARRAY[CLASS_TEXT] is
      once
         create Result.make(0)
      end

   warn_if_deleted (classname: CLASS_NAME) is
      local
         i: INTEGER; found: BOOLEAN
      do
         from
            i := all_classes.upper
         until
            found or else i < all_classes.lower
         loop
            found := all_classes.item(i).name = classname
            i := i - 1
         end
         if found then
            std_error.put_string(once "***** Warning: ")
            std_error.put_string(classname.to_string)
            std_error.put_string(once " does not exist anymore.%N")
         end
      end

feature {PARENT_LISTS}
	compile is
		do
			check
				False
			end
		end

end

