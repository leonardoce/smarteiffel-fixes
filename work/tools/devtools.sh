#!/bin/sh

# Test if SmartEiffel is installed
if ! which se >/dev/null; then
    echo "SmartEiffel is not installed." >&2
    exit 1
fi

# Get SmartEiffel's environment...
eval `se --environment`
# ... and the configuration directory
export SE_CONF=${SmartEiffel:-$HOME/.serc}
test -d $SE_CONF || mkdir -p $SE_CONF

# Create the configuration file for the dev tools
echo Creating devtools config
cat > $SE_CONF/devtools.se <<EOF
[Tools]
debug: debug.sh
make: make.sh
EOF

# Because of Cygwin:
exe_suffix=`test -e /bin/ls.exe && echo .exe`
build_dir=${SE_BIN%/}/.build

cd $SE_BIN

# Create the make tool
echo Creating make tool
cat > make.sh$exe_suffix <<EOF
#!/bin/sh

if [ x\$1 = x ]; then
    echo "Usage: se make <tool> <options>" >&2
    exit 1
fi

case \$1 in
    -version|--version|/version)
	echo "Local tool"
	exit
	;;
esac

tool="\$1"
shift

dir=$build_dir/boost
test -d \$dir || mkdir -p \$dir
cd \$dir
se c -verbose -boost -o "\$tool"$exe_suffix "\$tool" "\$@"
cp "\$tool"$exe_suffix $SE_BIN
EOF

# Create the debug tool
echo Creating debug tool
cat > debug.sh$exe_suffix <<EOF
#!/bin/sh

if [ x\$1 = x ]; then
    echo "Usage: se debug <tool> <options>" >&2
    exit 1
fi

case \$1 in
    -version|--version|/version)
	echo "Local tool"
	exit
	;;
esac

tool="\$1"
shift

export check=require
case x\$1 in
    *_check)
	check=\${1%_check}
	shift
	;;
    *-check)
	check=\${1%-check}
	shift
	;;
esac

dir=$build_dir/debug/$check
test -d \$dir || mkdir -p \$dir
cd \$dir
se c -verbose -\${check}_check -o "\$tool"$exe_suffix "\$tool" "\$@"
cp "\$tool"$exe_suffix $SE_BIN
EOF

# At last, make them executable
chmod +x make.sh$exe_suffix debug.sh$exe_suffix
