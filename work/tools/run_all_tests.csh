#!/bin/csh -f
#
# This script launches the whole test suite.
#
# Usage : run_all_tests.csh
#
###################################################################
if ($#argv == 0) then
   echo "run_all_tests.csh started."
else
   echo "run_all_tests.csh: arguments ignored."
   set argv=(-boost)
endif
source `dirname $0`/SE_conf || exit 1

setenv PATH ${SE}/bin:${PATH}
###################################################################
cd ${SE}
echo 'Running all the test_suite with "se test".'
find ${SE}/test_suite -name LOCK -exec /bin/rm -f {} \;
se test ${SE}/test_suite/language/simplify/ &
se test ${SE}/test_suite/language/error_warning_msg/ &
echo 'Launching for you an "se test" watcher in background with'
echo 'the following command:'
echo "   " "se test -watch ${SE}/test_suite"
echo 'You can quit this watcher. This wont stop the tests.'
echo 'You can relaunch the viewer at any time to see what happens.'
se test -watch ${SE}/test_suite &
se test ${SE}/test_suite/language/assertion/
se test ${SE}/test_suite/language/agent/
se test ${SE}/test_suite/lib &
se test ${SE}/test_suite/tutorial
se test ${SE}/test_suite/misc
se test ${SE}/test_suite/language/gc/
se test ${SE}/test_suite/language/cecil/ &
se test ${SE}/test_suite/language/exceptions/
se test ${SE}/test_suite/language/profile/ &
se test ${SE}/test_suite/language/expanded/
se test ${SE}/test_suite/language/unclassified/

## ****************************************************************
## *** Remainder should be done by eiffeltest too ... 
##     ... work still in progress.
## *** Dom. august 28th 2006. ***
## ****************************************************************
###################################################################
cd ${SE}/work
echo "Basic test of the new version."
echo "Computing c_self_test in file 'c_self_test.out'."
${SE}/work/tools/c_self_test >! c_self_test.out
diff c_self_test.out ${SE}/work/tools/c_self_test.ok
if ($status) then
   mv -f ${SmartEiffelBin}/compile_to_c.old ${SmartEiffelBin}/compile_to_c
   echo "c_self_test failed."
endif
/bin/rm -f compile_to_c.old 
echo "Self test succeeded."
###################################################################
${SE}/work/tools/VerifyAndStripTools
###################################################################
${SE}/work/lib_test4/c_self_test
###################################################################
${SE}/work/lib_test8/c_self_test
###################################################################
${SE}/work/lib_test9/c_self_test
###################################################################
${SE}/work/lib_test_c++/c_self_test
###################################################################
${SE}/work/tools/c_short_test
###################################################################
${SE}/work/tools/pretty_test
${SE}/work/lib_test_pretty/c_self_test
###################################################################
${SE}/work/tools/ace_test
###################################################################
${SE}/work/lib_test_special_cases/c_self_test
###################################################################
echo `date`
echo Done
