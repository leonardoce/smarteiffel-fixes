class STRIP_C_COMMENTS
--
-- Command line tool to remove C comments as well as some unuseful
-- generated characters of a generated C file.
-- This comand is used to save some space in the delivery (i.e. all
-- files of SmartEiffel/bin_c are stripped with this command).
--

inherit COMMAND_LINE_TOOLS

creation make

feature

   usage: STRING is "usage: strip_c_comments <files...>"

feature {}

   make is
      local
	 i, saved, total_new_bytes, total_old_bytes: INTEGER;
	 number_of_files: INTEGER
	 file: STRING; ratio: DOUBLE
      do
	 echo.set_verbose
	 if argument_count < 1 then
	    std_output.put_string(usage + "%N")
	    die_with_code(exit_failure_code)
	 end
	 from
	    i := 1
	 until
	    i > argument_count
	 loop
	    file := argument(i).twin
	    if file.last /= 'h' and file.last /= 'c' then
	       std_output.put_string(once "[-- ")
	       std_output.put_string(file)
	       std_output.put_string(once " (not a C file) --]%N")
	    elseif not file_tools.file_exists(file) then
	       std_output.put_string(once "[-- ")
	       std_output.put_string(file)
	       std_output.put_string(once " (file not found) --]%N")
	    else
	       strip_c_comments(file)
	       number_of_files := number_of_files + 1
	       total_old_bytes := total_old_bytes + old_file_size
	       total_new_bytes := total_new_bytes + new_file_size
	    end
	    i := i + 1
	 end
	 if number_of_files > 0 then
	    std_output.put_string(once "[-- ")
	    std_output.put_integer(number_of_files)
	    std_output.put_string(once " C file")
	    if number_of_files > 1 then
	       std_output.put_character('s')
	    end
	    std_output.put_character(' ')
	    if total_new_bytes = total_old_bytes then
	       std_output.put_string(once "(already stripped ;-)")
	    else
	       std_output.put_string(once "stripped - ")
	       saved := total_old_bytes - total_new_bytes
	       ratio := saved.to_double / total_old_bytes
	       ratio := ratio * 100.0
	       std_output.put_string(once " (")
	       std_output.put_integer(saved)
	       std_output.put_string(once " bytes saved - ")
	       std_output.put_double_format(ratio, 2)
	       std_output.put_string(once " %% saved)")
	    end
	    std_output.put_string(once " --]%N")
	 end
      end

   old_file: TEXT_FILE_READ

   new_file: TEXT_FILE_WRITE

   new_file_size, old_file_size: INTEGER

   state: INTEGER

   in_macro: BOOLEAN
         -- This flag effectively makes the states twice what they appear (an
         -- automaton product: with/without this flag set) It is more simple
         -- to manage than the whole states set, though.

   column: INTEGER

   max_columns: INTEGER is 132

   strip_c_comments(file: STRING) is
      local
	 old_file_name: STRING; cc: CHARACTER; debug_mode: BOOLEAN
      do
	 old_file_name := file + once ".orig"
	 file_tools.rename_to(file, old_file_name)
	 create old_file.connect_to(old_file_name)
	 if not old_file.is_connected then
	    std_error.put_string("strip_c_comments: error while %
                                 %stripping %"")
	    std_error.put_string(file)
	    std_error.put_string("%" (rename failed).")
	    die_with_code(exit_failure_code)
	 end
	 create new_file.connect_to(file)
	 if not new_file.is_connected then
	    std_error.put_string("strip_c_comments: error while %
                                 %stripping %"")
	    std_error.put_string(file)
	    std_error.put_string("%" (cannot create file).")
	    die_with_code(exit_failure_code)
	 end
	 from
	    state := 9
	    new_file_size := 0
	    old_file_size := 0
	    if not old_file.end_of_input then
	       cc := next_cc
	    end
	 until
	    old_file.end_of_input
	 loop
	    inspect
	       state
	    when 0 then -- Initial normal state.
	       inspect
		  cc
	       when '/' then
		  new_state(1, false, ' ')
	       when '%"' then
		  new_state(4, true, cc)
	       when '%'' then
		  new_state(6, true, cc)
	       when '\' then
		  new_state(8, true, cc)
               when '%T', ' ' then
                  new_state(10, false, ' ')
               when '%N' then
                  if column > max_columns then
                     new_state(9, true, '%N')
                  else
                     new_state(9, in_macro, '%N')
                  end
                  in_macro := False
	       else
		  new_state(state, true, cc)
	       end
	    when 1 then -- After / with this / delayed.
	       inspect
		  cc
	       when '*' then
		  new_state(2, false, ' ')
	       when '/' then
		  new_state(12, false, ' ')
	       when '\' then
                  write('/')
		  new_state(8, true, cc)
               when '%T', ' ' then
                  write('/')
                  new_state(10, false, ' ')
               when '%N' then
                  write('/')
                  if column > max_columns then
                     new_state(9, true, '%N')
                  else
                     new_state(9, in_macro, '%N')
                  end
                  in_macro := False
	       else
		  write('/')
		  new_state(0, true, cc)
	       end
	    when 2 then -- Inside C comment.
	       inspect
		  cc
	       when '*' then
		  new_state(3, false, ' ')
	       else
		  new_state(state, false, ' ')
	       end
	    when 3 then -- Inside C comment after closing *
	       inspect
		  cc
	       when '/' then
		  new_state(10, false, ' ')
               when '*' then
                  new_state(state, false, ' ')
	       else
		  new_state(2, false, ' ')
	       end
	    when 4 then -- Inside C string.
	       inspect
		  cc
	       when '%"' then
		  new_state(0, true, cc)
	       when '\' then
		  new_state(5, true, cc)
	       else
		  new_state(state, true, cc)
	       end
	    when 5 then -- Inside C string, after \
	       new_state(4, true, cc)
	    when 6 then -- Inside C character (after the first ' ).
	       inspect
		  cc
	       when '\' then
		  new_state(7, true, cc)
	       when '%'' then
		  new_state(0, true, cc)
	       else
		  new_state(6, true, cc)
	       end
	    when 7 then -- Inside C character (after '\ )
	       new_state(6, true, cc)
            when 8 then -- After a \
	       inspect
		  cc
               when '%T', ' ' then
                  new_state(10, true, cc)
               when '%N' then
                  new_state(11, true, cc)
               else
                  new_state(0, true, cc)
               end
            when 9 then -- First character of a line (not in string nor comment nor character)
               check
                  not in_macro
               end
	       inspect
		  cc
               when '#' then
                  in_macro := True
                  if column > 0 then
                     write('%N')
                  end
                  new_state(0, true, cc)
	       when '/' then
		  new_state(1, false, ' ')
	       when '%"' then
		  new_state(4, true, cc)
	       when '%'' then
		  new_state(6, true, cc)
	       when '\' then
		  new_state(8, true, cc)
               when '%N' then
                  new_state(state, false, ' ')
               when '%T', ' ' then
                  new_state(10, false, ' ')
	       else
		  new_state(0, true, cc)
	       end
            when 10 then -- Reading useless extra spaces
	       inspect
		  cc
	       when '/' then
		  new_state(1, false, ' ')
	       when '%"' then
		  new_state(4, true, cc)
	       when '%'' then
		  new_state(6, true, cc)
	       when '\' then
		  new_state(8, true, cc)
               when '%N' then
                  if column > max_columns then
                     new_state(11, true, '%N')
                  else
                     new_state(11, in_macro, '%N')
                  end
                  in_macro := False
               when '%T', ' ' then
                  new_state(state, false, ' ')
	       else
                  if column > 0
                     and then (last_written.is_letter_or_digit or else last_written = '_')
                     and then (cc.is_letter_or_digit or else cc = '_')
                   then
                     if column > max_columns then
                        write('%N')
                     else
                        write(' ')
                     end
                  elseif in_macro and then column > 1 then
                     if column > max_columns then
                        write('%N')
                     else
                        write(' ')
                     end
                  end
		  new_state(0, true, cc)
	       end
            when 11 then -- First character in a line, in extra-spaces mode
               check
                  not in_macro
               end
	       inspect
		  cc
               when '#' then
                  in_macro := True
                  if column > 0 then
                     write('%N')
                  end
                  new_state(0, true, cc)
	       when '/' then
		  new_state(1, false, ' ')
	       when '%"' then
		  new_state(4, true, cc)
	       when '%'' then
		  new_state(6, true, cc)
	       when '\' then
		  new_state(8, true, cc)
               when '%N' then
                  new_state(state, false, ' ')
               when '%T', ' ' then
                  new_state(10, false, ' ')
	       else
                  if column > 0
                     and then (last_written.is_letter_or_digit or else last_written = '_')
                     and then (cc.is_letter_or_digit or else cc = '_')
                   then
                     if column > max_columns then
                        write('%N')
                     else
                        write(' ')
                     end
                  elseif in_macro and then column > 1 then
                     if column > max_columns then
                        write('%N')
                     else
                        write(' ')
                     end
                  end
		  new_state(0, true, cc)
	       end
            when 12 then -- Inside a once-line comment
               inspect
                  cc
               when '%N' then
                  new_state(11, false, ' ')
               else
                  new_state(state, false, ' ')
               end
	    end
	    cc := next_cc
	 end
	 new_file.disconnect
	 old_file.disconnect
	 debug
	    debug_mode := true
	 end
	 if not debug_mode then
	    file_tools.delete(old_file_name)
	 end
      ensure
	 old_file_size >= new_file_size
      end

   next_cc: CHARACTER is
      require
	 not old_file.end_of_input
      do
	 old_file.read_character
	 if not old_file.end_of_input then
	    old_file_size := old_file_size + 1
	    Result := old_file.last_character
	    if Result = '%T' then
	       Result := ' '
	    end
	 end
      end

   previous_previous_state, previous_state: INTEGER

   new_state(s: INTEGER; call_write: BOOLEAN; c: CHARACTER) is
      do
	 debug
	    previous_previous_state := previous_state
	    previous_state := state
            if state /= s then
               new_file.put_string(once "/*")
               new_file.put_integer(state)
               new_file.put_string(once "*/")
            end
         end
	 state := s
	 if call_write then
	    write(c)
	 end
      end

   write(c: CHARACTER) is
      do
	 check
	    c /= '%U'
	 end
	 new_file.put_character(c)
	 new_file_size := new_file_size + 1
         if c = '%N' then
            column := 0
         else
            column := column + 1
         end
         last_written := c
      end

   last_written: CHARACTER

end
