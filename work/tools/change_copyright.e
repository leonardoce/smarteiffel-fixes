class CHANGE_COPYRIGHT
-- 
-- Usually used to update the copyright headers dates and names.
--
-- usage: change_copyright *.e
--

creation make

feature

   old_text: STRING is "{
-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- Copyright (C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.
--    - University of Nancy 1 - FRANCE
-- Copyright (C) 2003: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne
--    - University of Nancy 2 - FRANCE
-- Dominique COLNET, Suzanne COLLIN, Olivier ZENDRA, Philippe RIBET, Cyril ADRIAN
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr 
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU General Public License for more details.
-- You should have received a copy of the GNU General Public License along with
-- SmartEiffel; see the file COPYING. If not, write to the Free Software
-- Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

   }"

   new_text: STRING is "{
-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries
--
-- SmartEiffel is  free software;  you can redistribute it and/or  modify it
-- under  the terms of the  GNU General Public License, as published by  the
-- Free Software Foundation; either version 2, or (at your option) any later
-- version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT 
-- ANY WARRANTY;  without  even the implied warranty  of MERCHANTABILITY  or
-- FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
-- more details.  You should have received a copy of  the GNU General Public
-- License along with SmartEiffel;  see the file COPYING.  If not,  write to
-- the Free Software Foundation,  Inc., 51 Franklin St, Fifth Floor, Boston,
-- MA  02110-1301, USA
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.
--			   - University of Nancy 1 - FRANCE
-- Copyright(C) 2003:      INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne
--			   - University of Nancy 2 - FRANCE
--
--		 Dominique COLNET, Suzanne COLLIN, Olivier ZENDRA,
--			   Philippe RIBET, Cyril ADRIAN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
--

   }"
   
   tmp: STRING is "tmp.e"

   make is
      local
	 i: INTEGER; file: STRING
      do
	 from
	    i := 1
	 until
	    i > argument_count
	 loop
	    file := argument(i).twin
	    print("[--")
	    if file_tools.file_exists(file) then
	       change_file(file)
	    else
	       print(" " + file + " not found --")
	    end
	    print(" " + file + " --]%N")
	    i := i + 1
	 end
      end

   change_file(file: STRING) is
      local
	 stop: BOOLEAN; old_file: TEXT_FILE_READ; new_file: TEXT_FILE_WRITE
	 buffer: STRING; cc: CHARACTER
	 match_counter: INTEGER
      do
	 create old_file.connect_to(file)
	 create new_file.connect_to(tmp)
	 from
	    create buffer.make(1024)
	    check buffer.is_empty end
	 until
	    stop
	 loop
	    old_file.read_character
	    if old_file.end_of_input then
	       stop := True
	    else
	       cc := old_file.last_character
	       if cc = old_text.item(buffer.upper + 1) then
		  buffer.extend(cc)
		  if buffer.upper = old_text.upper then
		     match_counter := match_counter + 1
		     new_file.put_string(new_text)
		     buffer.clear
		  end
	       else
		  from
		     buffer.extend(cc)
		  until
		     buffer.is_empty
			or else
		     old_text.has_prefix(buffer)
		  loop
		     new_file.put_character(buffer.first)
		     buffer.remove_first(1)
		  end
	       end
	    end
	 end
	 if not buffer.is_empty then
	    new_file.put_string(buffer)
	    buffer.clear
	 end
	 new_file.disconnect
	 old_file.disconnect
	 print(" " + match_counter.to_string + " match --")
	 file_tools.rename_to(tmp, file)
      end

end
