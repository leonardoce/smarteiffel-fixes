@echo off
rem Boostrapping script.
rem Usage : c_bootstrap [args_for_compile]
rem For example to try a bootstrap on a small machine using gcc :
rem          bootstrap -gcc -boost 
rem
rem For example to try a bootstrap on a big machine using cc :
rem
rem              bootstrap -cc -check_all
rem
rem ###################################################################
cd /d %SmartEiffel%\misc
install.bat
rem ###################################################################
cd /d %SmartEiffel%\work
echo Compile compile command
set cmd=..\bin\compile -c_code -o compile %* COMPILE
echo %cmd%
%cmd%
if not exist compile.exe goto error_compile
del /f ..\bin\compile.exe
cp compile.exe ..\bin\compile.exe

rem ###################################################################
echo Try to compute compile_to_c1 with compile
set cmd=compile.exe -c_code -o compile_to_c1 %* compile_to_c
echo %cmd%
%cmd%
if not exist compile_to_c1.exe goto error_compile_to_c1

rem ###################################################################
echo Try to compute compile_to_c2 with compile_to_c1
set cmd=compile_to_c1 -o compile_to_c2 %* compile_to_c
echo %cmd%
%cmd%
if not exist compile_to_c.bat goto error_compile_to_c2
call compile_to_c.bat
if not exist compile_to_c2.exe goto error_compile_to_c2

rem ###################################################################
echo Try to compute compile_to_c3 with compile_to_c2
set cmd=compile_to_c2 -o compile_to_c3 %* compile_to_c
echo %cmd%
%cmd%
if not exist compile_to_c.bat goto error_compile_to_c3
call compile_to_c.bat
if not exist compile_to_c3.exe  goto error_compile_to_c3

rem ###################################################################
compile same_executables -boost -no_split -O6 -o same_executables 
if not exist same_executables.exe goto error_same_executables
set cmd=same_executables compile_to_c2.exe compile_to_c3.exe
echo %cmd%
%cmd%
if errorlevel 1 goto error_not_same_executables
echo compile_to_c2 seems to be a good one

rem ###################################################################
echo Basic test of the new version.
del /f /q ..\bin\compile_to_c.exe
cp compile_to_c2.exe ..\bin\compile_to_c.exe

rem ###################################################################
echo Compile compile_to_jvm
compile -o compile_to_jvm compile_to_jvm -c_code -verbose %*
del /f /q ..\bin\compile_to_jvm.exe
cp compile_to_jvm.exe ..\bin\compile_to_jvm.exe

echo FIN PROVISOIRE 
goto :EOF



@echo on

rename ..\bin\compile.exe ..\bin\compile.old
rename ..\bin\compile_to_c.exe ..\bin\compile_to_c.old
copy compile.exe ..\bin
echo Computing misc/self_test in file 'self_test.out'.
call %SmartEiffel%\misc\c_self_test.bat > self_test.out
fc self_test.out %SmartEiffel%\work\tools\c_self_test.ok
if errorlevel 1 goto error_bad_self_test
del /f /q compile_to_c.old 
echo Self test succeeded.

echo New Version Installed.
goto :EOF

rem ###################################################################
rem ######### Error section :
:error_compile
echo Cannot Compile compile 
goto :EOF

:error_compile_to_c1
echo Bad bootstrap : cannot produce compile_to_c1
goto :EOF

:error_compile_to_c2
echo Bad bootstrap : cannot produce compile_to_c2
goto :EOF

:error_compile_to_c3
echo Bad bootstrap : cannot produce compile_to_c3
goto :EOF

:error_same_executables
echo Cannot compile same_executables 
goto :EOF

:error_not_same_executables
echo Bad bootstrap : compile_to_c2 /= compile_to_c3
goto :EOF

:error_bad_self_test
del /f /q compile_to_c.exe
rename compile_to_c.old compile_to_c,exe
echo self_test failed.
goto :EOF

###################################################################
###################################################################
${SmartEiffel}/lib_msg/self_test
echo "Install New Version."
cd ${SmartEiffel}/bin_c
/bin/rm -f ${SmartEiffel}/bin_c/*
cp ../bin/loadpath.se .
# Small commands :
foreach cmd (compile finder clean)
	echo Compiling $cmd
	../bin/compile_to_c -o $cmd $cmd make -boost -no_split -O2 >! /dev/null
	if (!(-f ${cmd}.make)) then
		echo Error when compiling ${cmd}
		exit 1
	endif
end
# Big commands :
foreach cmd (compile_to_c compile_to_jvm pretty)
	echo Compiling $cmd
	../bin/compile_to_c -o $cmd $cmd make -boost -no_split -O3 >! /dev/null
	if (!(-f ${cmd}.make)) then
		echo Error when compiling ${cmd}
		exit 1
	endif
	mv ${cmd}.make ${cmd}.big
	../bin/compile_to_c -o $cmd $cmd make -boost -O2 >! /dev/null
	if (!(-f ${cmd}.make)) then
		echo Error when compiling ${cmd}
		exit 1
	endif
end
/bin/rm -f loadpath.se
