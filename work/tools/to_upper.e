class TO_UPPER

creation make

feature 

   make is
      local
	 str: STRING;
      do
	 if argument_count /= 1 then
	    std_error.put_string("usage: to_upper <string>%N");
	    die_with_code(exit_failure_code);
	 else
	    str := argument(1);
	    str.to_upper;
	    std_output.put_string(str);
	 end;
      end;

end
