--          This file is part of SmartEiffel The GNU Eiffel Compiler.
--          Copyright (C) 1994-98 LORIA - UHP - CRIN - INRIA - FRANCE
--            Dominique COLNET and Suzanne COLLIN - colnet@loria.fr 
--                       http://www.loria.fr/SmartEiffel
-- SmartEiffel is  free  software;  you can  redistribute it and/or modify it 
-- under the terms of the GNU General Public License as published by the Free
-- Software  Foundation;  either  version  2, or (at your option)  any  later 
-- version. SmartEiffel is distributed in the hope that it will be useful,but
-- WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
-- or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License 
-- for  more  details.  You  should  have  received a copy of the GNU General 
-- Public  License  along  with  SmartEiffel;  see the file COPYING.  If not,
-- write to the  Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
-- Boston, MA  02110-1301, USA
--
class CLASS_INDEX

creation make

feature 

   make is
      local
	 file_name: STRING;
	 sfr: STD_FILE_READ;
      do
	 if argument_count /= 1 then
	    std_error.put_string("usage: class_index <directory>%N");
	    die_with_code(exit_failure_code);
	 else
	    file_name := argument(1);
	    from
	       !!sfr.connect_to(file_name);
	       sfr.read_line;
	    until
	       sfr.end_of_input
	    loop
	       class_names.add_last(sfr.last_string.twin);
	       sfr.read_line;
	    end;
	    sfr.disconnect;
	    do_it;
	    sfw.disconnect;
	 end;
      end;

feature {NONE}

   class_names: ARRAY[STRING] is
      once
	 !!Result.with_capacity(200,1);
      end;

   sfw: STD_FILE_WRITE is
      once
	 !!Result.connect_to("_classes.html");
      end;

   do_it is
      local
	 letter: CHARACTER;
	 i1, i2: INTEGER;
      do
	 sfw.put_string(
         "<HTML><TABLE BORDER=0 CELLSPACING=5 CELLPADDING=5 COLS=1 WIDTH=%"100%%%"%
         %BGCOLOR=%"#3366FF%" NOSAVE >%N%
         %<TR NOSAVE>%N%
         %<TD NOSAVE>%N%
         %<CENTER><FONT COLOR=%"#FFFFFF%" SIZE=+4><B>SmartEiffel The GNU%N%
         %Eiffel Compiler</B></FONT></CENTER>%N%
         %</TD>%N</TR>%N</TABLE>%N<BR><BR>%N<UL>");
	 from
	    i1 := class_names.lower;
	 until
	    i1 > class_names.upper
	 loop
	    letter := class_names.item(i1).first;
	    from
	       i2 := i1;
	    until
	       i2 > class_names.upper or else
	       class_names.item(i2).first /= letter
	    loop
	       i2 := i2 + 1;
	    end;
	    i2 := i2 - 1;
	    machin(letter,i1,i2);
	    i1 := i2 + 1;
	 end;
	 sfw.put_string(
         "</UL>%N</HTML>%N");
      end;

   machin(letter: CHARACTER; i1, i2: INTEGER) is
      local
	 c: INTEGER
	 str: STRING
      do
	 from
	    c := i1;
	 until
	    c > i2
	 loop
	    sfw.put_string("<LI><A HREF=%"./");
	    str := class_names.item(c);
	    str.to_lower;
	    sfw.put_string(str);
	    sfw.put_string(".html%">");
	    str.to_upper;
	    sfw.put_string(str);
	    sfw.put_string("</A></LI>%N");
	    c := c + 1;
	 end
      end;

end
