rem 
rem Run this file to test your SmartEiffel compiler.
rem
rem Printed Output should be the same as the file
rem SmartEiffel/misc/self_test.good_output
rem
for %%flag in (-boost -no_check -require_check -ensure_check -invariant_check -loop_check -all_check -debug_check) do call %SmartEiffel%\work\tools\c_basic_test %flag%
echo Self Test of compile/compile_to_c Done.
