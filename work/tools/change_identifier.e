class CHANGE_IDENTIFIER
-- 
-- Command line tool to change the name of some identifier in one or more Eiffel files as well as shell 
-- scripts or C files.
--
-- Limitation: works only on Unix-like systems (actually this `change_identifier' command keep exactely 
-- all right permission of original files.
--

insert
	COMMAND_LINE_TOOLS

creation
	make

feature {ANY}

	command_line_name: STRING is "change_identifier"
	
   command_line_help_summary: STRING is "usage: change_identifier <old_identifier> <new_identifier> <files...>"

feature {}
	is_valid_argument_for_ace_mode (arg: STRING): BOOLEAN is
		do
		end
	
	valid_argument_for_ace_mode: STRING is "Not an ACE command."

	old_identifier, new_identifier: STRING

   tmp: STRING is "_tmp.e"

   make is
      local
			i: INTEGER; file: STRING
      do
			echo.set_verbose
			if not (create {FILE_TOOLS}).file_exists("/bin/cp") then
				echo.put_string("change_identifier: not on a Unix like system (limitation, sorry).%N")
				die_with_code(exit_failure_code)
			end
			if argument_count < 3 then
				print(command_line_help_summary + "%N")
				die_with_code(exit_failure_code)
			end
			old_identifier := argument(1).twin
			new_identifier := argument(2).twin
			if old_identifier.is_equal(new_identifier) then
				print(command_line_help_summary + "%NError: <old_identifier> identical to <new_identifier>%N")
				die_with_code(exit_failure_code)
			end
			print("Replacing %"" + old_identifier + "%" with %"" + new_identifier + "%"...%N")
			from
				i := 3
			until
				i > argument_count
			loop
				file := argument(i).twin
				print("[-- " + file)
				if (create {FILE_TOOLS}).file_exists(file) then
					replacement_counter := 0
					change_file(file)
					if replacement_counter > 0 then
						print(": " + replacement_counter.to_string + " times")
					end
				else
					print(file + " (file not found)")
				end
				print(once " --]%N")
				i := i + 1
			end
		end

   replacement_counter: INTEGER
   
   change_file (file: STRING) is
      local
			old_file: TEXT_FILE_READ; new_file: TEXT_FILE_WRITE
			line, cmd: STRING; idx, idx2: INTEGER
			stop_file, stop_line, before_flag, after_flag: BOOLEAN
			os: SYSTEM
      do
			-- To preserve access permission, the original file is copied first:
			cmd := "/bin/cp "
			cmd.append(file)
			cmd.extend(' ')
			cmd.append(tmp)
			os.execute_command_line(cmd)
			--
			create old_file.connect_to(file)
		   create new_file.connect_to(tmp)
			from
				if new_file.is_connected then
					stop_file := old_file.end_of_input
				else
					stop_file := True
					print(" no write permission")
				end
			until
				stop_file
			loop
				check
					not old_file.end_of_input
				end
				old_file.read_line
				line := old_file.last_string.twin
				-- Searching the `old_identifier':
				from
					stop_line := False
				until
					stop_line
				loop
					idx := line.first_substring_index(old_identifier)
					if idx = 0 then
						stop_line := True
					else
						sedb_breakpoint
						before_flag := False
						after_flag := False
						idx2 := idx + old_identifier.count
						if idx2 > line.count then -- end of line:
							after_flag := True
						else
							after_flag := separator_flag(line.item(idx2))
						end
						idx2 := idx - 1
						if idx2 = 0 then -- beginning of line:
							before_flag := True
						else
							before_flag := separator_flag(line.item(idx2))
						end
						if before_flag and after_flag then
							perform_replacement(line,idx)
						else
							stop_line := True
						end
					end
				end
				-- Writing the (modified?) line:
				new_file.put_string(line)
				if old_file.end_of_input then
					stop_file := True
				else
					new_file.put_character('%N')
				end
			end
			old_file.disconnect
			if new_file.is_connected then
				new_file.disconnect
				;(create {FILE_TOOLS}).rename_to(tmp,file)
			else
				cmd := "/bin/rm -f "
				cmd.append(tmp)
				os.execute_command_line(cmd)
			end
      end

   separator_flag (c: CHARACTER): BOOLEAN is
			-- True if `c' is a valid character before or after some identifier. 
      do
			inspect
				c
			when 'a' .. 'z', 'A' .. 'Z', '0' .. '9', '_' then
			else
				Result := True
			end
      end

   perform_replacement (line: STRING; idx: INTEGER) is
      local
			idx2: INTEGER
      do
			idx2 := idx + old_identifier.count - 1
			line.replace_substring(new_identifier, idx, idx2)
			replacement_counter := replacement_counter + 1
      end

end
