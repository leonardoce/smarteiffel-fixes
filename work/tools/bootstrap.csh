#!/bin/csh -f
#
# Main boostrapping script. See also run_all_test.csh to launch the whole test suite.
#
# Usage : bootstrap.csh [args_for_compile]
#
# Usually, this command is used as follow:
#
#         bootstrap.csh -boost 
#
# If you have a lot of time, you may consider to do:
#
#         bootstrap.csh -all_check
# or (more realistic):
#         bootstrap.csh -require_check -flat_check
#
###################################################################
if ($#argv == 0) then
   echo "Assume you want to use the -boost flag."
   set argv=(-boost)
endif
source `dirname $0`/SE_conf || exit 1

setenv PATH ${SE}/bin:${PATH}
###################################################################
cd ${SE}/work/no_gc
/bin/rm -f c2c no_gc_c2c[1-3]
###################################################################
echo "Bootstrap T1 no GC"
/bin/rm -f compile_to_c.make
set cmd="compile_to_c -no_gc -relax $* -o c2c compile_to_c"
$cmd
if ($status) then
   echo "Bad bootstrap (crash during T1 no GC)."
   exit 1
endif
if (!(-f compile_to_c.make)) then
   echo "Bad bootstrap (T1 no GC)."
   exit 1
endif
set verbose=1
source ./compile_to_c.make
unset verbose
if (!(-f c2c)) then
   echo "Bad bootstrap (T1 -no_gc)."
   exit 1
endif
cp c2c no_gc_c2c1
###################################################################
echo "Bootstrap T2 no GC"
/bin/rm -f c2c compile_to_c.make
set cmd="./no_gc_c2c1 -no_gc -relax -verbose $* -o c2c compile_to_c"
echo $cmd
$cmd
if ($status) then
   echo "Bad bootstrap (crash during T2 no GC)."
   exit 1
endif
if (!(-f compile_to_c.make)) then
   echo "Bad bootstrap (T2 no GC)."
   exit 1
endif
set verbose=1
source ./compile_to_c.make
unset verbose
if (!(-f c2c)) then
   echo "Bad bootstrap (T2 -no_gc)."
   exit 1
endif
cp c2c no_gc_c2c2
###################################################################
echo "Bootstrap T3 no GC"
/bin/rm -f c2c compile_to_c.make
set cmd="./no_gc_c2c2 -no_gc -relax -verbose $* -o c2c compile_to_c"
echo $cmd
$cmd
if ($status) then
   echo "Bad bootstrap (crash during T3 no GC)."
   exit 1
endif
if (!(-f compile_to_c.make)) then
   echo "Bad bootstrap (T3 no GC)."
   exit 1
endif
set verbose=1
source ./compile_to_c.make
unset verbose
if (!(-f c2c)) then
   echo "Bad bootstrap (T3 -no_gc)."
   exit 1
endif
mv c2c no_gc_c2c3
###################################################################
echo "Compile compile"
set cmd="./no_gc_c2c3 -no_gc -relax $* -o compile compile"
echo $cmd
$cmd
if ($status) then
   echo "Cannot Compile compile (crash)."
   exit 1
endif
if (!(-f compile.make)) then
   echo "Cannot Compile compile (no compile.make)."
   exit 1
endif
set verbose=1
source ./compile.make
unset verbose
if (!(-f compile)) then
   echo "Cannot Compile compile" 
   exit 1
endif
###################################################################
/bin/rm -f same_executables
./compile -clean -no_gc -relax -no_split $* same_executables -o same_executables 
if (!(-f same_executables)) then
   echo "Cannot Compile same_executables (no GC)" 
   exit 1
endif
set cmd="./same_executables no_gc_c2c2 no_gc_c2c3"
echo $cmd
$cmd
if ($status) then
   echo "Bootstrap not correct : T2 /= T3 (no GC)."
   exit 1
endif
echo "Bootstrap no GC correct."
###################################################################
cd ${SE}/work
mv -f ${SmartEiffelBin}/compile_to_c ${SmartEiffelBin}/compile_to_c.old
mv -f no_gc/no_gc_c2c2 ${SmartEiffelBin}/compile_to_c
mv -f no_gc/compile ${SmartEiffelBin}/compile
echo "New version installed."
###################################################################
cd ${SE}/work/with_gc
/bin/rm -f c2c with_gc_c2c[1-2]
cp -f ../no_gc/compile_to_c.id .
echo "Bootstrap T1 with GC"
/bin/rm -f compile_to_c.make
set arguments="$* -gc_info -o c2c compile_to_c"
#set arguments="$* -o c2c compile_to_c"
set cmd="../no_gc/no_gc_c2c3 -relax ${arguments}"
echo $cmd
$cmd
if ($status) then
   echo "Bad bootstrap (crash during T1 with GC)."
   exit 1
endif
if (!(-f compile_to_c.make)) then
   echo "Bad bootstrap (T1 with GC)."
   exit 1
endif
set verbose=1
source ./compile_to_c.make
unset verbose
if (!(-f c2c)) then
   echo "Bad bootstrap (T1 with GC no c2c file)."
   exit 1
endif
cp c2c with_gc_c2c1
###################################################################
echo "Bootstrap T2 with GC"
/bin/rm -f compile_to_c.make
set arguments="-verbose -relax $* -gc_info -o c2c compile_to_c"
#set arguments="-verbose $* -o c2c compile_to_c"
set cmd="./with_gc_c2c1 ${arguments}"
echo $cmd 
$cmd >&! info
if ($status) then
   echo "Bad bootstrap (crash during T2 with GC)."
   exit 1
endif
if (!(-f compile_to_c.make)) then
   echo "Bad bootstrap (T2 with GC)."
   exit 1
endif
set verbose=1
source ./compile_to_c.make
unset verbose
if (!(-f c2c)) then
   echo "Bad bootstrap (T2 with GC no c2c file)."
   exit 1
endif
mv c2c with_gc_c2c2
###################################################################
/bin/rm -f same_executables
compile -clean -no_gc -relax -no_split $* same_executables -o same_executables
if (!(-f same_executables)) then
   echo "Cannot Compile same_executables (with GC)." 
   exit 1
endif
set cmd="./same_executables with_gc_c2c1 with_gc_c2c2"
echo $cmd
$cmd
if ($status) then
   echo "Bootstrap not correct : T1 /= T2 (with GC)."
   exit 1
endif
echo "Bootstrap with GC correct."
echo "Consider launching run_all_tests.csh right now."
echo `date`
echo Done
