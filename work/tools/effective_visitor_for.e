class EFFECTIVE_VISITOR_FOR
	--
	-- Automatically create a visitor template (heir of SmartEiffel/tools/visitors/*.e)
	--

inherit
	ACE
		-- Cyril's trick to make simple SmartEiffel-related tools.

insert
	PARENT_LISTS_VISITOR
		-- to access CLASS_TEXT.inherit_list
	ARGUMENTS

creation {ANY}
	make

feature {}
	make is
		do
			if system_tools /= Void then
				-- Just to be sure that system_tools is created as early as possible.
			end
			set_root_class_name_using(fz_none)
			system_tools.read_loadpath_files
			echo.set_verbose
			get_started
			read_command_line
			generate_visitor
		end

	parents: FAST_ARRAY[STRING]

	pending_ancestors: RING_ARRAY[CLASS_NAME] is
		once
			create Result.with_capacity(16, 0)
		end

	registered_ancestors: HASHED_SET[CLASS_NAME] is
		once
			create Result.with_capacity(64)
		end

feature {}
	target_class_name, target_file_name: STRING

	read_command_line is
		local
			visitor_name: CLASS_NAME; i: INTEGER
		do
			if argument_count < 2 then
				std_error.put_string(once "Syntax:%N")
				std_error.put_string(argument(0))
				std_error.put_string(once " EFFECTIVE_VISITOR CLASS_1 [CLASS_2 ...]%N%
												  %Generate a visitor for CLASS_1, CLASS_2... and name it EFFECTIVE_VISITOR.%N%
												  %The generated effective_visitor.e contains `not_yet_implemented' instructions%N%
												  %that must be replaced with appropriate code.%N%
												  %CLASS_1_VISITOR, CLASS_2_VISITOR ... must already exist and be%N%
												  %reachable (through loadpath.se).%N")
				die_with_code(exit_failure_code)
			end
			target_class_name := argument(1).twin
			target_file_name := target_class_name.twin
			if target_class_name.has_suffix(once ".e") then
				target_class_name.remove_suffix(once ".e")
			else
				target_file_name.to_lower
				target_file_name.append(once ".e")
			end
			target_class_name.to_upper
			create parents.with_capacity(argument_count - 1)
			from
				i := 2
			until
				i > argument_count
			loop
				visitor_name := visitor_name_for(argument(i))
				parents.add_last(visitor_name.to_string)
				add_ancestor(visitor_name)
				i := i + 1
			end
		end

	add_ancestor (class_name: CLASS_NAME) is
		do
			if not registered_ancestors.has(class_name) then
				registered_ancestors.add(class_name)
				pending_ancestors.add_last(class_name)
			end
		end

	visitor_name_for (filename: STRING): CLASS_NAME is
		local
			s: STRING
		do
			s := once ""
			s.copy(filename)
			if s.has_suffix(once ".e") then
				s.remove_suffix(once ".e")
			end
			s.to_upper
			s.append(once "_VISITOR")
			create Result.unknown_position(string_aliaser.hashed_string(s))
		end

feature {}
	generate_visitor is
		local
			visitor_file: TEXT_FILE_WRITE; i: INTEGER; ct: CLASS_TEXT; ct_parents: FAST_ARRAY[PARENT_EDGE]
			visited_name, function_name: STRING; fn: FEATURE_NAME; parent_name: CLASS_NAME
		do
			create visitor_file.connect_to(target_file_name)
			function_name := ""
			visited_name := ""
			visitor_file.put_string(once "class ")
			visitor_file.put_string(target_class_name)
			visitor_file.put_string(once "%Ninherit%NVISITOR%N")
			from
				i := parents.lower
			until
				i > parents.upper
			loop
				visitor_file.put_string(parents.item(i))
				visitor_file.put_new_line
				i := i + 1
			end
			from
			until
				pending_ancestors.is_empty
			loop
				ct := smart_eiffel.class_text(pending_ancestors.first, True)
				pending_ancestors.remove_first
				ct_parents := ct.parent_lists.inherit_list
				if ct_parents /= Void then
					from
						i := ct_parents.lower
					until
						i > ct_parents.upper
					loop
						parent_name := ct_parents.item(i).class_text.name
						if parent_name.to_string.has_suffix(once "_VISITOR") then
							add_ancestor(parent_name)
						else
							visited_name.copy(ct.name.to_string)
							visited_name.remove_suffix(once "_VISITOR")
							function_name.copy(once "visit_")
							function_name.append(visited_name)
							function_name.to_lower
							create fn.unknown_position(function_name)
								if ct.proper_get(fn).is_deferred then
									visitor_file.put_string(once "feature {")
									visitor_file.put_string(visited_name)
									visitor_file.put_string(once "}%N")
									visitor_file.put_string(function_name)
									visitor_file.put_string(once " (visited: ")
									visitor_file.put_string(visited_name)
									visitor_file.put_string(once ") is%Ndo%Nnot_yet_implemented%Nend%N")
								end
							end
						i := i + 1
					end
				end
			end
			visitor_file.put_string(once "end%N")
			visitor_file.disconnect
		end

feature {}
	as_visitor: VISITOR is
		do
			check
				False
			end
		end

feature {PARENT_LISTS}
	visit_parent_lists (visited: PARENT_LISTS) is
		do
			check
				False
			end
		end

end -- class EFFECTIVE_VISITOR_FOR
