class BAD_ASSIGNMENT_TEST1
--
-- To test the ?:= operator.
--

inherit
	AUX_ASSIGNMENT_TEST2
		redefine animal
		end
	
creation
	make

feature
	animal: REFERENCE[CAT]
	
	make is
		local
			boolean: BOOLEAN; tmp: REFERENCE[ANIMAL]
		do
			create animal.set_item(create {CAT})
			boolean := assignment_test

			tmp := animal
			tmp.set_item(create {DOG}) -- Broken !! 
			
			boolean := assignment_test

		end
	
end
	
