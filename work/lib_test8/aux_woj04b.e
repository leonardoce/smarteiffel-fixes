class AUX_WOJ04B
inherit
   AUX_WOJ04A redefine make, code end

create
   make

feature
   make(v: INTEGER) is
      do
	 code := v
      end

   code: INTEGER

end
