class AUX_ONCE3A

inherit BAD_ONCE02

creation make_a

feature

   value: CHARACTER;
	
feature {NONE}
	
   make_a is
      local
			c: CHARACTER;
      do
			value := 'a';
			c := once_b.value;
      end

end
