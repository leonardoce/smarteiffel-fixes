class BAD_TYPE1

creation make

feature

   make is
      local
	 a, b: AUX_TYPE_A
      do
	 !AUX_TYPE_B!a
	 !!b
	 a.binary(b)
      end


end
