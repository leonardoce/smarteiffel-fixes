class BAD_REQUIRE1

--SZ:385: see bad_require2 too.
--	
-- For the first test, `to_integer' is generated but not called.
-- So the require is not tested.
-- Of course, fit_integer_32 is defined as `True' in INTEGER_32...
--
-- For the second test, the compiler generates the good code.
	
create make
	
feature

	make is
		do
			io.put_boolean((2^32 + 23).to_integer_32 > 0)
			io.put_new_line
		end
end
	
