class AUX_WOJ04A

create
   make

feature

   make(v: INTEGER) is
      do
	 private_code := v
      end

   code: INTEGER is
      do
	 Result := private_code
      end

feature {}
   private_code: INTEGER

end
