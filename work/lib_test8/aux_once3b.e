class AUX_ONCE3B

inherit BAD_ONCE02

creation make_b

feature

   value: CHARACTER;

   make_b is
      do
			value := 'b'
			is_true(once_a.value = 'a')
      end

end
