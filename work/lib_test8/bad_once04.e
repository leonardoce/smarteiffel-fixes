class BAD_ONCE04

creation make, a_make, b_make

feature

   value: INTEGER

   make is
      do
			value := 1 + once_b.value
			is_true(value = 3)
      end

   once_a: BAD_ONCE04 is
      once
			create Result.a_make
      end
	
   once_b: BAD_ONCE04 is
      once
			create Result.b_make
      end
	
   a_make is
      do
			value := 1 + once_b.value
      end
	
   b_make is
      do
			value := 1 + once_a.value
      end
	
   is_true(b: BOOLEAN) is
      do
			cpt := cpt + 1
			if not b then
				std_output.put_string("BAD_ONCE04: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
				crash
			end
      end
   
   cpt: INTEGER
	
end
