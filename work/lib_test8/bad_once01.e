class BAD_ONCE01
create make
feature
	make is
		local
			i: INTEGER
		do
			i := plante
		end

	plante: INTEGER is
			-- SE should detect this recursion
		once
			Result := plante
		end
end
