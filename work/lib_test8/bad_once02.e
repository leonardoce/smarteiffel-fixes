class BAD_ONCE02

creation make
	
feature
	
   make is
      do
			is_true(once_a.value = 'a')
      end

   once_a: AUX_ONCE3A is
      once
			create Result.make_a
      end

   once_b: AUX_ONCE3B is
      once
			create Result.make_b
      end

   is_true(b: BOOLEAN) is
      do
			cpt := cpt + 1
			if not b then
				std_output.put_string("BAD_ONCE03: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
				crash
			end
      end
   
   cpt: INTEGER

end
