class AUX_TYPE_B

inherit AUX_TYPE_A redefine binary end

feature

   binary(other: like Current) is
      do
	 other.a
      end

end
