class BAD_COPY_CALL1
-- From: Wolfgang Jansen <wolfgang@agnld.uni-potsdam.de>
creation make

feature

   make is
      local
	 x: AUX_WOJ04A; y: AUX_WOJ04B
      do
	 create x.make(10)
	 is_true(x.code = 10)
	 create y.make(20)
	 x.copy(y)
	 is_true(y.code = 20)
	 is_true(x.code = 20)
      end
   
   is_true(b: BOOLEAN) is
      do
	 cpt := cpt + 1
	 if not b then
	    sedb_breakpoint
	    std_output.put_string("TEST_WOJ04: ERROR Test # ")
	    std_output.put_integer(cpt)
	    std_output.put_string("%N")
	 end;
      end;
   
   cpt: INTEGER
   
   
end
