class BAD_VOID_TARGET

creation make

feature

   make is
      local
	 bug: STRING;
      do
	 bug := "foo";
	 bug := Void;
	 bug.extend(' ');
      end;

end
