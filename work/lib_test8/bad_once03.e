class BAD_ONCE03

creation make
   
feature 
   
   proc1val: INTEGER
   
   proc1 is
      once
			proc1
			proc1val := proc1val + 1
      end
   
   fonc1: INTEGER is
      once
			Result := fonc1
			Result := Result + 1
      end
   
   crossf1: INTEGER is
      once
			Result := 3 + crossf2
      end
   
   crossf2: INTEGER is
      once
			Result := 2 + crossf1
      end
   
   make is
      do
			proc1
			is_true(proc1val = 1)
			is_true(fonc1 = fonc1)
			is_true(fonc1 = 1)
			is_true(crossf1 = 3 or crossf1 = 5)
			-- What to think of this crazy call :-) 
			-- It is not really clear in E.T.L. :-(
      end
   
   is_true(b: BOOLEAN) is
      do
			cpt := cpt + 1
			if not b then
				std_output.put_string("BAD_ONCE03: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
			else
				-- std_output.put_string("Yes%N")
			end
      end
   
   cpt: INTEGER
   
end

