class BAD_REQUIRE2

--SZ:385: see bad_require1 too
--	
-- For the first test, `to_integer' is generated but not called.
-- So the require is not tested.
-- Of course, fit_integer_32 is defined as `True' in INTEGER_32...
--
-- For the second test, the compiler generates the good code.
	
create make
	
feature

	make is
		do
			io.put_integer((2^32 + 23).to_integer_32)
			io.put_new_line
		end
end
	
