class TEST_MULTIPLE_PATH

inherit
	AUX_MULTIPLE_PATH_B
	AUX_MULTIPLE_PATH_C

create make

feature
	make is
		do
			a
			b
			c
			assert(assert_counter.value = 3)
		end
end
