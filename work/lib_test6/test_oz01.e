class TEST_OZ01
-- From a bug report of Olivier ZENDRA
creation make

feature

   make is
      local
	 a: ARRAY[AUX_OZ01];
      do
	 !!a.make(1,2);
      end;

end
