class AUX_AS1_ANY

feature

   stream: STRING is 
      once
			!!Result.make(10);
      end;

   assert(b: BOOLEAN) is
      do
			cpt := cpt + 1;
			if not b then
				sedb_breakpoint
				std_output.put_string("TEST_AS1: ERROR Test # ");
				std_output.put_integer(cpt);
				std_output.put_string("%N");
			else
				-- std_output.put_string("Yes%N");
			end;
      end;
   
   cpt: INTEGER;

end
