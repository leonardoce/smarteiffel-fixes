class TEST_DEFERRED3

creation make

feature

   make is
      local
	 bool: BOOLEAN;
      do
	 if bool then
	    bool := call_deferred;
	 end
      end;

   call_deferred: BOOLEAN is
      deferred
      end;

end
