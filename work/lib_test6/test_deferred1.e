class TEST_DEFERRED1

creation make

feature

   make is
      local
	 bool: BOOLEAN;
      do
	 if bool then
	    call_deferred;
	 end
      end;

   call_deferred is
      deferred
      end;

end
