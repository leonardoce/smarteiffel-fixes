class AUX_AS1_A[G -> AUX_AS1_D]

inherit AUX_AS1_ANY;

creation make
	
feature

   static_b: AUX_AS1_B;
   static_e: AUX_AS1_E;
   static_d: AUX_AS1_D;
	
   make (init: G) is
      require
	 non_void_init: init /= Void
      do
	 !AUX_AS1_E!static_b;
	 static_b.message;
	 assert(stream.last = 'B');

	 !AUX_AS1_E!static_e;
	 static_e.message;
	 assert(stream.last = 'E');

	 !!static_d;
	 static_d.message;
	 assert(stream.last = 'D');

	 static_d := init;
	 static_d.message;
	 assert(stream.last = 'E');

	 !AUX_AS1_E!static_d;
	 static_d.message;
	 assert(stream.last = 'E');

      end;
			
end
