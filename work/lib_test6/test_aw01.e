class TEST_AW01
-- From a bug report of Arno Wagner

creation make

feature   

   make is
      local 
	 c: AUX_AW01C
	 b: AUX_AW01B
	 cc: COLLECTION[STRING]
	 bool: BOOLEAN;
      do
	 if bool then
	    b.procedure
	    cc := c.function
	 end
      end;
end
