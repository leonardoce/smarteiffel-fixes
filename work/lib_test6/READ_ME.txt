Directory "lib_test6" is a set of programs to check the option -no_warning.
Thus, a program in this directory must be compilable with 
some warning, but must also be able to produce no error
at run time for all modes.
