class TEST_CASE2

creation make

feature

   Xx : INTEGER;

   xx : INTEGER;

   make is
      do
	 xx := 2;
	 Xx := 3;
	 if xx /= 2 then
	    std_output.put_string("TEST_CASE2: ERROR Test #1%N");
	 elseif Xx /= 3 then
	    std_output.put_string("TEST_CASE3: ERROR Test #2%N");
	 end;
      end;

end
