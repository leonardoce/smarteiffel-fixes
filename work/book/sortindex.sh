#!/bin/sh

# This file is part of the SmartEiffel Reference Guide
#
# Copyright (C) 2002 the SmartEiffel team
#
# Permission is granted to copy, distribute and/or modify this document under
# the terms of  the GNU Free Documentation License,  Version 1.1 published by
# the  Free  Software  Foundation;  with  no  Invariant  Sections,  with  the
# Front-Cover Texts being _SmartEiffel  the GNU Eiffel suite Reference Guide_
# and _by the  SmartEiffel team_, and with no Back-Cover  Texts.  copy of the
# license  is  included in  the  section  entitled  ``GNU Free  Documentation
# License'' (see fdl.tex).

# This script is used by the makefile to ensure that "see" entries are
# displayed only once. The raw result in SmartEiffel.idx must be treated.

dos2unix SmartEiffel.idx >/dev/null 2>&1
grep -v '|see' SmartEiffel.idx > SmartEiffel.see
grep '|see' SmartEiffel.idx \
    | awk -F'{' '{printf("%s{%s{%s{%s{%s\n", $1, $2, $3, $4, $5)}' \
    | sort -u \
    | awk '{printf("%s{1}\n", $0)}' >> SmartEiffel.see
sort SmartEiffel.see > SmartEiffel.idx
