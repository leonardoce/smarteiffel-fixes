% This file is part of the SmartEiffel Reference Guide
%
% Copyright (C) 2003 the SmartEiffel team
%
% Permission is granted to copy, distribute and/or modify this document under
% the terms of  the GNU Free Documentation License,  Version 1.1 published by
% the  Free  Software  Foundation;  with  no  Invariant  Sections,  with  the
% Front-Cover Texts being _SmartEiffel  the GNU Eiffel suite Reference Guide_
% and _by the  SmartEiffel team_, and with no Back-Cover  Texts.  copy of the
% license  is  included in  the  section  entitled  ``GNU Free  Documentation
% License'' (see fdl.tex).

\chapter{Overview}

\section{Introduction}

\SE is written in pure Eiffel, and compiled exclusively by itself.  It
tends towards compliance to the \ELKS for the libraries, and \ETL for
the language itself.

We say ``tends towards'', not ``complies to'', because due to its
research origin, \SE sometimes shows radical opinions, which may diverge
from the ``official'' line of ESI and Bertrand~{\sc Meyer}.  Some of
those thoughts show themselves in the way the library are built
(\chap{libraries}); others appear in some difference ways the compiler
is built.

This chapter presents the global process used by \SE to achieve the
compilation of an Eiffel program.

Thus it mostly focuses on the \command{compile} programs
(\command{compile}, \command{compile\_to\_c},
\command{compile\_to\_jvm}) even if other tools (\command{short},
\command{pretty}) also use the parser and the live tree.

\section{Some general concepts}

We will call {\em tool classes} the classes in the
\url{SmartEiffel/tools} directory.

\SE uses many singletons.  They are implemeted using the {\tt once}
keyword in a class known as \seclass{GLOBALS} which is inherited by
virtually all the tool classes.

\section{The parser}

\subsection{Generalities}

The parser is an LL(1) parser.  It means that the parser tries to deduce
the structure of a source code from the keywords it is
given\footnote{Other parsers in the world are generally LR and LALR
parsers, which try to match the expected structure of a source with the
provided one.  If you want more details on what exactly it is all about,
please read \cite{DRAGON}.}.

In ``standard'' parsers, one can distinguish two phases: the words
recognit\-ion---that process splits the source file in words with
meaning, called {\em tokens}--- and the sentences recognition, i.e.,
giving semantics to the whole program.

Not so with \SE: the parser interleaves lexical (finding the tokens) and
syntactical (giving meaning) parsing.

\section{The live tree construction}

An Eiffel program is typically the instanciation of a root object, using
some root procedure.  What the program does is defined by the set of
instructions called by that procedure, recursively.  That set is named
the {\em call tree.}

An example of such a program is the typical {\em Hello World;} You can
find it in the tutorial.  Its call tree is shown \fig{hwcalls}.

\begin{figure}[hpbt]
\caption{\label{hwcalls}The call tree of \tutclass{HELLO\_WORLD}}

\begin{center}
\epslide{7cm}{hwcalls.eps}
\end{center}
\end{figure}

In the same way, the set of live types if the set of types ever created
by an instruction in the call tree (i.e., reached by the root
procedure).

The compiler follows the branches of that tree; each time a new type is
used (either by instanciation or merely by some entity declaration) its
corresponding class is first parsed (using the parser).  Then, if some
feature is called on that type, the compiler follows that new branch,
and so on.

The created types are known as {\em live types} (objects of the class
\seclass{RUN\_CLASS}); knowing the set of live types, the compiler will
be able to optimize the polymorphic calls (e.g., if only one type is
live, there cannot be any polymorphic call).

The features of the call tree, defined in a live type, are marked as
{\em live} too (indeed, some \seclass{RUN\_FEATURE} object is created in
its corresponding \seclass{RUN\_CLASS}).

\section{The tree optimization}

\textit{Dominique, that's up to you to fill the gap.  Here, just a small overview.}

\section{The code generation}

Depending on the tool, either \seclass{C\_PRETTY\_PRINTER} or
\seclass{JVM}, both inheriting from \seclass{CODE\_PRINTER}, are used to
output C code or Java bytecode.

The way of generating the code is basically simple: \SE knows all the
live features (\seclass{RUN\_FEATURE}'s) and simply ``ask them'' to
generate themselves.

\chapter{A \SE tour}

\section{Introduction}

Now we will visit the whole {\tt tools} directory (which can be viewed
as the main cluster of the tools of the \SE suite).  The following
chapters are organized by sub-cluster, the first one being {\tt tools}
itself.

The \fig{toolclusters} shows the cluster {\tt tools} and its
sub-clusters.

\begin{figtt}{\label{toolclusters}The sub-clusters of \texttt{tool}}
tools
   e\_type
   expression
   feature
   instruction
   misc
   run\_feature
\end{figtt}

\section{Tools}

This cluster contains many things:

\begin{itemize}

\item The root class of each tool of the suite: see the
\chap{toolroots}.

\item Some general-purpose classes: see the \chap{utilities}.

\end{itemize}

\subsection{\label{toolroots}The \SE tools}

Even though the \SE tools share a lot of code, each of them has its own
root class.  The name of those classes match the name of the tools:

\begin{itemize}

\item \seclass{CLEAN}
\item \seclass{COMPILE}
\item \seclass{COMPILE\_TO\_C}
\item \seclass{COMPILE\_TO\_JVM}
\item \seclass{FINDER}
\item \seclass{PRETTY}
\item \seclass{PRINT\_JVM\_CLASS}
\item \seclass{SHORT}

\end{itemize}

\subsection{\label{utilities}General-purpose classes}

Those classes are more or less grouped by intent or function.

\begin{itemize}

\item \seclass{GLOBALS} allows the \SE tools to share singletons.  Here
      is the list of singletons, in alphabetical order:

\begin{itemize}
  
\item \ttword{address\_of\_pool} registers the uses of the {\tt \$}
      operator.  It is important because \SE inlines a lot of
      code.  Without the use of {\tt \$}, some functions are never
      generated (because always inlined).  this class fores the
      generation of all the features that are called by address.

\item \ttword{ace} is used for ACE files parsing.

\item \ttword{assignment\_handler} registers any kinds of assignments
      (be they explicit, or implicit like formal-effective arguments
      attachment at the call sites).  It also handles implicit
      conversions.  All the assignments are part of a graph, the {\em
      assignment graph}, used to verify the global system validity.

\item \ttword{constant\_pool} is used by \command{compile\_to\_jvm} to
      generate one part of a class header (the constant pool; see
      \cite{JVMspec}).
  
\item \ttword{cpp} keeps some ``context'' for the C code generation, and
      provides many helpers.  See the \chap{compiletoc}.

\item \ttword{echo} is used by \SE to handle the \option{verbose} flag,
      acting either as a log sink or dispenser.

\item \ttword{eiffel\_parser} is the \SE parser.  See the
      \chap{theparser}.

\item \ttword{error\_handler} is used to generate well-formatted errors,
      warnings, and so on.

\item \ttword{frozen\_string\_list} is a class sharing a lot of
      strings.  It avoids having those strings manyfold in the program.

\item \ttword{gc\_handler} handles the memory allocation.  Particularly
      if the garbage collector is used, it generates the GC functions
      specific to the program being compiled.  Of course, this object is
      only used by \command{compile\_to\_c}.

\item \ttword{id\_provider} is a sort of counter: it provides a unique
      identifier for each run class.  It may be saved and restored (in
      the {\tt .id} file: see the \chap{cwarning} for details).
  
\item \ttword{jvm} provides some context management and some tools for
      the Java bytecode generation.  See the \chap{compiletojvm}.

\item \ttword{manifest\_array\_pool} manages the manifest arrays C
      functions.

\item \ttword{method\_info} is used by \command{compile\_to\_jvm}.

\item \ttword{once\_manifest\_string\_pool} gathers all the once strings
      of a program, and generates the code (either C or Java) to
      allocate them at the beginning of the generated program (before
      the root procedure is called).

\item \ttword{once\_routine\_pool} registers the once procedures and
      functions, and provides some helpers to generate the C code and
      the Java bytecode.

\item \ttword{pretty\_printer} is used by the \command{pretty} command
      to keep some information.

\item \ttword{smart\_eiffel} is the \seclass{SMART\_EIFFEL} object, see
      the \chap{ClassSmartEiffel}.
  
\item \ttword{string\_aliaser} implements one of the most constanly used
      patterns in \SE to manage the memory: give it a string, it will
      return an aliased version of it (i.e., always the same, but never
      the one you gave firsthand).  This way, the compiler never uses
      {\tt is\_equal}, but plain {\tt =} to compare strings.

\item \ttword{switch\_collection} collects the {\em switch sites,} i.e.,
      the function calls that need some dynamic dispatch.

\item \ttword{system\_tools} is a collection of tools used to manage
      system-dependant information.  Particularly, it handles the
      \ttword{SmartEiffel} environment variable.

\end{itemize}

\item \seclass{SMART\_EIFFEL} manages the compilation process.  It
      contains one important variable, \ttword{status}, which indicates the
      current compilation phase (see the \chap{ClassSmartEiffel}).

\item \seclass{ACE} is the LACE parser, used to gather general and
      per-cluster options, and some compiler options (how to generate
      the C code, should we use sedb---see the \chap{intsedb}---, and so
      on).

\item \seclass{EIFFEL\_PARSER} is the Eiffel parser, used to parse
      Eiffel source classes.  See the \chap{theparser}.

\item \seclass{CLUSTER} is used to organize the parsed classes
      (\seclass{BASE\_CLASS}'es) in a tree of clusters.  ``In traditional
      mode'', a cluster simply maps a directory; in ACE mode (i.e.,
      using an ACE file), the clusters can be explicitely declared to
      give them some options.

\item \seclass{BASE\_CLASS} is the internal representation of an Eiffel
      class.  It is a direct translation made by the
      \seclass{EIFFEL\_PARSER} of a source code into a tree of
      objects---also known as Abstract Syntactic Tree, or AST for
      short.  Such a class will be used later on to infer the
      \seclass{RUN\_CLASS}'es; see the \chap{runclass}.

\item \seclass{E\_FEATURE} could have been in this cluster instead of
      \ttword{feature} (see the \chap{toolfeatures}).  This class is the
      abstract interface of all the types of Eiffel feature: procedures,
      functions, attributes, once procedures and functions, deferred
      procedures and functions.  All the classes inheriting from this one
      are in the \ttword{feature} cluster.  The objects of that type are
      called {\em base features.}

\item \seclass{E\_TYPE} is the abstract interface of all the kinds of
      Eiffel types: simple class-based types; entity type markers
      written as {\em reference}, {\em expanded}, or {\em separate;}
      anchored types; and basic types (\libclass{INTEGER},
      \libclass{BOOLEAN} and so on).  All the descendant classes are in
      the \ttword{e\_type} cluster (see the \chap{tooltypes}).

\item \seclass{EXPRESSION} is the abstract interface of all the possible
      Eiffel expressions: that is, Eiffel constructs that may be used at
      the right of an assignment instruction or as argument of a feature
      call(roughly, ``Eiffel constructs having a result'').  It includes
      function calls, entity use, all kind of constants (integer,
      boolean constants, but also manifest strings and manifest arrays
      .~.~.) The whole tree of classes inheriting from
      \seclass{EXPRESSION} is in the \ttword{expression} cluster (see
      the \chap{toolexpressions}).

\item \seclass{INSTRUCTION} is the abstract interface of all the
      possible Eiffel instructions: that is, ``standalone'' Eiffel
      constructs (roughly, ``Eiffel constructs having no result'').  It
      includes procedure calls, assignments, loops, if-then-elses,
      creation calls, check instructions, and so on.  The whole tree of
      classes inheriting from \seclass{EXPRESSION} is in the
      \ttword{expression} cluster (see the \chap{toolexpressions}).

\item \seclass{RUN\_CLASS} is a live version of a given
      \seclass{BASE\_CLASS}.  One base class may have many run classes,
      or none.  See the \chap{runclass}.

\item \seclass{RUN\_FEATURE} is the live version of a given base
      feature.  One base feature may have many run features, or none.  In
      the same way that a base class contains base features, a run class
      contains run features.  See the \chap{runclass}.

\end{itemize}

\section{\label{tooltypes}Types}

This cluster contains all the classes conforming to
\seclass{E\_TYPE}.  They represent the type of en entity, be it an
attribute, an expression, a feature call, and so on.

To be more precise, the following entities have a type:

\begin{itemize}

 \item the target of a \seclass{RUN\_FEATURE};

 \item the result of a \seclass{RUN\_FEATURE} (this one may be {\tt
       Void} for a procedure).  Don't forget that an attribute is also a
       \seclass{RUN\_FEATURE};

 \item the type of any expression (\seclass{EXPRESSION}) is accessible
       via \ttword{result\_type}.  Current and Result are but special
       expressions, as also are feature calls.  See the expressions
       chapter (\chap{toolexpressions}) for details.

\end{itemize}

Some types are special: the {\em anchored} types.  This is the expression
of the Eiffel {\tt like} keyword.  The real type can be obtained by
\ttword{run\_type}.

\section{\label{toolexpressions}Expressions}

An expression is ``something that has a result''.  It may be a very
simple expression like $a+b$ or $io.last\_character$, or something very
complex like $my\_array.item(my\_indeces.get(i))$.

Each expression is described as one tree, the whole expression being the
``root'' or the tree and grafted on the abstract syntactic tree.

The expression is split in sub-expressions, each one having a result.

Some examples: see the \fig{exampleexpressions}.  They show that each
feature call (the most frequent expression kind) is split in its target
and its arguments.  The target and each argument are expressions of their
own.

\begin{figure}[hpbt]
\caption{\label{exampleexpressions}Some expressions and their syntactic tree}
\begin{tabular}{rm{5.8cm}}

~&~\\

$a+b$ & \epslide{2.5cm}{expression1.eps} \\

~&~\\

$io.last\_character$ & \epslide{1.5cm}{expression2.eps} \\
~&~\\

$my\_array.item(my\_indeces.get(i))$ &  \epslide{5cm}{expression3.eps} \\

\end{tabular}
\end{figure}

In the class \seclass{CALL}, you may find those elements:

\begin{itemize}

 \item the target expression is {\tt target};

 \item the arguments are gathered in {\tt arguments};

 \item the result type is {\tt result\_type}.

\end{itemize}

The most important expressions are:

\begin{itemize}

 \item \seclass{CALL} is a feature call.  The main element of this class
       is the feature it calls: the {\tt run\_feature}; see the
       \chap{toolrunfeatures};

 \item \seclass{CALL\_INFIX1} is the ``father'' of all known types of
       user-defineable infix features (``\textless'', ``\textgreater'',
       .~.~.);

 \item \seclass{CALL\_INFIX2} factorizes the common behavior of the
       ``='' and ``/='' operators;

 \item \seclass{CALL\_PREFIX} represents all the prefix features;

 \item \seclass{ABSTRACT\_CURRENT} represents the {\tt Current}
       pseudo-variable.  It is derived in \seclass{IMPLICIT\_CURRENT} and
       \seclass{WRITTEN\_CURRENT}.

 \item \seclass{ABSTRACT\_RESULT} represents the {\tt Result}
       pseudo-variable.  It is associated to the \seclass{RUN\_FEATURE}
       it is the result of.  It is derived in two classes:
       \seclass{ORDINARY\_RESULT} and \seclass{ONCE\_RESULT}.

 \item various constants (boolean, integer, bits, characters, manifest
       strings and arrays);

 \item agents and tuples (see the \chap{tupleagent});

 \item entities: local variables and arguments (respectively,
       \seclass{LOCAL\_NAME} and \seclass{LOCAL\_ARGUMENTS}).

 \item create expressions;

 \item precursor expressions.

\end{itemize}

\section{\label{toolinstructions}Instructions}

Instructions are the core of the language.  They manipulate expressions;
without instructions, an expressions, even very complex, is totally
useless!

In Eiffel, an instruction is an active component.  It {\em does
something} with an expression: store it, write it on the screen, give it
to some other object, or whatever you can imagine.

The way an instruction uses one or more expressions and instructions
depends on the considered instruction.

\section{\label{toolfeatures}Features}

Features are the AST representation of Eiffel features.  They are:

\begin{itemize}

 \item attributes (\seclass{ATTRIBUTE}), be they constant
       (\seclass{CST\_ATT} and its children: {\tt CST\_ATT\_BOOLEAN},
       {\tt CST\_ATT\_INTEGER} and so on) or writeable (the class
       \seclass{WRITABLE\_ATTRIBUTE});

 \item ``normal'' (effective) routines (\seclass{E\_FUNCTION},
       \seclass{E\_PROCEDURE});

 \item deferred routines;

 \item once routines;

 \item external routines.

\end{itemize}

This cluster also contains the external routines management classes: the
\seclass{NATIVE} class and its heirs (see the \chap{nativefeatures}).

\section{\label{toolrunfeatures}Runnable features}

The runnable features are a tailored version of the features (see the
\chap{toolfeatures}) for a known {\tt current\_type} (i.e.  a live type).

Each time a class is made alive (see the \chap{runclass}) and a feature
from its base class is used, a special version of the feature is
created, using the run class as its target type.

The \seclass{RUN\_FEATURE} class always defines a {\tt
result\_type}.  This one is {\tt Void} for a procedure, non-{\tt Void}
for a function.

The run feature classes are:

\newcounter{runfeaturecounter}
\setcounter{runfeaturecounter}{1}
\begin{list}{\seclass{RUN\_FEATURE\_\arabic{runfeaturecounter}}:%
\stepcounter{runfeaturecounter}}{}

 \item the constant attributes

 \item the writeable attributes

 \item the procedures (not returning a result; their calling may be used
       as instructions)

 \item the functions (returning a result; their calling may be used as
       expressions)

 \item the once procedures

 \item the once functions

 \item the external procedures

 \item the external functions

 \item the deferred procedures and functions

 \item the precursor procedures

 \item the precursor functions

\end{list}

\section{\label{toolmisc}Miscellaneous}

That cluster contains all the helper classes that do not fit in any
other cluster, or that are shared by classes of more than one cluster.

\chapter{Some compiler subsystems}

\section{Introduction}

After having wandered through the clusters, we will detail some of the
ways the compiler works.  How do all those classes fit together? That's
the question we will try to answer.

\section{\label{ClassSmartEiffel}The compilation process}

Here we describe the compilation process, using both a plane and a
microscope to look at it.

Let's start with a very basic class: \seclass{SMART\_EIFFEL}.  During the
compilation, this is the class that conducts the score and make the
music alive.

Both compilers (\command{compile\_to\_c} and \command{compile\_to\_jvm})
are but {\em facades} of that class, which provide for the
\command{compile\_to\_c} and \command{compile\_to\_jvm} services.  Each
of those two features describe how the compilation process should run.

Both features start by calling \ttword{get\_started} (see the
\chap{getstarted}), which creates an object representation of the
program to compile; then the code is produced by calling either
\command{compile\_to\_c} or \command{compile\_to\_jvm} on each run
class.

Other tools than the compilers use some subparts of
\ttword{get\_started}, and produce many kinds of information; in
particular, \command{short} uses a representation of a class (provided
by {\tt base\_class}) with its parents and its clients, to produce an
appropriate output.

\subsection{\label{getstarted}Getting started}

Despite its name, this feature is the real core of \SE; that is, its
front-end.  It gathers source code and creates an internal representation of
the whole program.

The variable \ttword{status} describes how far the process has
gone.  Some stripped-down code (i.e., without error handling) shows the
most important instructions for each phase.

\newcounter{smarteiffelstatuscounter}
\setcounter{smarteiffelstatuscounter}{0}
\begin{list}{\makebox[0.5cm][r]{\bf \arabic{smarteiffelstatuscounter}:}%
\stepcounter{smarteiffelstatuscounter}}{}

 \item The live code gathering has just started---i.e.  we started
       reading classes, starting from the one given as ``root class'';
       the instructions are deciphered to know which other classes are
       used.  Those classes are read in turn, and the processes goes on.

\fbox{
\begin{minipage}[t]{12cm}
{\em In {\ttfamily get\_started}:}

\begin{flushleft}
\begin{alltt}
create root\_name.unknown\_position(root\_class\_name)\\
root := base\_class(root\_name)\\
root\_type := run\_class\_for(root.name).current\_type\\
root\_procedure := root\_proc.to\_run\_feature(root\_type, sfn)
\end{alltt}
\end{flushleft}
\end{minipage}
}

 \item The ``fall-down'' process.  Starting from the root procedure (and
       Cecil procedures), all the call sites are visited again to take
       into account feature (re)definitions, renaming and so on.  New
       classes and new code may be gathered in this phase.

\fbox{
\begin{minipage}[t]{12cm}
{\em In {\ttfamily get\_started}:}

\begin{flushleft}
\begin{alltt}
status := 1\\
from\\
~~~falling\_down\\
until\\
~~~magic = magic\_count\\
loop\\
~~~magic := magic\_count\\
~~~falling\_down\\
end
\end{alltt}
\end{flushleft}
\end{minipage}
}

 \item The ``after-fall-down'' process.  Once again we step through the
       whole code to compute the actual set of live types.  At the end of
       this step, every live type is taken into account.  Said otherwise,
       after this time no source code should be loaded.  Also in this
       phase, automatic type converters (``cast'' functions) are
       created.

\fbox{
\begin{minipage}[t]{12cm}
{\em In {\ttfamily get\_started}:}

\begin{flushleft}
\begin{alltt}
status := 2\\
from\\
~~~afd\_check\\
until\\
~~~magic = magic\_count\\
loop\\
~~~magic := magic\_count\\
~~~afd\_check\\
end\\
assignment\_handler.finish\_falling\_down\\
from\\
until\\
~~~magic = magic\_count\\
loop\\
~~~magic := magic\_count\\
~~~falling\_down\\
~~~afd\_check\\
end
\end{alltt}
\end{flushleft}
\end{minipage}
}

 \item The whole system analysis is running.  The whole assignment graph
       is wandered across, and the ``dangerous'' assignments are shown.

\fbox{
\begin{minipage}[t]{12cm}
{\em In {\ttfamily get\_started}:}

\begin{flushleft}
\begin{alltt}
safety\_check
\end{alltt}
\end{flushleft}
\end{minipage}
}

\fbox{
\begin{minipage}[t]{12cm}
{\em In {\ttfamily safety\_check}:}

\begin{flushleft}
\begin{alltt}
from\\
~~~update\_run\_class\_map\\
~~~i := run\_class\_map.upper\\
until\\
~~~i < run\_class\_map.lower\\
loop\\
~~~rc := run\_class\_map.item(i)\\
~~~if rc.at\_run\_time then\\
~~~~~~rc.safety\_check\\
~~~end\\
~~~i := i - 1\\
end
\end{alltt}
\end{flushleft}
\end{minipage}
}

       {\tt rc.safety\_check} iterates over every run feature body, upon
       which it calls its {\tt safety\_check} feature.  That function is
       recursively called on every live instruction, and every live
       expression.  In particular, it is called on every
       \seclass{CALL\_PROC\_CALL}.

 \item The whole system analysis was performed, but it appears that the
       system is {\em not} type safe.  It means e.g.  that some dangerous
       covariant redefinitions are used; in that case, the user got
       warnings.

\fbox{
\begin{minipage}[t]{12cm}
{\em In {\ttfamily covariance\_check} (called by {\ttfamily
CALL\_PROC\_CALL.safety\_check}):}

\begin{flushleft}
\begin{alltt}
from\\
~~~r := 1\\
~~~up\_args := up\_rf.arguments\\
until\\
~~~r > run\_time\_set.count\\
loop\\
~~~dyn\_rf := run\_time\_set.item(r).dynamic(up\_rf)\\
~~~from\\
~~~~~~a := 1\\
~~~~~~dyn\_args := dyn\_rf.arguments\\
~~~until\\
~~~~~~a > up\_args.count\\
~~~loop\\
~~~~~~t1 := up\_args.type(a)\\
~~~~~~t2 := dyn\_args.type(a)\\
~~~~~~if t1.run\_time\_mark /= t2.run\_time\_mark then\\
~~~~~~~~~status := 4\\
~~~~~~end\\
~~~~~~a := a + 1\\
~~~end\\
~~~r := r + 1\\
end
\end{alltt}
\end{flushleft}
\end{minipage}
}

 \item The whole system analysis was not performed.  The analysis is only
       run in two cases: either the \option{boost} flag was used to
       produce an optimized executable, either the
       \option{safety\_check} flag was used to force the system
       analysis.

\fbox{
\begin{minipage}[t]{12cm}
{\em In {\ttfamily safety\_check} (code rewritten for legibility):}

\begin{flushleft}
\begin{alltt}
if not ace.safety\_check then\\
~~~status := 5\\
end
\end{alltt}
\end{flushleft}
\end{minipage}
}

 \item The whole system analysis was performed, and it computed that the
       system is type-safe.

\end{list}

During the three first phases (0, 1 and 2: while new code is still being
gathered), some code is made alive (see the \chap{runclass}).  Then, if
needed, the global analysis is performed: the whole assignment graph is
looked at, and if ``dangerous'' assignments are encountered, the
compiler emits warnings.

At last, the code production may begin: see \chap{compiletoc} and
\chap{compiletojvm}.  The fact that the system is not type-safe does not
stop the code generation.

\subsection{\label{theparser}The \SE parser}

This parser is a singleton shared {\em via} \seclass{GLOBALS}{\tt
.eiffel\_parser} (see the \chap{utilities}), of type
\seclass{EIFFEL\_PARSER}, .  Only one source may be parsed at one time.

\subsubsection{The parser interface}

The parser interface, \seclass{EIFFEL\_PARSER}, should never be directly
called by any class but \seclass{SMART\_EIFFEL} (see the
\chap{ClassSmartEiffel}).

\subsubsection{The parser algorithm}

Contrarily to ``standard'' LL parsers, the \SE parser interleaves
lexical and syntactical parsing.

Its entry point is \ttword{analyse\_class}.  This function takes a
\seclass{CLASS\_NAME} and returns a \seclass{BASE\_CLASS}, which
contains the whole syntactic tree of the read source code.

\subsection{\label{runclass}Making the code alive}

What does it mean, {\em making code alive?}

Its the opposite method of the one used by traditional compilers: {\em
finding dead code.} It is far simpler, and surely more robust.

A program is typically a tree: start from the root procedure, it is made
of instructions; each instruction is a series of other instructions and
expressions, and so on.  To find the live code is simply to go through
the graph.

The difficulty resides in Eiffel, in the fact that one class may produce
several times some live code.  Here is why:

\begin{itemize}

 \item \SE is built in the way that each {\em type} ({\em not} each
       class) has its own set of well-tailored features.  It allows for
       great customisation of the features, and therefore greater speed
       than functions always checking ``which is the type of the
       target?'' In other words, when a feature is made alive, it knows
       the {\em exact type of the target.} This will show off in the way
       the code (either C or bytecode) is produced.

       \SE associates one type to each expression, and one {\em run
       class} per type.

 \item The class is parametric (e.g., \libclass{ARRAY}): each time the
       class is used with different parameters (e.g., {\tt
       ARRAY[INTEGER]}), it is potentially a new type.

 \item Inheritance, without redefinition, produces many times the same
       feature.  Redefinition is but a particular case: instead of
       duplicating the same ``old'' (inherited) code for the new target,
       the ``new'' code is used.

       Note that multiple inheritance is not a problem, since one unique
       version of a feature is ever produced for a given
       type.  Redefinition, renaming, feature joints, and so on are
       handled by the compiler.

 \item The special case of \libclass{BIT\_N} is also handled that way.

 \item At last, the tuples and agents also produce tailored code.  See
       the \chap{tupleagent}.

\end{itemize}

The way of making code alive is the use of the \ttword{to\_runnable}
function.  This feature is available on every \seclass{INSTRUCTION} and
every \seclass{EXPRESSION}.

A \seclass{RUN\_CLASS} is produced by \seclass{SMART\_EIFFEL}{\tt
.run\_class}, which uses an \seclass{E\_TYPE} to know which run class to
produce.

A \seclass{RUN\_FEATURE} is produced by:

\begin{itemize}

 \item \seclass{RUN\_CLASS}{\tt .get\_rf\_with} uses the feature name in
       the class where the feature was (re)defined.  Possible renames are
       handled and the given run feature will have the {\em good}
       (i.e.  final) name.

 \item \seclass{RUN\_CLASS}{\tt .get\_feature\_with} uses the final name
       of the feature to know which one to look for;

 \item \seclass{RUN\_CLASS}{\tt .get\_feature} uses the final name of
       the feature to know which one to look for;

 \item \seclass{RUN\_CLASS}{\tt .dynamic\_dispatch} finds a feature
       which is redefined by the one given in argument.  Of course, the
       {\tt current\_type} of the argument must be a subtype of the run
       class;

 \item \seclass{RUN\_CLASS}{\tt .at} uses the name of the feature to
       know which one to look for.  May be {\tt Void} if the feature was
       not already fetched by one of the other functions above;

 \item some special functions of \seclass{RUN\_CLASS} look for special
       methods of a class: \ttword{dispose}, \ttword{default\_create},
       \ttword{copy} .~.~.

\end{itemize}

\subsection{The tree optimization}

\textit{Dominique, that's up to you to fill the gap.}

\subsection{\label{compiletoc}C code production}

% Bien parler de la pile de C_PRETTY_PRINTER

\subsection{\label{compiletojvm}Java bytecode production}

\section{\label{tupleagent}Tuples and agents}

\section{\label{nativefeatures}Native features}

\section{Error management}

\section{\label{intgc}The garbage collector}

\section{\label{externalse}Generated code pre-built pieces}

\subsection{Introduction}

As you look into the libraries, you can see that some stuff is defined with an
\ttword{external~"SmartEiffel"} clause.  This code is the ``basis'' upon which
everything else can be built.  Such code includes for example operators on
integers and floats, array allocations, input/output interactions, and so on.

There are two classes of basic codes: those hard-written in the compiler, and
the ``basic'' plugins.

Both kinds are available for both back-ends, that is, the C and the Java
bytecode generators.

\subsection{Hard-written basic codes}

Those are the ``most basic'' codes of the basic codes \mbox{{\tt ;-)}} You can
find their declaration in \seclass{NATIVE\_SMART\_EIFFEL}.  This class defines
the following external features:

\begin{itemize}

\item \libclass{GENERAL}{\tt .copy}

\item \libclass{GENERAL}{\tt .standard\_copy}

\item \libclass{GENERAL}{\tt .twin}

\item \libclass{GENERAL}{\tt .standard\_twin}

\item \libclass{GENERAL}{\tt .is\_equal}

\item \libclass{GENERAL}{\tt .standard\_is\_equal}

\item \libclass{GENERAL}{\tt .deep\_twin}

\item \libclass{GENERAL}{\tt .is\_deep\_equal}

\item \libclass{GENERAL}{\tt .generating\_type}

\item \libclass{GENERAL}{\tt .generator}

\item \libclass{GENERAL}{\tt .to\_pointer}

\item \libclass{GENERAL}{\tt .object\_size}

\item \libclass{GENERAL}{\tt .is\_basic\_expanded\_type}

\item \libclass{GENERAL}{\tt .is\_expanded\_type}

\item \libclass{POINTER}{\tt .is\_not\_null}

\item \libclass{MEMORY}{\tt .pointer\_size}

\item \libclass{GENERAL}{\tt .se\_argc}

\item \libclass{GENERAL}{\tt .se\_argv}

\item \libclass{GENERAL}{\tt .print\_run\_time\_stack}

\item \libclass{GENERAL}{\tt .print\_all\_run\_time\_stacks}

\item \libclass{GENERAL}{\tt .die\_with\_code}

\item \libclass{NATIVE\_ARRRAY} external features

\item \libclass{BIT\_N} external features

\item \libclass{INTEGER} (and other integer classes) external features

\item \libclass{REAL} (and other float classes) external features

\item \libclass{BOOLEAN} external features

\item \libclass{CHARACTER} external features

\item \libclass{EXCEPTIONS}{\tt .raise\_exception}

\item \libclass{EXCEPTIONS}{\tt .exception}

\item \libclass{MEMORY}{\tt .full\_collect}

\item \libclass{MEMORY}{\tt .collection\_on}

\item \libclass{MEMORY}{\tt .collection\_off}

\item \libclass{MEMORY}{\tt .collecting}

\item \libclass{MEMORY}{\tt .collector\_counter}

\item \libclass{GENERAL}{\tt .c\_inline\_h}

\item \libclass{GENERAL}{\tt .c\_inline\_c}

\item \libclass{PLATFORM} constants

\end{itemize}

Aside from defining those features, \seclass{NATIVE\_SMART\_EIFFEL} also
handles the protocol for the ``basic'' plugins (see the next chapter).

\subsection{\label{basic}The ``basic'' compiler plugins}

\subsubsection{Introduction}

Many \SE libraries\footnote{input/output, sequencer, vision, time} use
low-level operations that must interact with the underlying system.  Those
operations are ``imported'' in Eiffel via the {\em external} mechanism.  A
simple standard interface was designed to allow extensions: that is the
``basic'' plugin interface.

This interface is simple indeed: whenever a feature is named {\tt basic\_foo}
and is an \ttword{external~"SmartEiffel"}, a (C or Java) function with the same
name is sought and inserted.  The exact mechanism depends on whether the
back-end is the C compiler or the Java compiler.

\subsubsection{C ``basic'' plugins}

The plugins must be placed in the {\tt \$SmartEiffel/sys/runtime} directory.

If a feature is named {\tt basic\_group\_feature\_name}, the C function is
sought in the {\tt basic\_group.c} file; in that case, both the C header file
{\tt basic\_group.h} and the C definition file {\tt basic\_group.c} files are
inserted.

Those files must contain, the signature (in the header file), and the
definition of the {\tt basic\_group\_feature\_name}.  The signature of the
function must match the Eiffel one; {\em the target is never passed.}

\subsubsection{Java ``basic'' plugins}

All the features must be defined in the \ttword{SmartEiffelRuntime} class.  In
that case, a call to this feature is generated.

The signature of the feature must match the Eiffel one; {\em the target is
never passed.} The functions must be declared {\tt public static}.

\section{\label{intscoop}\SCOOP}

\section{\label{intsedb}The SmartEiffel debugger}

\chapter{Helping with the development}

\section{The bugs base: bugzilla}

\section{Giving money}

\section{Sending patches}

\subsection{How is the code written}

Some hints to write good patches:

\begin{itemize}

 \item memory management: limit the memory used; remember that the
       compiler is compiled with no GC.  Look at functions like
       \ttword{to\_runnable}: they show the appropriate pattern for good
       object reuse.

 \item C code generation: limit parentheses.  It is the {\em caller}
       which adds parentheses if needed, not the nested expression.

 \item don't expect acknowledgement mails.  Consider you are thanked for
       sending your patch.  It will be taken into account .~.~.  Some
       day.  Don't forget the team is very limited and the project is
       huge.

\end{itemize}
