class TEST_TEXT_FILE_READ_WRITE

creation make
   
feature 
   
   path: STRING is "test_std_file_read_write.dat"
   
   tfr: TEXT_FILE_READ
   
   tfw: TEXT_FILE_WRITE
   
   tfrw: TEXT_FILE_READ_WRITE
   
   
   before: STRING is "A? A%N? B%NCC? CC%NDDDDD%NEEEEEE%N"
   after : STRING is "A?1A%N?2B%NCC?3CC%NDDDDD%NEEEEEE%N"
   
   make is
      local
			i: INTEGER
			c: CHARACTER
      do
			-- Writing the model :
			create tfw.connect_to(path)
			is_true(tfw.is_connected)
			tfw.put_string(before)
			tfw.disconnect
			is_true(not tfw.is_connected)
			is_true((create {FILE_TOOLS}).is_readable(path))
			
			-- Reading :
			create tfr.connect_to(path)
			is_true(tfr.is_connected)
			from  
				i := 1
			until
				i > before.count
			loop
				is_true(not tfr.end_of_input)
				tfr.read_character
				is_true(tfr.last_character = before.item(i))
				i := i + 1
			end
			is_true(not tfr.end_of_input)
			tfr.disconnect
			is_true(not tfr.is_connected)
			
			-- Trying TEXT_FILE_READ_WRITE :
			create tfrw.connect_to(path)
			is_true(tfrw.is_connected)
			from  
				i := 1
			until
				i > before.count
			loop
				is_true(not tfrw.end_of_input)
				tfrw.read_character
				is_true(tfrw.last_character = before.item(i))
				i := i + 1
			end
			tfrw.disconnect
			is_true(not tfrw.is_connected)
			
			create tfrw.connect_to(path)
			is_true(tfrw.is_connected)
			from  
				i := 1
			until
				i > before.count
			loop
				is_true(not tfrw.end_of_input)
				tfrw.read_character
				is_true(tfrw.last_character = before.item(i))
				if before.item(i) = '?' then
					tfrw.put_character(after.item(i+1))
					i := i + 1
				end
				i := i + 1
			end
			tfrw.disconnect
			is_true(not tfrw.is_connected)
			
			create tfr.connect_to(path)
			is_true(tfr.is_connected)
			from  
				i := 1
			until
				i > after.count
			loop
				is_true(not tfr.end_of_input)
				tfr.read_character
				is_true(tfr.last_character = after.item(i))
				i := i + 1
			end
			tfr.disconnect
			is_true(not tfr.is_connected)
			
			create tfr.connect_to(path)
			is_true(tfr.is_connected)
			tfr.read_line
			is_true(tfr.last_string.is_equal("A?1A"))
			tfr.disconnect
			is_true(not tfr.is_connected)
			
			create tfrw.connect_to(path)
			tfrw.read_character
			c := tfrw.last_character
			tfrw.unread_character
			tfrw.read_character
			is_true(c = tfrw.last_character)
			tfrw.disconnect
			
			; (create {FILE_TOOLS}).delete(path)
			is_true(not (create {FILE_TOOLS}).is_readable(path))
      end
   
   is_true(b: BOOLEAN) is
      do
			cpt := cpt + 1
			if not b then
				std_output.put_string("TEST_TEXT_FILE_READ_WRITE: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
			else
				-- std_output.put_string("Yes%N")
			end
      end
   
   cpt: INTEGER
   
end
