
class TEST_EXECUTE

creation
   make

feature {ANY}

   sys : SYSTEM is
      once
         create Result
      end
   
   make is
      do
         test("true", exit_success_code)
         test("false", exit_failure_code)
      end

   test(cmd : STRING; expected : INTEGER) is
      do
         if sys.execute_command(cmd) /= expected then
            std_error.put_string("BAD EXIT STATUS FOR " + cmd + "%N")
         end
      end

end
