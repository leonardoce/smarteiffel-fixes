class AUX_ADDRESS_OF3
   
feature 
   
   char: CHARACTER;
   
   command is
      do
	 ext_command($char);
      end;
   
   ext_command(arg1: POINTER) is
      do
	 foo(arg1);
      end;
   
   foo(arg1: POINTER) is
      do
	 if arg1.is_not_null then
	 end;
      end;

end
   	 
