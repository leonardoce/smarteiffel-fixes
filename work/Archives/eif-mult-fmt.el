;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Cut Here ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;!emacs
;;
;; FILE:         eif-mult-fmt.el
;; SUMMARY:      Support for multi-line assignment, argument list and string
;;                 formatting in Eiffel.
;; USAGE:        GNU Emacs Lisp Library
;;
;; AUTHOR:       Bob Weiner
;; ORG:          Motorola Inc.
;; E-MAIL:       USENET:  weiner@novavax
;;
;; ORIG-DATE:     2-Feb-90
;; LAST-MOD:      6-Feb-90 at 15:11:03 by Bob Weiner
;;
;; Copyright (C) 1985 Free Software Foundation, Inc.
;; Copyright (C) 1990 Bob Weiner, Motorola Inc.
;; Available for use and distribution under the same terms as GNU Emacs.
;;
;; Loosely based upon 'lisp-mode.el'.
;;
;; This file is not yet part of GNU Emacs.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun eiffel-indent-assign ()
  "Return proper indentation for an assignment continuation line or nil."
  (save-excursion
    (let ((bol (save-excursion (beginning-of-line) (point)))
	  (start)
	  (in-col))
      (while
	  (and (re-search-backward ":=\\|;" nil t)
	       (setq start (match-beginning 0))
	       (re-search-backward "--" (save-excursion (beginning-of-line)
							(point))
				   t)))
      (and start
	   (goto-char start)
	   (looking-at ":=[ \t]*")
	   (goto-char (match-end 0))
	   (setq e-last-indent-type "ASSIGNMENT CONTINUED"
		 in-col (current-column))
	   ;; If any line between assignment start and line to be indented is
	   ;; indented less than the computed indent for current line
	   ;; 'in-col' then can't be an assignment continuation.  This solves
	   ;; most problems were assignment statements are not terminated by
	   ;; semicolons.
	   (while (and (forward-line 1)
		       (< (point) bol)
		       (progn (skip-chars-forward " \t")
			      (if (< (current-column) in-col)
				  (setq in-col nil)
				t)))))
      in-col)))

(defun eiffel-indent-multi-line (&optional parse-start)
  "Return integer giving appropriate indentation for current Eiffel code
line between parentheses or double quotes, otherwise -1.  Optional
PARSE-START is buffer position at which to begin parsing, default is to begin
at the feature enclosing or preceding point."
  (let ((eif-opoint (point))
	(indent-point (progn (beginning-of-line) (point)))
	(eif-ind-val -1)
	(eif-in-str nil)
	(eif-paren-depth 0)
	(retry t)
	state
	;; setting this to a number inhibits calling hook
	last-sexp containing-sexp)
    (if parse-start
	(goto-char parse-start)
      (eiffel-beginning-of-feature))
    ;; Find outermost containing sexp
    (while (< (point) indent-point)
      (setq state (parse-partial-sexp (point) indent-point 0)))
    ;; Find innermost containing sexp
    (while (and retry
		state
		(> (setq eif-paren-depth (elt state 0)) 0))
      (setq retry nil)
      (setq last-sexp (elt state 2))
      (setq containing-sexp (elt state 1))
      ;; Position following last unclosed open.
      (goto-char (1+ containing-sexp))
      ;; Is there a complete sexp since then?
      (if (and last-sexp (> last-sexp (point)))
	  ;; Yes, but is there a containing sexp after that?
	  (let ((peek (parse-partial-sexp last-sexp indent-point 0)))
	    (if (setq retry (car (cdr peek))) (setq state peek)))))
    (if retry
	nil
      ;; Innermost containing sexp found
      (goto-char (1+ containing-sexp))
      (if (not last-sexp)
	  ;; indent-point immediately follows open paren.
	  nil
	;; Find the start of first element of containing sexp.
	(parse-partial-sexp (point) last-sexp 0 t)
	(cond ((looking-at "\\s(")
	       ;; First element of containing sexp is a list.
	       ;; Indent under that list.
	       )
	      ((> (save-excursion (forward-line 1) (point))
		  last-sexp)
	       ;; This is the first line to start within the containing sexp.
	       (backward-prefix-chars))
	      (t
		;; Indent beneath first sexp on same line as last-sexp.
		;; Again, it's almost certainly a routine call.
		(goto-char last-sexp)
		(beginning-of-line)
		(parse-partial-sexp (point) last-sexp 0 t)
		(backward-prefix-chars))))
      (setq eif-ind-val (current-column))
      )
    ;; Point is at the point to indent under unless we are inside a string.
    (setq eif-in-str (elt state 3))
    (goto-char eif-opoint)
    (if (not eif-in-str)
	nil
      ;; Inside a string, indent 1 past string start
      (setq eif-paren-depth 1) ;; To account for being inside string
      (save-excursion
	(if (re-search-backward "\"" nil t)
	    (setq eif-ind-val (1+ (current-column)))
	  (goto-char indent-point)
	  (if (looking-at "^[ \t]*[^ \t\n]")
	      (e-move-to-prev-non-blank))
	  (skip-chars-forward " \t")
	  (setq eif-ind-val (current-column)))))
    (if (> eif-paren-depth 0) eif-ind-val -1)
    ))

(defun eiffel-delim-string ()
  "Places '\\' delimiters around multiple line string that point is within.
If point is not within a string, produces an error1."
  (interactive)
  (let (start end)
    (save-excursion
      (if (and (setq start (if (re-search-backward "\"" nil t) (point)))
	       (setq end (if (re-search-forward "\"" nil t 2) (point))))
	  (save-restriction
	    (narrow-to-region start end)
	    (goto-char (point-min))
	    (while 
		(progn (if (looking-at "\"?[ \t]*\"?$")
			   nil
			 (if (/= ?\" (following-char))
			     (progn (back-to-indentation)
				    (if (/= ?\\ (following-char))
				  (insert "\\"))))
			 (end-of-line)
			 (and (/= ?\" (preceding-char))
			      (/= ?\\ (preceding-char))
			      (insert "\\"))
			 )
		       (forward-line 1)
		       (not (eobp))
		       )))
	(error "Point is not within a double quoted string.")))))
	

(provide 'eif-mult-fmt)
