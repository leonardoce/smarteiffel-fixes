/*
-- This file is  free  software, which  comes  along  with  SmartEiffel. This
-- software  is  distributed  in the hope that it will be useful, but WITHOUT
-- ANY  WARRANTY;  without  even  the  implied warranty of MERCHANTABILITY or
-- FITNESS  FOR A PARTICULAR PURPOSE. You can modify it as you want, provided
-- this header is kept unaltered, and a notification of the changes is added.
-- You  are  allowed  to  redistribute  it and sell it, alone or as a part of
-- another product.
--       Copyright (C) 1994-2002 LORIA - INRIA - U.H.P. Nancy 1 - FRANCE
--          Dominique COLNET and Suzanne COLLIN - SmartEiffel@loria.fr
--                       http://SmartEiffel.loria.fr
--
*/
/*
  This file (SmartEiffel/sys/runtime/trace.h) is automatically included when
  `run_control.no_check' is true (ie. all modes except -boost).
  This file comes after no_check.[hc] to implements the -trace flag.
*/
#ifdef SE_WEDIT
  se_position se_trace(se_position p);
#elif SE_TRACE
#  define SE_DB_BUFMAX 512
  typedef struct _se_breakpoint se_breakpoint;
  struct _se_breakpoint {
    int name_flag; char name[SE_DB_BUFMAX];
    int file_flag; char file[SE_DB_BUFMAX];
    int line_flag; int line_minimum; int line_maximum;
    int stack_flag; int stack_limit; int stack_automatic;
    se_breakpoint* next; /* The next one or or NULL. */
  };
  void se_trace(se_dump_stack*ds,se_position p);
  void se_db_signal_handler(int sig);
  void se_db_back(se_dump_stack*ds,se_position p);
  void sedb_breakpoint(se_dump_stack*ds);
#endif
