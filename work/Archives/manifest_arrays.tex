\documentclass[a4paper,10pt]{article}

\usepackage[english]{babel}
\usepackage{alltt}

\hyphenation{Smart-Eiffel}

\newenvironment{slide}[1]%
{\begin{figure}[pbht]\caption{#1}\begin{center}\begin{tabular}{|p{11cm}|}\hline\begin{minipage}[t]{10.5cm}\begin{flushleft}\begin{alltt}\small}%
{\end{alltt}\end{flushleft}\end{minipage}\vspace{1ex}\\\hline\end{tabular}\end{center}\end{figure}}

\title{Tidying up the Manifest Arrays construct}

\author{The SmartEiffel team}

\listfiles

\begin{document}

\maketitle

\begin{abstract}
  The current {\tt{}Manifest\_Array} construct, {\tt{}<<...>>} has one defect:
  the compiler has to figure out its type, by selecting a common parent of all
  its elements. Sometimes, there is no unique solution (except {\sc{}any});
  some other times, the result is not what the user expects or wants. (Some
  compilers create context-dependent objects, we don't want that.)
 
  We want the user to decide by themselves what type of objects they want. And
  since we give that possibility, we will extend the construct to be able to
  give the user the choice of any type of {\em container}.

  Requirements:

  \begin{enumerate}
  \item the old construct {\tt{}<<...>>} should still be valid, at least for
    some time;

  \item the new construct must be able to precise:

    \begin{enumerate}

    \item the type of the container,

    \item the type of the elements;

    \end{enumerate}
   
  \item creation genericity, simplicity and performance.

  \end{enumerate}
 
  We will split this presentation in three parts: after having shown the
  problems of the current {\tt{}Manifest\_Array} construct, we will present a
  new syntax and semantics; we will finish by exposing some hints on how we
  intend to implement that construct.

\end{abstract}

\listoffigures


\section{\label{problems}Problems of the old construct}

Let's begin with an example, figure~\ref{oldclasses}.

\begin{slide}{\label{oldclasses}Some classes which show the problem}
{\bf{}deferred class} {\sc{}a}
{\bf{}end}

{\bf{}deferred class} {\sc{}b}
{\bf{}end}

{\bf{}class} {\sc{}c}
   {\bf{}inherit} {\sc{}a} {\sc{}b}
{\bf{}end}

{\bf{}class} {\sc{}d}
   {\bf{}inherit} {\sc{}a} {\sc{}b}
{\bf{}end}
\end{slide}

An array containing only {\sc{}c}'s is an {\sc{}array[c]}; ditto for {\sc{}d}.
But what of an array containing {\sc{}c}'s and {\sc{}d}'s
(figure~\ref{CandD})?  Should it be an {\sc{}array[a]}, an {\sc{}array[b]} or
an {\sc{}array}{\tt{}[}{\sc{}any}{\tt{}]}?  Currently, with no apparent reason
SmartEiffel creates an {\sc{}array}{\tt{}[}{\sc{}b}{\tt{}]}. Maybe the user
wanted one of the others .~.~.

The current way of achieving that is the one figure~\ref{oldsol}. We can see
that it is quite cumbersome (a lot of intermediate steps to make before being
able to create a ``manifest'' array, well, not that manifest anymore .~.~.)

\begin{slide}{\label{CandD}An array with {\sc{}c}'s and {\sc{}d}'s}
{\bf{}local}
   c1, c2: {\sc{}c}
   d1, d2: {\sc{}d}
   a: {\sc{}array}[{\sc{}any}]
{\bf{}do}
   a := << c1, c2, d1, d2 >>
   io.put\_string(a.generating\_type)
{\bf{}end}
\end{slide}

\begin{slide}{\label{oldsol}A possible solution, more like a workaround}
{\bf{}local}
   c1, c2: {\sc{}c}
   d1, d2: {\sc{}d}
   a1, a2, a3, a4: {\sc{}a}
   a: {\sc{}array}[{\sc{}any}]
{\bf{}do}
   a1 := c1
   a2 := c2
   a3 := d1
   a4 := d2
   a := << a1, a2, a3, a4 >>
   io.put\_string(a.generating\_type)
{\bf{}end}
\end{slide}

\section{\label{syntax}Syntax and semantics}

We want to propose a new construct that lets the user decide the type of the
elements; even further, we want them to decide the type of the container, be
it an array, a list or a set, or even a third-party library container.

The requirements for such a new construct are:

  \begin{enumerate}
  \item The old construct {\tt{}<<...>>} should still be valid. Of course, we
    must not break existing code; we just want to add a far more flexible
    construct.

  \item The new construct must be able to precise:

    \begin{enumerate}
      
    \item the type of the container, i.e. the type of the object that will
      contain the elements. The old construct only created {\sc{}array}'s, but
      why not creating {\sc{}list}'s or {\sc{}set}'s,
      
    \item the type of the elements, i.e. the type of the elements which should
      be put in the container. Of course, the ``manifest'' elements will have
      to conform to that type;

    \end{enumerate}
   
  \item creation genericity, simplicity and performance: the new construct
    must be able to cope with any type of container; it should not be complex
    to understand (we will choose an {\em Eiffel-like} syntax). Furthermore,
    it should not have negative effects on the preformance of the runtime,
    except in an understandable way.

  \end{enumerate}

You will find some very first examples figure~\ref{first}.

\begin{slide}{\label{first}Some first examples}
{\bf{}local}
   a: {\sc{}array}[{\sc{}integer}]
   b: {\sc{}collection}[{\sc{}integer}]
   c: {\sc{}collection}[{\sc{}float}]
   d: {\sc{}collection}[{\sc{}figure}]
{\bf{}do}
   a := \{{\bf{}like} a\} << 1, 2, 3 >>
   b := \{{\sc{}linked\_list}[{\sc{}integer\_16}]\} << 4, 5, 6 >>
   c := \{{\sc{}set}[{\sc{}float}]\} << 3.14159, 2.71828 >>
   d := << {\bf{}create} \{{\sc{}circle}\}, {\bf{}create} \{{\sc{}square}\} >>
{\bf{}end}
\end{slide}

The construct is built in two parts:

\begin{itemize}
  
\item an optional type mark (between curly brackets),
  
\item a compulsory {\em manifest contents}, marked by the opening and closing
  double angled brackets, enclosing the elements separated by optional commas.
\end{itemize}

We call the whole expression a {\em manifest container}. Its formal syntax is
presented figure~\ref{MCsyntax}.

\begin{slide}{\label{MCsyntax}The Manifest Container syntax}
Expression ::= \textit{-- other expressions...}
            |  Manifest\_Container

Manifest\_Container ::= "\{" Type\_Mark "\}"
                         "<<" Container\_Contents ">>"
                    |  "<<" Container\_Contents ">>"

Type\_Mark ::= Class\_Name
           |  "like" Entity

Container\_Contents ::= Expressions

Expressions ::= Expression
             |  Expression Optional\_Comma Expressions

Optional\_Comma ::= ","
                |  \(\epsilon\)
\end{slide}

If the type mark is not specified, the object defaults to an {\sc{}array} of
the smallest common ancestor (the old behavior: a type that all the elements
either conform or convert to, and that has no heirs with the same property).
We intend to obsolete this feature. The compiler will emit a warning when it
is used.

If the type mark is specified, it must be a non-deferred class with exactly
one formal argument (the effective argument of the type mark is known as the
{\em type of the elements}). The type of the elements in the manifest part
(the elements between double angle-brackets) {\em must} conform or convert to
the type given as effective argument.  If an elements converts to the type of
the elements of the container, then the conversion will occur.

The type mark may be an anchor. A classical use of such an anchor, generally
used to refactor old code with the new syntax, is presented
figure~\ref{anchors}.

\begin{slide}{\label{anchors}Anchor uses in manifest containers}
{\bf{}local}
   a: {\sc{}linked\_list}[{\sc{}integer}]
   b: {\sc{}collection}[{\sc{}integer}]
{\bf{}do}
   \textit{-- bring old constructs up to date: just}
   \textit{-- add a \{like left\} type mark}
   a := \{{\bf{}like} a\} << 1, 2, 3 >>

   \textit{-- ... or anchor it to another entity}
   b := \{{\bf{}like} a\} << 4, 5, 6 >>
{\bf{}end}
\end{slide}

This syntax may be nested; for the enclosed manifests, the rule is of course
the same.  Example: figure~\ref{nested}.

\begin{slide}{\label{nested}Nested manifest containers}
{\bf{}local}
   in: {\sc{}arrayed\_list}[{\sc{}integer}]
   ar: {\sc{}collection}[{\sc{}collection}[{\sc{}integer}]]
{\bf{}do}
   in := \{{\bf{}like} in\} << 1, 2, 3 >>

   ar := \{{\sc{}linked\_list}[{\sc{}list}[{\sc{}integer}]]\}
          <<
             in,
             \{{\bf{}like} in\} << 4, 5, 6 >>
          >>
{\bf{}end}
\end{slide}

\section{\label{classes}Implementation}

The syntax is not restricted to some well-known containers such as
{\sc{}array} or {\sc{}set}; indeed, third-party libraries should be able to
add new kinds of containers. For those containers to be able to use the
{\sc{}Manifest\_Container} syntax, they just have to inherit from the
{\sc{}container} class, presented figure~\ref{container}.

The {\tt{}*\_manifest} features are not exported by default: only the compiler
should use them.

Here is the protocol: when the compiler compiles a {\tt{}Manifest\_Container},
it:

\begin{enumerate}
  
\item creates the container with the {\tt{}create\_manifest} creator (it
  {\em{}must} be a creator),
  
\item inserts all the elements, using the {\tt{}put\_manifest} feature, with
  the given position (and corresponding values) in ascending order from~1 to
  the number of elements; the position may be used or not, depending on the
  container.

\end{enumerate}

{\em{}Note:} currently, we impose the containers to have one and only one
generic parameter (i.e. containers are ``vectors''). It means that we don't
support yet containers such as {\sc{}dictionary} or {\sc{}collection2} (we
keep that for later).


\begin{slide}{\label{container}The {\sc{}container} class interface}
{\bf{}deferred class interface} {\sc{}container}[{\sc{}e}]
   \textit{--}
   \textit{-- Any class inheriting from this class will automatically}
   \textit{-- benefit from the Manifest\_Container syntax.}
   \textit{--}
{\bf{}feature}

   has(value: {\sc{}e}): {\sc{}boolean} {\bf{}is}
         \textit{-- look for the `value'}
      {\bf{}end}

{\bf{}feature} \{{\sc{}none}\}

   create\_manifest(initial\_capacity: {\sc{}integer}) {\bf{}is}
         \textit{-- must be a creator}
      {\bf{}end}

   put\_manifest(position: {\sc{}integer}; value: {\sc{}e}) {\bf{}is}
         \textit{-- add the `value'}
      {\bf{}ensure}
         has(value)
      {\bf{}end}

{\bf{}end}
\end{slide}

\end{document}
