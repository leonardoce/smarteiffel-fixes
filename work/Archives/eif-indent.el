;;!emacs
;;
;; FILE:         eif-indent.el
;; SUMMARY:      Indent all Eiffel classes below a certain path.
;; USAGE:        GNU Emacs Lisp Library
;;
;; AUTHOR:       Bob Weiner
;; ORG:          Motorola, Inc., Communications Sector, Applied Research
;; E-MAIL:       USENET:  weiner@novavax.UUCP
;;
;; ORIG-DATE:     7-Dec-89 at 19:32:47
;; LAST-MOD:      6-Feb-90 at 18:47:34 by Bob Weiner
;;
;; Copyright (C) 1989, 1990 Bob Weiner, Motorola Inc.
;; Available for use and distribution under the same terms as GNU Emacs.
;;
;; This file is not part of GNU Emacs.
;;
;; DESCRIPTION:  
;;
;;  To indent all of ISE's Eiffel code, do the following over night since it
;;  takes a long time:
;;
;;	From a shell logged in as root:
;;
;;		emacs -q
;;
;;	In Emacs, assuming that Eiffel mode is automatically invoked whenever
;;      an Eiffel file is read in:
;;
;;		{M-x load-lib RTN default RTN}
;;		{M-x load-lib RTN eif-indent RTN}
;;		{M-x eif-indent-ise-code RTN}
;;
;;	As each file is indented, its path is displayed in the minibuffer.
;;	Eons later, "Done" will be displayed in the minibuffer; then you may
;;	terminate the Emacs session:
;;
;;		{C-x C-c}
;;
;;  I hope that ISE will eventually pick up on this more readable,
;;  space saving indentation style and will thereafter save the rest of the
;;  world from having to repeat this process with each Eiffel library release.
;;  (A recent mailing I received from them did use this style.)
;;
;; DESCRIP-END.

(defconst eif-install-dir "/usr/local/src/Eiffel/"
  "Directory below which ISE's Eiffel libraries and examples are stored.")


(defvar eif-num-indented 0)

(defun eif-indent-ise-code ()
  (interactive)
  (setq eif-num-indented 0)
  (eif-indent-classes
    (mapcar '(lambda (dir) (concat eif-install-dir dir))
	    '("library" "examples" "src/browser/eb"))))

(defun eif-indent-classes (search-dirs)
  "Takes a list of directories and indents and saves all Eiffel classes below them."
  (if (not (equal (user-real-login-name) "root"))
      (error "Must be root to call 'eif-indent-classes'."))
  (let ((make-backup-files nil)
	(delete-auto-save-files t))
    (delq nil
	  (mapcar
	    '(lambda (dir)
	       (setq dir (file-name-as-directory dir))
	       (let ((files (if (file-exists-p dir)
				(directory-files dir t "^[^.~#].*[^~#]$"))))
		 (delq nil
		       (mapcar
			 '(lambda (f)
			    (if (and (string-match "/\\([^/]+\\)\\.e$" f)
				     (not (file-directory-p f)))
				(progn
				  (find-file f)
				  (setq buffer-read-only nil)
				  (widen)
				  (auto-save-mode -1) ;; Off
				  ;; Delete excess end of line whitspace
				  ;; which messes up indenting routine.
				  (perform-replace "[ \t]+$" "" nil t nil)
				  (setq eif-num-indented (1+ eif-num-indented))
				  (message (format "(# %d) Indenting %s..."
						   eif-num-indented f))
				  (indent-region (point-min) (point-max) nil)
				  (save-buffer)
				  (kill-buffer (current-buffer)))))
			 files))
		 (eif-indent-classes
		   (delq nil
			 (mapcar '(lambda (f)
				    (and (file-directory-p f)
					 (not (string-match "\\.E$" f))
					 f))
				 files)))))
	    search-dirs)))
  (message "Done"))

(provide 'eif-indent)
