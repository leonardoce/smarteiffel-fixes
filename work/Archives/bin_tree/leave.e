class FEUIL

inherit BIN_ETIQ;

creation make

feature {NONE}

   g: BIN_ETIQ;


feature

   make(e: INTEGER) is
      do
         etiquette := e;
      end;

   max: INTEGER is
      do
         Result := etiquette;
      end;

   count: INTEGER is 1;

   appartient(valeur: INTEGER): BOOLEAN is
      do
         Result := etiquette = valeur;
      end;

   adjonction(valeur: INTEGER): BIN_ETIQ is
      -- ******** VIRER **********
      do
         if valeur > etiquette then
            !N_D!Result.make(etiquette,Current);
         else
            !N_G!Result.make(etiquette,Current);
         end;
         etiquette := valeur;
      ensure
         Result /= Current;
      end;

end

