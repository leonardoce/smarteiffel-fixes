class TEST

creation make

feature

   make is
      local
         a: BIN_ETIQ;
      do
         a := a1;
         is_true(a.max = 1);
         is_true(a.count = 1);
         is_true(a.appartient(1));
         is_true(not a.appartient(2));

         a := a2;
         is_true(a.max = 2);
         is_true(a.count = 2);
         is_true(a.appartient(1));
         is_true(a.appartient(2));

         a := a3;
         is_true(a.max = 2);
         is_true(a.count = 2);
         is_true(a.appartient(1));
         is_true(a.appartient(2));

         a := a4;
         is_true(a.max = 2);
         is_true(a.count = 2);
         is_true(a.appartient(1));
         is_true(a.appartient(2));

         a := a5;
         is_true(a.max = 2);
         is_true(a.count = 2);
         is_true(a.appartient(1));
         is_true(a.appartient(2));

         a := a6;
         is_true(a.max = 4);
         is_true(a.count = 3);
         is_true(a.appartient(1));
         is_true(a.appartient(2));
         is_true(not a.appartient(3));
         is_true(a.appartient(4));

         a := a7;
         is_true(a.max = 4);
         is_true(a.count = 3);
         is_true(a.appartient(1));
         is_true(a.appartient(2));
         is_true(not a.appartient(3));
         is_true(a.appartient(4));

         a := a8;
         is_true(a.max = 4);
         is_true(a.count = 4);
         is_true(a.appartient(1));
         is_true(a.appartient(2));
         is_true(a.appartient(3));
         is_true(a.appartient(4));

         a := a9;
         is_true(a.max = 7);
         is_true(a.count = 3);
         is_true(a.appartient(4));
         is_true(a.appartient(6));
         is_true(a.appartient(7));

      end;

   a1: BIN_ETIQ is
         -- arbre :     (1)
      do
         Result := feuil(1);
      end;

   a2: BIN_ETIQ is
         -- arbre :     (1)
         --               \
         --               (2)
      do
         Result := a1.adjonction(2);
      end;

   a3: BIN_ETIQ is
         -- arbre :     (1)
         --               \
         --               (2)
      do
         Result := n_d(1,feuil(2));
      end;

   a4: BIN_ETIQ is
         -- arbre :     (2)
         --             /
         --           (1)
      do
         Result := n_g(2,feuil(1));
      end;

   a5: BIN_ETIQ is
         -- arbre :     (2)
         --             /
         --           (1)
      do
         Result := feuil(2).adjonction(1);
      end;


   a6: BIN_ETIQ is
         -- arbre :     (2)
         --             / \
         --           (1)  (4)
      do
         Result := n_gd(2,feuil(1),feuil(4));
      end;


   a7: BIN_ETIQ is
         -- arbre :     (2)
         --             / \
         --           (1)  (4)
      do
         Result := feuil(2).adjonction(4).adjonction(1);
      end;


   a8: BIN_ETIQ is
         -- arbre :     (2)
         --             / \
         --           (1)  (4)
         --             \
         --             (3)
      do
         Result := a7.adjonction(3);
      end;


   a9: BIN_ETIQ is
         -- arbre :     (6)
         --             / \
         --           (4)  (7)
      do
         Result := n_g(6,feuil(4)).adjonction(7);
      end;


feature {NONE}

   feuil(e: INTEGER): FEUIL is
      do
         !!Result.make(e);
      end;

   n_g(e: INTEGER; g: BIN_ETIQ): N_G is
      do
         !!Result.make(e,g);
      end;

   n_d(e: INTEGER; d: BIN_ETIQ): N_D is
      do
         !!Result.make(e,d);
      end;

   n_gd(e: INTEGER; g, d: BIN_ETIQ): N_GD is
      do
         !!Result.make(e,g,d);
      end;


   is_true(b: BOOLEAN) is
      do
         cpt := cpt + 1;
         if not b then
            std_output.put_string("TEST: ERROR Test # ");
            std_output.put_integer(cpt);
            std_output.put_string("%N");
         else
            std_output.put_string("Yes%N");
         end;
      end;

   cpt: INTEGER;

end
