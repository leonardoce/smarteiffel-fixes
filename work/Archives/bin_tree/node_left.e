class N_G

inherit BIN_ETIQ;

creation make

feature {NONE}

   g: BIN_ETIQ;

feature

   make(e: INTEGER; fg: BIN_ETIQ) is
      do
         etiquette := e;
         g := fg;
      end;

   max: INTEGER is
      do
         Result := etiquette;
      end;

   count: INTEGER is
      do
         Result := 1 + g.count;
      end;

   appartient(valeur: INTEGER): BOOLEAN is
      do
         if valeur = etiquette then
            Result := true;
         elseif valeur < etiquette then
            Result := g.appartient(valeur);
         end;
      end;

   adjonction(valeur: INTEGER): BIN_ETIQ is
      do
         if valeur > etiquette then
            !FEUIL!Result.make(valeur);
            !N_GD!Result.make(etiquette,g,Result);
         else
            Result := Current;
            g := g.adjonction(valeur);
         end;
      ensure
         valeur < etiquette implies Result = Current;
         valeur > etiquette implies Result /= Current;
      end;

end
