class N_D

inherit BIN_ETIQ;

creation make

feature {NONE}

   d: BIN_ETIQ;


feature

   make(e: INTEGER; fd: BIN_ETIQ) is
      do
         etiquette := e;
         d := fd;
      end;

   max: INTEGER is
      do
         Result := d.max;
      end;

   count: INTEGER is
      do
         Result := 1 + d.count;
      end;

   appartient(valeur: INTEGER): BOOLEAN is
      do
         if valeur = etiquette then
            Result := true;
         elseif valeur > etiquette then
            Result := d.appartient(valeur);
         end;
      end;

   adjonction(valeur: INTEGER): BIN_ETIQ is
      do
         if valeur > etiquette then
            Result := Current;
            d := d.adjonction(valeur);
         else
            !FEUIL!Result.make(valeur);
            !N_GD!Result.make(etiquette,Result,d);
         end;
      ensure
         valeur > etiquette implies Result = Current;
         valeur < etiquette implies Result /= Current;
      end;

end
