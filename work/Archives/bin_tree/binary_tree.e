deferred class BINARY_TREE

feature

   etiquette: INTEGER;

   max: INTEGER is
      deferred
      ensure
         appartient(Result)
      end;

   count: INTEGER is
      deferred
      ensure
         Result >= 1;
      end;

   appartient(valeur: INTEGER): BOOLEAN is
      deferred
      end;

   adjonction(valeur: INTEGER): BIN_ETIQ is
      require
         not appartient(valeur);
      deferred
      ensure
         Result.count = 1 + old count;
      end;

end
