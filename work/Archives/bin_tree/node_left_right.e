class N_GD

inherit BIN_ETIQ;

creation make

feature {NONE}

   g: BIN_ETIQ;

   d: BIN_ETIQ;

feature

   make(e: INTEGER; fg, fd: BIN_ETIQ) is
      do
         etiquette := e;
         g := fg;
         d := fd;
      end;

   max: INTEGER is
      do
         Result := d.max;
      end;

   count: INTEGER is
      do
         Result := 1 + g.count + d.count;
      end;

   appartient(valeur: INTEGER): BOOLEAN is
      do
         if valeur = etiquette then
            Result := true;
         elseif valeur > etiquette then
            Result := d.appartient(valeur);
         else
            Result := g.appartient(valeur);
         end;
      end;

   adjonction(valeur: INTEGER): BIN_ETIQ is
      -- ********** VIRER ***********
      do
         Result := Current;
         if valeur > etiquette then
            d := d.adjonction(valeur);
         else
            g := g.adjonction(valeur);
         end;
      ensure
         Result = Current;
      end;

end
