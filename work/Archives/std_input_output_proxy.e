class STD_INPUT_OUTPUT_PROXY

inherit
   STD_INPUT_OUTPUT
      redefine
         same_is_connected,
         end_of_input,
         read_character,
         last_character,
         push_back_flag,
         unread_character,
         skip_separators,
         skip_separators_using,
         skip_remainder_of_line,
         read_integer,
         last_integer,
         read_real,
         last_real,
         read_double,
         last_double,
         last_string,
         read_line,
         read_word,
         newline,
         reach_and_skip,
         read_line_in,
         read_word_using,
         read_tail_in,
         put_character,
         put_string,
         put_integer,
         put_integer_format,
         put_real,
         put_real_format,
         put_double,
         put_double_format,
         put_number,
         put_boolean,
         put_pointer,
         put_new_line,
         put_spaces,
         append_file,
         available
      end

creation
   make

feature

   make_io is
         -- Must be done prior to any other use of `Current' (otherwise it does 
         -- nothing)
         -- Instanciate the `remote' io in the current processor. It is not 
         -- compulsory, but not dedicating a processor to IO leads to the 
         -- other processors not being able to output anything while the one 
         -- holding `remote' is working.
         -- (Remember that `remote' is a "false separate" because non-SCOOP 
         -- programs must run as intended--ie, without threads)
      do
         put_string_remote(remote, once "");
      end;

   same_is_connected: BOOLEAN is
      do
         Result := same_is_connected_remote(remote);
      end;

   end_of_input: BOOLEAN is
      do
         Result := end_of_input_remote(remote);
      end;

   read_character is
      do
         read_character_remote(remote);
      end;

   last_character: CHARACTER is
      do
         Result := last_character_remote(remote);
      end;

   push_back_flag: BOOLEAN is
      do
         Result := push_back_flag_remote(remote);
      end;

   unread_character is
      do
         unread_character_remote(remote);
      end;

   skip_separators is
      do
         skip_separators_remote(remote);
      end;

   skip_separators_using(separators: STRING) is
      do
         skip_separators_using_remote(remote, separators);
      end;

   skip_remainder_of_line is
      do
         skip_remainder_of_line_remote(remote);
      end;

   read_integer is
      do
         read_integer_remote(remote);
      end;

   last_integer: INTEGER is
      do
         Result := last_integer_remote(remote);
      end;

   read_real is
      do
         read_real_remote(remote);
      end;

   last_real: REAL is
      do
         Result := last_real_remote(remote);
      end;

   read_double is
      do
         read_double_remote(remote);
      end;

   last_double: DOUBLE is
      do
         Result := last_double_remote(remote);
      end;

   last_string: STRING is
      do
         Result := last_string_remote(remote);
      end;

   read_line is
      do
         read_line_remote(remote);
      end;

   read_word is
      do
         read_word_remote(remote);
      end;

   newline is
      do
         newline_remote(remote);
      end;

   reach_and_skip(keyword: STRING) is
      do
         reach_and_skip_remote(remote, keyword);
      end;

   read_line_in (str: STRING) is
      do
         read_line_in_remote(remote);
      end;

   read_word_using(separators: STRING) is
      do
         read_word_using_remote(remote, separators);
      end;

   read_tail_in(str: STRING) is
      do
         read_tail_in_remote(remote, str);
      end;

   put_character(c: CHARACTER) is
      do
         put_character_remote(remote, c);
      end;

   put_string(s: STRING) is
      do
         put_string_remote(remote, s);
      end;

   put_integer(i: INTEGER) is
      do
         put_integer_remote(remote, i);
      end;

   put_integer_format(i, s: INTEGER) is
      do
         put_integer_format_remote(remote, i, s);
      end;

   put_real(r: REAL) is
      do
         put_real_remote(remote, r);
      end;

   put_real_format(r: REAL; f: INTEGER) is
      do
         put_real_format_remote(remote, r, f);
      end;

   put_double(d: DOUBLE) is
      do
         put_double_remote(remote, d);
      end;

   put_double_format(d: DOUBLE; f: INTEGER) is
      do
         put_double_format_remote(remote, d, f);
      end;

   put_number(number: NUMBER) is
      do
         put_number_remote(remote, number);
      end;

   put_boolean(b: BOOLEAN) is
      do
         put_boolean_remote(remote, b);
      end;

   put_pointer(p: POINTER) is
      do
         put_pointer_remote(remote, p);
      end;

   put_new_line is
      do
         put_new_line_remote(remote);
      end;

   put_spaces(nb: INTEGER) is
      do
         put_spaces_remote(remote, nb);
      end;

   append_file(file_name: STRING) is
      do
         append_file_remote(remote, file_name);
      end;

   available: BOOLEAN is
      do
         Result := available_remote(remote);
      end;

feature {NONE} -- Critical sections:

   same_is_connected_remote(rem: like remote): BOOLEAN is
      do
         rem.same_is_connected;
      end;

   end_of_input_remote(rem: like remote): BOOLEAN is
      do
         rem.end_of_input;
      end;

   read_character_remote(rem: like remote) is
      do
         rem.read_character;
      end;

   last_character_remote(rem: like remote): CHARACTER is
      do
         rem.last_character;
      end;

   push_back_flag_remote(rem: like remote): BOOLEAN is
      do
         rem.push_back_flag;
      end;

   unread_character_remote(rem: like remote) is
      do
         rem.unread_character;
      end;

   skip_separators_remote(rem: like remote) is
      do
         rem.skip_separators;
      end;

   skip_separators_using_remote(rem: like remote; separators: STRING) is
      do
         rem.skip_separators_using(separators);
      end;

   skip_remainder_of_line_remote(rem: like remote) is
      do
         rem.skip_remainder_of_line;
      end;

   read_integer_remote(rem: like remote) is
      do
         rem.read_integer;
      end;

   last_integer_remote(rem: like remote): INTEGER is
      do
         rem.last_integer;
      end;

   read_real_remote(rem: like remote) is
      do
         rem.read_real;
      end;

   last_real_remote(rem: like remote): REAL is
      do
         rem.last_real;
      end;

   read_double_remote(rem: like remote) is
      do
         rem.read_double;
      end;

   last_double_remote(rem: like remote): DOUBLE is
      do
         rem.last_double;
      end;

   last_string_remote(rem: like remote): STRING is
      do
         rem.last_string;
      end;

   read_line_remote(rem: like remote) is
      do
         rem.read_line;
      end;

   read_word_remote(rem: like remote) is
      do
         rem.read_word;
      end;

   newline_remote(rem: like remote) is
      do
         rem.newline;
      end;

   reach_and_skip_remote(rem: like remote; keyword: STRING) is
      do
         rem.reach_and_skip(keyword);
      end;

   read_line_in_remote(rem: like remote; str: STRING) is
      do
         rem.read_line_in(str);
      end;

   read_word_using_remote(rem: like remote; separators: STRING) is
      do
         rem.read_word_using(separators);
      end;

   read_tail_in_remote(rem: like remote; str: STRING) is
      do
         rem.read_tail_in(str);
      end;

   put_character_remote(rem: like remote; c: CHARACTER) is
      do
         rem.put_character(c);
      end;

   put_string_remote(rem: like remote; s: STRING) is
      do
         rem.put_string(s);
      end;

   put_integer_remote(rem: like remote; i: INTEGER) is
      do
         rem.put_integer(i);
      end;

   put_integer_format_remote(rem: like remote; i, s: INTEGER) is
      do
         rem.put_integer_format(i, s);
      end;

   put_real_remote(rem: like remote; r: REAL) is
      do
         rem.put_real(r);
      end;

   put_real_format_remote(rem: like remote; r: REAL; f: INTEGER) is
      do
         rem.put_real_format(r, f);
      end;

   put_double_remote(rem: like remote; d: DOUBLE) is
      do
         rem.put_double(d);
      end;

   put_double_format_remote(rem: like remote; d: DOUBLE; f: INTEGER) is
      do
         rem.put_double_format(d, f);
      end;

   put_number_remote(rem: like remote; number: NUMBER) is
      do
         rem.put_number(number);
      end;

   put_boolean_remote(rem: like remote; b: BOOLEAN) is
      do
         rem.put_boolean(b);
      end;

   put_pointer_remote(rem: like remote; p: POINTER) is
      do
         rem.put_pointer(p);
      end;

   put_new_line_remote(rem: like remote) is
      do
         rem.put_new_line;
      end;

   put_spaces_remote(rem: like remote; nb: INTEGER) is
      do
         rem.put_spaces(nb);
      end;

   append_file_remote(rem: like remote; file_name: STRING) is
      do
         rem.append_file(file_name);
      end;

   available_remote(rem: like remote): BOOLEAN is
      do
         Result := rem.available;
      end;

feature {NONE}

   remote: separate STD_INPUT_OUTPUT is
      local
         lio: STD_INPUT_OUTPUT
      once
         create lio.make;
         Result := lio;
      ensure
         Result /= Void;
      end;

end -- STD_INPUT_OUTPUT_PROXY
