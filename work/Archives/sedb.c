/*
-- This file is  free  software, which  comes  along  with  SmartEiffel. This
-- software  is  distributed  in the hope that it will be useful, but WITHOUT
-- ANY  WARRANTY;  without  even  the  implied warranty of MERCHANTABILITY or
-- FITNESS  FOR A PARTICULAR PURPOSE. You can modify it as you want, provided
-- this header is kept unaltered, and a notification of the changes is added.
-- You  are  allowed  to  redistribute  it and sell it, alone or as a part of
-- another product.
--       Copyright (C) 1994-2002 LORIA - INRIA - U.H.P. Nancy 1 - FRANCE
--          Dominique COLNET and Suzanne COLLIN - SmartEiffel@loria.fr
--                       http://SmartEiffel.loria.fr
--
*/

/*
  This file (SmartEiffel/sys/runtime/trace.c) is automatically included when
  `run_control.no_check' is true (i.e. in all modes except -boost).
  This file comes after no_check.[hc] to implement the -trace flag.
*/


#ifdef SE_WEDIT
/*
   Smooth interface with the Wedit debugger.
*/
#define MAXBREAKPOINTS 256
static int __BreakpointsList[MAXBREAKPOINTS];
void SE_CallDebugger(void) {
}

se_position se_trace(se_position p) {
  int l = se_position2line(p);
  int c = se_position2column(p);
  int f = se_position2path_id(p);
  int i,s;

  s = (f <<16)|l;
  for (i=0; i< MAXBREAKPOINTS;i++) {
    if (__BreakpointsList[i] == s) {
      SE_CallDebugger();
    }
    else if (__BreakpointsList[i] == 0)
      break;
  }
  return p;
}
#elif SE_TRACE
/*
  The SmartEiffel -trace is used, so the command line SmartEiffel
  debugger based on the `se_trace' function is defined.
*/
static FILE* se_db_TraceFile = NULL; /* A non NULL value indicates that the user
				       is doing some profiling.
				    */

static char se_db_last_command[SE_DB_BUFMAX];

#define SE_DB_WaitingCommand 1
#define SE_DB_NextCommand 2
#define SE_DB_StepCommand 3
#define SE_DB_UpCommand 4
#define SE_DB_Running 5
#define SE_DB_TraceFile 6
#define SE_DB_NotInitialized 7

static int se_db_ModeFlag = SE_DB_NotInitialized;

static int se_db_SignalHandlerFlag = 0;

static int se_db_YesOrNo(char* question, int default_answer) {
  /* To ask some [yes/no] question interactively. */
  static char answer[SE_DB_BUFMAX];
  int i; char c;
 restart:
  i = 0;
  fprintf(SE_ERR,question);
  fprintf(SE_ERR," (yes/no)? [");
  fprintf(SE_ERR,(default_answer ? "yes" : "no"));
  fprintf(SE_ERR,"]");
  fflush(SE_ERR);

  c = getc(stdin);
  while (c != '\n') {
    if (('A' <= c) && (c <= 'Z')) c = c + 32; /* c = tolower(c); */
    answer[i++] = c;
    c = getc(stdin);
  }
  answer[i] = '\0';
  if (i == 0) {
    return default_answer;
  }
  else if ((strcmp("yes",answer)==0) || (strcmp("y",answer)==0) ||
	   (strcmp("oui",answer)==0) || (strcmp("o",answer)==0)) {
    return 1;
  }
  else if ((strcmp("no",answer)==0) || (strcmp("n",answer)==0) ||
	   (strcmp("non",answer)==0)) {
    return 0;
  }
  else {
    fprintf(SE_ERR,"This answer is not clear.\n");
    goto restart;
  }

}

static void se_db_OnLineHelp(void) {
  fprintf(SE_ERR,
          "SmartEiffel debugger help (list of commands):\n"
          " ?    : display this help message\n"
          " s    : single step, stepping into routine calls\n"
          " n    : single step, without stepping into routine calls\n"
          " f    : continue execution until the current routine finishes\n"
          " .    : display the current execution point\n"
          " u    : display the caller point (go up in the stack)\n"
          " d    : display the callee point (go down in the stack)\n"
          " S    : display the entire stack (all frames)\n"
          " b    : set a new breakpoint at the current execution point\n"
          " B    : display the entire breakpoint list\n"
          " -[#] : delete a breakpoint\n"
          " c    : continue execution until the next breakpoint\n"
          " G    : run the garbage collector now\n"
          " T    : switch to the \"trace.se\" file mode\n"
          " q    : (or Q) quit the debugger\n"
          " H    : display more help on using the debugger\n"
          " Enter: repeat the previous debugger command\n"
	  "\n");
}

static void se_db_OnLineMoreHelp(void) {
  if (se_db_YesOrNo("More help with the SmartEiffel debugger",1)) {
    /*      ------------------------------------------------------------------------------- */
    fprintf(SE_ERR,
	    "Yes, sedb is the SmartEiffel DeBugger. Keep in mind that you can trust sedb\n"
	    "if and only if you are using a freshly compiled system. To take in account\n"
	    "any modification of some Eiffel source file, you must leave sedb and\n"
	    "recompile your system first.\n"
	    "\n");
  }
  if (se_db_YesOrNo("More help with breakpoints",1)) {
    /*      ------------------------------------------------------------------------------- */
    fprintf(SE_ERR,
	    "A breakpoint is composed of four kinds of information/condition: (1) a routine\n"
	    "name indication, (2) a file name indication, (3) a line range, and (4) a stack\n"
	    "limit. Each kind of information may be separately selected or not. (You should\n"
	    "select at least one information/condition.) When more than one condition is\n"
	    "selected a logical \"and\" is performed to decide whether the breakpoint matches\n"
	    "the current execution point or not.\n"
	    "\n");
  }
  if (se_db_YesOrNo("More help with breakpoints - name indication",1)) {
    /*      ------------------------------------------------------------------------------- */
    fprintf(SE_ERR,
	    "The name information of the debugging stack contains information about the\n"
	    "name of the routine called as well as the base class where this routine is\n"
	    "defined. The name indication of the breakpoint can be any substring to be\n"
	    "searched within the name information of the stack. For example, if the name\n"
	    "indication is \"item\", the execution would stop in feature item of STRING\n"
	    "but also in feature item of ARRAY, in feature item of DICTIONARY, etc.\n"
	    "If the name indication is \"item STRING\", execution would stop only in\n"
	    "routine item defined in class STRING. Also note that the name indication\n"
	    "can be any substring. For example, if the name indication is \"is_\", the\n"
	    "execution would stop in is_empty, is_integer, is_real, etc. As another\n"
	    "example, if the name indication is \"STRI\", the execution would stop at\n"
	    "any place in classes STRING, ARRAY[STRING], DICTIONARY[STRING,FOO], etc.\n"
	    "Finally, also note that the \"invariant\" string is used when the execution\n"
	    "is in the class invariant of some class, hence allowing you to spot all\n"
	    "class invariant execution.\n"
	    "\n");
  }
  if (se_db_YesOrNo("More help with breakpoints - file indication",1)) {
    /*      ------------------------------------------------------------------------------- */
    fprintf(SE_ERR,
	    "If you want to stop each time the execution reaches the string.e file class,\n"
	    "it can be achieved easily by setting the file indication to \"string.e\".\n"
	    "Because the file name indication is applied on the whole path of the file,\n"
	    "the file indication allows you to filter more than a simple file name. For\n"
	    "example, if the file indication is \"lib/kernel\", the execution would stop each\n"
	    "time execution reaches some code in the kernel library of SmartEiffel.\n"
	    "\n");
  }
  if (se_db_YesOrNo("More help with breakpoints - line range",1)) {
    /*      ------------------------------------------------------------------------------- */
    fprintf(SE_ERR,
	    "The line range information allows you to select the line range to be\n"
	    "considered.  For example, to stop each time execution reaches line 12 or 13\n"
	    "in some file, just enter [12,13] as a line range.\n"
	    "\n");
  }
  if (se_db_YesOrNo("More help with breakpoints - stack limit",1)) {
    /*      ------------------------------------------------------------------------------- */
    fprintf(SE_ERR,
	    "The stack limit condition allows you to watch the stack size during\n"
	    "execution. (This is useful to debug bad recursive calls.) For example, a stack\n"
	    "limit of 10 would stop the execution as soon as the stack size reaches this\n"
	    "10 limit. The automatic incrementation option causes the stack-limit to be\n"
	    "incremented each time the corresponding breakpoint matches. For example, a\n"
	    "breakpoint composed only of a single stack limit condition is a perfect\n"
	    "watch dog of stack memory consumption.\n"
	    "\n");
  }
  if (se_db_YesOrNo("More help about GENERAL.sedb_breakpoint",1)) {
    /*      ------------------------------------------------------------------------------- */
    fprintf(SE_ERR,
	    "The feature `sedb_breakpoint' defined in class GENERAL can be used to set a\n"
	    "breakpoint directly in your Eiffel source file. Keep in mind that you have\n"
	    "to recompile your system each time some Eiffel source file is modified. Thus\n"
	    "each time you add a \"sedb_breakpoint;\" call to some Eiffel source file\n"
	    "you must recompile your code first. Also note that a \"sedb_breakpoint\" is\n"
	    "always enabled even when the corresponding source file is not traced (see ACE\n"
	    "file mode).\n"
	    "\n");
  }
  if (se_db_YesOrNo("More help about granularity of debugging",1)) {
    /*      ------------------------------------------------------------------------------- */
    fprintf(SE_ERR,
	    "When using the traditional command line mode (i.e. no ACE files) the debugger\n"
	    "examines all traced points in the Eiffel source code. Thus, all possible points\n"
	    "of your Eiffel system are examined (even points of the BOOLEAN class itself!).\n"
	    "This is great for learning Eiffel but it may slow down debugging especially for\n"
	    "large systems. Thanks to the ACE files facility (see examples in directory\n"
	    "SmartEiffel/lib_show/ace), you can decide which clusters/files are to be traced.\n"
	    "(You have to use the trace clauses of the ACE file to select the debugging\n"
	    "granularity.) This is good to know. Also keep in mind that you can still use\n"
	    "the GENERAL.trace_switch feature to switch the granularity of the\n"
	    "debugging dynamically.\n"
	    "\n");
  }
  if (se_db_YesOrNo("More help about CTRL-C keyboard interruption",1)) {
    /*      ------------------------------------------------------------------------------- */
    fprintf(SE_ERR,
	    "While running a program under sedb, you can hit CTRL-C at any time\n"
	    "in order to view what the program is doing right now.\n"
	    "\n");
  }
  if (se_db_YesOrNo("More help with garbage collector information",1)) {
    /*      ------------------------------------------------------------------------------- */
    fprintf(SE_ERR,
	    "The 'G' command of sedb allows you to run the garbage collector at any time\n"
	    "while debugging. You can get more information about the number of objects\n"
	    "used for each Eiffel reference type. This is achieved in traditional command\n"
	    "line mode with the -gc_info flag. In ACE file mode, you have to switch on the\n"
	    "\"gc_info\" option in the \"generate\" section to achieve the same behavior.\n"
	    "\n");
  }
  if (se_db_YesOrNo("More help with \"trace.se\" file mode",1)) {
    /*      ------------------------------------------------------------------------------- */
    fprintf(SE_ERR,
	    "The 'T' command of sedb allows you to switch to the \"trace.se\" file mode.\n"
	    "The goal of this mode is to collect profiling information into the \"trace.se\"\n"
	    "file. The profiling information indicates which line, which column, in which\n"
	    "Eiffel source file execution goes. In traditional command line mode, because\n"
	    "all classes are traced, the \"trace.se\" file is usually a huge file even for a\n"
	    "small program. Thanks to the ACE file facility, you can decide to trace or not\n"
	    "to trace each class separately (see \"More help about granularity of debugging\").\n"
	    "Feature GENERAL.trace_switch allows trace generation to be turned on/off\n"
	    "dynamically. At run time, if the argument of feature trace_switch is true, trace\n"
	    "printing is switched on. (Conversely, false means switch off trace printing.)\n"
	    "When the -trace option is not specified, calls to trace_switch are ignored by\n"
	    "compile_to_c (no C code is produced).\n"
	    "\n");
  }
  if (se_db_YesOrNo("More help about sedb itself",1)) {
    /*      ------------------------------------------------------------------------------- */
    fprintf(SE_ERR,
	    "This debugger is written in C (sorry :-). The complete source code of this\n"
	    "debugger is in the file ${SmartEiffel}/sys/runtime/trace.c\". Please, feel\n"
	    "free to debug or to complete this debugger. Any problem should be reported to\n"
	    "the SmartEiffel mailing list at \"SmartEiffel@loria.fr\". (Do not forget to\n"
	    "visit our WEB pages at \"http://SmartEiffel.loria.fr\" too.)\n"
	    "\n");
  }
}

static void se_db_ShowPoint(se_dump_stack*ds, int l, int c, int f) {
  /* Try to printf information about the Current point. */
  char* path; FILE *file; int line = 1; char cc; int after;

  fprintf(SE_ERR,"---- Stack-frame: %d ---- inside ",se_stack_size(ds));
  fflush(SE_ERR);
  if (!se_print_one_frame(ds)) {
    fflush(SE_ERR);
    return ;
  }
  fflush(SE_ERR);
  path = p[f];
  if (path == NULL) {
    fprintf(SE_ERR,"line %d column %d of some unknown file.\n",l,c);
    return ;
  }
  file = fopen(path,"r");
  if (file == NULL) {
    fprintf(SE_ERR,"Unable to access source file \"%s\".\n",path);
    fprintf(SE_ERR,"\t(Are you sure you're debugging a freshly compiled program?)\n");
    fprintf(SE_ERR,"\t(Debugging should be done in the compilation directory.)\n");
    fprintf(SE_ERR,"line %d column %d of \"%s\".\n",l,c,path);
    return ;
  }
  fprintf(SE_ERR,"------------------------------------------------------------------------\n");
  while (line < (l - 5)) {
    cc = fgetc(file);
    if (cc == '\n') {
      line++;
    }
  }
  fprintf(SE_ERR," %d\t",line);
  while (line < l) { /* To show lines before the point. */
    cc = fgetc(file);
    if (cc == '\n') {
      line++;
      if (line != l) fprintf(SE_ERR,"\n %d\t",line);
    }
    else {
      fprintf(SE_ERR,"%c",cc);
    }
  }
  fprintf(SE_ERR,"\n>%d++\t",line);
  while (line == l) { /* To show the point. */
    cc = fgetc(file);
    if (cc == '\n') {
      line++;
      fprintf(SE_ERR,"\n %d\t",line);
    }
    else {
      fprintf(SE_ERR,"%c",cc);
    }
  }
  after = l + 3;
  while (line < after) {
    /* To show some lines after the point. */
    cc = fgetc(file);
    if (cc == EOF) break; 
    if (cc == '\n') {
      line++;
      if (line != after) {
	fprintf(SE_ERR,"\n %d\t",line);
      }
    }
    else {
      fprintf(SE_ERR,"%c",cc);
    }
  }
  fprintf(SE_ERR,"\n");
  fclose(file);
  return ;
}

static void se_db_ShowPointFrame(se_dump_stack*ds) {
  se_position position = ds->p;
  int li = se_position2line(position);
  int co = se_position2column(position);
  int fi = se_position2path_id(position);

  se_db_ShowPoint(ds,li,co,fi);
}

static int se_db_ViewBreakPoint(se_breakpoint* bp) {
  /* A 0 result means that this is really a stupid breakpoint. */
  int result = 0;

  if (bp->name_flag) {
    result = 1;
    fprintf(SE_ERR,"   name indication = \"%s\".\n",bp->name);
  }
  if (bp->file_flag) {
    result = 1;
    fprintf(SE_ERR,"   file indication = \"%s\".\n",bp->file);
  }
  if (bp->line_flag) {
    result = 1;
    fprintf(SE_ERR,"   line range [%d,%d].\n",bp->line_minimum,bp->line_maximum);
  }
  if (bp->stack_flag) {
    result = 1;
    fprintf(SE_ERR,"   stack limit = %d  automatic: %s\n",
	    bp->stack_limit,
	    (bp->stack_automatic?"yes":"no"));
  }
  return result;
}

static int se_db_GetInt(char* value_name, int default_value) {
  /* To get some integer value. The `default_value' is considered only
     when greater than 0.
  */
  static char answer[SE_DB_BUFMAX];
  int i; char c; int result;
 restart:
  result = 0; i = 0;
  fprintf(SE_ERR,value_name);
  fprintf(SE_ERR," = ");
  if (default_value > 0) {
    fprintf(SE_ERR,"[%d] ",default_value);
  }
  fflush(SE_ERR);
  c = getc(stdin);
  while (c != '\n') {
    answer[i++] = c;
    c = getc(stdin);
  }
  answer[i] = '\0';
  if (i == 0) {
    return default_value;
  }
  i = 0;
  while (answer[i] != '\0') {
    c = answer[i++];
    if (('0' <= c) && (c <= '9')) {
      result = (result * 10) + (c - '0');
    }
    else {
      fprintf(SE_ERR,"Bad integer natural value: \"%s\".\n",answer);
      goto restart;
    }
  }
  return result;
}

void se_db_GetString(char*name, char* buffer) {
  int i; char c;
 restart: {
    i = 0;
    fprintf(SE_ERR,"%s: ",name);
    fflush(SE_ERR);
    c = getc(stdin);
    while (c != '\n') {
      buffer[i++] = c;
      c = getc(stdin);
    }
    buffer[i] = '\0';
  }
  if (i == 0) {
    fprintf(SE_ERR,"The empty string is not allowed.\n");
    goto restart;
  }
  fprintf(SE_ERR,"%s = \"%s\"",name,buffer);
  if (!se_db_YesOrNo(" confirm",1)) goto restart;
  return ;
}

static se_breakpoint* se_db_GetNewBreakPoint(void) {
  se_breakpoint* bp = se_malloc(sizeof(se_breakpoint));
  char* question; int default_answer = 1;
  question = "name indication";
  if (se_db_YesOrNo(question,default_answer)) {
    bp->name_flag = 1;
    se_db_GetString(question,bp->name);
    default_answer = 0;
  }
  else {
    bp->name_flag = 0;
  }
  question = "file indication";
  if (se_db_YesOrNo(question,default_answer)) {
    bp->file_flag = 1;
    se_db_GetString(question,bp->file);
    default_answer = 0;
  }
  else {
    bp->file_flag = 0;
  }
  if (se_db_YesOrNo("line range",default_answer)) {
  restart_line_range:
    bp->line_flag = 1;
    bp->line_minimum = se_db_GetInt("line minimum",0);
    bp->line_maximum = se_db_GetInt("line maximum",bp->line_minimum);
    if (bp->line_maximum < bp->line_minimum) {
      fprintf(SE_ERR,"Bad line range [%d,%d].(Try again.)\n",
	     bp->line_minimum,bp->line_maximum);
      goto restart_line_range;
    }
    default_answer = 0;
   }
  else {
    bp->line_flag = 0;
  }
  question = "stack limit";
  if (se_db_YesOrNo(question,default_answer)) {
    bp->stack_flag = 1;
    bp->stack_limit = se_db_GetInt(question,10);
    bp->stack_automatic = se_db_YesOrNo("automatic incrementation of the limit",1);
    default_answer = 0;
  }
  else {
    bp->stack_flag = 0;
  }
  bp->next = NULL;
  fprintf(SE_ERR,"Here is the breakpoint you have just defined:\n");
  if (0 == se_db_ViewBreakPoint(bp)) {
    fprintf(SE_ERR,"\tNot a breakpoint (no condition selected).\n");
    return NULL;
  }
  if (se_db_YesOrNo("Confirm this breakpoint",1)) {
    return bp;
  }
  else {
    return NULL;
  }
}

static int se_db_MatchString(char* indication, char* frame) {
  int i1 = 0; int i2 = 0; int r;  char c1, c2;
  while (1) {
    c1 = indication[i1];
    c2 = frame[i2];
    if (c1 == '\0') {r = 1; break;}
    if (c2 == '\0') {r = 0; break;}
    if (c1 != c2) {
      i2 = i2 - i1 + 1;
      i1 = 0;
    }
    else {
      i1++; i2++;
    }
  }
  return r;
}

static int se_db_BreakPointMatch(se_dump_stack*ds, int l, int f,
				int breakpointNumber, se_breakpoint* bp) {
  /* Does this `bp' match the current point. */
  int StackSize;
  int stack_automatic = 0;

  if (bp->stack_flag) {
    StackSize = se_stack_size(ds);
    if (StackSize < bp->stack_limit) return 0;
    stack_automatic = bp->stack_automatic;
  }
  if (bp->line_flag) {
    if (l < bp->line_minimum) return 0;
    if (l > bp->line_maximum) return 0;
  }
  if (bp->name_flag) {
    se_frame_descriptor* fd = ds->fd;
    if (fd == NULL) return 0;
    if (fd->name == NULL) return 0;
    if (!se_db_MatchString(bp->name,fd->name)) return 0;
  }
  if (bp->file_flag) {
    if (p[f] == NULL) return 0;
    if (!se_db_MatchString(bp->file,p[f])) return 0;
  }
  fprintf(SE_ERR,"Breakpoint #%d match.\n",breakpointNumber);
  se_db_ViewBreakPoint(bp);
  if (stack_automatic)
    bp->stack_limit = StackSize+1;
  return 1;
}

static void se_db_CompactViewOfTheStack(se_dump_stack* top) {
  se_dump_stack* ds = NULL;
  int frame_count = 1;

  ds = top;
  if (ds == NULL) {
    fprintf(SE_ERR,"Empty stack.\n");
    return ;
  }
  else {
    while (ds->caller != NULL) {
      ds = ds->caller;
      frame_count++;
    }
  }
  fprintf(SE_ERR,"===== %d frames in current stack:\n",frame_count);
  while (ds != NULL) {
    se_frame_descriptor* fd = ds->fd;
    if (fd == NULL) {
      fprintf(SE_ERR,"External CECIL call.\n");
    }
    else {
      fprintf(SE_ERR,"%s\n",fd->name);
    }
    /* Next frame : */
    if (ds == top) {
      ds = NULL;
    }
    else {
      se_dump_stack* ds2;
      ds2 = top;
      while (ds2->caller != ds) {
	ds2 = ds2->caller;
      }
      ds = ds2;
    }
  }
}

void se_trace(se_dump_stack*ds, se_position position) {
  static char line [SE_DB_BUFMAX];
  int i, cc, nb;
  int li = se_position2line(position);
  int co = se_position2column(position);
  int fi = se_position2path_id(position);
  static int previous_ss;
  static se_dump_stack*previous_ds;
  static se_breakpoint* breakpoints;
  se_dump_stack* plus_minus = ds;

  ds->p = position; /* To update the stack information. */
  if (!se_trace_flag) return ;
  if (se_db_ModeFlag == SE_DB_TraceFile) {
    fprintf(se_db_TraceFile,"line %d column %d in %s\n",li,co,p[fi]);
    fflush(se_db_TraceFile);
    return ;
  }
  else if (se_db_ModeFlag == SE_DB_Running) {
    se_breakpoint* bpl= breakpoints;
    nb = 1;
    while (bpl != NULL) {
      if (se_db_BreakPointMatch(ds,li,fi,nb,bpl)) {
	se_db_ShowPoint(ds,li,co,fi);
	se_db_ModeFlag = SE_DB_WaitingCommand;
	goto loop1;
      }
      nb ++;
      bpl = bpl->next;
    }
    return ;
  }
  else if (se_db_ModeFlag == SE_DB_WaitingCommand) {
  loop1: /* Really get a new line from keyboard: */
    se_db_SignalHandlerFlag = 0;
    fprintf(SE_ERR,"(sedb) ");
    fflush(SE_ERR);
    i = 0; cc = '\0';
    while (cc != '\n') {
      cc =  getc(stdin);
      if (se_db_SignalHandlerFlag) {
	se_db_SignalHandlerFlag = 0;
	strcpy(se_db_last_command,".");
	goto loop1;
      }
      line[i++] = cc;
    }
    line[--i] = '\0';
    if (i != 0) strcpy(se_db_last_command,line);
    goto loop3;
  loop2: /* Replay from scratch analysis of the `line': */
    fprintf(SE_ERR,"(sedb) %s\n",line);
  loop3:
    cc = line[0];
    if (cc == '\0') { /* Empty command line. */
      strcpy(line,se_db_last_command);
      goto loop2;
    }
    else if ((cc == ' ') || (cc == '\t')) {
      for (i = 0; line[i] != '\0' ; i++) {
	line[i] = line[i + 1];
      }
      goto loop2;
    }
    else if ((cc == '?') || (cc == 'h')) {
      se_db_OnLineHelp();
      goto loop1;
    }
    else if (cc == 'S') {
      if (se_db_YesOrNo("Compact view of the stack",1))
	se_db_CompactViewOfTheStack(ds);
      else
	se_print_run_time_stack();
      goto loop1;
    }
    else if (cc == 'u') {
      if (plus_minus->caller == NULL) {
	se_db_ShowPointFrame(plus_minus);
	fprintf(SE_ERR,"Top of the stack reached.\n");
	goto loop1;
      }
      plus_minus = plus_minus->caller;
      se_db_ShowPointFrame(plus_minus);
      goto loop1;
    }
    else if (cc == 'd') {
      se_dump_stack* plus = ds;
      if (plus_minus == ds) {
	se_db_ShowPointFrame(plus_minus);
	fprintf(SE_ERR,"This is the current point (bottom of the stack).\n");
	goto loop1;
      }
      while (plus->caller != plus_minus) {
	plus = plus->caller;
      }
      plus_minus = plus;
      se_db_ShowPointFrame(plus_minus);
      goto loop1;
    }
    else if (cc == 's') {
      se_db_ModeFlag = SE_DB_StepCommand;
      return ;
    }
    else if (cc == 'n') {
      previous_ds = ds;
      previous_ss = se_stack_size(ds);
      se_db_ModeFlag = SE_DB_NextCommand;
      return ;
    }
    else if (cc == 'f') {
      previous_ds = ds;
      previous_ss = se_stack_size(ds);
      se_db_ModeFlag = SE_DB_UpCommand;
      return ;
    }
    else if (cc == 'Q') {
      exit(EXIT_SUCCESS);
    }
    else if (cc == 'q') {
      if (se_db_YesOrNo("Really quit",0)) {
	fprintf(SE_ERR,"Bye.\n");
	exit(EXIT_SUCCESS);
      }
      else {
	goto loop1;
      }
    }
    else if (cc == '.') {
      se_db_ShowPoint(ds,li,co,fi);
      goto loop1;
    }
    else if (cc == 'G') {
#ifdef SE_GC_LIB
      fprintf(SE_ERR,"GC is running...\n");
      gc_start();
      fprintf(SE_ERR,"GC done.\n");
#else
      fprintf(SE_ERR,"This system is not equipped with the SmartEiffel garbage collector.\n");
#endif
      goto loop1;
    }
    else if (cc == 'c') {
      fprintf(SE_ERR,"running...\n");
      se_db_ModeFlag = SE_DB_Running;
      return ;
    }
    else if (cc == 'b') {
      se_breakpoint* bpl= breakpoints;
      se_breakpoint* bp = se_db_GetNewBreakPoint();
      if (bp != NULL) {
	if (bpl == NULL) {
	  breakpoints = bp;
	}
	else {
	  while (bpl->next != NULL) {
	    bpl = bpl->next;
	  }
	  bpl->next = bp;
	}
      }
      strcpy(se_db_last_command,"B");
      goto loop1;
    }
    else if (cc == 'B') {
      se_breakpoint* bpl= breakpoints;
      if (bpl == NULL) {
	fprintf(SE_ERR,"No breakpoints defined.\n");
	goto loop1;
      }
      nb = 1;
      while (bpl != NULL) {
	fprintf(SE_ERR,"breakpoint #%d:\n",nb);
	se_db_ViewBreakPoint(bpl);
	bpl = bpl-> next;
	nb ++;
      }
      goto loop1;
    }
    else if (cc == '-') {
      se_breakpoint* bpl= breakpoints;
      if (bpl == NULL) {
	fprintf(SE_ERR,"No breakpoint to delete.\n");
	goto loop1;
      }
      nb = 0;
      for (i = 1; line[i] == ' ' ; i++){};
      while (line[i] != '\0') {
	cc = line[i++];
	if (('0' <= cc) && (cc <= '9'))
	  nb = (nb * 10) + (cc - '0');
	else { nb =0; break; }
      }
      if (nb == 1) {
	breakpoints = breakpoints->next;
	strcpy(se_db_last_command,"B");
	goto loop1;
      }
      else if (nb > 1) {
	i = 2;
	while ((bpl->next != NULL) && (i < nb)) {
	  bpl = bpl->next;
	  i++;
	}
	if (i != nb) {
	  fprintf(SE_ERR,"No breakpoint #%d to delete.\n",nb);
	  goto loop1;
	}
	bpl->next = bpl->next->next;
	strcpy(se_db_last_command,"B");
	goto loop1;
      }
      else {
	while (breakpoints != NULL) {
	  se_db_ViewBreakPoint(breakpoints);
	  if (se_db_YesOrNo("delete this one",0))
	    breakpoints = breakpoints->next;
	  else break;
	}
	if ((bpl = breakpoints) != NULL) {
	  while (bpl->next != NULL) {
	    se_db_ViewBreakPoint(bpl->next);
	    if (se_db_YesOrNo("delete this one",0))
	      bpl->next = bpl->next->next;
	    else {
	      bpl = bpl->next;
	    }
	  }
	}
      }
      strcpy(se_db_last_command,"B");
      goto loop1;
    }
    else if (cc == 'H') {
      se_db_OnLineMoreHelp();
      strcpy(se_db_last_command,"?");
      goto loop1;
    }
    else if (cc == 'T') {
      fprintf(SE_ERR,"Switching to trace mode may create a huge \"trace.se\" file.\n");
      if (breakpoints != NULL) {
	fprintf(SE_ERR,"Switching to the file trace mode disables breakpoints.\n");
      }
      if (se_db_YesOrNo("Write the execution trace in \"trace.se\" file",0)) {
	se_db_ModeFlag = SE_DB_TraceFile;
	if (se_db_TraceFile == NULL) {
	  se_db_TraceFile = fopen("trace.se","w");
	}
      }
    }
    else {
      fprintf(SE_ERR,"sedb: \"%s\": unknown command. Type ? for help.\n",line);
      goto loop1;
    }
  }
  else if (se_db_ModeFlag == SE_DB_StepCommand) {
    se_db_ShowPoint(ds,li,co,fi);
    strcpy(se_db_last_command,"s");
    se_db_ModeFlag = SE_DB_WaitingCommand;
    goto loop1;
  }
  else if (se_db_ModeFlag == SE_DB_NextCommand) {
    if ((ds == previous_ds) || (previous_ss > se_stack_size(ds))) {
      se_db_ShowPoint(ds,li,co,fi);
      strcpy(se_db_last_command,"n");
      se_db_ModeFlag = SE_DB_WaitingCommand;
      goto loop1;
    }
    else {
      return ;
    }
  }
  else if (se_db_ModeFlag == SE_DB_UpCommand) {
    if (previous_ss > se_stack_size(ds)) {
      se_db_ShowPoint(ds,li,co,fi);
      strcpy(se_db_last_command,"f");
      se_db_ModeFlag = SE_DB_WaitingCommand;
      goto loop1;
    }
    else {
      return ;
    }
  }
  else if (se_db_ModeFlag == SE_DB_NotInitialized) {
    se_db_ModeFlag = SE_DB_WaitingCommand;
    strcpy(se_db_last_command,"?");
    se_db_OnLineHelp();
    se_trace(ds,position);
    return ;
  }
  else {
    fprintf(SE_ERR,"Internal error#2 in (sedb).\n");
    fprintf(SE_ERR,"Unknown se_db_ModeFlag: %d.\n",se_db_ModeFlag);
    exit (EXIT_FAILURE);
  }
}
void se_db_signal_handler(int sig) {
  if (se_db_YesOrNo("Really exit",0)) {
    if (se_db_TraceFile != NULL) {
      fclose(se_db_TraceFile);
    }
    exit(EXIT_SUCCESS);
  }
  else {
    se_db_SignalHandlerFlag = 1;
    se_db_ModeFlag = SE_DB_WaitingCommand;
    fprintf(SE_ERR,"Back to sedb.\n");
    fflush(SE_ERR);
  }
}

void se_db_back(se_dump_stack*ds,se_position p) {
  fprintf(SE_ERR,"Back to sedb (stopped at the error point).\n");
  if (ds == NULL) {
    fprintf(SE_ERR,"Internal error #1 in (sedb).\n");
    se_print_run_time_stack();
    exit(EXIT_FAILURE);
  }
  if (p == 0) p = ds->p;
  se_db_ModeFlag = SE_DB_WaitingCommand;
  strcpy(se_db_last_command,".");
  se_trace(ds,p);
}

void sedb_breakpoint(se_dump_stack*ds) {
  se_db_ShowPointFrame(ds);
  fprintf(SE_ERR,"*** sedb_breakpoint encountered.\n");
  se_db_ModeFlag = SE_DB_WaitingCommand;
  strcpy(se_db_last_command,".");
  se_trace(ds,ds->p);
}

#endif
