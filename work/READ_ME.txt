READ_ME.txt file of the "SmartEiffel/work" directory.
------------------------------------------------------------------------------
Congratulations!

If you read this file, you are probably a member of the SmartEiffel
team or someone who got the read/write permission on the SVN server of
the SmartEiffel project.

Here are some hints and some informations about the "SmartEiffel/work"
directory.

First, keep in mind that the "SmartEiffel/work" directory is not part of
the SmartEiffel delivery. The "SmartEiffel/work" directory is, at time 
being, used to store various scripts as well as some useful files for
our web site ("SmartEiffel/work/html").

About SVN, here is the typical information one is supposed to have in
his/her .profile bash init file:

export SVNEDITOR="emacs "


Note that we are all using GNU emacs, not only because it is a
wonderful editor, but also because we are all using the same emacs
Eiffel ("SmartEiffel/misc/eiffel.el"). By doing so, the text file layout
is common and it is more convenient to watch files diff.

Here is the typical information one is supposed to have in
his/her .emacs init file where <PATH> is the path (terminated with /)
of the directory that contains the eiffel.el (typically "SmartEiffel/misc/"):

(setq load-path  (cons (expand-file-name "<PATH>") load-path))
(add-to-list 'auto-mode-alist '("\\.e\\'" . eiffel-mode))
(autoload 'eiffel-mode "eiffel" "Major mode for Eiffel programs" t)


For an easy install, I suggest you first to install a public delivery
of SmartEiffel on your favorite Unix box in your home directory:

    You need some public rsa key: ssh-keygen -t rsa (if not already done)
    The key is in: .ssh/id_rsa.pub
    Register your key at: http://gforge.inria.fr/account/editsshkeys.php
    Wait the key is registered as explained.

    cd
    svn checkout svn+ssh://developername@scm.gforge.inria.fr/svn/smarteiffel/trunk SmartEiffel

Then, just "cd SmartEiffel" and type "make" and .... wait (if you like
coffe, it is a good idea to have it during install which is quite long).

Now you are ready to work. The most important shell scripts are
   "SmartEiffel/work/tools/bootstrap.csh" 
and
   "SmartEiffel/work/tools/run_all_test.csh".
Actually, "bootstrap.csh" recompile the compiler with itself three times and test the stability.
As the nane "run_all_test.csh" says, this script launches the whole test suite, mostly by using
the "se test" tools. You must be patient because a complete execution in -boost mode of 
"run_all_test.csh" is about 12 hours on a 2 GHZ machine. 
Well, favorite commands of a SmartEiffel developper are:

    SmartEiffel/work/tools/bootstrap.csh -boost
and, before leaving the office:
    SmartEiffel/work/tools/run_all_test.csh -boost

When we are in a stable status (i.e. just before a plain release or
also before a beta release), the output allows you to be sure at a
glance that all non-regression tests are passed. By reading the
"run_all_test.csh" file, you will also learn how to call separately various
components of the non-regression test suite.

If you have problem to get started, keep in mind that you have a germ
of command "compile_to_c" in directory "SmartEiffel/work/germ"
(details below).

Also note the "SmartEiffel/work/tools/VerifyAndStripTools" script,
which compiles all the SmartEiffel commands and prepares the release
germ (the one in the "SmartEiffel/install/germ" directory).

Also keep in mind that when you fix a SmartZilla bug, the message
information of the SVN commit must indicate the number of the
SmartZilla bug using our SZ:* notation. As an example, if your commit
is supposed to fix the SmartZilla bug #1789, the message must contains
the string "SZ:1789". The message can be for example:

    "To fix the SZ:1789 bug report ... blah blah ..."

The snapshot script transforms such a pattern into a link to the
SmartZilla entry.

Note: currently most scripts are written with csh. Thus you need csh
to be installed on your machine.

-- WHEN YOU CHANGE THE GERM

If you change the germ (because your changes are such that an old germ
cannot bootstrap the new compiler), you must do the following steps:

    cd SmartEiffel/work/germ
    se c2c new_germ.ace
    cvs commit compile_to_c.[ch]

-- WHEN THE GERM HAS CHANGED

If someone changed the germ, you need to recompile all the tools:

    cd SmartEiffel/work/germ
    ./new_compile_to_c_from_germ.sh
    cd ../tools
    ./VerifyAndStripTools
    cd ../..
    make

------------------------------------------------------------------------------
Please, feel free to complete this attempt of internal documentation.
