class BAD_CALL01

creation make

feature

   make is
      local
	 i: INTEGER;
      do
	 i := foo;
      end;

   foo: INTEGER is
      do
	 Result := foo;
      end;

end
