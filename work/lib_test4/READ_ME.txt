-- Part of SmartEiffel -- Read DISCLAIMER file -- Copyright (C) 
-- Dominique COLNET and Suzanne COLLIN -- colnet@loria.fr
--
Directory "lib_test4" is a set of bad programs used to check 
the quality of -boost errors/warnings messages printed by SmartEiffel.
Those bad programs are to be compiled only using -boost mode.
