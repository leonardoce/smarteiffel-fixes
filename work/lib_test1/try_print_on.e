class TRY_PRINT_ON

creation 
   make
   
feature
   
   cat: CAT
   
   point: LIB_TEST_POINT
   
   make is
      do
			io.put_string("-- TRY_PRINT_ON ----------------------------------------%N")
			io.put_string("'a'.print_on(io)=")
			'a'.print_on(io)
			io.put_new_line
	 
			io.put_string("1.print_on(io)=")
			1.print_on(io)
			io.put_new_line
			
			io.put_string("{INTEGER_16 1}.print_on(io)=")
			{INTEGER_16 1}.print_on(io)
			io.put_new_line
			
			io.put_string("1.5.print_on(io)=")
			1.5.print_on(io)
			io.put_new_line
			
			io.put_string("1.5.force_to_real_32.print_on(io)=")
			1.5.force_to_real_32.print_on(io)
			io.put_new_line
	 
			io.put_string("%"foo :-)%".print_on(io)=")
			"foo :-)".print_on(io)
			io.put_new_line

			io.put_string("io.put_integer(%"foo :-)%".count)=")
			io.put_integer("foo :-)".count)
			io.put_new_line

			io.put_string("(once %"foo :-)%").print_on(io)=")
			(once "foo :-)").print_on(io)
			io.put_new_line

			io.put_string("io.put_integer((once %"foo :-)%").count)=")
			io.put_integer((once "foo :-)").count)
			io.put_new_line
			
			io.put_string("True.print_on(io): ")
			True.print_on(io)
			io.put_new_line

			io.put_string("False.print_on(io): ")
			False.print_on(io)
			io.put_new_line

			io.put_string("print((False).to_string)=")
			print(False.to_string)
			io.put_new_line
			
			io.put_string("print((True).to_string)=")
			print(True.to_string)
			io.put_new_line
			
			io.put_string("'a'.print_on(io)=")
			'a'.print_on(io)
			io.put_new_line

			io.put_string("print('a'.to_string)=")
			print('a'.to_string)
			io.put_new_line

			io.put_string("print(0.to_string)=")
			print(0.to_string)
			io.put_new_line

			io.put_string("print(1.to_string)=")
			print(1.to_string)
			io.put_new_line
			
			io.put_string("print(1.5.to_string)=")
			print(1.5.to_string)
			io.put_new_line

			io.put_string("print(1.5.force_to_real_32.to_string)=")
			print(1.5.force_to_real_32.to_string)
			io.put_new_line

			io.put_string("print(%"foo%")=")
			print("foo")
			io.put_new_line

			io.put_string("-- TRY_PRINT_ON (done.) --------------------------------%N")
      end
   
end -- TRY_PRINT_ON
