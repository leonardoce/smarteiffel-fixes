class TRY_ANIMAL
   
creation make
   
feature
   
   x: ANIMAL
   
   make is
      local 
			dog: DOG; cat: CAT; mille_pattes: MILLE_PATTES
			animal: ANIMAL
      do
			create dog
			create cat
			create mille_pattes
			un("cat",cat)
			animal := (cat).twin
			un("cat",animal)
			animal := (dog).twin
			un("dog",animal)
			animal := (mille_pattes).twin
			un("rien du tout",animal)
			
			!MILLE_PATTES!animal
			animal := (animal).twin
			un("mille pattes",animal)
			
			if dog.is_equal(dog) then
				std_output.put_string("Good :-)%N")
			else
				std_output.put_string("Bad :-(%N")
			end
			if animal.is_equal(animal) then
				std_output.put_string("Good :-)%N")
			else
				std_output.put_string("Bad :-(%N")
			end
			if animal.is_equal(animal) then
				std_output.put_string("Good :-)%N")
			else
				std_output.put_string("Bad :-(%N")
			end
      end
   
   un(quoi: STRING; a: ANIMAL) is
      do
			std_output.put_string("Here is a ")
			std_output.put_string(quoi)
			std_output.put_string(": ")
			if a /= Void then
				a.cry
			end
			std_output.put_new_line
      end
   
end
