class TESTARGS
--
-- Test Access to command-line arguments :
--

insert
	ARGUMENTS
   
creation make
   
feature 
   
   make is
      do
	 if argument_count = 0 then
	    if not argument(0).has_suffix("testargs") then
	       std_output.put_string("TESTARGS error #1%N")
	    end
	 elseif argument_count = 1 then
	    if not argument(0).has_suffix("testargs") then
	       std_output.put_string("TESTARGS error #2.0%N")
	    end
	    if not ("1").is_equal(argument(1)) then
	       std_output.put_string("TESTARGS error #2.1%N")
	    end
	 elseif argument_count = 2 then
	    if not argument(0).has_suffix("testargs") then
	       std_output.put_string("TESTARGS error #3.1%N")
	    end
	    if not ("2").is_equal(argument(1)) then
	       std_output.put_string("TESTARGS error #3.1%N")
	    end
	    if not ("arg2").is_equal(argument(2)) then
	       std_output.put_string("TESTARGS error #3.2%N")
	    end
	 else
	    std_output.put_string("TESTARGS error #4%N")
	 end
	 if argument_count /= command_arguments.upper then
	    std_output.put_string("TESTARGS error #5%N")
	 end
      end
   
end -- TESTARGS
