-- This is the READ_ME.txt file of SmartEiffel depot --
-- How to make a debian archive --

1. Setting up the skeleton of the archive

	In the « SmartEiffel » folder :

		$ cd Smarteiffel
		$ dh_make

	When the question
	
		Type of package: single binary, multiple binary, library, or kernel module?
		[s/m/l/k]
	
	We have to type « s » because our archive will be a single archive (single binary)
	dh_make print us some informations from our operating system :
	
		Maintainer name : remin
		Email-Address   : remin@unknown 
		Date            : Thu, 27 Mar 2008 23:01:51 +0100
		Package Name    : smarteiffel
		Version         : 2.4
		License         : blank
		Type of Package : Single
		Hit <enter> to confirm: 
	
	Press Enter
	
	A folder named « debian » has been created, it contains some files.
	We have to clean this folder in aim to keep only useful files for building the debian archive.
	
		$ rm -f debian/*.ex debian/*.EX debian/README.Debian debian/docs



2. - Changes in the file « debian/control »

	We have to write the identity of the package. The most simply solution consist in completing the existing file:

		Source: smarteiffel
		Section: unknown
		Priority: extra
		Maintainer: remin <remin@unknown>
		Build-Depends: debhelper (>= 5), autotools-dev
		Standards-Version: 3.7.2

		Package: smarteiffel
		Architecture: any
		Depends: ${shlibs:Depends}, ${misc:Depends}
		Description: <insert up to 60 chars description>
		 <insert long description, indented with spaces>
	
	The field named section is used to range packages. We have to choose the « devel » section.
	Priority is optional
	The field maintainer is describe who maintain the package.
	We have to write which are the depends of the package
	Finally we have to write a short and a long description of the package. The « . » character make separations between the differents paragraphs.


3. - Changes in the file « debian/changelog »

	We have to precise the history of the package.
	This file will change le name of the package because it names precisely the version number of the package.
	
		smarteiffel (2.4-1) unstable; urgency=low

		  * Initial release (Closes: #nnnn)  <nnnn is the bug number of your ITP>

		 -- remin <remin@unknown>  Thu, 27 Mar 2008 23:08:41 +0100
	
	
	A norm describe how to determine a version number, we concat the version number of the software with a « - » then the number version of the package, « unbuntu » and the version number of ubuntu.
	We have : *<upstream_version>-<debian_version>ubuntu<ubuntu_version>*.

	The « * » character precedes each entry of this file and describe a new version of the package. It mus be indicate the number version of the package, the author of the package, the date of packaging and at least a detail of the changes in the new version.


4. - The file « debian/compat »

	The file contains a single character: « 5 ».
	It seems to be really unsignificant but this file is requested for building the package.
	It describe which compatibility use debhelpers.
	The « 5 » compatibility is the last and we want our package will be compatible.
	
	
5. - Changes in the file « debian/copyright »

	We have to write the copyrigths of the package in aim to let users know what are the conditions for the use of the package.

	We just have to complete the existing file:

		This package was debianized by remin <remin@unknown> on
		Thu, 27 Mar 2008 23:08:41 +0100.

		It was downloaded from <url://example.com>

		Upstream Author(s): 

			<put author's name and email here>
			<likewise for another author>

		Copyright: 

			<Copyright (C) YYYY Name OfAuthor>
			<likewise for another author>

		License:

			<Put the license of the package here indented by 4 spaces>

		The Debian packaging is (C) 2008, remin <remin@unknown> and
		is licensed under the GPL, see `/usr/share/common-licenses/GPL'.

		# Please also look if there are files or directories which have a
		# different copyright/license attached and list them here.

	A file named « COPYING » exists and contains copyrigths and license of SmartEiffel.


6. - The file « debian/dirs »

	This file used by dh_installdirs makes sudirectory during the installation.
	We have to replace his content by « /usr/local/lib/smarteiffel ».


7. - Changes in the file « debian/rules »

	This file is the most important for building the package. I's written as a Makefile and contains some requested targets.
	The « install » target installs all files of the package in the temporary folder « debian ».
	smarteiffel/.
	
	Our installation method make us delete the target « configure.status ». This file run ./configure and the configure in SmartEiffel print just what are the files needed for building.
	Needed librairies are installed by using dependancies.
	
	The cible « build-stamp » isn't needed for the package.
	
	We have now to change the target « install ». We have to make the folder «  debian/smarteiffel/user/local » in which we will copy all the smarteiffel folder except the « debian » folder. Finally we must change « prefix » in « USRDIR » and do this modification in the « make_release.sh » file in aim to install all files in the folder « debian/smarteiffel/usr ».
	
	We have :
	
		install: build
				dh_testdir
				dh_testroot
				dh_clean -k
				dh_installdirs
	
		mkdir debian/smarteiffel/usr/local
	
		cp configure COPYING GNU_LICENSE index.html install.exe Makefile make_release.sh READ_ME.txt debian/smarteiffel/usr/local
		cp -R bin/ contrib/ grand_book/ install/ lib/ man/ misc/ short/ sys/ test_suite/ tools/ tutorial/ work/ debian/smarteiffel/usr/local
		# In a future version, the 2 preceding lines can be replace by this : rsync -r * debian/smarteiffel/usr/local/bin/smarteiffel/ --exclude=debian/
		# Add here commands to install the package into debian/smarteiffel.
		$(MAKE) USRDIR=$(CURDIR)/debian/smarteiffel/usr install

	
	Finally we have to edit the Makefile in aim it run with new parameters.

8. - Changes in the file « make_release.sh »

	Files in the debain folder are ready for building the package.
	We have to edit the file « make_release.sh » in aim to install files in the good folder.
	
	We have to do that:
	
	1st:
	
			do_install() {
				PREFIX=${USRDIR:-/usr/local}
				BIN=${PREFIX}/bin

		will be:

			do_install() {
				PREFIX=${USRDIR:-/usr}/local
				BIN=${PREFIX}/bin
				
	2nd:

			LIB=`pwd`
			export LIB
			DOC=$LIB
			export DOC
			generate_default_config

		will be:

			LIB=$USRDIR/local #`pwd`
			export LIB
			DOC=$LIB
			export DOC
			generate_default_config
	
	3th:

		edit line:	
			SE_APP=/usr/local/lib/smarteiffel
		
		before:
	
			progress 30 0 3 vanilla.se
			test -r $SmartEiffel/vanilla.se || cat > ${SmartEiffel}/vanilla.se <<EOF
	
	4th:

			[General]
			bin: ${SE_BIN}
			sys: ${SE_SYS}
			short: ${SE_SHORT}
			os: $OS
			flavor: $flavor
			tag: 3
			jobs: 4

			[Environment]
			path_lib: ${SE_LIB}

			[Loadpath]
			lib: \${path_lib}loadpath.se

		will be:
				
			[General]
			bin: ${SE_APP}/bin/
			sys: ${SE_APP}/sys/
			short: ${SE_APP}/short/
			os: $OS
			flavor: $flavor
			tag: 3
			jobs: 4

			[Environment]
			path_lib: ${SE_APP}/lib/

			[Loadpath]
			lib: \${path_lib}loadpath.se

	5th:
	
			[Environment]
			path_tools: ${SE_TOOLS}

			[Loadpath]
			tools: \${path_tools}loadpath.se

		will be:
		
			[Environment]
			path_tools: ${SE_APP}/tools/

			[Loadpath]
			tools: \${path_tools}loadpath.se

	6th:
	
			[Environment]
			path_tutorial: ${SE_TUTORIAL}


			[Loadpath]
			tutorial: \${path_tutorial}loadpath.se

		will be:
		
			[Environment]
			path_tutorial: /usr/local/doc/smarteiffel/tutorial/

			[Loadpath]
			tutorial: \${path_tutorial}loadpath.se

	cf install/package/debian/make_release.sh

9. - Building the package

	All files are ready to build the package. We just have to use « dpkg-buildpackage » to run the building. The « -rfakeroot » option let us to build the package without the admin rights:

	$ sudo dpkg-buildpackage -rfakeroot
