#!/bin/sh
#
# Simple script to build SmartEiffel without using the ugly interactive installer
#
# Usage:
# make_release.sh [build|install] {plain}
#

cd `dirname $0`
LOG=`pwd`/make_release.log
export LOG

C2C_OPTIONS=${C2C_OPTIONS:-'-boost -no_gc -verbose'}
EXE_SUFFIX=''
OS=UNIX
if test -x /usr/bin/cygpath.exe; then
    # Cygwin specific
    EXE_SUFFIX=".exe"
    OS=Cygwin
    flavor=generic
else
    flavor=`uname -s`
fi

# Tools using the AST. Those tools are split. Does not include compile_to_c.
LARGE_TOOLS="short class_check pretty eiffeldoc finder extract_internals" # compile_to_jvm

# Small helper tools. Those tools are not split. Does not include se, compile, clean.
HELPER_TOOLS="eiffeltest ace_check" # print_jvm_class

if [ "$2" = plain ]; then
    plain=TRUE
else
    plain=FALSE
    eval `tset -s`
    tput init
fi

bold() {
    test $plain != TRUE && tput bold
}

normal() {
    test $plain != TRUE && tput sgr0
}

italic() {
    test $plain != TRUE && tput sitm
}

underline() {
    test $plain != TRUE && tput smul
}

foreground() {
    if test $plain = FALSE; then
	case $1 in
	    black)
		tput setaf 0
		;;
	    red)
		tput setaf 1
		;;
	    green)
		tput setaf 2
		;;
	    yellow)
		tput setaf 3
		;;
	    blue)
		tput setaf 4
		;;
	    magenta)
		tput setaf 5
		;;
	    cyan)
		tput setaf 6
		;;
	    white)
		tput setaf 7
		;;
	esac
    fi
}

background() {
    if test $plain = FALSE; then
	case $1 in
	    black)
		tput setab 0
		;;
	    red)
		tput setab 1
		;;
	    green)
		tput setab 2
		;;
	    yellow)
		tput setab 3
		;;
	    blue)
		tput setab 4
		;;
	    magenta)
		tput setab 5
		;;
	    cyan)
		tput setab 6
		;;
	    white)
		tput setab 7
		;;
	esac
    fi
}

title() {
    if test $plain = TRUE; then
	echo "$1"
    else
	tput el
	foreground blue
	bold
	echo "$1"
	foreground black
	normal
    fi
}

show_status() {
    if test $plain = TRUE; then
	if test $1 = 0; then
	    echo "    OK"
	else
	    echo "    KO"
	fi
    else
	tput el
	if test $1 = 0; then
	    tput setaf 2
	    echo "    OK"
	else
	    tput bold
	    tput setaf 1
	    echo "    KO"
	fi
	tput setaf 0
	tput sgr0
	echo
    fi
}

progress() {
    label="$4"
    if test $plain = TRUE; then
	echo "$label"
    else
	col=`expr \`tput cols\` - $1 - 11`
	tput setaf 0
	tput sgr0
	size=$1
	current=$2
	max=$3
	awk 'BEGIN {
                fill=int('$size' * '$current' / '$max' + .5);
                printf(" '`tput bold`'%3.1f%%'`tput sgr0`'\t'`tput setab 6`'", 100*'$current'/'$max');
                for (i=0;    i<fill; i++) printf(" ");
                printf("'`tput setab 4`'");
                for (i=fill; i<'$size'; i++) printf(" ");
                printf("'`tput sgr0`' '`tput setaf 5`'%-'$col'.'$col's'`tput sgr0`' \r", "'"$label"'");
                exit;
            }' </dev/null >/dev/tty
    fi
}

error() {
    if test $plain = FALSE; then
	tput el
	tput setaf 1
	tput bold
    fi
    echo "$2 failed with status $1"
    echo "Please look at $LOG"
    if test $plain = FALSE; then
	tput setaf 0
	tput sgr0
    fi
}

run() {
    echo "$@" >> $LOG
    if eval "$@" >>$LOG 2>&1; then
	status=0
    else
	s=$?
	error $s "$1"
	status=1
    fi
    return $status
}

ok() {
    return
}

# ---------------------------------------------------------------------------------------------------------- #

generate_default_config() {
    title "Generating configuration in $SmartEiffel"

    if [ ! -d $SmartEiffel ]; then
	if [ -e $SmartEiffel ]; then
            mkdir ${SmartEiffel}.dir
            mv ${SmartEiffel} ${SmartEiffel}.dir/vanilla.se
            mv ${SmartEiffel}.dir $SmartEiffel
	else
            mkdir -p $SmartEiffel
	fi
    fi

    SE_BIN=${SE_BIN:-$LIB/bin/}
    SE_SYS=${SE_SYS:-$LIB/sys/}
    SE_SHORT=${SE_SHORT:-$LIB/short/}
    SE_TUTORIAL=${SE_TUTORIAL:-$DOC/tutorial/}
    SE_TOOLS=${SE_TOOLS:-$LIB/tools/}
    SE_LIB=${SE_LIB:-$LIB/lib/}

    SE_BIN=`echo $SE_BIN/|sed 's/\/*\/$/\//'`
    SE_SYS=`echo $SE_SYS/|sed 's/\/*\/$/\//'`
    SE_SHORT=`echo $SE_SHORT/|sed 's/\/*\/$/\//'`
    SE_TUTORIAL=`echo $SE_TUTORIAL/|sed 's/\/*\/$/\//'`
    SE_TOOLS=`echo $SE_TOOLS/|sed 's/\/*\/$/\//'`
    SE_LIB=`echo $SE_LIB/|sed 's/\/*\/$/\//'`
	
	SE_APP=/usr/local/lib/smarteiffel
	
    progress 30 0 3 vanilla.se
    test -r $SmartEiffel/vanilla.se || cat > ${SmartEiffel}/vanilla.se <<EOF
[General]
bin: ${SE_APP}/bin/
sys: ${SE_APP}/sys/
short: ${SE_APP}/short/
os: $OS
flavor: $flavor
tag: 3
jobs: 4

[Environment]
path_lib: ${SE_APP}/lib/

[Loadpath]
lib: \${path_lib}loadpath.se

[Tools]
c: compile
c2c: compile_to_c
clean: clean
pretty: pretty
short: short
find: finder
ace_check: ace_check
class_check: class_check
doc: eiffeldoc
test: eiffeltest
x_int: extract_internals

[boost]
c_compiler_type: gcc
c_compiler_options: -pipe -Os
cpp_compiler_type: g++
cpp_compiler_options: -pipe -Os

[no_check]
c_compiler_type: gcc
c_compiler_options: -pipe -O1
cpp_compiler_type: g++
cpp_compiler_options: -pipe -O1

[require_check]
c_compiler_type: gcc
c_compiler_options: -pipe
cpp_compiler_type: g++
cpp_compiler_options: -pipe

[ensure_check]
c_compiler_type: gcc
c_compiler_options: -pipe
cpp_compiler_type: g++
cpp_compiler_options: -pipe

[invariant_check]
c_compiler_type: gcc
c_compiler_options: -pipe
cpp_compiler_type: g++
cpp_compiler_options: -pipe

[loop_check]
c_compiler_type: gcc
c_compiler_options: -pipe
cpp_compiler_type: g++
cpp_compiler_options: -pipe

[all_check]
c_compiler_type: gcc
c_compiler_options: -pipe
cpp_compiler_type: g++
cpp_compiler_options: -pipe

[debug_check]
c_compiler_type: gcc
c_compiler_options: -pipe -g
cpp_compiler_type: g++
cpp_compiler_options: -pipe -g
smarteiffel_options: -no_strip

[release]
c_compiler_type: gcc
c_compiler_options: -pipe -O3 -fomit-frame-pointer
cpp_compiler_type: g++
cpp_compiler_options: -pipe -O3 -fomit-frame-pointer
smarteiffel_options: -no_split
EOF

    progress 30 1 3 tools.se
    test -r $SmartEiffel/tools.se || cat > ${SmartEiffel}/tools.se <<EOF
[Environment]
path_tools: ${SE_APP}/tools/

[Loadpath]
tools: \${path_tools}loadpath.se
EOF

    progress 30 2 3 doc.se
    test -r $SmartEiffel/doc.se || cat > ${SmartEiffel}/doc.se <<EOF
[Environment]
path_tutorial: /usr/local/doc/smarteiffel/tutorial/

[Loadpath]
tutorial: \${path_tutorial}loadpath.se
EOF

    progress 30 3 3 java.se
#     test -r $SmartEiffel/java.se || cat > ${SmartEiffel}/java.se <<EOF
# [Tools]
# java: compile_to_jvm
# javap: print_jvm_class
# 
# [Java]
# jar: jar
# jvm: java
# java_compiler: javac
# EOF

    show_status 0
}

# ---------------------------------------------------------------------------------------------------------- #

do_build() {
    echo

    title "SmartEiffel `grep 'release_number:' tools/smart_eiffel.e | awk -F'\"' '{print $2}' | sed 's/%N/\n/g'`"
    echo

    se=$SmartEiffel
    if [ `id | sed -n 's/^[^0-9]*\([0-9]*\).*$/\1/p'` = 0 ]; then
	echo
	echo '*** Installing SmartEiffel as root'
	echo
	SmartEiffel=${SmartEiffel:-/etc/serc}
    else
	SmartEiffel=${SmartEiffel:-$HOME/.serc}
    fi
    export SmartEiffel
    if [ "$SmartEiffel" != "$se" -a "$SmartEiffel" != /etc/serc -a "$SmartEiffel" != $HOME/.serc ]; then
	foreground red
	echo
	echo '*** The SmartEiffel configuration directory is not standard.'
	echo '*** To use SmartEiffel, you will have to set the SmartEiffel variable to'
	echo '*** "'"$SmartEiffel"'".'
	echo
	foreground black
    fi
    LIB=$USRDIR/local #`pwd`
    export LIB
    DOC=$LIB
    export DOC
    generate_default_config
    if [ ! -r $SmartEiffel/vanilla.se ]; then
	foreground red
	echo
	echo '*** The configuration directory has not been correctly generated!'
	echo "*** ($SmartEiffel)"
	echo '*** Please download a fresh SmartEiffel tarball of svn release and try again.'
	echo
	foreground black
	exit 1
    fi

    if [ -x bin/compile_to_c$EXE_SUFFIX ]; then
	if  [ -f work/germ/compile_to_c.c ]; then
	    cat > bin/fstat.c <<EOF
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

main() {
  struct stat bin, work;
  if (!stat("work/germ/compile_to_c.c", &work) && !stat("bin/compile_to_c$EXE_SUFFIX", &bin)) {
    if (work.st_atime < bin.st_atime)
      return 0;
  }
  return 1;
}
EOF
	    gcc -o bin/fstat$EXE_SUFFIX bin/fstat.c
	    rm -f bin/fstat.c
	    if bin/fstat$EXE_SUFFIX; then
		foreground magenta
		echo "Using the existing compile_to_c as bootstrap compiler"
		foreground black
		echo
	    else
		title "Bootstrapping compile_to_c from the subversion germ"
		run gcc -pipe -O0 -lm -o bin/compile_to_c$EXE_SUFFIX work/germ/compile_to_c.c
		show_status $?
	    fi
	    rm -f bin/fstat$EXE_SUFFIX
	else
	    foreground magenta
	    echo "Using the existing compile_to_c as bootstrap compiler"
	    foreground black
	    echo
	fi
    elif [ -f work/germ/compile_to_c.c ]; then
	title "Bootstrapping compile_to_c from the subversion germ"
	run gcc -pipe -O0 -lm -o bin/compile_to_c$EXE_SUFFIX work/germ/compile_to_c.c
	show_status $?
    elif [ -f install/germ/compile_to_c.h ]; then
	title "Bootstrapping compile_to_c from the install germ"
	run gcc -pipe -O0 -lm -o bin/compile_to_c$EXE_SUFFIX install/germ/compile_to_c*.c
	show_status $?
    else
	foreground red
	echo
	echo "*** Can't find a working compiler for bootstrapping."
	echo '*** Please download a fresh SmartEiffel tarball of svn release and try again.'
	echo
	foreground black
	exit 1
    fi

    cd bin/

    title "Building the compiler"
    progress 30 0 10 compile_to_c
    run ./compile_to_c$EXE_SUFFIX "$@" $C2C_OPTIONS compile_to_c -o compile_to_c$EXE_SUFFIX
    s=$?
    n=`grep -v '^#' ./compile_to_c.make | wc -l`
    i=8
    n=`expr $n "*" 10 + 8`
    grep -v '^#' ./compile_to_c.make | while read line; do
    	progress 30 $i $n "$line"
    	run $line
	s=`expr $s + $?`
    	i=`expr $i + 8`
    done
    progress 30 8 10 "Saving install germ"
    rm -f ../install/germ/compile_to_c*
    cp compile_to_c*.[ch] ../install/germ/

    progress 30 8 10 compile
    run ./compile_to_c$EXE_SUFFIX "$@" $C2C_OPTIONS -no_split compile -o compile$EXE_SUFFIX
    s=`expr $s + $?`
    n=`grep -v '^#' ./compile.make | wc -l`
    i=`expr $n "*" 8 + 1`
    n=`expr $n "*" 10 + 1`
    grep -v '^#' ./compile.make | while read line; do
    	progress 30 $i $n "$line"
    	eval "$line"
    	i=`expr $i + 1`
    done

    progress 30 9 10 clean
    run ./compile_to_c$EXE_SUFFIX "$@" $C2C_OPTIONS -no_split clean -o clean$EXE_SUFFIX
    s=`expr $s + $?`
    n=`grep -v '^#' ./clean.make | wc -l`
    i=`expr $n "*" 9 + 1`
    n=`expr $n "*" 10 + 1`
    grep -v '^#' ./clean.make | while read line; do
    	progress 30 $i $n "$line"
    	run $line
	s=`expr $s + $?`
    	i=`expr $i + 1`
    done

    progress 30 3 3 'Cleaning...'
    run ./clean$EXE_SUFFIX compile_to_c
    s=`expr $s + $?`
    run ./clean$EXE_SUFFIX compile
    s=`expr $s + $?`
    run ./clean$EXE_SUFFIX clean
    s=`expr $s + $?`

    show_status $s

    title "Building large tools (using the SmartEiffel parser and AST)"
    n=`echo $LARGE_TOOLS | awk '{printf("%d\n",NF)}'`
    i=0
    s=0
    for exe in $LARGE_TOOLS; do
    	progress 30 $i $n $exe
    	run ./compile$EXE_SUFFIX "$@" $C2C_OPTIONS -clean $exe -o $exe$EXE_SUFFIX
	s=`expr $s + $?`
    	i=`expr $i + 1`
    done

    show_status $s

    title "Building helper tools"
    n=`expr \`echo $HELPER_TOOLS | awk '{printf("%d\n",NF)}'\` + 1`
    i=0
    s=0
    for exe in se $HELPER_TOOLS; do
    	progress 30 $i $n $exe
    	run ./compile$EXE_SUFFIX "$@" $C2C_OPTIONS -no_split -clean $exe -o $exe$EXE_SUFFIX
	s=`expr $s + $?`
    	i=`expr $i + 1`
    done

    show_status $s

    title "Generating API docs"
    cd ../work/html/site
    if test $plain = TRUE; then
	run ./new_libs
	s=$?
    else
	tput sgr0
	tput setaf 5
	col=`expr \`tput cols\` - 8` # remove so many characters because SmartEiffel outputs tabs which count as one char only for awk
	if ./new_libs 2>>$LOG | tee -a $LOG | awk '{printf("'"`tput el`"'%-'${col}.${col}'s\r", $0)}'; then
	    s=0
	else
	    s=$?
	    error $s ./new_libs
	fi
    fi

    show_status $s
}

do_install() {
    PREFIX=${USRDIR:-/usr}/local
    BIN=${PREFIX}/bin
    LIB=${PREFIX}/lib/smarteiffel
    DOC=${DOCDIR:-${PREFIX}/doc}/smarteiffel
    CONF=${ETCDIR:-${USRDIR:-}/etc}
    SmartEiffel=${CONF}/serc
    export SmartEiffel
    if [ -n "$SE_PREFIX" ]; then
	# useful for boxed installs
	SE_BIN=${SE_BIN:-${LIB#$SE_PREFIX}/bin/}
	SE_SYS=${SE_SYS:-${LIB#$SE_PREFIX}/sys/}
	SE_SHORT=${SE_SHORT:-${LIB#$SE_PREFIX}/short/}
	SE_TUTORIAL=${SE_TUTORIAL:-${DOC#$SE_PREFIX}/tutorial/}
	SE_TOOLS=${SE_TOOLS:-${LIB#$SE_PREFIX}/tools/}
	SE_LIB=${SE_LIB:-${LIB#$SE_PREFIX}/lib/}
    fi
    if [ $SmartEiffel != /etc/serc -a $SmartEiffel != $HOME/.serc ]; then
	foreground red
	bold
	echo
	echo '*** The SmartEiffel location is not standard.'
	echo '*** To use SmartEiffel, please set the SmartEiffel variable to'
	echo '*** "'"$SmartEiffel"'".'
	echo
	normal
	foreground black
    fi
    generate_default_config

    title "Installing programs:"
    mkdir -p ${BIN}
    mkdir -p ${LIB}/bin
    mkdir -p ${DOC}/api
    echo " - se (in ${BIN})"
    install -m 755 bin/se$EXE_SUFFIX ${BIN}/se$EXE_SUFFIX
    for exe in clean compile compile_to_c `echo $LARGE_TOOLS $HELPER_TOOLS`; do
	echo " - $exe (in ${LIB}/bin)"
	install -m 755 bin/${exe}$EXE_SUFFIX ${LIB}/bin/${exe}$EXE_SUFFIX
    done

    title "Installing libraries:"
    for dir in lib tools sys; do
	echo " - $dir (in ${LIB}/$dir)"
	n=`find ${dir}/ -name .svn -prune -o '(' -type f -print ')' | wc -l`
	i=0
	find ${dir}/ -name .svn -prune -o '(' -type f -print ')' | while read f; do
	    progress 30 $i $n `basename $f`
	    mkdir -p `dirname ${LIB}/$f`
	    install -m 644 $f ${LIB}/$f
	    i=`expr $i + 1`
	done
    done

    ok

    title "Installing data files:"
    for dir in short misc; do
	echo " - $dir (in ${LIB}/$dir)"
	n=`find ${dir}/ -name .svn -prune -o '(' -type f -print ')' | wc -l`
	i=0
	find ${dir}/ -name .svn -prune -o '(' -type f -print ')' | while read f; do
	    progress 30 $i $n `basename $f`
	    mkdir -p `dirname ${LIB}/$f`
	    install -m 644 $f ${LIB}/$f
	    i=`expr $i + 1`
	done
    done

    ok

    title "Installing doc:"
    for dir in tutorial; do
	echo " - $dir (in ${DOC}/$dir)"
	n=`find ${dir}/ -name .svn -prune -o '(' -type f -print ')' | wc -l`
	i=0
	find ${dir}/ -name .svn -prune -o '(' -type f -print ')' | while read f; do
	    progress 30 $i $n `basename $f`
	    mkdir -p `dirname ${DOC}/$f`
	    install -m 644 $f ${DOC}/$f
	    i=`expr $i + 1`
	done
    done
    cd work/html/site/export/
    for api in libraries tools; do
	echo " - $api API (in ${DOC}/api/$api)"
	n=`find $api/ '(' -name \*.html -o -name \*.css -o -name \*.js ')' -print | wc -l`
	i=0
	find $api/ '(' -name \*.html -o -name \*.css -o -name \*.js ')' -print | while read f; do
	    progress 30 $i $n `basename $f`
	    mkdir -p `dirname ${DOC}/api/$f`
	    install -m 644 $f ${DOC}/api/$f
	    i=`expr $i + 1`
	done
    done

    progress 30 1 1 index.html
    install -m 644 api.html ${DOC}/api/index.html

    ok
}

### MAIN ###

case "$1" in
    build|install)
	rm -f $LOG
	touch $LOG
	do_$1
	title "Done."
	normal
	exit 0
	;;
    *)
	foreground red
	echo "Usage: $0 [build|install]" >&2
	echo "For install, you may use the USRDIR, DOCDIR, and ETCDIR environment variables" >&2
	echo "You may also use SE_PREFIX which is removed from the above variables" >&2
	echo "when creating the configuration files." >&2
	normal
	exit 1
	;;
esac
