Format: 1.0
Source: smarteiffel
Binary: smarteiffel
Architecture: any
Version: 2.4-1Ubuntu1
Maintainer: Domique Colnet <Dominique.Colnet@loria.fr>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5), autotools-dev, build-essential
Files: 
 35a73832dd13a72d470b30eea761b7ab 11991451 smarteiffel_2.4.orig.tar.gz
 d1873609c4a08d94ae41755ff26c09b9 1190188 smarteiffel_2.4-1Ubuntu1.diff.gz
