#!/usr/bin/env bash

DIRS="contrib lib man misc short sys test_suite tools tutorial work/germ work/html/site/source work/tools"
FILES="work/html/site/new_libs work/html/site/export/api.html COPYING make_release.sh READ_ME.txt"

curdir=$(pwd)

cd $(dirname $0)
cygdir=$(pwd)
dir=$cygdir
tmp=${TMPDIR:-/tmp}/cyg-se$$
mkdir -p $tmp

# look for the SmartEiffel base directory
cd $dir/../../..
dir=$(pwd)

# compute the subversion release number
ver=$(svnversion -n lib):$(svnversion -n tools):$(svnversion -n sys):$(svnversion tutorial):$(svnversion test_suite)
ver=$(echo $ver | tr ':' '\n' | sort -n -u | awk '{if (NR==1) printf("%s",$0); else last=$0} END {printf(":%s\n",last)}' | sed 's=^:==;s=:$==')

# now build the thing. It's cygwin, hence we need a really powerful PC or a lot of time. Anyway.
packagename=smarteiffel-$ver
cleanup() {
    cd $curdir
    rm -rf $tmp
    exit 1
}
trap cleanup INT TERM QUIT
sed 's=%SRCURI%=file:///tmp/'$packagename'.tgz=' < $cygdir/smarteiffel2.cygport.template > $tmp/$packagename.cygport

if svn status | grep -q '^\? '; then
    echo "[1;31m"
    echo "*** Warning:" >&2
    echo "*** You are not using a fresh checkout but a live development directory." >&2
    echo "*** You should use a fresh svn checkout and then call cygport." >&2
    echo "[0;0m"
fi

echo
echo 'Building '$packagename' source...'
echo
cd $dir/..
files=$(for f in $DIRS $FILES; do echo $(basename $dir)/$f; done)
tar cfz $tmp/$packagename.tgz --exclude .svn $files

echo
echo 'Porting to Cygwin...'
echo
cd $tmp
cygport $packagename all > cygport.log 2>&1 || ${PAGER:-less} cygport.log

# at last, bring back pieces to the directory from which this script was called
mv $tmp/*.cygport $tmp/*.bz2 $curdir

cd $curdir
rm -rf $tmp
echo
echo 'Portage done.'
