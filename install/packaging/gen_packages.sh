#!/bin/sh

DIRS="contrib lib man misc short sys/plugins sys/runtime test_suite tools tutorial work/germ work/html/site/source work/tools"
FILES="work/html/site/new_libs work/html/site/export/api.html COPYING make_release.sh READ_ME.txt"

if test $# -ne 1
then
    echo
    echo "Usage: $0 [-D|-S|-R]" 2>&1
    echo
    exit 1
fi

curdir=$(pwd)
tmp=${TMPDIR:-/tmp}/genpak-se$$
mkdir $tmp

# look for the SmartEiffel base directory
cd $(dirname $0)/../..
dir=$(pwd)

# compute the subversion release number
ver=$(svnversion -n lib):$(svnversion -n tools):$(svnversion -n sys):$(svnversion tutorial):$(svnversion test_suite)
ver=$(echo $ver | tr ':' '\n' | sort -n -u | awk '{if (NR==1) printf("%s",$0); else last=$0} END {printf(":%s\n",last)}' | sed 's=^:==;s=:$==')

packagename=smarteiffel-$ver
cleanup() {
    cd $curdir
    rm -rf $tmp
    exit 1
}
trap cleanup INT TERM QUIT

echo
echo 'Building '$packagename' source...'
echo
cd $dir/..
files=$(for f in $DIRS $FILES; do echo $(basename $dir)/$f; done)
tar cfz $tmp/${packagename}-src.tgz --exclude .svn $files

cd $tmp
mkdir src inst
cd src
tar xfz $tmp/${packagename}-src.tgz
cd SmartEiffel
mkdir bin
export SmartEiffel=$(pwd)/sys/system.se
./make_release.sh build

export SE_PREFIX=$tmp/inst
export USRDIR=$SE_PREFIX/usr
export ETCDIR=$SE_PREFIX/etc
export DOCDIR=$SE_PREFIX/usr/share
export SmartEiffel=$ETCDIR/serc
./make_release.sh install

fakeroot checkinstall -y $1 --pkgname="smarteiffel" --pkgversion="$ver" --pkglicense="GPL and MIT/X11" --pkgsource="http://smarteiffel.loria.fr" --maintainer="smarteiffel@loria.fr" --requires="gcc" --install=no --fstrans=yes --deldoc --backup=no cp -R $tmp/inst/* /

cd $curdir
mv $tmp/smarteiffel[-_]$ver* .
#rm -rf $tmp
