
Function CheckForSpaces
 Push $R1
 Push $R2
 Push $R3
 Push $R4
 StrCpy $R1 -1
 StrCpy $R3 0
 loop:
   StrCpy $R2 $INSTDIR 1 $R1
   IntOp $R1 $R1 - 1
   StrCmp $R2 "" done
   StrCmp $R2 " " found
   StrCpy $R4 "$R2$R4"
 Goto loop
 found:
   IntOp $R3 $R3 + 1
 Goto loop
 done:
   StrCmp $R3 0 +3
   MessageBox MB_OK|MB_ICONEXCLAMATION "Error: The Installaton directory \
 has $R3 spaces.$\nPlease remove the spaces."
   Abort
 Pop $R4
 Pop $R3
 Pop $R2
 Pop $R1
FunctionEnd
