This directory contains all the material to build the "SmartEiffel.exe"
auto-extractible file (the file to install SmartEiffel on Windows).
See the "install.nsi" file to know how to proceed to rebuild a new
"SmartEiffel.exe" auto-extractible file.
