/* install.nsi
 Here is the recipe to build a new "SmartEiffel.exe" file.

 What is the "SmartEiffel.exe" file?
   - it is an auto-extractible file intended to be easy-to-use for windows
   users,
   - the lcc-win32 compiler is automatically included in the "SmartEiffel.exe"
   file and the "SmartEiffel.exe file" only install SmartEiffel together 
   with the lcc-win32 C compiler.

----------------------------------------------
 Now the recipe to build a new SmartEiffel.exe file: 
 
 Setp 1: Boot on your windows system and make sure you have NSIS intalled as 
    well as the lcc-win32 C compiler.

 Step 2: Install SmartEiffel / lcc-win32 using the classic "install.exe" file.
    Select only the lcc-win32 C compiler during this installation.
    The "SmartEiffel" variable must be set with "C:\SmartEiffel\sys\system.se".

 Step 3:
    Check the new compiler and do not forget to remove unwanted directories like
    for example directory "SmartEiffel\work" as well as ".svn" directories.
    Also check that "SmartEiffel\www\index.html" do exists.
      
 Step 4: Make sure that a copy of the "lccwin32.exe" file is in the current 
    directory together with this "install.nsi" script (i.e. copy "lccwin32.exe" 
    into "C:\SmartEiffel\install\packaging\windows\lccwin32.exe".

 Step 5: Right click the install.nsi script and select "Compile NSIS script" and
    the new "SmartEiffel.exe" file will be automatically created in "C:\SmartEiffel.exe".
*/
Name "SmartEiffel"
OutFile "C:\SmartEiffel.exe"
InstallDir "C:\SmartEiffel\"
ShowInstDetails hide
AutoCloseWindow false
BrandingText /TRIMCENTER "SmartEiffel - NSIS Install"
; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKEY_CURRENT_USER "Software\SmartEiffel" "Install_Dir"

LicenseLangString myLicenseData ${LANG_ENGLISH} "license.txt"
LicenseData $(myLicenseData)
!define ALL_USERS
!include WriteEnvStr.nsh
!include PathManipulation.nsh
!include CheckForSpaces.nsh
!include AdvReplaceInFile.nsh
!include GetParent.nsh
;--------------------------------
; Pages
Page license
Page directory "" "" "CheckForSpaces"
Page instfiles
UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------------------------------------------------
Section "SmartEiffel installation"
  SectionIn RO
  ; Set output path to the installation directory:
  SetOutPath $INSTDIR
  ; Write the installation path into the registry:
  WriteRegStr HKEY_CURRENT_USER Software\SmartEiffel "Install_Dir" "$INSTDIR"

  DetailPrint "Remove the expert installation stuff..."
!system "del /Q C:\SmartEiffel\install.exe"
!system "del /Q C:\SmartEiffel\install.h"
!system "del /Q C:\SmartEiffel\install.c"
!system "del /Q C:\SmartEiffel\make_release.sh"
!system "del /Q C:\SmartEiffel\make_release.log"
!system "del /Q C:\SmartEiffel\se_install"

  DetailPrint "Includes everything..."
  File /r c:\SmartEiffel\*.*
  ; Write the uninstall keys for Windows:
  WriteRegStr HKLM \
     "Software\Microsoft\Windows\CurrentVersion\Uninstall\SmartEiffel" \
     "DisplayName" "NSIS SmartEiffel"
  WriteRegStr HKLM \
     "Software\Microsoft\Windows\CurrentVersion\Uninstall\SmartEiffel" \
     "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM \
     "Software\Microsoft\Windows\CurrentVersion\Uninstall\SmartEiffel" \
     "NoModify" 1
  WriteRegDWORD HKLM \
     "Software\Microsoft\Windows\CurrentVersion\Uninstall\SmartEiffel" \
     "NoRepair" 1
  WriteUninstaller "uninstall.exe"  
  DetailPrint "Updating Path variable using '$INSTDIR\bin'"
  Push $INSTDIR\bin
  Call AddToPath
  ; ---
  DetailPrint "Checking that system.se is at the right place..."
  IfFileExists $INSTDIR\sys\system.se +2 0
  Abort "Internal error in installer (sys\system.se not found)"
  ; ---
  DetailPrint "Fixing file '$INSTDIR\sys\system.se'"
  Push "C:\SmartEiffel"
  Push "$INSTDIR"
  Push all
  Push all
  Push "$INSTDIR\sys\system.se"  
  Call AdvReplaceInFile
  ; ---
  DetailPrint "Checking that lccwin32.exe is in the archive..."
  IfFileExists $INSTDIR\install\packaging\windows\lccwin32.exe +2 0
  Abort "Internal error in installer (lccwin32.exe not found)"
  ; ---
  DetailPrint "Checking that compile.exe is in the archive..."
  IfFileExists $INSTDIR\bin\compile.exe +2 0
  Abort "Internal error in installer (bin\compile.exe not found)"
  ; ---
  DetailPrint "Checking that compile_to_c.exe is in the archive..."
  IfFileExists $INSTDIR\bin\compile_to_c.exe +2 0
  Abort "Internal error in installer (bin\compile_to_c.exe not found)"
  ; ---
  DetailPrint "Checking that clean.exe is in the archive..."
  IfFileExists $INSTDIR\bin\clean.exe +2 0
  Abort "Internal error in installer (bin\clean.exe not found)"
  ; ---
  DetailPrint "Checking that install.exe is not in the archive..."
  IfFileExists $INSTDIR\install.exe 0 Start_lcc_check
  Abort "Internal error in installer (install.exe still in the delivery)"
  ; ---
Re_install_lcc:
  MessageBox MB_OK "Now installing lcc-win32 C compiler:"
  ExecWait '"$INSTDIR\install\packaging\windows\lccwin32.exe"'
Start_lcc_check:
  DetailPrint "Checking whether lcc-win32 is installed..."
  ReadRegStr $R0 HKEY_CURRENT_USER "Software\lcc\compiler" includepath
  IfErrors 0 Lcc_bin_path
  Goto Re_install_lcc
Lcc_bin_path:
  Push $R0
  Call GetParent
  Pop $R0
  DetailPrint "Checking that the lcc.exe file actually exists:"
  ClearErrors
  IfFileExists $R0\bin\lcc.exe 0 Re_install_lcc
  DetailPrint "The lcc-win32 C compiler appears to be installed in '$R0'"
  DetailPrint "Updating Path variable for lcc using '$R0\bin'"
  Push $R0\bin
  Call AddToPath
  DetailPrint "End of checks for lcc-win32"
  MessageBox MB_OK "Congratulations, you have installed SmartEiffel sucessfully! $\n\
                    The SmartEiffel compiler is ready for use. Note that this release only $\n\
                    includes command line tools (open a command line window and try $\n\
                    examples of our tutorial in $INSTDIR\tutorial directory). $\n\
                    Also browse file://$INSTDIR\www\index.html for more documentation. $\n\
                    Feel free to visit us at http://SmartEiffel.loria.fr. The SmartEiffel team."
  DetailPrint "Installation of SmartEiffel sucessfully done."
SectionEnd
;--------------------------------------------------------------------------
Section "Uninstall"
  DetailPrint "Remove SmartEiffel registry keys"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SmartEiffel"
  DeleteRegKey HKEY_CURRENT_USER "Software\SmartEiffel"
  DetailPrint "Remove '$INSTDIR\bin' from Path"
  Push $INSTDIR\bin
  Call un.RemoveFromPath
  DetailPrint "Remove install directory"
  MessageBox MB_YESNO "Remove the '$INSTDIR' directory?" IDNO end_of_uninstall
  RMDir /r /REBOOTOK "$INSTDIR"
end_of_uninstall:
  MessageBox MB_OK "The lcc-win32 C compiler is still installed. $\n\
                   Have a look in the lcc-win32 directory to uninstall \
                   lcc-win32 by yourself if you want to do so."
SectionEnd
;--------------------------------------------------------------------------
