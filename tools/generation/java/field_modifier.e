-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class FIELD_MODIFIER
	--
	-- Holds JVM field modifiers.
	--

insert
	GLOBALS

feature {ANY}
	is_transient: BOOLEAN

	set_transient is
		do
			is_transient := True
		end

	clear_transient is
		do
			is_transient := False
		end

feature {ANY}
	is_volatile: BOOLEAN

	set_volatile is
		do
			is_volatile := True
		end

	clear_volatile is
		do
			is_volatile := False
		end

feature {ANY}
	access_flags: INTEGER is
		do
			Result := 1
			if is_transient then
				Result := Result + 128
			end
			if is_volatile then
				Result := Result + 64
			end
		end

end -- class FIELD_MODIFIER
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2004: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
