class UNICODE_TRANSLATOR

inherit
	UNICODE_FROM_STRING_GET_TEXT

creation
	connect_to

feature {ANY}
	text_domain: STRING is "vision_example_unicode"

end -- class UNICODE_TRANSLATOR
