class DOG

inherit
	ANIMAL

feature {ANY}
	cry: STRING is
		do
			Result := "BARK"
		end

end -- class DOG
