class FAST_ARRAY3_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	make is
		local
			c3: FAST_ARRAY3[INTEGER]
		do
			create c3.make(upper1 + 1, upper2 + 1, upper3 + 1)
			do_test_for(c3)
		end

end -- class FAST_ARRAY3_BENCH
