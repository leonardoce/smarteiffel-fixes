class ARRAY2_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	cltn: ARRAY2[REAL]

	make is
		do
			create cltn.make(1, count1, 1, count2)
			bench
		end

end -- class ARRAY2_BENCH
