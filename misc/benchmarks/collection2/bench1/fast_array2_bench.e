class FAST_ARRAY2_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	cltn: FAST_ARRAY2[REAL]

	make is
		do
			create cltn.make(count1, count2)
			bench
		end

end -- class FAST_ARRAY2_BENCH
