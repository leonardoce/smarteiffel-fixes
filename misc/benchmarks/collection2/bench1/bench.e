deferred class BENCH
	--
	-- Comparison : ARRAY2 Vs. FIXED_ARRAY2.
	--

feature {ANY}
	-- According to the power of your computer, set `tuning' to a good positive value. Default is for very
	-- small computer:
	tuning: INTEGER is 20
			-- 2000

feature {}
	cltn: COLLECTION2[REAL] is
		deferred
		end

	count1: INTEGER is
		do
			Result := 1 + tuning
		end

	count2: INTEGER is
		do
			Result := 7 + tuning
		end

	frozen bench is
		require
			cltn.count = count1 * count2
		do
			increment(cltn, 2.5)
			increment(cltn, -2.5)
			increment(cltn, 2.5)
			increment(cltn, -2.5)
			check_all(cltn, 0.0)
		end

	increment (c: like cltn; value: REAL) is
		local
			i1, i2: INTEGER
		do
			from
				i1 := c.upper1
			until
				i1 < c.lower1
			loop
				from
					i2 := c.upper2
				until
					i2 < c.lower2
				loop
					c.put(c.item(i1, i2) + value, i1, i2)
					i2 := i2 - 1
				end
				i1 := i1 - 1
			end
		end

	check_all (c: like cltn; value: REAL) is
		local
			i1, i2: INTEGER
		do
			from
				i1 := c.upper1
			until
				i1 < c.lower1
			loop
				from
					i2 := c.upper2
				until
					i2 < c.lower2
				loop
					if c.item(i1, i2) /= value then
						std_output.put_string("Error bench.%N")
					end
					i2 := i2 - 1
				end
				i1 := i1 - 1
			end
		end

end -- class BENCH
