class POINT

creation make

feature

   x, y: REAL_64

   make(vx, vy: REAL_64) is
      do
			x := vx
			y := vy
      end

end
