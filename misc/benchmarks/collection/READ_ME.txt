Short description of COLLECTION's benchmarks.
----
bench1 directory: usage of feature `put' and `item' while reading the collection
   from the `lower' to the `upper' index. 
----
bench2 directory: usage of feature `put' and `item' while reading the collection
   from the `upper' to the `lower' index.  (i.e. note that the
   traversal is reversed !).
----
bench3 directory: usage of feature `add_first'.
----
bench4 directory: usage of feature `add_last'.
----
