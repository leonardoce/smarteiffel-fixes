class TWO_WAY_LINKED_LIST_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	cltn: TWO_WAY_LINKED_LIST[INTEGER]

	make is
		local
			i: INTEGER
		do
			from
				create cltn.make
				i := count
			until
				i = 0
			loop
				cltn.add_last(0)
				i := i - 1
			end
			bench
		end

end -- class TWO_WAY_LINKED_LIST_BENCH
