class ARRAY_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	cltn: ARRAY[INTEGER]

	make is
		do
			create cltn.make(10, 9 + count)
			bench
		end

end -- class ARRAY_BENCH
