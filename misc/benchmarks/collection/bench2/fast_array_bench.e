class FAST_ARRAY_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	cltn: FAST_ARRAY[INTEGER]

	make is
		do
			create cltn.make(count)
			bench
		end

end -- class FAST_ARRAY_BENCH
