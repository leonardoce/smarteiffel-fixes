deferred class BENCH
	--
	-- Comparison :  ARRAY, FAST_ARRAY, RING_ARRAY, LINKED_LIST and TWO_WAY_LINKED_LIST.
	-- (If you change this file, please update documentation in the ../READ_ME.txt file.)
	--

feature {ANY}
	-- According to the power of your computer, set `tuning' to a good positive value. Default is for very small
	-- computer:
	tuning: INTEGER is 6 -- 60_000

feature {}
	cltn: COLLECTION[INTEGER] is
		deferred
		end

	count: INTEGER is
		do
			Result := 1 + tuning
		end

	frozen bench is
		require
			cltn.count = count
		do
			increment(cltn, 1)
			increment(cltn, 1)
			increment(cltn, -2)
			check_all(cltn, 0)
		end

	increment (c: like cltn; value: INTEGER) is
		local
			i: INTEGER
		do
			from
				i := c.upper
			until
				i = c.lower
			loop
				c.put(c.item(i - 1), i)
				i := i - 1
			end
		end

	check_all (c: like cltn; value: REAL) is
		local
			i: INTEGER
		do
			from
				i := c.upper
			until
				i = c.lower
			loop
				if c.item(i) /= value then
					std_output.put_string("Error in bench.%N")
				end
				i := i - 1
			end
		end

end -- class BENCH
