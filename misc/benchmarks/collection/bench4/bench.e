deferred class BENCH
	--
	-- Comparison :  ARRAY, FAST_ARRAY, RING_ARRAY, LINKED_LIST and TWO_WAY_LINKED_LIST.
	-- (If you change this file, please update documentation in the ../READ_ME.txt file.)
	--

feature {ANY}
	-- According to the power of your computer, set `tuning' to a good positive value. Default is for very small
	-- computer :
	tuning: INTEGER is 3 -- 300000;

feature {}
	frozen bench (cltn: COLLECTION[INTEGER]) is
		require
			cltn.count = 0
		local
			i: INTEGER
		do
			from
				i := tuning + 1
			until
				i = 0
			loop
				cltn.add_last(i)
				i := i - 1
			end
			debug
				from
					i := cltn.upper - 1
				until
					i < cltn.lower
				loop
					check
						cltn.item(i) = cltn.item(i + 1) + 1
					end
					i := i - 1
				end
			end
		end

end -- class BENCH
