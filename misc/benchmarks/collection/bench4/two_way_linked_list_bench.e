class TWO_WAY_LINKED_LIST_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	make is
		local
			link2_list: TWO_WAY_LINKED_LIST[INTEGER]
		do
			create link2_list.make
			bench(link2_list)
		end

end -- class TWO_WAY_LINKED_LIST_BENCH
