class ARRAY_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	make is
		local
			array: ARRAY[INTEGER]
		do
			create array.make(1, 0)
			bench(array)
		end

end -- class ARRAY_BENCH
