class FAST_ARRAY_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	make is
		local
			fast_array: FAST_ARRAY[INTEGER]
		do
			create fast_array.make(count)
			bench(fast_array)
		end

end -- class FAST_ARRAY_BENCH
