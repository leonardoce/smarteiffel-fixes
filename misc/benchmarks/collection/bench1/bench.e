deferred class BENCH
--
-- Comparison :  ARRAY, FAST_ARRAY, RING_ARRAY, LINKED_LIST and TWO_WAY_LINKED_LIST.
-- (If you change this file, please update documentation in the ../READ_ME.txt file.)
--
feature {ANY}
	-- According to the power of your computer, set `tuning' to a good positive value. Default is for very
	-- small computer:
	tuning: INTEGER is 4
			-- 400;

feature {}
	count: INTEGER is
		do
			Result := tuning + 1
		end

	frozen bench (cltn: COLLECTION[INTEGER]) is
		require
			cltn.count = count
		local
			inner, outer, nb_loops, value: INTEGER
		do
			nb_loops := tuning * tuning
			from
				outer := 1
			until
				outer > nb_loops
			loop
				from
					inner := cltn.lower
				until
					inner > cltn.upper
				loop
					value := cltn.item(inner)
					cltn.put(value + 1, inner)
					inner := inner + 1
				end
				outer := outer + 1
			end
		end

end -- class BENCH
