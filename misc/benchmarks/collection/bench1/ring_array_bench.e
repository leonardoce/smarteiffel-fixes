class RING_ARRAY_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	make is
		local
			ring_array: RING_ARRAY[INTEGER]
		do
			create ring_array.make(10, 9 + count)
			bench(ring_array)
		end

end -- class RING_ARRAY_BENCH
