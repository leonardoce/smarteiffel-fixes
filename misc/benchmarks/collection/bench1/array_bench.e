class ARRAY_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	make is
		local
			array: ARRAY[INTEGER]
		do
			create array.make(10, 9 + count)
			bench(array)
		end

end -- class ARRAY_BENCH
