class TWO_WAY_LINKED_LIST_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	make is
		local
			link2_list: TWO_WAY_LINKED_LIST[INTEGER]; i: INTEGER
		do
			from
				create link2_list.make
				i := count
			until
				i = 0
			loop
				link2_list.add_first(0)
				i := i - 1
			end
			bench(link2_list)
		end

end -- class TWO_WAY_LINKED_LIST_BENCH
