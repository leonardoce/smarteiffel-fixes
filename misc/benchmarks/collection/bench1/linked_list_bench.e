class LINKED_LIST_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	make is
		local
			link_list: LINKED_LIST[INTEGER]; i: INTEGER
		do
			from
				create link_list.make
				i := count
			until
				i = 0
			loop
				link_list.add_first(0)
				i := i - 1
			end
			bench(link_list)
		end

end -- class LINKED_LIST_BENCH
