class LINKED_LIST_BENCH

inherit
	BENCH

creation {ANY}
	make

feature {ANY}
	make is
		local
			link_list: LINKED_LIST[INTEGER]
		do
			create link_list.make
			bench(link_list)
		end

end -- class LINKED_LIST_BENCH
