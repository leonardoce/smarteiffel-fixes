# Completion patterns for the "se" command. To be source in your interactive bash session (e.g. in ~/.bashrc)

# helper: true if the pattern exists in one word of the command line
function _se__has_pattern() {
    for word in ${COMP_WORDS[*]}; do
	for pattern in "$@"; do
	    test -z "${word##$pattern}" && return 0
	done
    done
    return 1
}

# helper: similar to _filedir but accepts more than one suffix.
function _se__filedir() {
    _expand || return 0
    COMPREPLY=(${COMPREPLY[@]:-} $(compgen -d -- "$cur"))
    if [ "${1:-}" != '-d' ]; then
	COMPREPLY=(${COMPREPLY[@]:-} $(compgen -f -X "${1:-}" -- "$cur"))
    fi
}

# the completion command
function _se__complete() {
    local cur prev extglob
    shopt extglob|grep -q on
    extglob=$?
    shopt -s extglob
    # the possible completion words
    COMPREPLY=()
    # the current word to be completed, can be empty
    cur="${COMP_WORDS[COMP_CWORD]}"
    # the previous word, useful for some options completion
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    # test if the "se" command exists and is executable
    se="$(which se)" && [ -x "$se" ] && {
	if [ $COMP_CWORD -eq 1 ]; then
	    # the first word is either -version or -help or a known command
	    case "$cur" in
		-*)
		    COMPREPLY=($(compgen -W "-version -help" -- "$cur"))
		    ;;
		*)
		    COMPREPLY=($(compgen -W "$("$se" -version 2>&1 | awk '{if(NR>9) print $1}')" -- "$cur"))
		    ;;
	    esac
	else
	    # the following words depend on the first word
	    case "$cur" in
		-*)
		    # if it is an option, ask the available options to the command itself
		    COMPREPLY=( $(compgen -W "$("$se" ${COMP_WORDS[1]} -help | grep -E '^[ 	]*-.*' | awk '{print $1}')" -- "$cur"))
		    ;;
		*)
		    # otherwise, it depends on the actually called command
		    case ${COMP_WORDS[1]} in
			c|c2c)
			    # compile_to_c
			    case $prev in
				-o)
				    _se__filedir
				    ;;
				-c_mode)
				    # the C modes are in the serc file. Use a trick to find this file (finder hello_world).
				    serc="$("$se" find hello_world -verbose | grep -E "^Using the configuration file:" | cut -c31-)" && {
					c_modes="$(cat "$serc" | awk -F'[][]' '/^\[.*\]$/ {print $2}' | grep -vE '^(General|Environment|Loadpath|Tools|Java)$')"
					COMPREPLY=($(compgen -W "$c_modes" -- "$cur"))
				    }
				    ;;
				-loadpath|-cecil)
				    _se__filedir '!*.se'
				    ;;
				*)
				    if _se__has_pattern '*.e' '*.ace'; then
					_se__filedir '!*.@(c|h|o|so)'
				    else
					_se__filedir '!*.@(e|ace)'
				    fi
				    ;;
			    esac
			    ;;
			java)
			    # compile_to_jvm
			    case $prev in
				-o)
				    _se__filedir
				    ;;
				-loadpath)
				    _se__filedir'!*.se'
				    ;;
				-use_jar|-use_jvm)
				    _se__filedir
				    ;;
				-classpath)
				    _se__filedir -d
				    ;;
				*)
				    if _se__has_pattern '*.e' '*.ace'; then
					_se__filedir '!*.@(java|class|jar)'
				    else
					_se__filedir '!*.@(e|ace)'
				    fi
				    ;;
			    esac
			    ;;
			javap)
			    # print_jvm_class
			    _se__has_pattern '*.class' || _se__filedir '!*.class'
			    ;;
			ace_check)
			    # ace_check
			    _se__has_pattern '*.ace' || _se__filedir '!*.ace'
			    ;;
			doc)
			    # eiffeldoc
			    _se__has_pattern '*.se' '*.ace' || _se__filedir '!*.@(se|ace)'
			    ;;
			pretty)
			    # pretty
			    _se__filedir '!*.e'
			    ;;
			clean)
			    # clean
			    _se__has_pattern '*.e' || _se__filedir '!*.e'
			    ;;
			test)
			    # eiffeltest
			    _se__filedir -d
			    ;;
			*)
			    # default (don't forget that the user can add commands to their serc file)
			    _se__has_pattern '*.e' '*.ace' || _se__filedir '!*.@(e|ace)'
			    ;;
		    esac
		    ;;
	    esac
	fi
    }
    if [ $extglob = 1 ]; then
	shopt -u extglob
    fi
}
complete -F _se__complete se
