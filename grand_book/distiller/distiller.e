-- See the Copyright notice at the end of this file.
--
class DISTILLER

insert
	COMMAND_LINE_TOOLS

creation {ANY}
	main

feature {ANY}
	command_name: STRING is "distiller"

	is_valid_argument_for_ace_mode (arg: STRING): BOOLEAN is
		do
		end

	valid_argument_for_ace_mode: STRING is ""

	command_line_help_summary: STRING is "[
													  Usage: distiller [options]
													  ]"

	grand_book_directory_path: STRING

	wiki_snapshot_directory_path: STRING

	language_list: COLLECTION[STRING] is
		once
			Result := {FAST_ARRAY[STRING] << "fr", "de", "es", "en" >> }
		end

	file_ignored: COLLECTION[STRING] is
		once
			Result := {FAST_ARRAY[STRING] << ".", "..", ".svn" >> }
		end

	main is
		local
			basic_directory,temp_directory: BASIC_DIRECTORY; entry,language_directory_path: STRING; parser: WIKI_TEXT_PARSER
		do
			search_for_verbose_flag
			echo.put_string("Grand Book distiller started.%N")
			basic_directory.connect_to_current_working_directory
			if not basic_directory.is_connected then
				echo.w_put_string("Unable to read current working directory.%N")
				die_with_code(exit_failure_code)
			end
			grand_book_directory_path := basic_directory.last_entry.twin
			basic_directory.disconnect
			basic_directory.compute_parent_directory_of(grand_book_directory_path)
			grand_book_directory_path.copy(basic_directory.last_entry)
			echo.put_string("Now computing `wiki_snapshot_directory_path'.%N")
			basic_directory.compute_subdirectory_with(grand_book_directory_path, once "wiki_snapshot")
			wiki_snapshot_directory_path := basic_directory.last_entry.twin
			basic_directory.connect_to(wiki_snapshot_directory_path)
			if not basic_directory.is_connected then
				echo.w_put_string("Unable to read wiki_snapshot directory.%N")
				die_with_code(exit_failure_code)
			end
			from
				basic_directory.read_entry
			until
				basic_directory.end_of_input
			loop
				entry := basic_directory.last_entry.twin
				if language_list.has(entry) then
					echo.put_string(entry)
					echo.put_string("%N")
					basic_directory.compute_subdirectory_with(wiki_snapshot_directory_path, entry)
					language_directory_path := basic_directory.last_entry.twin
					temp_directory.connect_to(language_directory_path)
					if not temp_directory.is_connected then
						echo.w_put_string("Unable to read language directory.%N")
						die_with_code(exit_failure_code)
					end
					from
						temp_directory.read_entry
					until
						temp_directory.end_of_input
					loop
						entry := temp_directory.last_entry.twin
						if not file_ignored.has(entry) then
							create parser.make(entry, temp_directory.last_entry)
							echo.put_string("Fichier en cours de traitement : ")
							echo.put_string(temp_directory.last_entry)
							echo.put_string("%N")
						end
						temp_directory.read_entry
					end
					temp_directory.disconnect
				end
				basic_directory.read_entry
			end
			basic_directory.disconnect
			echo.put_string("Grand Book ready.%N")
		end

end -- class DISTILLER
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
