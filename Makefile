#          This file is part of SmartEiffel The GNU Eiffel Compiler.
#       Copyright (C) 1994-2002 LORIA - INRIA - U.H.P. Nancy 1 - FRANCE
#          Dominique COLNET and Suzanne COLLIN - SmartEiffel@loria.fr
#                       http://SmartEiffel.loria.fr
# SmartEiffel is  free  software;  you can  redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software  Foundation;  either  version  2, or (at your option)  any  later
# version. SmartEiffel is distributed in the hope that it will be useful,but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
# for  more  details.  You  should  have  received a copy of the GNU General
# Public  License  along  with  SmartEiffel;  see the file COPYING.  If not,
# write to the  Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
# Boston, MA  02110-1301, USA
#
# Available targets:
#
#  - all         Starts the installer in automatic mode--use it if you know the
#                installer is smart enough to compute default values, and those
#                values suit your needs. The default values are known to be
#                correctly computed on Windows and Linux.
#
#  - interactive Starts the installer in interactive mode: use the menu to
#                install SmartEiffel
#
#  - elate       Starts the installer for ELATE systems
#
#  - openvms     Starts the installer for OpenVMS systems
#
#  - clean       Cleans the installation.
#
# Also set PLAIN=plain for all/install if you don't want visual effects but
# a real loggable trace.
#

SmartEiffel ?= ${HOME}/.serc

.SILENT:

.PHONY: clean interactive all install rm_serc

all: 
	sh ./make_release.sh build $(PLAIN)

install: all
	sh ./make_release.sh install $(PLAIN)

interactive: se_install
	./se_install

clean:
	echo "Removing all executable tools"
	/bin/rm -f bin/*
	/bin/rm -f se_install
	/bin/rm -f rm_serc

install/germ/compile_to_c.h: bin/compile_to_c tools/commands/compile_to_c.e
	echo "Compiling install germ"
	cd install/germ &&\
	/bin/rm -f compile_to_c*.c compile_to_c.h &&\
	../../bin/compile_to_c -boost -no_gc compile_to_c

install.c: bin/compile_to_c tools/commands/install.e
	echo 'Compiling installer (install.e).'
	bin/compile_to_c -boost -no_split -no_gc install

se_install: rm_serc ${SmartEiffel} install/germ/compile_to_c.h install.c
	echo 'Building installer (compiling install.c from install.e).'
	gcc -o se_install install.c
	./rm_serc

bin/compile_to_c: work/germ/compile_to_c.c work/germ/compile_to_c.h
	echo "Compiling bootstrap compiler"
	cd work/germ &&\
	gcc -pipe compile_to_c.c -o ../../bin/compile_to_c

rm_serc:
	/bin/rm -f rm_serc
	echo "#!/bin/sh" > rm_serc
	echo "/bin/rm \$$0" >> rm_serc
	chmod u+x rm_serc

${SmartEiffel}:
	sed "s/\$${HOME}\/SmartEiffel/`pwd|sed "s/\//\\\\\\\\\//g"`/g" work/germ/system.se > ${SmartEiffel}
	echo "/bin/rm ${SmartEiffel}" >> rm_serc

# Are these targets still in use?

elate: install/germ/compile_to_c.h se_install
	echo "If you use this target please mail to SmartEiffel@loria"
	echo "otherwise it will disappear in the next release"
	vpcc -o se_install install.c
	./se_install
	/bin/rm -f ./se_install.00

openvms: [.install.germ]compile_to_c.h install.c
	echo "If you use this target please mail to SmartEiffel@loria"
	echo "otherwise it will disappear in the next release"
	cc/warning=disable=(embedcomment,longextern) install
	link/exe=se_install.exe install.obj
	mcr se_install
	delete se_install;
