/*
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2005: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
*/

import java.io.*;
import java.util.*;
import java.lang.*;

public class SmartEiffelBasicTime {

    public static double basic_time_time() {
	return Double.longBitsToDouble( System.currentTimeMillis() );
    }

    public static int basic_time_difftime( double t2, double t1 ) {
	return (int) ( ( Double.doubleToLongBits( t2 ) - Double.doubleToLongBits( t1) )/1000 );
    }

    public static int basic_time_getyear( double t, int mode ) {
	java.util.Date aDate = new java.util.Date( Double.doubleToLongBits( t ) );
	java.util.Calendar aCalendar = java.util.Calendar.getInstance();
	aCalendar.setTime( aDate );
	if ( mode == 1 ) {
	    java.util.TimeZone zone = java.util.TimeZone.getTimeZone( "GMT" );
	    aCalendar.setTimeZone( zone );
	}
	return aCalendar.get( java.util.Calendar.YEAR );
    }

    public static int basic_time_getmonth( double t, int mode ) {
	java.util.Date aDate = new java.util.Date( Double.doubleToLongBits( t ) );
	java.util.Calendar aCalendar = java.util.Calendar.getInstance();
	aCalendar.setTime( aDate );
	if ( mode == 1 ) {
	    java.util.TimeZone zone = java.util.TimeZone.getTimeZone( "GMT" );
	    aCalendar.setTimeZone( zone );
	}
	return ( aCalendar.get( java.util.Calendar.MONTH ) + 1 );
    }

    public static int basic_time_getday( double t, int mode ) {
	java.util.Date aDate = new java.util.Date( Double.doubleToLongBits( t ) );
	java.util.Calendar aCalendar = java.util.Calendar.getInstance();
	aCalendar.setTime( aDate );
	if ( mode == 1 ) {
	    java.util.TimeZone zone = java.util.TimeZone.getTimeZone( "GMT" );
	    aCalendar.setTimeZone( zone );
	}
	return aCalendar.get( java.util.Calendar.DAY_OF_MONTH );
    }

    public static int basic_time_gethour( double t, int mode ) {
	java.util.Date aDate = new java.util.Date( Double.doubleToLongBits( t ) );
	java.util.Calendar aCalendar = java.util.Calendar.getInstance();
	aCalendar.setTime( aDate );
	if ( mode == 1 ) {
	    java.util.TimeZone zone = java.util.TimeZone.getTimeZone( "GMT" );
	    aCalendar.setTimeZone( zone );
	}
	return aCalendar.get( java.util.Calendar.HOUR );
    }

    public static int basic_time_getminute( double t, int mode ) {
	java.util.Date aDate = new java.util.Date( Double.doubleToLongBits( t ) );
	java.util.Calendar aCalendar = java.util.Calendar.getInstance();
	aCalendar.setTime( aDate );
	if ( mode == 1 ) {
	    java.util.TimeZone zone = java.util.TimeZone.getTimeZone( "GMT" );
	    aCalendar.setTimeZone( zone );
	}
	return aCalendar.get( java.util.Calendar.MINUTE );
    }

    public static int basic_time_getsecond( double t, int mode ) {
	java.util.Date aDate = new java.util.Date( Double.doubleToLongBits( t ) );
	java.util.Calendar aCalendar = java.util.Calendar.getInstance();
	aCalendar.setTime( aDate );
	if ( mode == 1 ) {
	    java.util.TimeZone zone = java.util.TimeZone.getTimeZone( "GMT" );
	    aCalendar.setTimeZone( zone );
	}
	return aCalendar.get( java.util.Calendar.SECOND );
    }

    public static boolean basic_time_is_summer_time_used( double t ) {
	java.util.Date aDate = new java.util.Date( Double.doubleToLongBits( t ) );
	java.util.TimeZone zone = java.util.TimeZone.getDefault();
	return zone.inDaylightTime( aDate );
    }

    public static int basic_time_getyday( double t, int mode ) {
	java.util.Date aDate = new java.util.Date( Double.doubleToLongBits( t ) );
	java.util.Calendar aCalendar = java.util.Calendar.getInstance();
	aCalendar.setTime( aDate );
	if ( mode == 1 ) {
	    java.util.TimeZone zone = java.util.TimeZone.getTimeZone( "GMT" );
	    aCalendar.setTimeZone( zone );
	}
	return aCalendar.get( java.util.Calendar.DAY_OF_YEAR );
    }

    public static int basic_time_getwday( double t, int mode ) {
	java.util.Date aDate = new java.util.Date( Double.doubleToLongBits( t ) );
	java.util.Calendar aCalendar = java.util.Calendar.getInstance();
	aCalendar.setTime( aDate );
	if ( mode == 1 ) {
	    java.util.TimeZone zone = java.util.TimeZone.getTimeZone( "GMT" );
	    aCalendar.setTimeZone( zone );
	}
      return ( aCalendar.get( java.util.Calendar.DAY_OF_WEEK ) - 1 );
    }

    public static double basic_time_mktime( int a_year, int a_mon, int a_day,
					    int a_hour, int a_min, int a_sec ) {
	java.util.Calendar aCalendar = java.util.Calendar.getInstance();
	aCalendar.set( a_year, (a_mon - 1), a_day, a_hour, a_min, a_sec );
	return Double.longBitsToDouble( aCalendar.getTime().getTime() );
    }

    public static double basic_time_add_second( double t2, double t1 ) {
      return Double.longBitsToDouble( Double.doubleToLongBits( t2 ) - Double.doubleToLongBits( t1) );
    }
}
