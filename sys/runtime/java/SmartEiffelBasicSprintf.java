/*
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2005: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
*/

import java.io.*;
import java.util.*;
import java.lang.*;

public class SmartEiffelBasicSprintf {

    public static void basic_sprintf_double (byte buffer[], int f, double d) {
	String s = Double.toString(d);
	int ib = 0; int is = 0;	int iE;	int idot; int exp; int i;

	if (s.charAt(is) == '-') {
	    buffer[ib++] = '-';
	    s = s.substring(1,s.length());
	}
	iE = s.indexOf('E');
	idot = s.indexOf('.');
	if (iE < 0) {
	    /* ddd.ddd notation. */
	    for (; is < idot ; is++) {
		buffer[ib++] = ((byte)(s.charAt(is)));
	    }
	    if (f > 0) {
		buffer[ib++] = '.';
		is++;
	    }
	    for (; ((f > 0) && (is < s.length())) ; f--) {
		buffer[ib++] = ((byte)(s.charAt(is++)));
	    }
	    for (; f > 0 ; f--) {
		buffer[ib++] = '0';
	    }
	    buffer[ib] = 0;
	}
	else {
	    /* m.ddddE-xx notation. */
	    exp = Integer.parseInt(s.substring(iE+1,s.length()));
	    if (exp >= 0) {
		for (; is < idot ; is++) {
		    buffer[ib++] = ((byte)(s.charAt(is)));
		}
		is++;
		for (; exp > 0 ; exp--) {
		    if (is < iE) {
			buffer[ib++] = ((byte)(s.charAt(is++)));
		    }
		    else {
			buffer[ib++] = '0';
		    }
		}
		if (f > 0) {
		    buffer[ib++] = '.';
		}
		for (; f > 0 ; f--) {
		    if (is < iE) {
			buffer[ib++] = ((byte)(s.charAt(is++)));
		    }
		    else {
			buffer[ib++] = '0';
		    }
		}
		buffer[ib] = 0;
	    }
	    else {
		i = idot + exp;
		if (i <= 0) {
		    buffer[ib++] = '0';
		}
		else {
		    for (; is < i ; is++) {
			buffer[ib++] = ((byte)(s.charAt(is++)));
		    }
		}
		if (f > 0) {
		    buffer[ib++] = '.';
		}
		for (; f > 0 ; f--) {
		    if (i < 0) {
			buffer[ib++] = '0';
			i++;
		    }
		    else if (is < iE) {
			if (is == idot) is++;
			buffer[ib++] = ((byte)(s.charAt(is++)));
		    }
		    else {
			buffer[ib++] = '0';
		    }
		}
		buffer[ib] = 0;
	    }
	}
    }
}
