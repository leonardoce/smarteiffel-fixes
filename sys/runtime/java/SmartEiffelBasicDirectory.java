/*
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2005: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
*/

import java.io.*;
import java.util.*;
import java.lang.*;

public class SmartEiffelBasicDirectory {

    static class DirectoryStream {
	/* To simulate a BASIC_DIRECTORY stream. */
	File directory;
	String[] list;
	int index;

	public DirectoryStream read_entry() {
	    if (++index < list.length) {
		return this;
	    }
	    else {
		return null;
	    }
	}

	public Object get_entry_name() {
	    String entry;
	    if (index == -2) {
		entry = ".";
	    }
	    else if (index == -1) {
		entry = "..";
	    }
	    else {
		entry = list[index];
	    }
	    return SmartEiffelRuntime.StringToNullTerminatedBytes(entry);
	}
    }

    public static Object basic_directory_open(Object path_pointer) {
	String path = new String((byte[])path_pointer);
	File directory = new File(path);
	if (directory.isDirectory()) {
	    DirectoryStream dirstream = new DirectoryStream();
	    dirstream.directory = directory;
	    dirstream.list = directory.list();
	    dirstream.index = -3;
	    return dirstream;
	}
	else {
	    return null;
	}
    }

    public static Object basic_directory_read_entry(Object dirstream_pointer) {
	DirectoryStream dirstream = ((DirectoryStream)dirstream_pointer);
	return dirstream.read_entry();
    }

    public static Object basic_directory_get_entry_name(Object dirstream_pointer) {
	DirectoryStream dirstream = ((DirectoryStream)dirstream_pointer);
	return dirstream.get_entry_name();
    }

    public static boolean basic_directory_close(Object stream) {
	return true;
    }

    public static Object basic_directory_current_working_directory() {
	String cwd = System.getProperty("user.dir");
	return SmartEiffelRuntime.StringToNullTerminatedBytes(cwd);
    }

    public static boolean basic_directory_chdir(Object dirpath_pointer) {
	String path = new String((byte[])dirpath_pointer);
	File directory = new File(path);
	if (directory.isDirectory()) {
	    Properties p = System.getProperties();
	    p.put("user.dir",path);
	    return true;
	}
	else {
	    return false;
	}
    }

    public static boolean basic_directory_mkdir(Object dirpath_pointer) {
	String name = SmartEiffelRuntime.NullTerminatedBytesToString(dirpath_pointer);
	File directory = new File(name);
	directory.mkdir();
	return true;
    }

    public static boolean basic_directory_rmdir(Object dirpath_pointer) {
	String name = SmartEiffelRuntime.NullTerminatedBytesToString(dirpath_pointer);
	File directory = new File(name);
	directory.delete();
	return true;
    }
}
