-- See the Copyright notice at the end of this file.
--
class SPLICE_JOB

inherit
	JOB

creation {STREAM_SPLICE}
	make

feature {STREAM_SPLICE}
	connect_to (a_instream: like instream; a_outstream: like outstream) is
		require
			not is_connected
			a_instream.is_connected
			a_outstream.is_connected
		do
			instream := a_instream
			outstream := a_outstream
		ensure
			is_connected
		end

	is_connected: BOOLEAN is
		do
			Result := instream /= Void and then instream.is_connected and then outstream /= Void and then outstream.is_connected
		end

	disconnect is
		do
			instream := Void
			outstream := Void
		end

feature {LOOP_ITEM}
	prepare (events: EVENTS_SET) is
		do
			events.expect(instream.event_can_read)
			events.expect(outstream.event_can_write)
		end

	is_ready (events: EVENTS_SET): BOOLEAN is
		do
			Result := events.event_occurred(instream.event_can_read) and then events.event_occurred(outstream.event_can_write)
		end

	continue is
		do
			instream.read_character
			outstream.put_character(instream.last_character)
		end

	done: BOOLEAN is
		do
			Result := not is_connected or else instream.end_of_input
		end

	restart is
		do
		end

feature {}
	make is
		do
		end

feature {STREAM_SPLICE}
	instream: INPUT_STREAM

	outstream: OUTPUT_STREAM

end -- class SPLICE_JOB
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
