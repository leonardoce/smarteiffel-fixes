-- See the Copyright notice at the end of this file.
--
class STREAM_SPLICE

creation {ANY}
	connect_to, make

feature {ANY}
	connect_to (a_instream: INPUT_STREAM; a_outstream: OUTPUT_STREAM) is
			-- Connect streams
		require
			not is_connected
			a_instream.is_connected
			a_outstream.is_connected
		do
			if job = Void then
				create job.make
			end
			job.connect_to(a_instream, a_outstream)
		ensure
			is_connected
		end

	disconnect is
			-- Disconnect streams
		require
			is_connected
		do
			job.disconnect
		ensure
			not is_connected
			old (instream.is_connected) = (old instream).is_connected
			old (outstream.is_connected) = (old outstream).is_connected
		end

	is_connected: BOOLEAN is
			-- Are streams connected?
		do
			Result := job /= Void and then job.is_connected
		ensure
			Result implies instream /= Void
			Result implies outstream /= Void
		end

	splice is
			-- Move data from instream to outstream while there is anything to move, using an internal stack.
			--
			-- See also `splice_in_stack'.
		require
			is_connected
		do
			splice_in_stack(splice_stack)
			splice_stack.run
		end

	splice_in_stack (a_stack: like splice_stack) is
			-- Puts a job in `a_stack' that moves data from instream to outstream. No provision is made to start
			-- the stack.
			--
			-- See also `splice'.
		require
			is_connected
		do
			a_stack.add_job(job)
		end

feature {}
	make is
			-- Create a non-connected splice
		do
			create job.make
		end

	splice_stack: LOOP_STACK is
			-- A stack for splice jobs. See `splice'.
		once
			create Result.make
		end

	job: SPLICE_JOB
			-- The job that will be placed on the stack

	instream: STREAM is
		do
			Result := job.instream
		end

	outstream: STREAM is
		do
			Result := job.outstream
		end

invariant
	is_connected implies instream.is_connected
	is_connected implies outstream.is_connected

end -- class STREAM_SPLICE
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
