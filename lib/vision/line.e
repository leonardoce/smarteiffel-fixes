-- See the Copyright notice at the end of this file.
--
deferred class LINE
	-- Use line as widget (insert in container then position, draw and size
	-- are automatic). Common ancestor for `HORIZONTAL_LINE' and `VERTICAL_LINE'

inherit
	WIDGET
	DRAWABLE_HANDLER
	GRAPHIC
		--TODO: suppress ?

feature {ANY}
	width, height: INTEGER

	thickness: INTEGER

	style: DRAW_STYLE

	set_thickness (thick: INTEGER) is
			-- Set the thickness of the line (drawing' witdh)
		require
			thick > 0
		deferred
		ensure
			thick = thickness
		end

	set_style (s: DRAW_STYLE) is
			-- Change the style used to draw the line.
			-- NOTE: The screen is not updated. --TODO: change this ?
		do
			style := s
		ensure
			style = s
		end

	reset_default_style is
			-- The renderer will be used to draw the line.
			-- NOTE: The screen is not updated. --TODO: change this ?
		do
			style := Void
		end

feature {LAYOUT}
	set_geometry (x, y, w, h: INTEGER) is
		do
			pos_x := x
			pos_y := y
			width := w
			height := h
		end

feature {}
	resize (w, h: INTEGER) is
		do
			width := w
			height := h
		end

end -- class LINE
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
