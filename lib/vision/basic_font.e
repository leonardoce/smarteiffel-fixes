-- See the Copyright notice at the end of this file.
--
class BASIC_FONT
	-- BASIC_FONT describe basic font properties (like size, font family...)
	-- and allow to draw characters (see class `FONT' for options like
	-- underline).
	-- Use `FONT_MANAGER' for `BASIC_FONT' creation.
	--

inherit
	GRAPHIC
	UNICODE_STRING_HANDLER

creation {ANY}
	make_system_specific

creation {FONT_MANAGER}
	make_from_id

feature {}
	make_system_specific (font_name: STRING) is
			-- It's recommended not to use this function.
			-- The `font_name' is the font name in the system syntax (ex: XLFD
			-- for X11).
		require
			font_name /= Void
			font_manager.font_exist_by_name(font_name)
		do
			make_from_id(basic_font_new(font_name.to_external))
		end

	make_from_id (font_id: POINTER) is
		do
			font := font_id
			font_desc := basic_font_properties(font)
			height := basic_font_height(font_desc)
			base_line := basic_font_ascent(font_desc)
		end --|*** name: STRING

feature {ANY}
	height: INTEGER

	base_line: INTEGER -- height from the top to the bottom of characters like 'P' or 'k'. Height is bigger due to characters that go under the base line like 'j' or 'p'.

	text_width (text: UNICODE_STRING): INTEGER is
		do
			--TODO: surrogate characters !
			--TODO: combining characters !
			Result := basic_font_text_width(font_desc, font, text.storage.to_external, text.count)
		end

feature {BASIC_FONT, DRAW_STYLE}
	font: POINTER

	font_desc: POINTER --TODO: suppress this pointer

feature {}
	basic_font_properties (font_id: POINTER): POINTER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "font"
			feature_name: "basic_font_properties"
			}"
		end

	default_font_name: STRING is "-*-clearlyu-*-*-*-*-*-300-*-*-p-*-iso10646-1"
			--TODO: implement font lookup.
			--default_font_name: STRING is "-adobe-times-medium-r-normal--14-*-*-*-*-*-iso8859-1"
			--default_font_name: STRING is "fixed" --*-*-*-*-*-*-*-*-*-*-*-*-*-*"
			--default_font_name: STRING is "-adobe-times-*-i-*-*-*-300-*-*-*-*-iso8859-1"
			--default_font_name: STRING is "-*-charter-*-*-*-*-*-500-*-*-*-*-iso8859-1"
			--default_font_name: STRING is "-monotype-corsiva-*-*-*-*-80-*-100-100-*-*-iso10646-*"
			--default_font_name: STRING is "-gnu-*-*-*-*-*-16-*-75-*-*-*-iso10646-*"

	basic_font_new (font_name: POINTER): POINTER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "font"
			feature_name: "basic_font_new"
			}"
		end

	basic_font_height (f: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "font"
			feature_name: "basic_font_height"
			}"
		end

	basic_font_ascent (f: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "font"
			feature_name: "basic_font_ascent"
			}"
		end

	basic_font_text_width (fdes, fon: POINTER; str: POINTER; size: INTEGER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "font"
			feature_name: "basic_font_text_width"
			}"
		end

end -- class BASIC_FONT
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
