-- See the Copyright notice at the end of this file.
--
class VISION
	-- `VISION' object is singleton, accessible via `vision' from `GRAPHIC'
	-- This object is responsible of "graphic mode" initialisation and
	-- graphic events management.
	-- This class give access to the events' loop (see also `start')
	-- and to the display size.

creation {GRAPHIC}
	graphic_init

feature {}
	graphic_init is
			-- Initialize graphic using default values.
		local
			g_c: INTEGER; ls: VISION_LOOP_STACK
		do
			g_c := basic_vision_init
			if g_c /= 0 then
				create graphic_connection.set_descriptor(g_c)
			else
				io.put_string("Vision is currently written for X and Windows.%N")
				not_yet_implemented
			end
			display_width := basic_vision_display_width
			display_height := basic_vision_display_height
			create ls.make
			ls.set_vision(Current)
			loop_stack := ls
			create event_catcher.make(graphic_connection)
			widget := event_catcher.widget
			loop_stack.add_job(event_catcher)
		end

feature {ANY}
	display_width: INTEGER

	display_height: INTEGER

	loop_stack: LOOP_STACK
			-- The loop_stack gives possibility to add some `JOB', break
			-- current events loop...

	start is
			-- Start to run current events loop. This call returns only
			-- when you `break' this loop.
		do
			event_catcher.continue
			loop_stack.run
		end

	new_loop is
			-- You need new loop if you want modal window.
			-- When you create this new loop, existing windows will be
			-- insensitive and jobs will be suspended. Then you create
			-- your new window and all it's widgets and finally call `start'.
			-- The state is "restored" when you `break' this loop.
			-- NOTE: loops may be reused (restared) when you need to
			-- reuse the same modal window.
		do
			loop_stack.new_loop
		end

	default_draw_kit: DRAW_KIT is
		once
			create Result
		end

	font_manager: FONT_MANAGER is
		once
			create Result.init
		end

	last_character: INTEGER is
			-- return unicode character.
			-- NOTE: only access this information if last event is key event.
		require
			current_event_type.in_range(2, 3)
		do
			Result := basic_vision_character
		end

	last_key: INTEGER is
		require
			current_event_type.in_range(2, 3)
		do
			Result := basic_vision_key
			if Result = 0 then
				Result := last_character
			end
		end

	key_symbol (name: STRING): INTEGER is
		require
			name /= Void and then not name.is_empty
		do
			Result := basic_vision_get_keysym(name.to_external)
		ensure
			Result >= 0
		end

	pointer_x: INTEGER is
			-- retrun pointer x coordinate relative to the window who
			-- received the event.
			-- NOTE: only access this information if last event is button
			-- or wheel event.
		require
			current_event_type.in_range(40, 59)
		do
			Result := basic_vision_pointer_x
		end

	pointer_y: INTEGER is
			-- retrun pointer y coordinate relative to the window who
			-- received the event.
			-- NOTE: only access this information if last event is button
			-- or wheel event.
		require
			current_event_type.in_range(40, 59)
		do
			Result := basic_vision_pointer_y
		end

	pointer_x_root: INTEGER is
			-- retrun pointer x coordinate relative to the screen.
			-- NOTE: only access this information if the last event is a mouse
			-- event (button/wheel/move).
		require
			current_event_type = 100 or else current_event_type.in_range(40, 59)
		do
			Result := basic_vision_pointer_x_root
		end

	pointer_y_root: INTEGER is
			-- retrun pointer y coordinate relative to the screen.
			-- NOTE: only access this information if the last event is a mouse
			-- event (button/wheel/move).
		require
			current_event_type = 100 or else current_event_type.in_range(40, 59)
		do
			Result := basic_vision_pointer_y_root
		end

	event_time: INTEGER is
			-- return the date the event occured. Origin is undefined,
			-- unit is millisecond. Difference give delay.
			-- NOTE: only access this information if last event is button
			-- or wheel event or pointer move.
		require
			current_event_type = 100 or else current_event_type.in_range(40, 59)
		do
			Result := basic_vision_event_time
		end

	expose_area: RECT is
			-- return the rectangular area the expose event is relative to.
			-- Coordinates are relative to the window who received the event.
			-- NOTE: only access this information if last event is expose_event.
		require
			current_event_type = 12
		do
			Result.make(basic_vision_expose_x, basic_vision_expose_y, basic_vision_expose_width, basic_vision_expose_height)
		end

	border_width: INTEGER is
			--TODO: suppress ?
		require
			current_event_type = 101
		do
			Result := basic_vision_geometry_border
		end

	is_left_down: BOOLEAN is
			-- Is mouse left button down ?
			-- NOTE: only access this information if last event is pointer_enter
			-- or pointer_leave.
		require
			current_event_type = 7 or else current_event_type = 8
		do
			Result := basic_vision_is_left_down
		end

	is_middle_down: BOOLEAN is
			-- Is mouse middle button down ?
			-- NOTE: only access this information if last event is pointer_enter
			-- or pointer_leave.
		require
			current_event_type = 7 or else current_event_type = 8
		do
			Result := basic_vision_is_middle_down
		end

	is_right_down: BOOLEAN is
			-- Is mouse right button down ?
			-- NOTE: only access this information if last event is pointer_enter
			-- or pointer_leave.
		require
			current_event_type = 7 or else current_event_type = 8
		do
			Result := basic_vision_is_right_down
		end

	current_event_type: INTEGER is
			-- Needed for some assertion. This is the number associated
			-- with the last event.
		do
			Result := event_catcher.current_event_type
		end

	current_event_native_type: INTEGER is
			-- Real event type for extension devices
		require
			current_event_type = 110
		do
			Result := event_catcher.current_event_native_type
		end

	when_focus_in (p: PROCEDURE[TUPLE]) is
		do
			connect0(root_window, p, event_catcher.event.item(9))
		end

	when_focus_in_signal: SIGNAL_0 is
		do
			Result := event_catcher.event.item(9).at(root_window)
		end

	when_focus_out (p: PROCEDURE[TUPLE]) is
		do
			connect0(root_window, p, event_catcher.event.item(10))
		end

	when_focus_out_signal: SIGNAL_0 is
		do
			Result := event_catcher.event.item(10).at(root_window)
		end

	preprocess_left_down (p: PROCEDURE[TUPLE]) is
		do
			event_catcher.preprocess_left_down(p)
		end

feature {ANY}
	root_window: ROOT_WINDOW is
			-- Gives access to the root window. Only "WINDOW" should 
			-- access the root_window. In rare case, low level access to 
			-- device may require access to the root_window.
			-- Use with care!!!
		once
			create Result
		end

feature {SENSITIVE}
	register (s: SENSITIVE) is
			-- Each widget who need to receive events has to be registred.
			-- NOTE: Register only once. Only windows need this, so don't
			-- care because precursor register your window for you when
			-- it is created.
		require
			not widget.fast_has(s.widget)
--***			s.widget /= default_pointer
--*** Hi Philippe, could you check this assertion which always 
--*** fails on Windows. (Dom. july 2nd 2007)
			
		do
			check
				not widget.fast_has(s.widget)
			end
			widget.add(s, s.widget)
		ensure
			widget.fast_has(s.widget)
		end

	unregister (s: SENSITIVE) is
			--TODO: how to manage destroy ?
		require
			widget.fast_has(s.widget)
		do
			check
				widget.fast_has(s.widget)
			end
			widget.remove(s.widget)
		ensure
			not widget.fast_has(s.widget)
		end

feature {SENSITIVE} -- All *_connect functions, used only in WHEN_* classes.
	key_down_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(2))
		end

	key_up_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(3))
		end

	pointer_enter_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(7))
		end

	pointer_leave_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(8))
		end

	expose_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(12))
		end

	unmapped_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(18))
		end

	mapped_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(19))
		end

	left_down_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(40))
		end

	left_up_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(50))
		end

	middle_down_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(41))
		end

	middle_up_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(51))
		end

	right_down_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(42))
		end

	right_up_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(52))
		end

	wheel_up_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(43))
		end

	wheel_down_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(44))
		end

	fully_visible_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(60))
		end

	partially_visible_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(61))
		end

	not_visible_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(62))
		end

	close_requested_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE]) is
		do
			connect0(s, p, event_catcher.event.item(63))
		end

	pointer_move_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE[INTEGER, INTEGER]]) is
		do
			if not event_catcher.pointer_move_event.has(s) then
				event_catcher.pointer_move_event.put(create {SIGNAL_2[INTEGER, INTEGER]}.make, s)
			end
			event_catcher.pointer_move_event.at(s).connect(p)
		end

	geometry_change_event_connect (s: SENSITIVE; p: PROCEDURE[TUPLE[INTEGER, INTEGER, INTEGER, INTEGER]]) is
		do
			if not event_catcher.geometry_change_event.has(s) then
				event_catcher.geometry_change_event.put(create {SIGNAL_4[INTEGER, INTEGER, INTEGER, INTEGER]}.make, s)
			end
			event_catcher.geometry_change_event.at(s).connect(p)
		end

feature {SENSITIVE} -- All *_signal functions, used only in WHEN_* classes.
	key_down_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(2).at(s)
		end

	key_up_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(3).at(s)
		end

	pointer_enter_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(7).at(s)
		end

	pointer_leave_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(8).at(s)
		end

	expose_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(12).at(s)
		end

	unmapped_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(18).at(s)
		end

	mapped_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(19).at(s)
		end

	left_down_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(40).at(s)
		end

	left_up_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(50).at(s)
		end

	middle_down_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(41).at(s)
		end

	middle_up_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(51).at(s)
		end

	right_down_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(42).at(s)
		end

	right_up_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(52).at(s)
		end

	wheel_up_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(43).at(s)
		end

	wheel_down_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(44).at(s)
		end

	fully_visible_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(60).at(s)
		end

	partially_visible_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(61).at(s)
		end

	not_visible_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(62).at(s)
		end

	close_requested_signal (s: SENSITIVE): SIGNAL_0 is
		do
			Result := event_catcher.event.item(63).at(s)
		end

	pointer_move_signal (s: SENSITIVE): SIGNAL_2[INTEGER, INTEGER] is
		do
			Result := event_catcher.pointer_move_event.at(s)
		end

	geometry_change_signal (s: SENSITIVE): SIGNAL_4[INTEGER, INTEGER, INTEGER, INTEGER] is
		do
			Result := event_catcher.geometry_change_event.at(s)
		end

feature {VISION_LOOP_STACK}
	event_catcher: EVENT_CATCHER
			-- Direct access to the current event catcher.

	new_event_catcher is
			-- New event catcher is needed each time new loop is needed.
			-- This new job is waiting on the same graphic connection.
		do
			create event_catcher.make(graphic_connection)
			widget := event_catcher.widget
			loop_stack.add_job(event_catcher)
		end

	change_event_catcher (ec: like event_catcher) is
		do
			event_catcher := ec
			widget := ec.widget
		end

feature {EXTENSION_DEVICE}
	register_extension(extension_device: EXTENSION_DEVICE) is
		require
			extension_device /= Void
		do
			event_catcher.add_extension(extension_device)
		end

feature {ANY}
	renderer: RENDERER is
		do
			Result := renderer_memory
			if Result = Void then
				Result := default_renderer
				renderer_memory := Result
			end
		ensure
			Result /= Void
		end

	set_renderer (a_renderer: like renderer) is
		require
			a_renderer /= Void
		do
			renderer_memory := a_renderer
		ensure
			renderer = a_renderer
		end

feature {}
	renderer_memory: RENDERER

	default_renderer: RENDERER is
		local
			system: SYSTEM
			style: STRING
		once
			style := system.get_environment_variable("VISION_STYLE")
			if style = Void then
				create {DEFAULT_RENDERER} Result
			else
				inspect style
				when "basic" then
					create {BASIC_RENDERER} Result
				when "classic" then
					create {CLASSIC_RENDERER} Result
				else
					create {DEFAULT_RENDERER} Result
				end
			end
		ensure
			Result /= Void
		end

feature {}
	graphic_connection: GRAPHIC_CONNECTION

	widget: DICTIONARY[SENSITIVE, POINTER]
			-- same as event_catcher.widget (often used in this class)

	connect0 (s: SENSITIVE; p: PROCEDURE[TUPLE]; d: DICTIONARY[SIGNAL_0, SENSITIVE]) is
		do
			--TODO: use reference_at
			if not d.fast_has(s) then
				d.add(create {SIGNAL_0}.make, s)
			end
			d.at(s).connect(p)
		end

	basic_vision_init: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_init"
	 }"
		end

	basic_vision_character: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_character"
	 }"
		end

	basic_vision_key: INTEGER is
		external "plug_in"
		alias "{
	location: "${sys}/plugins"
	module_name: "vision"
	feature_name: "basic_vision_key"
	}"
		end

	basic_vision_get_keysym (s: POINTER): INTEGER is
		external "plug_in"
		alias "{
	location: "${sys}/plugins"
	module_name: "vision"
	feature_name: "basic_vision_get_keysym"
	}"
		end

	basic_vision_pointer_x: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_pointer_x"
	 }"
		end

	basic_vision_pointer_y: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_pointer_y"
	 }"
		end

	basic_vision_pointer_x_root: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_pointer_x_root"
	 }"
		end

	basic_vision_pointer_y_root: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_pointer_y_root"
	 }"
		end

	basic_vision_event_time: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_event_time"
	 }"
		end

	basic_vision_expose_x: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_expose_x"
	 }"
		end

	basic_vision_expose_y: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_expose_y"
	 }"
		end

	basic_vision_expose_width: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_expose_width"
	 }"
		end

	basic_vision_expose_height: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_expose_height"
	 }"
		end

	basic_vision_geometry_border: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_geometry_border"
	 }"
		end

	basic_vision_display_width: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_display_width"
	 }"
		end

	basic_vision_display_height: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_display_height"
	 }"
		end

	basic_vision_is_left_down: BOOLEAN is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_is_left_down"
	 }"
		end

	basic_vision_is_middle_down: BOOLEAN is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_is_middle_down"
	 }"
		end

	basic_vision_is_right_down: BOOLEAN is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_is_right_down"
	 }"
		end

end -- class VISION
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
