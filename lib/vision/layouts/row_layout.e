-- See the Copyright notice at the end of this file.
--
class ROW_LAYOUT
	-- Puts all the WIDGETs in a row.

inherit
	LAYOUT
	GRAPHIC
		-- needed for expose_area acces from vision...
		redefine default_create
		end

creation {ANY}
	default_create

feature {}
	position_x, position_y: FAST_ARRAY[INTEGER]

	default_create is
		do
			create position_x.make(0)
			create position_y.make(0)
		end

feature {ANY}
	border: INTEGER

	spacing: INTEGER

	shrink_allowed_min_width, expand_allowed_std_width: INTEGER

	shrink_allowed_first, expand_allowed_first: WIDGET

	set_border (size: INTEGER) is
			-- space between the border and objects inside
		require
			size >= 0
		do
			if border /= size then
				border := size
				update_requisition
				--container.invalidate_layout
			end
		ensure
			border = size
		end

	set_spacing (size: INTEGER) is
			-- space between objects in the container
		require
			size >= 0
		do
			if spacing /= size then
				spacing := size
				update_requisition
				--container.invalidate_layout
			end
		ensure
			spacing = size
		end

	insert_button_space is
			-- Allow to group buttons before this space and after this space.
			-- Used before the first button or after the last: the button
			-- will not be attached to the border.
			-- NOTE: use only NOT expand_allowed objects.
		do
			container.child_attach(create {BUTTON_SPACE}.make)
		end

feature {CONTAINER}
	redo_layout (x, y: INTEGER) is
		do
			debug
				io.put_string("row_layout: redo layout%N")
			end
			if container.width = container.std_width then
				basic_dispatch(x, y)
			elseif container.width < container.std_width then
				shrink_dispatch(x, y)
			else
				if expand_allowed_first /= Void then
					expand_dispatch(x, y)
				else
					expand_spacing(x, y)
				end
			end
		ensure then
			container.child.count = position_y.count
			position_x.count = position_y.count
		end

	update_requisition is
		local
			child: FAST_ARRAY[WIDGET]; i: INTEGER; c: WIDGET; min_w, min_h: INTEGER; std_w, std_h: INTEGER
		do
			debug
				io.put_string("row_layout: update_requisition%N")
			end
			child := container.child
			from
				min_w := spacing * (child.count - 1).max(0) + 2 * border
				std_w := min_w
				shrink_allowed_min_width := 0
				expand_allowed_std_width := 0
				shrink_allowed_first := Void
				expand_allowed_first := Void
				i := child.upper
			until
				i < 0
			loop
				c := child.item(i)
				if c.y_shrink_allowed then
					min_h := min_h.max(c.min_height)
				else
					min_h := min_h.max(c.std_height)
				end
				std_w := std_w + c.std_width
				std_h := std_h.max(c.std_height)
				if c.x_shrink_allowed then
					shrink_allowed_first := c
					shrink_allowed_min_width := shrink_allowed_min_width + c.min_width.max(1)
					min_w := min_w + c.min_width
				else
					min_w := min_w + c.std_width
				end
				if c.x_expand_allowed then
					expand_allowed_first := c
					expand_allowed_std_width := expand_allowed_std_width + c.std_width
				end
				i := i - 1
			end
			min_h := min_h + 2 * border
			std_h := (std_h + 2 * border).max(1)
			std_w := std_w.max(1)
			container.set_requisition(min_w, min_h, std_w, std_h)
		end

	expose_paint is
		local
			child: FAST_ARRAY[WIDGET]; i: INTEGER; area: RECT; bottom, right: INTEGER
		do
			area := vision.expose_area
			from
				child := container.child
				i := child.lower
			until
				i > child.upper or else position_x.item(i) + child.item(i).width > area.x
			loop
				i := i + 1
			end
			from
				bottom := area.y + area.height
				right := area.x + area.width
			until
				i > child.upper or else position_x.item(i) >= right
			loop
				if bottom > position_y.item(i) and then position_y.item(i) + child.item(i).height > area.y then
					child.item(i).expose_paint
				end
				i := i + 1
			end
		end

feature {}
	basic_dispatch (x, y: INTEGER) is
			-- Display each child with std_width and use standard spacing
		local
			child: FAST_ARRAY[WIDGET]; i: INTEGER; x2, y2: INTEGER; w2, h2: INTEGER; h: INTEGER; c: WIDGET
		do
			from
				child := container.child
				i := child.upper
				position_x.resize(child.count)
				position_y.resize(child.count)
				h := container.height - border * 2
				x2 := x + container.width - border + spacing
			until
				i < 0
			loop
				c := child.item(i)
				w2 := c.std_width
				x2 := x2 - w2 - spacing
				position_x.put(x2, i)
				if h > c.std_height and then not c.y_expand_allowed then
					h2 := c.std_height
					y2 := y + border + (h - h2) #// 2
				else
					h2 := h
					y2 := y + border
				end
				position_y.put(y2, i)
				c.set_geometry(x2, y2, w2, h2)
				i := i - 1
			end
			check
				child.count /= 0 implies x2 = x + border
			end
		end

	expand_spacing (x, y: INTEGER) is
			-- No child may be expanded, so expand spaces between children
		local
			child: FAST_ARRAY[WIDGET]; i: INTEGER; x2, y2: INTEGER; w2, h2: INTEGER; h: INTEGER; c: WIDGET; more: INTEGER
			new_spacing: INTEGER; distribute: INTEGER
		do
			-- reminder pixels
			from
				child := container.child
				i := child.upper
				position_x.resize(child.count)
				position_y.resize(child.count)
				h := container.height - border * 2
				x2 := x + container.width - border + spacing
				more := container.width - container.std_width
				new_spacing := more #// (child.count + 1) + spacing
				distribute := more #\\ (child.count + 1)
			until
				i < 0
			loop
				c := child.item(i)
				w2 := c.std_width
				if distribute > 0 then
					x2 := x2 - w2 - new_spacing - 1
					distribute := distribute - 1
				else
					x2 := x2 - w2 - new_spacing
				end
				position_x.put(x2, i)
				if h > c.std_height and then not c.y_expand_allowed then
					h2 := c.std_height
					y2 := y + border + (h - h2) #// 2
				else
					h2 := h
					y2 := y + border
				end
				position_y.put(y2, i)
				c.set_geometry(x2, y2, w2, h2)
				i := i - 1
			end
			check
				distribute = 0
				child.count > 0 implies x2 = x + border + new_spacing - spacing
			end
		end

	expand_dispatch (x, y: INTEGER) is
			-- At least one child have to be expanded to fill the space
		local
			child: FAST_ARRAY[WIDGET]; i: INTEGER; x2, y2: INTEGER; w2, h2: INTEGER; h: INTEGER; c: WIDGET; grow: REAL
			distribute: INTEGER
		do
			from
				child := container.child
				i := child.upper
				position_x.resize(child.count)
				position_y.resize(child.count)
				h := container.height - border * 2
				x2 := x + container.width - border + spacing
				distribute := container.width - container.std_width
				grow := distribute / expand_allowed_std_width + 1.0
				check
					grow >= 1.0
				end
			until
				i < 0
			loop
				c := child.item(i)
				if c.x_expand_allowed then
					if c = expand_allowed_first then
						w2 := c.std_width + distribute
					else
						w2 := (c.std_width * grow).force_to_integer_32
						distribute := distribute - (w2 - c.std_width)
					end
				else
					w2 := c.std_width
				end
				x2 := x2 - w2 - spacing
				position_x.put(x2, i)
				if h > c.std_height and then not c.y_expand_allowed then
					h2 := c.std_height
					y2 := y + border + (h - h2) #// 2
				else
					h2 := h
					y2 := y + border
				end
				position_y.put(y2, i)
				c.set_geometry(x2, y2, w2, h2)
				i := i - 1
			end
		end

	shrink_dispatch (x, y: INTEGER) is
			-- At least one child have to be shrinked
		local
			child: FAST_ARRAY[WIDGET]; i: INTEGER; x2, y2: INTEGER; w2, h2: INTEGER; h: INTEGER; c: WIDGET; grow: REAL
			distribute: INTEGER
		do
			from
				child := container.child
				i := child.upper
				position_x.resize(child.count)
				position_y.resize(child.count)
				h := container.height - border * 2
				x2 := x + container.width - border + spacing
				distribute := container.width - container.min_width
				if not child.is_empty then
					grow := distribute / shrink_allowed_min_width + 1.0
				end
				check
					child.count > 0 implies grow >= 1.0
				end
			until
				i < 0
			loop
				c := child.item(i)
				if c.x_shrink_allowed then
					if c = shrink_allowed_first then
						w2 := c.min_width + distribute
					else
						w2 := (c.min_width.max(1) * grow).force_to_integer_32
						distribute := distribute - (w2 - c.min_width)
						if distribute < 0 then
							w2 := w2 + distribute
							distribute := 0
						end
					end
				else
					w2 := c.std_width
				end
				x2 := x2 - w2 - spacing
				position_x.put(x2, i)
				if h > c.std_height and then not c.y_expand_allowed then
					h2 := c.std_height
					y2 := y + border + (h - h2) #// 2
				else
					h2 := h
					y2 := y + border
				end
				position_y.put(y2, i)
				c.set_geometry(x2, y2, w2, h2)
				i := i - 1
			end
		end

	move (diff_x, diff_y: INTEGER) is
		local
			i: INTEGER
		do
			from
				i := position_x.upper
			until
				i < 0
			loop
				position_x.put(position_x.item(i) + diff_x, i)
				position_y.put(position_y.item(i) + diff_y, i)
				i := i - 1
			end
		end

end -- class ROW_LAYOUT
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
