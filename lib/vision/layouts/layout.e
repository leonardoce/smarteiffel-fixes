-- See the Copyright notice at the end of this file.
--
deferred class LAYOUT
	-- A LAYOUT is an algorithm that organise WIDGETs in a CONTAINER.

insert
	ANY
		redefine default_create
		end

feature {}
	default_create is
		deferred
		ensure
			container = Void
		end

feature {ANY}
	container: CONTAINER

feature {CONTAINER}
	set_container (c: CONTAINER) is
		require
			container = Void
			c /= Void
			c.layout = Current
		do
			container := c
		ensure
			container = c
		end

	detach is
		require
			container /= Void
		do
			container := Void
		ensure
			container = Void
		end

	redo_layout (x, y: INTEGER) is
		require
			container /= Void
		deferred
		ensure
			valid_size(container)
		end

	update_requisition is
		require
			container /= Void
		deferred
		end

	expose_paint is
		require
			container /= Void
		deferred
		end

	valid_size (c: CONTAINER): BOOLEAN is
		local
			i: INTEGER; child: FAST_ARRAY[WIDGET]; w: WIDGET
		do
			from
				Result := True
				child := c.child
				i := child.upper
			until
				i < child.lower or Result = False
			loop
				w := child.item(i)
				Result := w.valid_width(w.width) and w.valid_height(w.height)
				i := i - 1
			end
		end

end -- class LAYOUT
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
