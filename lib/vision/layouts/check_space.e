-- See the Copyright notice at the end of this file.
--
class CHECK_SPACE
	-- This class is only for inserting the space needed to draw a check box.
	-- TODO: Should be created by renderer.

inherit
	WIDGET
		redefine default_create
		end
	GRAPHIC -- need `renderer'
		redefine default_create
		end

creation {CHECK_BUTTON}
	default_create

feature {}
	default_create is
		do
			x_expand_allowed := False
			y_expand_allowed := False
			min_width := 10
			min_height := 10
			std_width := 10
			std_height := 10
			width := 10
			height := 10
		end

feature {ANY}
	min_width: INTEGER

	min_height: INTEGER

	std_width: INTEGER

	std_height: INTEGER

	width: INTEGER

	height: INTEGER

feature {LAYOUT}
	expose_paint is
		do
			renderer.draw_check_button(Current)
		end

	set_geometry (x, y, w, h: INTEGER) is
		do
			pos_x := x
			pos_y := y
			width := w
			height := h
		end

feature {}
	resize (w, h: INTEGER) is
		do
			check
				False
			end
		end

end -- class CHECK_SPACE
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
