-- See the Copyright notice at the end of this file.
--
expanded class RECT
	-- Describe rectangular area.

feature {ANY}
	x: INTEGER

	y: INTEGER

	width: INTEGER

	height: INTEGER

	make (new_x, new_y, new_width, new_height: INTEGER) is
			-- Initialize all values at the same time.
		require
			new_width >= 0
			new_height >= 0
		do
			x := new_x
			y := new_y
			width := new_width
			height := new_height
		ensure
			x = new_x
			y = new_y
			width = new_width
			height = new_height
		end

	set_x (new_x: INTEGER) is
		do
			x := new_x
		ensure
			x = new_x
		end

	set_y (new_y: INTEGER) is
		do
			y := new_y
		ensure
			y = new_y
		end

	move_to (new_x, new_y: INTEGER) is
			-- Move the rectangle origin to (new_x, new_y) point.
		do
			x := new_x
			y := new_y
		ensure
			x = new_x
			y = new_y
		end

	offset (dx, dy: INTEGER) is
			-- Move the rectangle origin by `dx' and `dy' offsets (delta).
		do
			x := x + dx
			y := y + dy
		ensure
			x = old x + dx
			y = old y + dy
		end

	resize (new_width, new_height: INTEGER) is
			-- Change the rectangle size to `new_width' x `new_height'
		require
			new_width >= 0
			new_height >= 0
		do
			height := new_height
			width := new_width
		ensure
			width = new_width
			height = new_height
		end

	inflate (dw, dh: INTEGER) is
			-- Change width by `dw' value and height by `dh' value (delta).
		require
			width + dw >= 0
			height + dh >= 0
		do
			width := width + dw
			height := height + dh
		ensure
			width = old width + dw
			height = old height + dh
		end

	set_empty is
		do
			width := 0
			height := 0
		ensure
			width = 0
			height = 0
		end

	is_empty: BOOLEAN is
		do
			Result := width = 0 or else height = 0
		ensure
			Result = (width = 0 or height = 0)
		end

	intersect (other: RECT): RECT is
			-- Return the area common to `Current' with `other'.
			-- Result width and height are 0 if intersect area is empty.
		local
			rx, ry, rw, rh: INTEGER
		do
			rx := x.max(other.x)
			rw := (x + width).min(other.x + other.width) - rx
			if rw > 0 then
				ry := y.max(other.y)
				rh := (y + height).min(other.y + other.height) - ry
				if rh > 0 then
					Result.make(rx, ry, rw, rh)
				end
			end
		ensure
			Result.width = 0 = (Result.height = 0)
			Result.width = 0 implies Result.x = 0
			Result.width = 0 implies Result.y = 0
			;(other.x < x + width and other.y < y + height and x < other.x + other.width and y < other.y + other.height) = (Result.width /= 0)
		end

	intersect_def (ox, oy, ow, oh: INTEGER): RECT is
			-- Same as `intersect' with other given by it's values
			-- rather than a RECT object.
		require
			ow >= 0
			oh >= 0
		local
			rx, ry, rw, rh: INTEGER
		do
			rx := x.max(ox)
			rw := (x + width).min(ox + ow) - rx
			if rw > 0 then
				ry := y.max(oy)
				rh := (y + height).min(oy + oh) - ry
				if rh > 0 then
					Result.make(rx, ry, rw, rh)
				end
			end
		ensure
			Result.width = 0 = (Result.height = 0)
			Result.width = 0 implies Result.x = 0
			Result.width = 0 implies Result.y = 0
			;(ox < x + width and oy < y + height and x < ox + ow and y < oy + oh) = (Result.width /= 0)
		end

	union (other: RECT): RECT is
			-- Return the smallest rectangle containing `Current' and `other'.
		local
			rx, ry, rw, rh: INTEGER
		do
			if is_empty then
				Result := other
			elseif other.is_empty then
				Result := Current
			else
				rx := x.min(other.x)
				ry := y.min(other.y)
				rw := (x + width).max(other.x + other.width) - rx
				rh := (y + height).max(other.y + other.height) - ry
				Result.make(rx, ry, rw, rh)
			end
		ensure
			Current.is_empty implies Result = other
			not Current.is_empty and other.is_empty implies Result = Current
			not Current.is_empty implies Result.intersect(Current) = Current
			not other.is_empty implies Result.intersect(other) = other
		end

	contain, include (point_x, point_y: INTEGER): BOOLEAN is
			-- Does the point belongs to the rectangle?
		do
			Result := point_x >= x and then point_y >= y and then point_x < x + width and then point_y < y + height
		ensure
			Result = not Current.intersect_def(point_x, point_y, 1, 1).is_empty
		end

invariant
	width >= 0
	height >= 0

end -- class RECT
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
