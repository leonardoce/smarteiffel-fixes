-- See the Copyright notice at the end of this file.
--
deferred class IMAGE
	-- This class allow to draw an image in memory (process memory)
	-- and send it to the display. Each draw require lots of network
	-- if the display is remote. A draw require hard work to convert
	-- the image into display format (be the display local or remote).
	-- See `PIXMAP' for image stored in the "display" memory.

inherit
	DRAWABLE

feature {}
	widget_size_init (width_, height_: INTEGER) is
		require
			width_ > 0
			height_ > 0
		do
			width := width_
			height := height_
			min_width := width
			min_height := height
			std_width := width
			std_height := height
			--create storage.make_filled('%/000/',width*height*4) --(depth//8))
			count := width * height * 4
			storage := storage.calloc(count)
			--|*** Warning, `depth' is not initialized. *** Dom sept 27th 2004 ***
			widget := basic_image_create(width_, height_, depth, storage.to_external, $drawing_widget)
		end

feature {ANY}
	width: INTEGER

	height: INTEGER

	depth: INTEGER

	min_width: INTEGER

	min_height: INTEGER

	std_width: INTEGER

	std_height: INTEGER

	widget: POINTER

	storage: NATIVE_ARRAY[CHARACTER]

	count: INTEGER

	put (c: CHARACTER; i: INTEGER) is
			-- Put `c' at position `index'.
		require
			valid_index: i.in_range(0, count - 1)
		do
			storage.put(c, i)
		end

feature {LAYOUT}
	expose_paint is
		do
			basic_image_draw(parent.drawing_widget, drawing_widget, pos_x, pos_y, width, height)
		end

	set_geometry (x, y, w, h: INTEGER) is
		do
			pos_x := x
			pos_y := y
			width := w
			height := h
		end

feature {}
	basic_image_create (w, h, d: INTEGER; data: POINTER; drawable: POINTER): POINTER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "image"
	 feature_name: "basic_image_create"
	 }"
		end

	basic_image_draw (win: POINTER; img: POINTER; x, y, w, h: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "image"
	 feature_name: "basic_image_draw"
	 }"
		end

end -- class IMAGE
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
