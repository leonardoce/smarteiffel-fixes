-- See the Copyright notice at the end of this file.
--
class TEXT_FIELD
	--
	-- Class creates a text field which allows the user to grab a single line
	-- of text.

inherit
	SUB_WINDOW
		redefine make
		end

creation {ANY}
	make

feature {}
	make (p: like parent) is
		do
			Precursor(p)
			set_background_color(white_color)
			set_x_expand(True)
			create text.make(0)
			-- label will contain typed text
			create label.make(text)
			label.set_x_expand(True)
			-- attaches label to the SUB_WINDOW
			child_attach(label)
			child_attach(create {CURSOR_SPACE})
			--TODO: try to avoid this
			when_key_down(agent key_is_down)
			when_pointer_enter(agent start_cursor)
			when_pointer_leave(agent stop_cursor)
			map
		end

feature {ANY}
	text: UNICODE_STRING

	set_text (new_text: like text) is
		do
			text.copy(new_text)
			label.set_text(text)
		end

feature {}
	label: LABEL

	key_is_down is
		do
			if vision.last_character = 8 then
				-- backspace pressed, so we remove last character
				text.remove_last
				-- Update to screen
				label.set_text(text)
			elseif vision.last_character > 27 then
				-- character pressed, append to text field
				text.add_last(vision.last_character)
				-- Update to screen
				label.set_text(text)
			end
			cursor_pos_x := label.std_width
			blink_cursor.move
		end

	start_cursor is
		do
			blink_cursor.start_in(Current)
		end

	stop_cursor is
		do
			blink_cursor.stop
		end

	blink_cursor: TEXT_CURSOR_JOB is
		once
			create Result
		end

feature {TEXT_CURSOR_JOB}
	cursor_pos_x: INTEGER

	cursor_pos_y: INTEGER

	cursor_height: INTEGER is
		do
			Result := label.height
		end

end -- class TEXT_FIELD
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
