-- See the Copyright notice at the end of this file.
--
deferred class STATE
	--
	-- This class describe the state of widgets.
	--
	
insert
	STATE_CONSTANTS

feature {ANY}
	state: INTEGER -- use values from STATE_CONSTANTS

	frozen is_state_normal: BOOLEAN is
		do
			Result := state = state_normal
		end

	frozen is_state_active: BOOLEAN is
		do
			Result := state = state_active
		end

	frozen is_state_prelight: BOOLEAN is
		do
			Result := state = state_prelight
		end

	frozen is_state_selected: BOOLEAN is
		do
			Result := state = state_selected
		end

	frozen is_state_insensitive: BOOLEAN is
		do
			Result := state = state_insensitive
		end

feature {}
	frozen set_state_normal is
		do
			set_state(state_normal)
		end

	frozen set_state_active is
		do
			set_state(state_active)
		end

	frozen set_state_prelight is
		do
			set_state(state_prelight)
		end

	frozen set_state_selected is
		do
			set_state(state_selected)
		end

	frozen set_state_insensitive is
		do
			set_state(state_insensitive)
		end

	set_state (n: INTEGER) is
		do
			state := n
		end

end -- class STATE
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
