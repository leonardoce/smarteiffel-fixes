-- See the Copyright notice at the end of this file.
--
class CONTAINER
	-- A CONTAINER is designed to contain many widgets (called children).
	-- Children are arranged in the container using some LAYOUT, and painted.
	-- The CONTAINER may be seen as a shell: if the container is not
	-- shrinkable, it will not shrink any of it's children. If the
	-- container is shrinkable, it may shrink children that are shrinkable
	-- but never shrink not shrinkable children (it's like a shell again).
	--
	-- The LAYOUT is an algorithm attached to the container. It has to
	-- decide size and position for children.
	--

inherit
	DRAWABLE
		redefine default_create, computing_size, set_parent
		end
	SENSITIVE
		redefine default_create
		end

creation {ANY}
	default_create, make_layout

feature {}
	default_create is
		do
			container_init
			min_width := 0
			min_height := 0
			std_width := 1
			std_height := 1
			width := 1
			height := 1
		end

	make_layout (p: like parent; lo: like layout) is
		require
			p /= Void
			lo /= Void
			lo.container = Void
		do
			default_create
			layout := lo
			lo.set_container(Current)
			p.child_attach(Current)
		ensure
			parent = p
			layout = lo
		end

	container_init is
		do
			create child.make(0)
		end

feature {ANY}
	layout: LAYOUT

	width, height: INTEGER

	min_width, min_height: INTEGER

	std_width, std_height: INTEGER

feature {LAYOUT}
	child: FAST_ARRAY[like last_child]

feature {ANY}
	layout_update_paused: BOOLEAN --TODO: suppress. Handle this with mapped

	set_layout (l: like layout) is
			-- Change the layout for the container (layout choose children
			-- position and size). The layout has to be free (not used
			-- by another container).
		require
			l /= Void
			l.container = Void
		do
			if layout /= Void then
				layout.detach
			end
			layout := l
			l.set_container(Current)
			child_requisition_changed
		ensure
			layout = l
			layout.container = Current
			not layout_update_paused implies layout_ready
		end

	layout_pause is
			--TODO: remove when mapped ready
		require
			not layout_update_paused
		do
			layout_update_paused := True
		ensure
			layout_update_paused
		end

	layout_continue is
			--TODO: remove when mapped ready
		require
			layout_update_paused
		do
			layout_update_paused := False
			child_requisition_changed
		ensure
			layout_ready
			not layout_update_paused
		end

	child_attach (w: like last_child) is
			-- Add widget `w' in this container.
			--
			-- See also `child_detach', `has_child'.
		require
			layout /= Void
			w /= Void
			w.parent = Void
			not has_child(w)
		do
			child.add_last(w)
			w.set_parent(Current)
			child_requisition_changed
		ensure
			w.parent = Current
			has_child(w)
			last_child = w
			not layout_update_paused implies layout_ready or not done --TODO: not mapped implies not layout_ready???
		end

	child_detach (w: like last_child) is
			-- Remove widget `w' from this container.
			--
			-- See also `child_attach', `has_child'.
		require
			w /= Void
			has_child(w)
		do
			w.set_parent(Void)
			child.remove(child.fast_first_index_of(w))
			child_requisition_changed
		ensure
			child.count = old child.count - 1
			not has_child(w)
			w.parent = Void
			not layout_update_paused implies layout_ready --TODO: not mapped implies not layout_ready???
		end

	has_child (w: like last_child): BOOLEAN is
		do
			Result := child.fast_has(w)
		end

	last_child: WIDGET is
		require
			not is_empty
		do
			Result := child.last
		end

	is_empty: BOOLEAN is
		do
			Result := child.is_empty
		end

	clear_area (x, y, w, h: INTEGER) is
			-- clear area and emit expose event (contents will be drawn)
			-- x and y are relative to current object
		require
			w > 0
			h > 0
			area.include(x, y)
			area.include(x + w - 1, y + h - 1)
		do
			basic_window_clear_area(widget, x, y, w, h)
		end

	clear_without_expose is
		do
			basic_window_clear_area_no_expose(widget, pos_x, pos_y, width, height)
		end

	refresh, clear is
			-- clear and update entire object (sub_window(s) are not updated).
		do
			clear_area(pos_x, pos_y, width, height)
		end

feature {WIDGET}
	as_x_root (x: INTEGER): INTEGER is
		require
			x >= pos_x
			x < pos_x + width
		do
			Result := parent.as_x_root(x)
		end

	as_y_root (y: INTEGER): INTEGER is
		require
			y >= pos_y
			y < pos_y + height
		do
			Result := parent.as_y_root(y)
		end

	done: BOOLEAN --TODO: suppress. Handle this with mapped

	child_requisition_changed is
		require
			layout /= Void
		do
			if not layout_update_paused then
				done := True
				layout.update_requisition
			else
				done := False
			end
		ensure
			not layout_update_paused implies layout_ready or not done
		end

feature {LAYOUT}
	expose_paint is
		require
			layout /= Void
		do
			check
				drawing_widget = default_pointer
			end
			drawing_widget := parent.drawing_widget
			if not layout_ready then
				dispatch
				check
					False
				end
			end
			layout.expose_paint
			drawing_widget := default_pointer
		end

	set_geometry (x, y, w, h: INTEGER) is
			-- Warning: do not redraw content if not window subtype
		do
			--TODO: detect move only cases.
			if width /= w or else height /= h or else pos_x /= x or else pos_y /= y then
				pos_x := x
				pos_y := y
				width := w
				height := h
				invalidate_layout
				dispatch
			end
		end

	set_requisition (min_w, min_h, std_w, std_h: INTEGER) is
		require
			min_w.in_range(0, std_w)
			min_h.in_range(0, std_h)
		do
			if min_width /= min_w then
				min_width := min_w
				requisition_changed := True
			end
			if min_height /= min_h then
				min_height := min_h
				requisition_changed := True
			end
			if std_width /= std_w then
				std_width := std_w
				requisition_changed := True
			end
			if std_height /= std_h then
				std_height := std_h
				requisition_changed := True
			end
			invalidate_layout
			if requisition_changed then
				parent.child_requisition_changed
				requisition_changed := False
				done := parent.done
			else
				-- we are on top of hierarchy concerned with the change.
				-- Now we need to erase the area, redo layouts and redraw
				clear_area(pos_x, pos_y, width, height)
			end
			if not layout_ready and then done then
				if parent.layout_ready then
					dispatch
				else
					done := False
				end
			end
		ensure
			not layout_update_paused implies layout_ready or not done
		end

feature {CONTAINER}
	layout_ready: BOOLEAN

	set_parent (p: CONTAINER) is
		do
			parent := p
			if p = Void then
				widget := default_pointer
			else
				widget := p.widget
			end
		end

feature {}
	requisition_changed: BOOLEAN

	invalidate_layout is
		do
			layout_ready := False
		ensure
			not layout_ready
		end

	computing_size: BOOLEAN is
			-- This is only used for invariant defined in `widget'. Sizes
			-- may be invalid while they are computed
		do
			Result := not layout_ready --TODO: find better definition
		end

	update_decoration is
		do
		end

	resize (w, h: INTEGER) is
			-- Warning: do not redraw content if not window subtype
		do
			if width /= w or else height /= h then
				width := w
				height := h
				child_requisition_changed
			end
		end

	dispatch is
			-- Should be called only from `set_geometry' and `set_requisition'.
		require
			not layout_ready
			layout /= Void
		do
			layout.redo_layout(pos_x, pos_y)
			layout_ready := True
		ensure
			layout_ready
		end

	basic_window_clear_area (window: POINTER; x, y, w, h: INTEGER) is
			--TODO: invalid for pixmap. Redefine allowed ?
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "window"
			feature_name: "basic_window_clear_area"
			}"
		end

	basic_window_clear_area_no_expose (window: POINTER; x, y, w, h: INTEGER) is
			--TODO: invalid for pixmap. Redefine allowed ?
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "window"
			feature_name: "basic_window_clear_area_no_expose"
			}"
		end

invariant
	child /= Void

end -- class CONTAINER
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
