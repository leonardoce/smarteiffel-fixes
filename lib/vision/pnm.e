-- See the Copyright notice at the end of this file.
--
class PNM
	-- This object is able to read PNM file format.
	-- 
	-- The pixels data are in `image' array. Either modifiy the image 
	-- by changing values in this array or use a `PIXMAP' object to 
	-- display this image.

create {ANY}
	make

feature {}
	make (file_to_read: STRING) is
		require
			file_tools.is_file(file_to_read) and then file_tools.is_readable(file_to_read)
		local
			text_file_read: TEXT_FILE_READ
		do
			create text_file_read.connect_to(file_to_read)
			read(text_file_read)
		end

feature {ANY}
	width, height, max_value: INTEGER

	bitmap, grey_scale, color: BOOLEAN
	
	image: FAST_ARRAY[CHARACTER]
	
feature {}
	read(input: INPUT_STREAM) is
		local
			read_max, ascii: BOOLEAN
			i, size: INTEGER
			valid: BOOLEAN
		do
			-- Reset default values
			bitmap := False
			grey_scale := False
			color := False
			read_max := False
			
			-- Read magic value
			input.read_character
			valid := not input.end_of_input and then input.last_character = 'P'
			check
				valid
			end
			if valid then
				input.read_character
				valid := not input.end_of_input
				check
					valid
				end
			end
			if valid then
				inspect input.last_character
				when '1', '4' then
					max_value := 1
					bitmap := True
				when '2', '5' then
					read_max := True
					grey_scale := True
				when '3', '6' then
					read_max := True
					color := True
				else
					valid := False
					check
						False
					end
				end
				ascii := input.last_character <= '3'
			end

			-- Read image width
			if valid then
				valid := read_integer(input)
				width := last_integer
				check
					valid
				end
			end

			-- Read image height
			if valid then
				valid := read_integer(input)
				height := last_integer
				check
					valid
				end
			end

			-- Read max grey
			if valid and then read_max  then
				valid := read_integer(input)
				max_value := last_integer
				check
					valid
					max_value < 256
				end
			end

			-- Init storage
			if valid then
				size := width * height
				if color then
					size := size * 3
				end
				if image = Void then
 					create image.make(size)
				else
					image.resize(size)
				end
			end
			
			-- Read image data
			if valid then
				if ascii then
					from
						i := 0
					until
						i >= size or else not valid
					loop
						valid := read_integer(input)
						check
							valid
							last_integer <= max_value
						end
						image.put(last_integer.to_character, i)
						i := i + 1
					end
				else
					input.read_character
					valid := not input.end_of_input
					check
						valid
					end
					if valid then
						from
							input.read_character
							i := 0
						until
							i >= size or else input.end_of_input
						loop
							check
								input.last_character.code <= max_value
							end
							image.put(input.last_character, i)
							i := i + 1
							input.read_character
						end
					end
				end
			end
		end
	
	read_integer(input: INPUT_STREAM): BOOLEAN is
		do
			from
				input.read_character
			until
				input.end_of_input or else input.last_character.is_digit
			loop
				if input.last_character = '#' then
					input.read_line
					if not input.end_of_input then
						input.read_character
					end
				else
					input.read_character
				end
			end
			Result := not input.end_of_input
			check
				Result
			end
			if Result then
				input.unread_character
				input.read_integer
				last_integer := input.last_integer
				Result := not input.end_of_input
				check
					Result
				end
			end
		end

	last_integer: INTEGER

	file_tools: FILE_TOOLS
end -- class PNM
			
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
