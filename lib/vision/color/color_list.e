-- See the Copyright notice at the end of this file.
--
class COLOR_LIST
	--
	-- THIS FILE WAS GENERATED. DON'T EDIT
	--

insert
	ANY

feature {}
	white_color: COLOR is
		once
			create Result.like_rgb_8(255, 255, 255)
		end

	black_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 0)
		end

	dim_grey_color: COLOR is
		once
			create Result.like_rgb_8(105, 105, 105)
		end

	dark_grey_color: COLOR is
		once
			create Result.like_rgb_8(169, 169, 169)
		end

	grey_color: COLOR is
		once
			create Result.like_rgb_8(190, 190, 190)
		end

	light_grey_color: COLOR is
		once
			create Result.like_rgb_8(211, 211, 211)
		end

	dark_blue_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 139)
		end

	medium_blue_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 205)
		end

	blue_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 255)
		end

	royal_blue_color: COLOR is
		once
			create Result.like_rgb_8(65, 105, 225)
		end

	deep_sky_blue_color: COLOR is
		once
			create Result.like_rgb_8(0, 191, 255)
		end

	sky_blue_color: COLOR is
		once
			create Result.like_rgb_8(135, 206, 235)
		end

	light_sky_blue_color: COLOR is
		once
			create Result.like_rgb_8(135, 206, 250)
		end

	steel_blue_color: COLOR is
		once
			create Result.like_rgb_8(70, 130, 180)
		end

	light_steel_blue_color: COLOR is
		once
			create Result.like_rgb_8(176, 196, 222)
		end

	light_blue_color: COLOR is
		once
			create Result.like_rgb_8(173, 216, 230)
		end

	pale_turquoise_color: COLOR is
		once
			create Result.like_rgb_8(175, 238, 238)
		end

	dark_turquoise_color: COLOR is
		once
			create Result.like_rgb_8(0, 206, 209)
		end

	medium_turquoise_color: COLOR is
		once
			create Result.like_rgb_8(72, 209, 204)
		end

	turquoise_color: COLOR is
		once
			create Result.like_rgb_8(64, 224, 208)
		end

	dark_cyan_color: COLOR is
		once
			create Result.like_rgb_8(0, 139, 139)
		end

	cyan_color: COLOR is
		once
			create Result.like_rgb_8(0, 255, 255)
		end

	light_cyan_color: COLOR is
		once
			create Result.like_rgb_8(224, 255, 255)
		end

	dark_green_color: COLOR is
		once
			create Result.like_rgb_8(0, 100, 0)
		end

	green_color: COLOR is
		once
			create Result.like_rgb_8(0, 255, 0)
		end

	light_green_color: COLOR is
		once
			create Result.like_rgb_8(144, 238, 144)
		end

	yellow_green_color: COLOR is
		once
			create Result.like_rgb_8(154, 205, 50)
		end

	dark_khaki_color: COLOR is
		once
			create Result.like_rgb_8(189, 183, 107)
		end

	khaki_color: COLOR is
		once
			create Result.like_rgb_8(240, 230, 140)
		end

	yellow_color: COLOR is
		once
			create Result.like_rgb_8(255, 255, 0)
		end

	light_yellow_color: COLOR is
		once
			create Result.like_rgb_8(255, 255, 224)
		end

	gold_color: COLOR is
		once
			create Result.like_rgb_8(255, 215, 0)
		end

	beige_color: COLOR is
		once
			create Result.like_rgb_8(245, 245, 220)
		end

	chocolate_color: COLOR is
		once
			create Result.like_rgb_8(210, 105, 30)
		end

	firebrick_color: COLOR is
		once
			create Result.like_rgb_8(178, 34, 34)
		end

	brown_color: COLOR is
		once
			create Result.like_rgb_8(165, 42, 42)
		end

	dark_salmon_color: COLOR is
		once
			create Result.like_rgb_8(233, 150, 122)
		end

	salmon_color: COLOR is
		once
			create Result.like_rgb_8(250, 128, 114)
		end

	light_salmon_color: COLOR is
		once
			create Result.like_rgb_8(255, 160, 122)
		end

	dark_orange_color: COLOR is
		once
			create Result.like_rgb_8(255, 140, 0)
		end

	orange_color: COLOR is
		once
			create Result.like_rgb_8(255, 165, 0)
		end

	orange_red_color: COLOR is
		once
			create Result.like_rgb_8(255, 69, 0)
		end

	dark_red_color: COLOR is
		once
			create Result.like_rgb_8(139, 0, 0)
		end

	red_color: COLOR is
		once
			create Result.like_rgb_8(255, 0, 0)
		end

	hot_pink_color: COLOR is
		once
			create Result.like_rgb_8(255, 105, 180)
		end

	deep_pink_color: COLOR is
		once
			create Result.like_rgb_8(255, 20, 147)
		end

	pink_color: COLOR is
		once
			create Result.like_rgb_8(255, 192, 203)
		end

	light_pink_color: COLOR is
		once
			create Result.like_rgb_8(255, 182, 193)
		end

	pale_violet_red_color: COLOR is
		once
			create Result.like_rgb_8(219, 112, 147)
		end

	maroon_color: COLOR is
		once
			create Result.like_rgb_8(176, 48, 96)
		end

	medium_violet_red_color: COLOR is
		once
			create Result.like_rgb_8(199, 21, 133)
		end

	violet_red_color: COLOR is
		once
			create Result.like_rgb_8(208, 32, 144)
		end

	violet_color: COLOR is
		once
			create Result.like_rgb_8(238, 130, 238)
		end

	dark_magenta_color: COLOR is
		once
			create Result.like_rgb_8(139, 0, 139)
		end

	magenta_color: COLOR is
		once
			create Result.like_rgb_8(255, 0, 255)
		end

	dark_violet_color: COLOR is
		once
			create Result.like_rgb_8(148, 0, 211)
		end

	blue_violet_color: COLOR is
		once
			create Result.like_rgb_8(138, 43, 226)
		end

	medium_purple_color: COLOR is
		once
			create Result.like_rgb_8(147, 112, 219)
		end

	purple_color: COLOR is
		once
			create Result.like_rgb_8(160, 32, 240)
		end

end -- class COLOR_LIST
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
