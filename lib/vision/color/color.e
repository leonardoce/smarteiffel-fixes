-- See the Copyright notice at the end of this file.
--
class COLOR

creation {ANY}
	like_rgb, like_rgb_8, like_rgb_16

feature {}
	like_rgb (r, g, b: REAL) is
		require
			r.in_range(0.0, 1.0)
			g.in_range(0.0, 1.0)
			b.in_range(0.0, 1.0)
		do
			red := (r * 65535).force_to_integer_32
			green := (g * 65535).force_to_integer_32
			blue := (b * 65535).force_to_integer_32
			storage := basic_vision_get_color($red, $green, $blue)
		end

	like_rgb_8 (r, g, b: INTEGER) is
		require
			r.in_range(0, 255)
			g.in_range(0, 255)
			b.in_range(0, 255)
		do
			red := r * 256
			green := g * 256
			blue := b * 256
			storage := basic_vision_get_color($red, $green, $blue)
		end

	like_rgb_16 (r, g, b: INTEGER) is
		require
			r.in_range(0, 65535)
			g.in_range(0, 65535)
			b.in_range(0, 65535)
		do
			red := r
			green := g
			blue := b
			storage := basic_vision_get_color($red, $green, $blue)
		end

feature {ANY}
	red: INTEGER

	green: INTEGER

	blue: INTEGER

feature {WINDOW, DRAW_STYLE}
	storage: POINTER

feature {}
	basic_vision_get_color (r, g, b: POINTER): POINTER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_get_color"
	 }"
		end

invariant
	red.in_range(0, 65535)
	green.in_range(0, 65535)
	blue.in_range(0, 65535)

end -- class COLOR
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
