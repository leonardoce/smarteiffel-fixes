-- See the Copyright notice at the end of this file.
--
expanded class FULL_COLOR_LIST
	--
	-- THIS FILE WAS GENERATED. DON'T EDIT
	--

feature {ANY}
	snow_color: COLOR is
		once
			create Result.like_rgb_8(255, 250, 250)
		end

	ghost_white_color: COLOR is
		once
			create Result.like_rgb_8(248, 248, 255)
		end

	white_smoke_color: COLOR is
		once
			create Result.like_rgb_8(245, 245, 245)
		end

	gainsboro_color: COLOR is
		once
			create Result.like_rgb_8(220, 220, 220)
		end

	floral_white_color: COLOR is
		once
			create Result.like_rgb_8(255, 250, 240)
		end

	old_lace_color: COLOR is
		once
			create Result.like_rgb_8(253, 245, 230)
		end

	linen_color: COLOR is
		once
			create Result.like_rgb_8(250, 240, 230)
		end

	antique_white_color: COLOR is
		once
			create Result.like_rgb_8(250, 235, 215)
		end

	papaya_whip_color: COLOR is
		once
			create Result.like_rgb_8(255, 239, 213)
		end

	blanched_almond_color: COLOR is
		once
			create Result.like_rgb_8(255, 235, 205)
		end

	bisque_color: COLOR is
		once
			create Result.like_rgb_8(255, 228, 196)
		end

	peach_puff_color: COLOR is
		once
			create Result.like_rgb_8(255, 218, 185)
		end

	navajo_white_color: COLOR is
		once
			create Result.like_rgb_8(255, 222, 173)
		end

	moccasin_color: COLOR is
		once
			create Result.like_rgb_8(255, 228, 181)
		end

	cornsilk_color: COLOR is
		once
			create Result.like_rgb_8(255, 248, 220)
		end

	ivory_color: COLOR is
		once
			create Result.like_rgb_8(255, 255, 240)
		end

	lemon_chiffon_color: COLOR is
		once
			create Result.like_rgb_8(255, 250, 205)
		end

	seashell_color: COLOR is
		once
			create Result.like_rgb_8(255, 245, 238)
		end

	honeydew_color: COLOR is
		once
			create Result.like_rgb_8(240, 255, 240)
		end

	mint_cream_color: COLOR is
		once
			create Result.like_rgb_8(245, 255, 250)
		end

	azure_color: COLOR is
		once
			create Result.like_rgb_8(240, 255, 255)
		end

	alice_blue_color: COLOR is
		once
			create Result.like_rgb_8(240, 248, 255)
		end

	lavender_color: COLOR is
		once
			create Result.like_rgb_8(230, 230, 250)
		end

	lavender_blush_color: COLOR is
		once
			create Result.like_rgb_8(255, 240, 245)
		end

	misty_rose_color: COLOR is
		once
			create Result.like_rgb_8(255, 228, 225)
		end

	white_color: COLOR is
		once
			create Result.like_rgb_8(255, 255, 255)
		end

	black_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 0)
		end

	dark_slate_grey_color: COLOR is
		once
			create Result.like_rgb_8(47, 79, 79)
		end

	dim_grey_color: COLOR is
		once
			create Result.like_rgb_8(105, 105, 105)
		end

	slate_grey_color: COLOR is
		once
			create Result.like_rgb_8(112, 128, 144)
		end

	light_slate_grey_color: COLOR is
		once
			create Result.like_rgb_8(119, 136, 153)
		end

	grey_color: COLOR is
		once
			create Result.like_rgb_8(190, 190, 190)
		end

	light_grey_color: COLOR is
		once
			create Result.like_rgb_8(211, 211, 211)
		end

	midnight_blue_color: COLOR is
		once
			create Result.like_rgb_8(25, 25, 112)
		end

	navy_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 128)
		end

	navy_blue_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 128)
		end

	cornflower_blue_color: COLOR is
		once
			create Result.like_rgb_8(100, 149, 237)
		end

	dark_slate_blue_color: COLOR is
		once
			create Result.like_rgb_8(72, 61, 139)
		end

	slate_blue_color: COLOR is
		once
			create Result.like_rgb_8(106, 90, 205)
		end

	medium_slate_blue_color: COLOR is
		once
			create Result.like_rgb_8(123, 104, 238)
		end

	light_slate_blue_color: COLOR is
		once
			create Result.like_rgb_8(132, 112, 255)
		end

	medium_blue_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 205)
		end

	royal_blue_color: COLOR is
		once
			create Result.like_rgb_8(65, 105, 225)
		end

	blue_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 255)
		end

	dodger_blue_color: COLOR is
		once
			create Result.like_rgb_8(30, 144, 255)
		end

	deep_sky_blue_color: COLOR is
		once
			create Result.like_rgb_8(0, 191, 255)
		end

	sky_blue_color: COLOR is
		once
			create Result.like_rgb_8(135, 206, 235)
		end

	light_sky_blue_color: COLOR is
		once
			create Result.like_rgb_8(135, 206, 250)
		end

	steel_blue_color: COLOR is
		once
			create Result.like_rgb_8(70, 130, 180)
		end

	light_steel_blue_color: COLOR is
		once
			create Result.like_rgb_8(176, 196, 222)
		end

	light_blue_color: COLOR is
		once
			create Result.like_rgb_8(173, 216, 230)
		end

	powder_blue_color: COLOR is
		once
			create Result.like_rgb_8(176, 224, 230)
		end

	pale_turquoise_color: COLOR is
		once
			create Result.like_rgb_8(175, 238, 238)
		end

	dark_turquoise_color: COLOR is
		once
			create Result.like_rgb_8(0, 206, 209)
		end

	medium_turquoise_color: COLOR is
		once
			create Result.like_rgb_8(72, 209, 204)
		end

	turquoise_color: COLOR is
		once
			create Result.like_rgb_8(64, 224, 208)
		end

	cyan_color: COLOR is
		once
			create Result.like_rgb_8(0, 255, 255)
		end

	light_cyan_color: COLOR is
		once
			create Result.like_rgb_8(224, 255, 255)
		end

	cadet_blue_color: COLOR is
		once
			create Result.like_rgb_8(95, 158, 160)
		end

	medium_aquamarine_color: COLOR is
		once
			create Result.like_rgb_8(102, 205, 170)
		end

	aquamarine_color: COLOR is
		once
			create Result.like_rgb_8(127, 255, 212)
		end

	dark_green_color: COLOR is
		once
			create Result.like_rgb_8(0, 100, 0)
		end

	dark_olive_green_color: COLOR is
		once
			create Result.like_rgb_8(85, 107, 47)
		end

	dark_sea_green_color: COLOR is
		once
			create Result.like_rgb_8(143, 188, 143)
		end

	sea_green_color: COLOR is
		once
			create Result.like_rgb_8(46, 139, 87)
		end

	medium_sea_green_color: COLOR is
		once
			create Result.like_rgb_8(60, 179, 113)
		end

	light_sea_green_color: COLOR is
		once
			create Result.like_rgb_8(32, 178, 170)
		end

	pale_green_color: COLOR is
		once
			create Result.like_rgb_8(152, 251, 152)
		end

	spring_green_color: COLOR is
		once
			create Result.like_rgb_8(0, 255, 127)
		end

	lawn_green_color: COLOR is
		once
			create Result.like_rgb_8(124, 252, 0)
		end

	green_color: COLOR is
		once
			create Result.like_rgb_8(0, 255, 0)
		end

	chartreuse_color: COLOR is
		once
			create Result.like_rgb_8(127, 255, 0)
		end

	medium_spring_green_color: COLOR is
		once
			create Result.like_rgb_8(0, 250, 154)
		end

	green_yellow_color: COLOR is
		once
			create Result.like_rgb_8(173, 255, 47)
		end

	lime_green_color: COLOR is
		once
			create Result.like_rgb_8(50, 205, 50)
		end

	yellow_green_color: COLOR is
		once
			create Result.like_rgb_8(154, 205, 50)
		end

	forest_green_color: COLOR is
		once
			create Result.like_rgb_8(34, 139, 34)
		end

	olive_drab_color: COLOR is
		once
			create Result.like_rgb_8(107, 142, 35)
		end

	dark_khaki_color: COLOR is
		once
			create Result.like_rgb_8(189, 183, 107)
		end

	khaki_color: COLOR is
		once
			create Result.like_rgb_8(240, 230, 140)
		end

	pale_goldenrod_color: COLOR is
		once
			create Result.like_rgb_8(238, 232, 170)
		end

	light_goldenrod_yellow_color: COLOR is
		once
			create Result.like_rgb_8(250, 250, 210)
		end

	light_yellow_color: COLOR is
		once
			create Result.like_rgb_8(255, 255, 224)
		end

	yellow_color: COLOR is
		once
			create Result.like_rgb_8(255, 255, 0)
		end

	gold_color: COLOR is
		once
			create Result.like_rgb_8(255, 215, 0)
		end

	light_goldenrod_color: COLOR is
		once
			create Result.like_rgb_8(238, 221, 130)
		end

	goldenrod_color: COLOR is
		once
			create Result.like_rgb_8(218, 165, 32)
		end

	dark_goldenrod_color: COLOR is
		once
			create Result.like_rgb_8(184, 134, 11)
		end

	rosy_brown_color: COLOR is
		once
			create Result.like_rgb_8(188, 143, 143)
		end

	indian_red_color: COLOR is
		once
			create Result.like_rgb_8(205, 92, 92)
		end

	saddle_brown_color: COLOR is
		once
			create Result.like_rgb_8(139, 69, 19)
		end

	sienna_color: COLOR is
		once
			create Result.like_rgb_8(160, 82, 45)
		end

	peru_color: COLOR is
		once
			create Result.like_rgb_8(205, 133, 63)
		end

	burlywood_color: COLOR is
		once
			create Result.like_rgb_8(222, 184, 135)
		end

	beige_color: COLOR is
		once
			create Result.like_rgb_8(245, 245, 220)
		end

	wheat_color: COLOR is
		once
			create Result.like_rgb_8(245, 222, 179)
		end

	sandy_brown_color: COLOR is
		once
			create Result.like_rgb_8(244, 164, 96)
		end

	tan_color: COLOR is
		once
			create Result.like_rgb_8(210, 180, 140)
		end

	chocolate_color: COLOR is
		once
			create Result.like_rgb_8(210, 105, 30)
		end

	firebrick_color: COLOR is
		once
			create Result.like_rgb_8(178, 34, 34)
		end

	brown_color: COLOR is
		once
			create Result.like_rgb_8(165, 42, 42)
		end

	dark_salmon_color: COLOR is
		once
			create Result.like_rgb_8(233, 150, 122)
		end

	salmon_color: COLOR is
		once
			create Result.like_rgb_8(250, 128, 114)
		end

	light_salmon_color: COLOR is
		once
			create Result.like_rgb_8(255, 160, 122)
		end

	orange_color: COLOR is
		once
			create Result.like_rgb_8(255, 165, 0)
		end

	dark_orange_color: COLOR is
		once
			create Result.like_rgb_8(255, 140, 0)
		end

	coral_color: COLOR is
		once
			create Result.like_rgb_8(255, 127, 80)
		end

	light_coral_color: COLOR is
		once
			create Result.like_rgb_8(240, 128, 128)
		end

	tomato_color: COLOR is
		once
			create Result.like_rgb_8(255, 99, 71)
		end

	orange_red_color: COLOR is
		once
			create Result.like_rgb_8(255, 69, 0)
		end

	red_color: COLOR is
		once
			create Result.like_rgb_8(255, 0, 0)
		end

	hot_pink_color: COLOR is
		once
			create Result.like_rgb_8(255, 105, 180)
		end

	deep_pink_color: COLOR is
		once
			create Result.like_rgb_8(255, 20, 147)
		end

	pink_color: COLOR is
		once
			create Result.like_rgb_8(255, 192, 203)
		end

	light_pink_color: COLOR is
		once
			create Result.like_rgb_8(255, 182, 193)
		end

	pale_violet_red_color: COLOR is
		once
			create Result.like_rgb_8(219, 112, 147)
		end

	maroon_color: COLOR is
		once
			create Result.like_rgb_8(176, 48, 96)
		end

	medium_violet_red_color: COLOR is
		once
			create Result.like_rgb_8(199, 21, 133)
		end

	violet_red_color: COLOR is
		once
			create Result.like_rgb_8(208, 32, 144)
		end

	magenta_color: COLOR is
		once
			create Result.like_rgb_8(255, 0, 255)
		end

	violet_color: COLOR is
		once
			create Result.like_rgb_8(238, 130, 238)
		end

	plum_color: COLOR is
		once
			create Result.like_rgb_8(221, 160, 221)
		end

	orchid_color: COLOR is
		once
			create Result.like_rgb_8(218, 112, 214)
		end

	medium_orchid_color: COLOR is
		once
			create Result.like_rgb_8(186, 85, 211)
		end

	dark_orchid_color: COLOR is
		once
			create Result.like_rgb_8(153, 50, 204)
		end

	dark_violet_color: COLOR is
		once
			create Result.like_rgb_8(148, 0, 211)
		end

	blue_violet_color: COLOR is
		once
			create Result.like_rgb_8(138, 43, 226)
		end

	purple_color: COLOR is
		once
			create Result.like_rgb_8(160, 32, 240)
		end

	medium_purple_color: COLOR is
		once
			create Result.like_rgb_8(147, 112, 219)
		end

	thistle_color: COLOR is
		once
			create Result.like_rgb_8(216, 191, 216)
		end

	snow1_color: COLOR is
		once
			create Result.like_rgb_8(255, 250, 250)
		end

	snow2_color: COLOR is
		once
			create Result.like_rgb_8(238, 233, 233)
		end

	snow3_color: COLOR is
		once
			create Result.like_rgb_8(205, 201, 201)
		end

	snow4_color: COLOR is
		once
			create Result.like_rgb_8(139, 137, 137)
		end

	seashell1_color: COLOR is
		once
			create Result.like_rgb_8(255, 245, 238)
		end

	seashell2_color: COLOR is
		once
			create Result.like_rgb_8(238, 229, 222)
		end

	seashell3_color: COLOR is
		once
			create Result.like_rgb_8(205, 197, 191)
		end

	seashell4_color: COLOR is
		once
			create Result.like_rgb_8(139, 134, 130)
		end

	antiquewhite1_color: COLOR is
		once
			create Result.like_rgb_8(255, 239, 219)
		end

	antiquewhite2_color: COLOR is
		once
			create Result.like_rgb_8(238, 223, 204)
		end

	antiquewhite3_color: COLOR is
		once
			create Result.like_rgb_8(205, 192, 176)
		end

	antiquewhite4_color: COLOR is
		once
			create Result.like_rgb_8(139, 131, 120)
		end

	bisque1_color: COLOR is
		once
			create Result.like_rgb_8(255, 228, 196)
		end

	bisque2_color: COLOR is
		once
			create Result.like_rgb_8(238, 213, 183)
		end

	bisque3_color: COLOR is
		once
			create Result.like_rgb_8(205, 183, 158)
		end

	bisque4_color: COLOR is
		once
			create Result.like_rgb_8(139, 125, 107)
		end

	peachpuff1_color: COLOR is
		once
			create Result.like_rgb_8(255, 218, 185)
		end

	peachpuff2_color: COLOR is
		once
			create Result.like_rgb_8(238, 203, 173)
		end

	peachpuff3_color: COLOR is
		once
			create Result.like_rgb_8(205, 175, 149)
		end

	peachpuff4_color: COLOR is
		once
			create Result.like_rgb_8(139, 119, 101)
		end

	navajowhite1_color: COLOR is
		once
			create Result.like_rgb_8(255, 222, 173)
		end

	navajowhite2_color: COLOR is
		once
			create Result.like_rgb_8(238, 207, 161)
		end

	navajowhite3_color: COLOR is
		once
			create Result.like_rgb_8(205, 179, 139)
		end

	navajowhite4_color: COLOR is
		once
			create Result.like_rgb_8(139, 121, 94)
		end

	lemonchiffon1_color: COLOR is
		once
			create Result.like_rgb_8(255, 250, 205)
		end

	lemonchiffon2_color: COLOR is
		once
			create Result.like_rgb_8(238, 233, 191)
		end

	lemonchiffon3_color: COLOR is
		once
			create Result.like_rgb_8(205, 201, 165)
		end

	lemonchiffon4_color: COLOR is
		once
			create Result.like_rgb_8(139, 137, 112)
		end

	cornsilk1_color: COLOR is
		once
			create Result.like_rgb_8(255, 248, 220)
		end

	cornsilk2_color: COLOR is
		once
			create Result.like_rgb_8(238, 232, 205)
		end

	cornsilk3_color: COLOR is
		once
			create Result.like_rgb_8(205, 200, 177)
		end

	cornsilk4_color: COLOR is
		once
			create Result.like_rgb_8(139, 136, 120)
		end

	ivory1_color: COLOR is
		once
			create Result.like_rgb_8(255, 255, 240)
		end

	ivory2_color: COLOR is
		once
			create Result.like_rgb_8(238, 238, 224)
		end

	ivory3_color: COLOR is
		once
			create Result.like_rgb_8(205, 205, 193)
		end

	ivory4_color: COLOR is
		once
			create Result.like_rgb_8(139, 139, 131)
		end

	honeydew1_color: COLOR is
		once
			create Result.like_rgb_8(240, 255, 240)
		end

	honeydew2_color: COLOR is
		once
			create Result.like_rgb_8(224, 238, 224)
		end

	honeydew3_color: COLOR is
		once
			create Result.like_rgb_8(193, 205, 193)
		end

	honeydew4_color: COLOR is
		once
			create Result.like_rgb_8(131, 139, 131)
		end

	lavenderblush1_color: COLOR is
		once
			create Result.like_rgb_8(255, 240, 245)
		end

	lavenderblush2_color: COLOR is
		once
			create Result.like_rgb_8(238, 224, 229)
		end

	lavenderblush3_color: COLOR is
		once
			create Result.like_rgb_8(205, 193, 197)
		end

	lavenderblush4_color: COLOR is
		once
			create Result.like_rgb_8(139, 131, 134)
		end

	mistyrose1_color: COLOR is
		once
			create Result.like_rgb_8(255, 228, 225)
		end

	mistyrose2_color: COLOR is
		once
			create Result.like_rgb_8(238, 213, 210)
		end

	mistyrose3_color: COLOR is
		once
			create Result.like_rgb_8(205, 183, 181)
		end

	mistyrose4_color: COLOR is
		once
			create Result.like_rgb_8(139, 125, 123)
		end

	azure1_color: COLOR is
		once
			create Result.like_rgb_8(240, 255, 255)
		end

	azure2_color: COLOR is
		once
			create Result.like_rgb_8(224, 238, 238)
		end

	azure3_color: COLOR is
		once
			create Result.like_rgb_8(193, 205, 205)
		end

	azure4_color: COLOR is
		once
			create Result.like_rgb_8(131, 139, 139)
		end

	slateblue1_color: COLOR is
		once
			create Result.like_rgb_8(131, 111, 255)
		end

	slateblue2_color: COLOR is
		once
			create Result.like_rgb_8(122, 103, 238)
		end

	slateblue3_color: COLOR is
		once
			create Result.like_rgb_8(105, 89, 205)
		end

	slateblue4_color: COLOR is
		once
			create Result.like_rgb_8(71, 60, 139)
		end

	royalblue1_color: COLOR is
		once
			create Result.like_rgb_8(72, 118, 255)
		end

	royalblue2_color: COLOR is
		once
			create Result.like_rgb_8(67, 110, 238)
		end

	royalblue3_color: COLOR is
		once
			create Result.like_rgb_8(58, 95, 205)
		end

	royalblue4_color: COLOR is
		once
			create Result.like_rgb_8(39, 64, 139)
		end

	blue1_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 255)
		end

	blue2_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 238)
		end

	blue3_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 205)
		end

	blue4_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 139)
		end

	dodgerblue1_color: COLOR is
		once
			create Result.like_rgb_8(30, 144, 255)
		end

	dodgerblue2_color: COLOR is
		once
			create Result.like_rgb_8(28, 134, 238)
		end

	dodgerblue3_color: COLOR is
		once
			create Result.like_rgb_8(24, 116, 205)
		end

	dodgerblue4_color: COLOR is
		once
			create Result.like_rgb_8(16, 78, 139)
		end

	steelblue1_color: COLOR is
		once
			create Result.like_rgb_8(99, 184, 255)
		end

	steelblue2_color: COLOR is
		once
			create Result.like_rgb_8(92, 172, 238)
		end

	steelblue3_color: COLOR is
		once
			create Result.like_rgb_8(79, 148, 205)
		end

	steelblue4_color: COLOR is
		once
			create Result.like_rgb_8(54, 100, 139)
		end

	deepskyblue1_color: COLOR is
		once
			create Result.like_rgb_8(0, 191, 255)
		end

	deepskyblue2_color: COLOR is
		once
			create Result.like_rgb_8(0, 178, 238)
		end

	deepskyblue3_color: COLOR is
		once
			create Result.like_rgb_8(0, 154, 205)
		end

	deepskyblue4_color: COLOR is
		once
			create Result.like_rgb_8(0, 104, 139)
		end

	skyblue1_color: COLOR is
		once
			create Result.like_rgb_8(135, 206, 255)
		end

	skyblue2_color: COLOR is
		once
			create Result.like_rgb_8(126, 192, 238)
		end

	skyblue3_color: COLOR is
		once
			create Result.like_rgb_8(108, 166, 205)
		end

	skyblue4_color: COLOR is
		once
			create Result.like_rgb_8(74, 112, 139)
		end

	lightskyblue1_color: COLOR is
		once
			create Result.like_rgb_8(176, 226, 255)
		end

	lightskyblue2_color: COLOR is
		once
			create Result.like_rgb_8(164, 211, 238)
		end

	lightskyblue3_color: COLOR is
		once
			create Result.like_rgb_8(141, 182, 205)
		end

	lightskyblue4_color: COLOR is
		once
			create Result.like_rgb_8(96, 123, 139)
		end

	slategrey1_color: COLOR is
		once
			create Result.like_rgb_8(198, 226, 255)
		end

	slategrey2_color: COLOR is
		once
			create Result.like_rgb_8(185, 211, 238)
		end

	slategrey3_color: COLOR is
		once
			create Result.like_rgb_8(159, 182, 205)
		end

	slategrey4_color: COLOR is
		once
			create Result.like_rgb_8(108, 123, 139)
		end

	lightsteelblue1_color: COLOR is
		once
			create Result.like_rgb_8(202, 225, 255)
		end

	lightsteelblue2_color: COLOR is
		once
			create Result.like_rgb_8(188, 210, 238)
		end

	lightsteelblue3_color: COLOR is
		once
			create Result.like_rgb_8(162, 181, 205)
		end

	lightsteelblue4_color: COLOR is
		once
			create Result.like_rgb_8(110, 123, 139)
		end

	lightblue1_color: COLOR is
		once
			create Result.like_rgb_8(191, 239, 255)
		end

	lightblue2_color: COLOR is
		once
			create Result.like_rgb_8(178, 223, 238)
		end

	lightblue3_color: COLOR is
		once
			create Result.like_rgb_8(154, 192, 205)
		end

	lightblue4_color: COLOR is
		once
			create Result.like_rgb_8(104, 131, 139)
		end

	lightcyan1_color: COLOR is
		once
			create Result.like_rgb_8(224, 255, 255)
		end

	lightcyan2_color: COLOR is
		once
			create Result.like_rgb_8(209, 238, 238)
		end

	lightcyan3_color: COLOR is
		once
			create Result.like_rgb_8(180, 205, 205)
		end

	lightcyan4_color: COLOR is
		once
			create Result.like_rgb_8(122, 139, 139)
		end

	paleturquoise1_color: COLOR is
		once
			create Result.like_rgb_8(187, 255, 255)
		end

	paleturquoise2_color: COLOR is
		once
			create Result.like_rgb_8(174, 238, 238)
		end

	paleturquoise3_color: COLOR is
		once
			create Result.like_rgb_8(150, 205, 205)
		end

	paleturquoise4_color: COLOR is
		once
			create Result.like_rgb_8(102, 139, 139)
		end

	cadetblue1_color: COLOR is
		once
			create Result.like_rgb_8(152, 245, 255)
		end

	cadetblue2_color: COLOR is
		once
			create Result.like_rgb_8(142, 229, 238)
		end

	cadetblue3_color: COLOR is
		once
			create Result.like_rgb_8(122, 197, 205)
		end

	cadetblue4_color: COLOR is
		once
			create Result.like_rgb_8(83, 134, 139)
		end

	turquoise1_color: COLOR is
		once
			create Result.like_rgb_8(0, 245, 255)
		end

	turquoise2_color: COLOR is
		once
			create Result.like_rgb_8(0, 229, 238)
		end

	turquoise3_color: COLOR is
		once
			create Result.like_rgb_8(0, 197, 205)
		end

	turquoise4_color: COLOR is
		once
			create Result.like_rgb_8(0, 134, 139)
		end

	cyan1_color: COLOR is
		once
			create Result.like_rgb_8(0, 255, 255)
		end

	cyan2_color: COLOR is
		once
			create Result.like_rgb_8(0, 238, 238)
		end

	cyan3_color: COLOR is
		once
			create Result.like_rgb_8(0, 205, 205)
		end

	cyan4_color: COLOR is
		once
			create Result.like_rgb_8(0, 139, 139)
		end

	darkslategrey1_color: COLOR is
		once
			create Result.like_rgb_8(151, 255, 255)
		end

	darkslategrey2_color: COLOR is
		once
			create Result.like_rgb_8(141, 238, 238)
		end

	darkslategrey3_color: COLOR is
		once
			create Result.like_rgb_8(121, 205, 205)
		end

	darkslategrey4_color: COLOR is
		once
			create Result.like_rgb_8(82, 139, 139)
		end

	aquamarine1_color: COLOR is
		once
			create Result.like_rgb_8(127, 255, 212)
		end

	aquamarine2_color: COLOR is
		once
			create Result.like_rgb_8(118, 238, 198)
		end

	aquamarine3_color: COLOR is
		once
			create Result.like_rgb_8(102, 205, 170)
		end

	aquamarine4_color: COLOR is
		once
			create Result.like_rgb_8(69, 139, 116)
		end

	darkseagreen1_color: COLOR is
		once
			create Result.like_rgb_8(193, 255, 193)
		end

	darkseagreen2_color: COLOR is
		once
			create Result.like_rgb_8(180, 238, 180)
		end

	darkseagreen3_color: COLOR is
		once
			create Result.like_rgb_8(155, 205, 155)
		end

	darkseagreen4_color: COLOR is
		once
			create Result.like_rgb_8(105, 139, 105)
		end

	seagreen1_color: COLOR is
		once
			create Result.like_rgb_8(84, 255, 159)
		end

	seagreen2_color: COLOR is
		once
			create Result.like_rgb_8(78, 238, 148)
		end

	seagreen3_color: COLOR is
		once
			create Result.like_rgb_8(67, 205, 128)
		end

	seagreen4_color: COLOR is
		once
			create Result.like_rgb_8(46, 139, 87)
		end

	palegreen1_color: COLOR is
		once
			create Result.like_rgb_8(154, 255, 154)
		end

	palegreen2_color: COLOR is
		once
			create Result.like_rgb_8(144, 238, 144)
		end

	palegreen3_color: COLOR is
		once
			create Result.like_rgb_8(124, 205, 124)
		end

	palegreen4_color: COLOR is
		once
			create Result.like_rgb_8(84, 139, 84)
		end

	springgreen1_color: COLOR is
		once
			create Result.like_rgb_8(0, 255, 127)
		end

	springgreen2_color: COLOR is
		once
			create Result.like_rgb_8(0, 238, 118)
		end

	springgreen3_color: COLOR is
		once
			create Result.like_rgb_8(0, 205, 102)
		end

	springgreen4_color: COLOR is
		once
			create Result.like_rgb_8(0, 139, 69)
		end

	green1_color: COLOR is
		once
			create Result.like_rgb_8(0, 255, 0)
		end

	green2_color: COLOR is
		once
			create Result.like_rgb_8(0, 238, 0)
		end

	green3_color: COLOR is
		once
			create Result.like_rgb_8(0, 205, 0)
		end

	green4_color: COLOR is
		once
			create Result.like_rgb_8(0, 139, 0)
		end

	chartreuse1_color: COLOR is
		once
			create Result.like_rgb_8(127, 255, 0)
		end

	chartreuse2_color: COLOR is
		once
			create Result.like_rgb_8(118, 238, 0)
		end

	chartreuse3_color: COLOR is
		once
			create Result.like_rgb_8(102, 205, 0)
		end

	chartreuse4_color: COLOR is
		once
			create Result.like_rgb_8(69, 139, 0)
		end

	olivedrab1_color: COLOR is
		once
			create Result.like_rgb_8(192, 255, 62)
		end

	olivedrab2_color: COLOR is
		once
			create Result.like_rgb_8(179, 238, 58)
		end

	olivedrab3_color: COLOR is
		once
			create Result.like_rgb_8(154, 205, 50)
		end

	olivedrab4_color: COLOR is
		once
			create Result.like_rgb_8(105, 139, 34)
		end

	darkolivegreen1_color: COLOR is
		once
			create Result.like_rgb_8(202, 255, 112)
		end

	darkolivegreen2_color: COLOR is
		once
			create Result.like_rgb_8(188, 238, 104)
		end

	darkolivegreen3_color: COLOR is
		once
			create Result.like_rgb_8(162, 205, 90)
		end

	darkolivegreen4_color: COLOR is
		once
			create Result.like_rgb_8(110, 139, 61)
		end

	khaki1_color: COLOR is
		once
			create Result.like_rgb_8(255, 246, 143)
		end

	khaki2_color: COLOR is
		once
			create Result.like_rgb_8(238, 230, 133)
		end

	khaki3_color: COLOR is
		once
			create Result.like_rgb_8(205, 198, 115)
		end

	khaki4_color: COLOR is
		once
			create Result.like_rgb_8(139, 134, 78)
		end

	lightgoldenrod1_color: COLOR is
		once
			create Result.like_rgb_8(255, 236, 139)
		end

	lightgoldenrod2_color: COLOR is
		once
			create Result.like_rgb_8(238, 220, 130)
		end

	lightgoldenrod3_color: COLOR is
		once
			create Result.like_rgb_8(205, 190, 112)
		end

	lightgoldenrod4_color: COLOR is
		once
			create Result.like_rgb_8(139, 129, 76)
		end

	lightyellow1_color: COLOR is
		once
			create Result.like_rgb_8(255, 255, 224)
		end

	lightyellow2_color: COLOR is
		once
			create Result.like_rgb_8(238, 238, 209)
		end

	lightyellow3_color: COLOR is
		once
			create Result.like_rgb_8(205, 205, 180)
		end

	lightyellow4_color: COLOR is
		once
			create Result.like_rgb_8(139, 139, 122)
		end

	yellow1_color: COLOR is
		once
			create Result.like_rgb_8(255, 255, 0)
		end

	yellow2_color: COLOR is
		once
			create Result.like_rgb_8(238, 238, 0)
		end

	yellow3_color: COLOR is
		once
			create Result.like_rgb_8(205, 205, 0)
		end

	yellow4_color: COLOR is
		once
			create Result.like_rgb_8(139, 139, 0)
		end

	gold1_color: COLOR is
		once
			create Result.like_rgb_8(255, 215, 0)
		end

	gold2_color: COLOR is
		once
			create Result.like_rgb_8(238, 201, 0)
		end

	gold3_color: COLOR is
		once
			create Result.like_rgb_8(205, 173, 0)
		end

	gold4_color: COLOR is
		once
			create Result.like_rgb_8(139, 117, 0)
		end

	goldenrod1_color: COLOR is
		once
			create Result.like_rgb_8(255, 193, 37)
		end

	goldenrod2_color: COLOR is
		once
			create Result.like_rgb_8(238, 180, 34)
		end

	goldenrod3_color: COLOR is
		once
			create Result.like_rgb_8(205, 155, 29)
		end

	goldenrod4_color: COLOR is
		once
			create Result.like_rgb_8(139, 105, 20)
		end

	darkgoldenrod1_color: COLOR is
		once
			create Result.like_rgb_8(255, 185, 15)
		end

	darkgoldenrod2_color: COLOR is
		once
			create Result.like_rgb_8(238, 173, 14)
		end

	darkgoldenrod3_color: COLOR is
		once
			create Result.like_rgb_8(205, 149, 12)
		end

	darkgoldenrod4_color: COLOR is
		once
			create Result.like_rgb_8(139, 101, 8)
		end

	rosybrown1_color: COLOR is
		once
			create Result.like_rgb_8(255, 193, 193)
		end

	rosybrown2_color: COLOR is
		once
			create Result.like_rgb_8(238, 180, 180)
		end

	rosybrown3_color: COLOR is
		once
			create Result.like_rgb_8(205, 155, 155)
		end

	rosybrown4_color: COLOR is
		once
			create Result.like_rgb_8(139, 105, 105)
		end

	indianred1_color: COLOR is
		once
			create Result.like_rgb_8(255, 106, 106)
		end

	indianred2_color: COLOR is
		once
			create Result.like_rgb_8(238, 99, 99)
		end

	indianred3_color: COLOR is
		once
			create Result.like_rgb_8(205, 85, 85)
		end

	indianred4_color: COLOR is
		once
			create Result.like_rgb_8(139, 58, 58)
		end

	sienna1_color: COLOR is
		once
			create Result.like_rgb_8(255, 130, 71)
		end

	sienna2_color: COLOR is
		once
			create Result.like_rgb_8(238, 121, 66)
		end

	sienna3_color: COLOR is
		once
			create Result.like_rgb_8(205, 104, 57)
		end

	sienna4_color: COLOR is
		once
			create Result.like_rgb_8(139, 71, 38)
		end

	burlywood1_color: COLOR is
		once
			create Result.like_rgb_8(255, 211, 155)
		end

	burlywood2_color: COLOR is
		once
			create Result.like_rgb_8(238, 197, 145)
		end

	burlywood3_color: COLOR is
		once
			create Result.like_rgb_8(205, 170, 125)
		end

	burlywood4_color: COLOR is
		once
			create Result.like_rgb_8(139, 115, 85)
		end

	wheat1_color: COLOR is
		once
			create Result.like_rgb_8(255, 231, 186)
		end

	wheat2_color: COLOR is
		once
			create Result.like_rgb_8(238, 216, 174)
		end

	wheat3_color: COLOR is
		once
			create Result.like_rgb_8(205, 186, 150)
		end

	wheat4_color: COLOR is
		once
			create Result.like_rgb_8(139, 126, 102)
		end

	tan1_color: COLOR is
		once
			create Result.like_rgb_8(255, 165, 79)
		end

	tan2_color: COLOR is
		once
			create Result.like_rgb_8(238, 154, 73)
		end

	tan3_color: COLOR is
		once
			create Result.like_rgb_8(205, 133, 63)
		end

	tan4_color: COLOR is
		once
			create Result.like_rgb_8(139, 90, 43)
		end

	chocolate1_color: COLOR is
		once
			create Result.like_rgb_8(255, 127, 36)
		end

	chocolate2_color: COLOR is
		once
			create Result.like_rgb_8(238, 118, 33)
		end

	chocolate3_color: COLOR is
		once
			create Result.like_rgb_8(205, 102, 29)
		end

	chocolate4_color: COLOR is
		once
			create Result.like_rgb_8(139, 69, 19)
		end

	firebrick1_color: COLOR is
		once
			create Result.like_rgb_8(255, 48, 48)
		end

	firebrick2_color: COLOR is
		once
			create Result.like_rgb_8(238, 44, 44)
		end

	firebrick3_color: COLOR is
		once
			create Result.like_rgb_8(205, 38, 38)
		end

	firebrick4_color: COLOR is
		once
			create Result.like_rgb_8(139, 26, 26)
		end

	brown1_color: COLOR is
		once
			create Result.like_rgb_8(255, 64, 64)
		end

	brown2_color: COLOR is
		once
			create Result.like_rgb_8(238, 59, 59)
		end

	brown3_color: COLOR is
		once
			create Result.like_rgb_8(205, 51, 51)
		end

	brown4_color: COLOR is
		once
			create Result.like_rgb_8(139, 35, 35)
		end

	salmon1_color: COLOR is
		once
			create Result.like_rgb_8(255, 140, 105)
		end

	salmon2_color: COLOR is
		once
			create Result.like_rgb_8(238, 130, 98)
		end

	salmon3_color: COLOR is
		once
			create Result.like_rgb_8(205, 112, 84)
		end

	salmon4_color: COLOR is
		once
			create Result.like_rgb_8(139, 76, 57)
		end

	lightsalmon1_color: COLOR is
		once
			create Result.like_rgb_8(255, 160, 122)
		end

	lightsalmon2_color: COLOR is
		once
			create Result.like_rgb_8(238, 149, 114)
		end

	lightsalmon3_color: COLOR is
		once
			create Result.like_rgb_8(205, 129, 98)
		end

	lightsalmon4_color: COLOR is
		once
			create Result.like_rgb_8(139, 87, 66)
		end

	orange1_color: COLOR is
		once
			create Result.like_rgb_8(255, 165, 0)
		end

	orange2_color: COLOR is
		once
			create Result.like_rgb_8(238, 154, 0)
		end

	orange3_color: COLOR is
		once
			create Result.like_rgb_8(205, 133, 0)
		end

	orange4_color: COLOR is
		once
			create Result.like_rgb_8(139, 90, 0)
		end

	darkorange1_color: COLOR is
		once
			create Result.like_rgb_8(255, 127, 0)
		end

	darkorange2_color: COLOR is
		once
			create Result.like_rgb_8(238, 118, 0)
		end

	darkorange3_color: COLOR is
		once
			create Result.like_rgb_8(205, 102, 0)
		end

	darkorange4_color: COLOR is
		once
			create Result.like_rgb_8(139, 69, 0)
		end

	coral1_color: COLOR is
		once
			create Result.like_rgb_8(255, 114, 86)
		end

	coral2_color: COLOR is
		once
			create Result.like_rgb_8(238, 106, 80)
		end

	coral3_color: COLOR is
		once
			create Result.like_rgb_8(205, 91, 69)
		end

	coral4_color: COLOR is
		once
			create Result.like_rgb_8(139, 62, 47)
		end

	tomato1_color: COLOR is
		once
			create Result.like_rgb_8(255, 99, 71)
		end

	tomato2_color: COLOR is
		once
			create Result.like_rgb_8(238, 92, 66)
		end

	tomato3_color: COLOR is
		once
			create Result.like_rgb_8(205, 79, 57)
		end

	tomato4_color: COLOR is
		once
			create Result.like_rgb_8(139, 54, 38)
		end

	orangered1_color: COLOR is
		once
			create Result.like_rgb_8(255, 69, 0)
		end

	orangered2_color: COLOR is
		once
			create Result.like_rgb_8(238, 64, 0)
		end

	orangered3_color: COLOR is
		once
			create Result.like_rgb_8(205, 55, 0)
		end

	orangered4_color: COLOR is
		once
			create Result.like_rgb_8(139, 37, 0)
		end

	red1_color: COLOR is
		once
			create Result.like_rgb_8(255, 0, 0)
		end

	red2_color: COLOR is
		once
			create Result.like_rgb_8(238, 0, 0)
		end

	red3_color: COLOR is
		once
			create Result.like_rgb_8(205, 0, 0)
		end

	red4_color: COLOR is
		once
			create Result.like_rgb_8(139, 0, 0)
		end

	deeppink1_color: COLOR is
		once
			create Result.like_rgb_8(255, 20, 147)
		end

	deeppink2_color: COLOR is
		once
			create Result.like_rgb_8(238, 18, 137)
		end

	deeppink3_color: COLOR is
		once
			create Result.like_rgb_8(205, 16, 118)
		end

	deeppink4_color: COLOR is
		once
			create Result.like_rgb_8(139, 10, 80)
		end

	hotpink1_color: COLOR is
		once
			create Result.like_rgb_8(255, 110, 180)
		end

	hotpink2_color: COLOR is
		once
			create Result.like_rgb_8(238, 106, 167)
		end

	hotpink3_color: COLOR is
		once
			create Result.like_rgb_8(205, 96, 144)
		end

	hotpink4_color: COLOR is
		once
			create Result.like_rgb_8(139, 58, 98)
		end

	pink1_color: COLOR is
		once
			create Result.like_rgb_8(255, 181, 197)
		end

	pink2_color: COLOR is
		once
			create Result.like_rgb_8(238, 169, 184)
		end

	pink3_color: COLOR is
		once
			create Result.like_rgb_8(205, 145, 158)
		end

	pink4_color: COLOR is
		once
			create Result.like_rgb_8(139, 99, 108)
		end

	lightpink1_color: COLOR is
		once
			create Result.like_rgb_8(255, 174, 185)
		end

	lightpink2_color: COLOR is
		once
			create Result.like_rgb_8(238, 162, 173)
		end

	lightpink3_color: COLOR is
		once
			create Result.like_rgb_8(205, 140, 149)
		end

	lightpink4_color: COLOR is
		once
			create Result.like_rgb_8(139, 95, 101)
		end

	palevioletred1_color: COLOR is
		once
			create Result.like_rgb_8(255, 130, 171)
		end

	palevioletred2_color: COLOR is
		once
			create Result.like_rgb_8(238, 121, 159)
		end

	palevioletred3_color: COLOR is
		once
			create Result.like_rgb_8(205, 104, 137)
		end

	palevioletred4_color: COLOR is
		once
			create Result.like_rgb_8(139, 71, 93)
		end

	maroon1_color: COLOR is
		once
			create Result.like_rgb_8(255, 52, 179)
		end

	maroon2_color: COLOR is
		once
			create Result.like_rgb_8(238, 48, 167)
		end

	maroon3_color: COLOR is
		once
			create Result.like_rgb_8(205, 41, 144)
		end

	maroon4_color: COLOR is
		once
			create Result.like_rgb_8(139, 28, 98)
		end

	violetred1_color: COLOR is
		once
			create Result.like_rgb_8(255, 62, 150)
		end

	violetred2_color: COLOR is
		once
			create Result.like_rgb_8(238, 58, 140)
		end

	violetred3_color: COLOR is
		once
			create Result.like_rgb_8(205, 50, 120)
		end

	violetred4_color: COLOR is
		once
			create Result.like_rgb_8(139, 34, 82)
		end

	magenta1_color: COLOR is
		once
			create Result.like_rgb_8(255, 0, 255)
		end

	magenta2_color: COLOR is
		once
			create Result.like_rgb_8(238, 0, 238)
		end

	magenta3_color: COLOR is
		once
			create Result.like_rgb_8(205, 0, 205)
		end

	magenta4_color: COLOR is
		once
			create Result.like_rgb_8(139, 0, 139)
		end

	orchid1_color: COLOR is
		once
			create Result.like_rgb_8(255, 131, 250)
		end

	orchid2_color: COLOR is
		once
			create Result.like_rgb_8(238, 122, 233)
		end

	orchid3_color: COLOR is
		once
			create Result.like_rgb_8(205, 105, 201)
		end

	orchid4_color: COLOR is
		once
			create Result.like_rgb_8(139, 71, 137)
		end

	plum1_color: COLOR is
		once
			create Result.like_rgb_8(255, 187, 255)
		end

	plum2_color: COLOR is
		once
			create Result.like_rgb_8(238, 174, 238)
		end

	plum3_color: COLOR is
		once
			create Result.like_rgb_8(205, 150, 205)
		end

	plum4_color: COLOR is
		once
			create Result.like_rgb_8(139, 102, 139)
		end

	mediumorchid1_color: COLOR is
		once
			create Result.like_rgb_8(224, 102, 255)
		end

	mediumorchid2_color: COLOR is
		once
			create Result.like_rgb_8(209, 95, 238)
		end

	mediumorchid3_color: COLOR is
		once
			create Result.like_rgb_8(180, 82, 205)
		end

	mediumorchid4_color: COLOR is
		once
			create Result.like_rgb_8(122, 55, 139)
		end

	darkorchid1_color: COLOR is
		once
			create Result.like_rgb_8(191, 62, 255)
		end

	darkorchid2_color: COLOR is
		once
			create Result.like_rgb_8(178, 58, 238)
		end

	darkorchid3_color: COLOR is
		once
			create Result.like_rgb_8(154, 50, 205)
		end

	darkorchid4_color: COLOR is
		once
			create Result.like_rgb_8(104, 34, 139)
		end

	purple1_color: COLOR is
		once
			create Result.like_rgb_8(155, 48, 255)
		end

	purple2_color: COLOR is
		once
			create Result.like_rgb_8(145, 44, 238)
		end

	purple3_color: COLOR is
		once
			create Result.like_rgb_8(125, 38, 205)
		end

	purple4_color: COLOR is
		once
			create Result.like_rgb_8(85, 26, 139)
		end

	mediumpurple1_color: COLOR is
		once
			create Result.like_rgb_8(171, 130, 255)
		end

	mediumpurple2_color: COLOR is
		once
			create Result.like_rgb_8(159, 121, 238)
		end

	mediumpurple3_color: COLOR is
		once
			create Result.like_rgb_8(137, 104, 205)
		end

	mediumpurple4_color: COLOR is
		once
			create Result.like_rgb_8(93, 71, 139)
		end

	thistle1_color: COLOR is
		once
			create Result.like_rgb_8(255, 225, 255)
		end

	thistle2_color: COLOR is
		once
			create Result.like_rgb_8(238, 210, 238)
		end

	thistle3_color: COLOR is
		once
			create Result.like_rgb_8(205, 181, 205)
		end

	thistle4_color: COLOR is
		once
			create Result.like_rgb_8(139, 123, 139)
		end

	grey0_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 0)
		end

	grey1_color: COLOR is
		once
			create Result.like_rgb_8(3, 3, 3)
		end

	grey2_color: COLOR is
		once
			create Result.like_rgb_8(5, 5, 5)
		end

	grey3_color: COLOR is
		once
			create Result.like_rgb_8(8, 8, 8)
		end

	grey4_color: COLOR is
		once
			create Result.like_rgb_8(10, 10, 10)
		end

	grey5_color: COLOR is
		once
			create Result.like_rgb_8(13, 13, 13)
		end

	grey6_color: COLOR is
		once
			create Result.like_rgb_8(15, 15, 15)
		end

	grey7_color: COLOR is
		once
			create Result.like_rgb_8(18, 18, 18)
		end

	grey8_color: COLOR is
		once
			create Result.like_rgb_8(20, 20, 20)
		end

	grey9_color: COLOR is
		once
			create Result.like_rgb_8(23, 23, 23)
		end

	grey10_color: COLOR is
		once
			create Result.like_rgb_8(26, 26, 26)
		end

	grey11_color: COLOR is
		once
			create Result.like_rgb_8(28, 28, 28)
		end

	grey12_color: COLOR is
		once
			create Result.like_rgb_8(31, 31, 31)
		end

	grey13_color: COLOR is
		once
			create Result.like_rgb_8(33, 33, 33)
		end

	grey14_color: COLOR is
		once
			create Result.like_rgb_8(36, 36, 36)
		end

	grey15_color: COLOR is
		once
			create Result.like_rgb_8(38, 38, 38)
		end

	grey16_color: COLOR is
		once
			create Result.like_rgb_8(41, 41, 41)
		end

	grey17_color: COLOR is
		once
			create Result.like_rgb_8(43, 43, 43)
		end

	grey18_color: COLOR is
		once
			create Result.like_rgb_8(46, 46, 46)
		end

	grey19_color: COLOR is
		once
			create Result.like_rgb_8(48, 48, 48)
		end

	grey20_color: COLOR is
		once
			create Result.like_rgb_8(51, 51, 51)
		end

	grey21_color: COLOR is
		once
			create Result.like_rgb_8(54, 54, 54)
		end

	grey22_color: COLOR is
		once
			create Result.like_rgb_8(56, 56, 56)
		end

	grey23_color: COLOR is
		once
			create Result.like_rgb_8(59, 59, 59)
		end

	grey24_color: COLOR is
		once
			create Result.like_rgb_8(61, 61, 61)
		end

	grey25_color: COLOR is
		once
			create Result.like_rgb_8(64, 64, 64)
		end

	grey26_color: COLOR is
		once
			create Result.like_rgb_8(66, 66, 66)
		end

	grey27_color: COLOR is
		once
			create Result.like_rgb_8(69, 69, 69)
		end

	grey28_color: COLOR is
		once
			create Result.like_rgb_8(71, 71, 71)
		end

	grey29_color: COLOR is
		once
			create Result.like_rgb_8(74, 74, 74)
		end

	grey30_color: COLOR is
		once
			create Result.like_rgb_8(77, 77, 77)
		end

	grey31_color: COLOR is
		once
			create Result.like_rgb_8(79, 79, 79)
		end

	grey32_color: COLOR is
		once
			create Result.like_rgb_8(82, 82, 82)
		end

	grey33_color: COLOR is
		once
			create Result.like_rgb_8(84, 84, 84)
		end

	grey34_color: COLOR is
		once
			create Result.like_rgb_8(87, 87, 87)
		end

	grey35_color: COLOR is
		once
			create Result.like_rgb_8(89, 89, 89)
		end

	grey36_color: COLOR is
		once
			create Result.like_rgb_8(92, 92, 92)
		end

	grey37_color: COLOR is
		once
			create Result.like_rgb_8(94, 94, 94)
		end

	grey38_color: COLOR is
		once
			create Result.like_rgb_8(97, 97, 97)
		end

	grey39_color: COLOR is
		once
			create Result.like_rgb_8(99, 99, 99)
		end

	grey40_color: COLOR is
		once
			create Result.like_rgb_8(102, 102, 102)
		end

	grey41_color: COLOR is
		once
			create Result.like_rgb_8(105, 105, 105)
		end

	grey42_color: COLOR is
		once
			create Result.like_rgb_8(107, 107, 107)
		end

	grey43_color: COLOR is
		once
			create Result.like_rgb_8(110, 110, 110)
		end

	grey44_color: COLOR is
		once
			create Result.like_rgb_8(112, 112, 112)
		end

	grey45_color: COLOR is
		once
			create Result.like_rgb_8(115, 115, 115)
		end

	grey46_color: COLOR is
		once
			create Result.like_rgb_8(117, 117, 117)
		end

	grey47_color: COLOR is
		once
			create Result.like_rgb_8(120, 120, 120)
		end

	grey48_color: COLOR is
		once
			create Result.like_rgb_8(122, 122, 122)
		end

	grey49_color: COLOR is
		once
			create Result.like_rgb_8(125, 125, 125)
		end

	grey50_color: COLOR is
		once
			create Result.like_rgb_8(127, 127, 127)
		end

	grey51_color: COLOR is
		once
			create Result.like_rgb_8(130, 130, 130)
		end

	grey52_color: COLOR is
		once
			create Result.like_rgb_8(133, 133, 133)
		end

	grey53_color: COLOR is
		once
			create Result.like_rgb_8(135, 135, 135)
		end

	grey54_color: COLOR is
		once
			create Result.like_rgb_8(138, 138, 138)
		end

	grey55_color: COLOR is
		once
			create Result.like_rgb_8(140, 140, 140)
		end

	grey56_color: COLOR is
		once
			create Result.like_rgb_8(143, 143, 143)
		end

	grey57_color: COLOR is
		once
			create Result.like_rgb_8(145, 145, 145)
		end

	grey58_color: COLOR is
		once
			create Result.like_rgb_8(148, 148, 148)
		end

	grey59_color: COLOR is
		once
			create Result.like_rgb_8(150, 150, 150)
		end

	grey60_color: COLOR is
		once
			create Result.like_rgb_8(153, 153, 153)
		end

	grey61_color: COLOR is
		once
			create Result.like_rgb_8(156, 156, 156)
		end

	grey62_color: COLOR is
		once
			create Result.like_rgb_8(158, 158, 158)
		end

	grey63_color: COLOR is
		once
			create Result.like_rgb_8(161, 161, 161)
		end

	grey64_color: COLOR is
		once
			create Result.like_rgb_8(163, 163, 163)
		end

	grey65_color: COLOR is
		once
			create Result.like_rgb_8(166, 166, 166)
		end

	grey66_color: COLOR is
		once
			create Result.like_rgb_8(168, 168, 168)
		end

	grey67_color: COLOR is
		once
			create Result.like_rgb_8(171, 171, 171)
		end

	grey68_color: COLOR is
		once
			create Result.like_rgb_8(173, 173, 173)
		end

	grey69_color: COLOR is
		once
			create Result.like_rgb_8(176, 176, 176)
		end

	grey70_color: COLOR is
		once
			create Result.like_rgb_8(179, 179, 179)
		end

	grey71_color: COLOR is
		once
			create Result.like_rgb_8(181, 181, 181)
		end

	grey72_color: COLOR is
		once
			create Result.like_rgb_8(184, 184, 184)
		end

	grey73_color: COLOR is
		once
			create Result.like_rgb_8(186, 186, 186)
		end

	grey74_color: COLOR is
		once
			create Result.like_rgb_8(189, 189, 189)
		end

	grey75_color: COLOR is
		once
			create Result.like_rgb_8(191, 191, 191)
		end

	grey76_color: COLOR is
		once
			create Result.like_rgb_8(194, 194, 194)
		end

	grey77_color: COLOR is
		once
			create Result.like_rgb_8(196, 196, 196)
		end

	grey78_color: COLOR is
		once
			create Result.like_rgb_8(199, 199, 199)
		end

	grey79_color: COLOR is
		once
			create Result.like_rgb_8(201, 201, 201)
		end

	grey80_color: COLOR is
		once
			create Result.like_rgb_8(204, 204, 204)
		end

	grey81_color: COLOR is
		once
			create Result.like_rgb_8(207, 207, 207)
		end

	grey82_color: COLOR is
		once
			create Result.like_rgb_8(209, 209, 209)
		end

	grey83_color: COLOR is
		once
			create Result.like_rgb_8(212, 212, 212)
		end

	grey84_color: COLOR is
		once
			create Result.like_rgb_8(214, 214, 214)
		end

	grey85_color: COLOR is
		once
			create Result.like_rgb_8(217, 217, 217)
		end

	grey86_color: COLOR is
		once
			create Result.like_rgb_8(219, 219, 219)
		end

	grey87_color: COLOR is
		once
			create Result.like_rgb_8(222, 222, 222)
		end

	grey88_color: COLOR is
		once
			create Result.like_rgb_8(224, 224, 224)
		end

	grey89_color: COLOR is
		once
			create Result.like_rgb_8(227, 227, 227)
		end

	grey90_color: COLOR is
		once
			create Result.like_rgb_8(229, 229, 229)
		end

	grey91_color: COLOR is
		once
			create Result.like_rgb_8(232, 232, 232)
		end

	grey92_color: COLOR is
		once
			create Result.like_rgb_8(235, 235, 235)
		end

	grey93_color: COLOR is
		once
			create Result.like_rgb_8(237, 237, 237)
		end

	grey94_color: COLOR is
		once
			create Result.like_rgb_8(240, 240, 240)
		end

	grey95_color: COLOR is
		once
			create Result.like_rgb_8(242, 242, 242)
		end

	grey96_color: COLOR is
		once
			create Result.like_rgb_8(245, 245, 245)
		end

	grey97_color: COLOR is
		once
			create Result.like_rgb_8(247, 247, 247)
		end

	grey98_color: COLOR is
		once
			create Result.like_rgb_8(250, 250, 250)
		end

	grey99_color: COLOR is
		once
			create Result.like_rgb_8(252, 252, 252)
		end

	grey100_color: COLOR is
		once
			create Result.like_rgb_8(255, 255, 255)
		end

	dark_grey_color: COLOR is
		once
			create Result.like_rgb_8(169, 169, 169)
		end

	dark_blue_color: COLOR is
		once
			create Result.like_rgb_8(0, 0, 139)
		end

	dark_cyan_color: COLOR is
		once
			create Result.like_rgb_8(0, 139, 139)
		end

	dark_magenta_color: COLOR is
		once
			create Result.like_rgb_8(139, 0, 139)
		end

	dark_red_color: COLOR is
		once
			create Result.like_rgb_8(139, 0, 0)
		end

	light_green_color: COLOR is
		once
			create Result.like_rgb_8(144, 238, 144)
		end

end -- class FULL_COLOR_LIST
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
