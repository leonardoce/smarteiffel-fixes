-- See the Copyright notice at the end of this file.
--
class WHEN_DOUBLE_CLICKED

inherit
	WHEN_LEFT_CLICKED
		rename when_left_clicked_init as when_double_clicked_init
		redefine when_double_clicked_init, private_left_down, private_left_up
		end

feature {ANY}
	double_click_delay: INTEGER

	double_click_signal: SIGNAL_0

	when_double_clicked (p: PROCEDURE[TUPLE]) is
			-- double click on left button
			-- NOTE: click has been generated before!
		do
			double_click_signal.connect(p)
		end

	set_double_click_delay (cd: INTEGER) is
			-- Set the maximum time (in milliseconds) between a up and
			-- next down sequence for a double-click to be emitted
		require
			cd > 0 and then cd < 1000
		do
			double_click_delay := cd
		ensure
			double_click_delay = cd
		end

feature {}
	default_double_click_delay: INTEGER is 150
			-- Maximum time (milliseconds) for a up/nextdown sequence to be
			-- considered as double click

	when_double_clicked_init is
		do
			Precursor
			create double_click_signal.make
			double_click_delay := default_double_click_delay
		end

	left_up_time: INTEGER

	private_left_down is
		require
		--not left_is_down
		do
			left_is_down := True
			set_state_active
			if not double_click_signal.is_empty and then (vision.event_time - left_up_time).abs < double_click_delay then
				double_click_signal.emit
				left_up_time := 0
				left_may_click := False
			else
				left_may_click := True
			end
			--paint(pos_x,pos_y)
		end

	private_left_up is
		do
			if is_pointer_inside then
				set_state_prelight
				if left_may_click then
					left_up_time := vision.event_time
					left_click_signal.emit
				end
			end
			left_is_down := False
			left_may_click := False
		end

end -- class WHEN_DOUBLE_CLICKED
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
