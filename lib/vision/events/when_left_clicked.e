-- See the Copyright notice at the end of this file.
--
class WHEN_LEFT_CLICKED

inherit
	POINTER_STATUS

insert
	STATE
	WHEN_LEFT_DOWN
	WHEN_LEFT_UP
		--TODO: should set_state_* function really be used here?

feature {ANY}
	left_click_signal: SIGNAL_0

	when_left_clicked (p: PROCEDURE[TUPLE]) is
		do
			left_click_signal.connect(p)
		end

	left_is_down: BOOLEAN

feature {}
	when_left_clicked_init is
		do
			pointer_status_init
			create left_click_signal.make
			when_left_down(agent private_left_down)
			when_left_up(agent private_left_up)
			when_pointer_enter(agent private_left_pointer_enter)
			when_pointer_leave(agent private_left_pointer_leave)
		end

	left_may_click: BOOLEAN

	private_left_down is
		do
			left_is_down := True
			left_may_click := True
			set_state_active
			--paint(pos_x,pos_y)
		ensure
			left_is_down
		end

	private_left_up is
		require
			left_is_down
		do
			if is_pointer_inside then
				set_state_prelight
				if left_may_click then
					left_click_signal.emit
				end
			end
			left_is_down := False
			left_may_click := False
		ensure
			not left_is_down
		end

	private_left_pointer_enter is
		do
			left_is_down := vision.is_left_down
			if left_may_click then
				set_state_active
			else
				set_state_prelight
			end
		end

	private_left_pointer_leave is
		do
			set_state_normal
		end

end -- class WHEN_LEFT_CLICKED
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
