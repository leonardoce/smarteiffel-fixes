-- See the Copyright notice at the end of this file.
--
class WHEN_MIDDLE_CLICKED

inherit
	POINTER_STATUS

insert
	WHEN_MIDDLE_DOWN
	WHEN_MIDDLE_UP

feature {ANY}
	middle_click_signal: SIGNAL_0

	when_middle_clicked (p: PROCEDURE[TUPLE]) is
		do
			middle_click_signal.connect(p)
		end

	middle_is_down: BOOLEAN

feature {}
	when_middle_clicked_init is
		do
			pointer_status_init
			create middle_click_signal.make
			when_middle_down(agent private_middle_down)
			when_middle_up(agent private_middle_up)
			when_pointer_enter(agent private_middle_pointer_enter)
		end

	middle_may_click: BOOLEAN

	private_middle_down is
		do
			middle_is_down := True
			middle_may_click := True
			--paint(pos_x,pos_y)
		ensure
			middle_is_down
		end

	private_middle_up is
		require
			middle_is_down
		do
			if middle_may_click and then is_pointer_inside then
				middle_click_signal.emit
			end
			middle_is_down := False
			middle_may_click := False
		ensure
			not middle_is_down
		end

	private_middle_pointer_enter is
		do
			middle_is_down := vision.is_middle_down
		end

end -- class WHEN_MIDDLE_CLICKED
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
