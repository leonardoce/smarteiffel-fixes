-- See the Copyright notice at the end of this file.
--
class RECTANGLE
	-- Use rectangle as widget (insert in container then position, draw
	-- and size are automatic).

inherit
	WIDGET
	DRAWABLE_HANDLER
	GRAPHIC
		--TODO: supress ?

creation {ANY}
	make

feature {}
	make (min_w, min_h, std_w, std_h: INTEGER) is
		do
			min_width := min_w
			min_height := min_h
			std_width := std_w
			std_height := std_h
			width := std_w
			height := std_h
			x_expand_allowed := True
			y_expand_allowed := True
		end

feature {ANY}
	min_width, min_height: INTEGER

	std_width, std_height: INTEGER

	width, height: INTEGER

	style: DRAW_STYLE

	set_style (s: DRAW_STYLE) is
			-- Change the style used to draw the rectangle.
			-- NOTE: The screen is not updated. --TODO: change this ?
		require
			s /= Void
		do
			style := s
		end

	reset_default_style is
			-- The renderer will be used to draw the line.
			-- NOTE: The screen is not updated. --TODO: change this ?
		do
			style := Void
		end

feature {LAYOUT}
	expose_paint is
		do
			--TODO: handle the width
			if style /= Void then
				style.draw_rectangle(parent.drawing_widget, pos_x, pos_y, width, height)
			else
				renderer.draw_rectangle(parent, pos_x, pos_y, width, height, state)
			end
		end

	set_geometry (x, y, w, h: INTEGER) is
		do
			pos_x := x
			pos_y := y
			width := w
			height := h
		end

feature {}
	resize (w, h: INTEGER) is
		do
			width := w
			height := h
		end

end -- class RECTANGLE
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
