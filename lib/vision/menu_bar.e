-- See the Copyright notice at the end of this file.
--
class MENU_BAR

inherit
	SUB_WINDOW
		redefine make, layout
		end

creation {ANY}
	make

feature {}
	make (p: like parent) is
		local
			rl: ROW_LAYOUT
		do
			create rl
			create actions.with_capacity(4)
			make_layout(p, rl)
			set_background_color(grey_color)
			rl.set_border(5)
			rl.set_spacing(5)
			set_x_expand(True)
			when_left_down(agent left_down)
		end

feature {ANY}
	add_text_entry (text: UNICODE_STRING; menu: MENU) is
		require
			text /= Void
			menu /= Void
		local
			label: LABEL
		do
			create label.make(text)
			actions.add_last(agent menu.open)
			child_attach(label)
		end

	add_entry (w: WIDGET; action: PROCEDURE[TUPLE[INTEGER, INTEGER]]) is
		require
			w /= Void
		do
			child_attach(w)
			actions.add_last(action)
		end

	add_space is
		do
			layout.insert_button_space
			actions.add_last(Void)
		end

	layout: ROW_LAYOUT

feature {}
	actions: FAST_ARRAY[PROCEDURE[TUPLE[INTEGER, INTEGER]]]

	left_down is
		local
			min, max, tmp: INTEGER; stop: BOOLEAN; x, y: INTEGER
		do
			x := vision.pointer_x
			y := vision.pointer_y
			check
				child.count = actions.count
			end
			if not child.is_empty then
				from
					min := child.lower
					max := child.upper
				variant
					max - min
				until
					stop
				loop
					if min = max then
						stop := True
						if child.item(min).area.contain(x, y) then
							if actions.item(min) /= Void then
								x := as_x_root(child.item(min).pos_x)
								y := as_y_root(height - 1) + 1
								actions.item(min).call([x, y])
							end
						end
					else
						check
							min < max
						end
						tmp := (min + max) #// 2
						check
							tmp < max
						end
						if child.item(tmp + 1).pos_x > x then
							max := tmp
						else
							min := tmp + 1
						end
					end
				end
			end
		end

invariant
	actions.count = child.count or actions.count + 1 = child.count

end -- class MENU_BAR
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
