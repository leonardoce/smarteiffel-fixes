-- See the Copyright notice at the end of this file.
--
class VISION_LOOP_STACK
	-- It's some loop_stack (see lib/sequencer) `EVENT_CATCHER' as
	-- "special job". Use it like `LOOP_STACK'.

inherit
	LOOP_STACK
		redefine make, new_loop, push_loop, break
		end

creation {VISION}
	make

feature {}
	vision: VISION

	make is
		do
			Precursor
			create event_catcher_stack.make(0)
		end

feature {EVENT_CATCHER}
	event_catcher_stack: FAST_ARRAY[EVENT_CATCHER]

feature {VISION}
	set_vision (p: VISION) is
		do
			vision := p
		end

feature {ANY}
	new_loop is
			-- create new loop with graphic events management and push it
			-- on the stack
		do
			event_catcher_stack.add_last(vision.event_catcher)
			vision.event_catcher.break
			Precursor
			vision.new_event_catcher
		end

	push_loop (l: like current_loop) is
			-- `l' is restarted and pushed on the stack
			-- Use `push_loop' to reuse some loop (reuse the same modal window).
		local
			ec: EVENT_CATCHER; i: INTEGER
		do
			event_catcher_stack.add_last(vision.event_catcher)
			vision.event_catcher.break
			Precursor(l)
			from
				i := l.job_list.lower
			until
				ec /= Void
			loop
				ec ?= l.job_list.item(i)
				i := i + 1
			end
			vision.change_event_catcher(ec)
		end

	break is
		do
			Precursor
			vision.event_catcher.break
			if not loop_stack.is_empty then
				vision.change_event_catcher(event_catcher_stack.last)
				event_catcher_stack.remove_last
			end
		end

end -- class VISION_LOOP_STACK
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
