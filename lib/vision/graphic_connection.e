-- See the Copyright notice at the end of this file.
--
class GRAPHIC_CONNECTION
	-- This internal class allow to see the graphic connection as a
	-- file who the sequencer has to wait data from.
	-- Some graphic program may wait data from multiple source (files)
	-- and wait for user requests (from the user interface) at the same time.
	-- See lib/sequencer for more details.

inherit
	INPUT_STREAM
		redefine descriptor, dispose
		end

creation {VISION}
	set_descriptor

feature {}
	set_descriptor (desc: INTEGER) is
		do
			descriptor := desc
		end

feature {FILTER}
	filtered_descriptor: INTEGER is
		do
			Result := descriptor
		end

	filtered_has_descriptor: BOOLEAN is
		do
			Result := descriptor /= 0
		end

	filtered_stream_pointer: POINTER is
		do
			check
				False
			end
		end

	filtered_has_stream_pointer: BOOLEAN is
		do
		end

feature {ANY}
	can_read_character, can_unread_character, can_read_line, valid_last_character: BOOLEAN is False

	end_of_input: BOOLEAN is True

	disconnect is
		do
			check
				False
			end
		end

feature {FILTER_INPUT_STREAM}
	filtered_read_character, filtered_unread_character is
		do
			check
				False
			end
		end

	filtered_last_character: CHARACTER is
		do
			check
				False
			end
		end

feature {ANY}
	descriptor: INTEGER

	can_disconnect: BOOLEAN is True

	is_connected: BOOLEAN is
		do
			Result := descriptor /= 0
		end

feature {}
	dispose is
		do
		end
	
end -- class GRAPHIC_CONNECTION
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
