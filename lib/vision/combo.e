-- See the Copyright notice at the end of this file.
--
class COMBO
	--
	-- Allow to select one element from a list.

inherit
	SUB_WINDOW
		rename make as sub_window_make
		redefine layout
		end

creation {ANY}
	make, with_text

feature {}
	make (p: like parent; elements: COLLECTION[WIDGET]) is
		require
			p /= Void
			elements /= Void
		do
			sub_window_make(p)
			create handle
			set_background_color(white_color)
			change_items(elements)
			child_attach(handle)
			when_left_down(agent open_list)
		ensure
			items = elements
			elements.is_empty implies active_item = elements.lower - 1
			not elements.is_empty implies active_item = elements.lower
		end

	with_text (p: like parent; elements: COLLECTION[UNICODE_STRING]) is
		require
			p /= Void
			elements /= Void
		local
			i: INTEGER
		do
			from
				i := elements.lower
				items := create {ARRAY[WIDGET]}.make(elements.lower, elements.upper)
			until
				i > elements.upper
			loop
				items.put(create {LABEL}.make(elements.item(i)), i)
				i := i + 1
			end
			private_labels := True
			make(p, items)
		ensure
			items.lower = elements.lower
			items.upper = elements.upper
			elements.is_empty implies active_item = elements.lower - 1
			not elements.is_empty implies active_item = elements.lower
		end

feature {ANY}
	items: COLLECTION[WIDGET]

	active_item: INTEGER

	layout: ROW_LAYOUT

	change_items (elements: COLLECTION[WIDGET]) is
		require
			elements /= Void
		do
			if elements.is_empty then
				layout.insert_button_space
			else
				child.force(elements.first, 0)
				if child.first.parent /= Void then
					child.first.set_parent(Void)
				end
				child.first.set_parent(Current)
				child_requisition_changed
			end
			items := elements
			active_item := items.lower
			private_labels := False
		ensure
			items = elements
			elements.is_empty implies active_item = elements.lower - 1
			not elements.is_empty implies active_item = elements.lower
		end

	change_items_with_text (elements: COLLECTION[UNICODE_STRING]) is
		require
			elements /= Void
		local
			i: INTEGER; arr: ARRAY[WIDGET]; label: LABEL
		do
			if private_labels then
				arr ?= items
				arr.resize(elements.lower, elements.upper)
			else
				create arr.make(elements.lower, elements.upper)
				items := arr
				private_labels := True
			end
			from
				i := elements.lower
			until
				i > elements.upper
			loop
				label ?= arr.item(i)
				if label /= Void then
					label.set_text(elements.item(i))
				else
					arr.put(create {LABEL}.make(elements.item(i)), i)
				end
				i := i + 1
			end
			change_items(items)
		ensure
			items.lower = elements.lower
			items.upper = elements.upper
			elements.is_empty implies active_item = elements.lower - 1
			not elements.is_empty implies active_item = elements.lower
		end

	activate (i: like active_item) is
		require
			items.valid_index(i) or else i = items.lower - 1
		do
			if active_item /= i then
				active_item := i
				if when_value_change_signal /= Void then
					when_value_change_signal.emit(i)
				end
			end
			if active_item /= items.lower - 1 then
				child.put(items.item(active_item), 0)
				if child.first.parent /= Void then
					child.first.set_parent(Void)
				end
				child.first.set_parent(Current)
				child_requisition_changed
			end
		ensure
			active_item = i
		end

	when_value_change_signal: SIGNAL_1[INTEGER]

	when_value_change (p: PROCEDURE[TUPLE[INTEGER]]) is
		do
			if when_value_change_signal = Void then
				create when_value_change_signal.make
			end
			when_value_change_signal.connect(p)
		end

feature {}
	handle: COMBO_HANDLE

	private_labels: BOOLEAN

	open_list is
		local
			list_window: LIST_WINDOW
		do
			list_window := shared_list_window
			if list_window.close_time /= vision.event_time or else list_window.items /= items then
				list_window.open(items, active_item, agent activate, as_x_root(0), as_y_root(0) + height)
			end
		end

	shared_list_window: LIST_WINDOW is
		once
			create Result
		end

invariant
	items.valid_index(active_item) or else active_item = items.lower - 1

end -- class COMBO
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
