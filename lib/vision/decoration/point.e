-- See the Copyright notice at the end of this file.
--
expanded class POINT

insert
	HASHABLE
		redefine out
		end

creation {ANY}
	default_create

feature {ANY}
	is_equal (other: like Current): BOOLEAN is
		do
			Result := other.x = x and then other.y = y
		end

	out: STRING is
		do
			Result := out_buffer
			Result.clear_count
			Result.extend('(')
			x.append_in(Result)
			Result.extend(',')
			y.append_in(Result)
			Result.extend(')')
		end

	hash_code: INTEGER is
		do
			Result := x * 65536 + y
		end

feature {DECORATOR, POINT}
	set (x_, y_: INTEGER) is
		do
			if not storage.is_not_null then
				make_storage
			end
			basic_guikit_point(to_external, x_, y_)
		end

	set_x (x_: INTEGER) is
		do
			set(x_, y)
		end

	set_y (y_: INTEGER) is
		do
			set(x, y_)
		end

	to_external: POINTER is
		do
			Result := storage.to_external
		end

	x: INTEGER is
		do
			Result := basic_guikit_point_get_x(storage.to_external)
		end

	y: INTEGER is
		do
			Result := basic_guikit_point_get_y(storage.to_external)
		end

feature {DECORATOR}
	export_data (store: like storage; index: INTEGER) is
		do
			store.copy_at(basic_guikit_point_size * index, storage, basic_guikit_point_size)
		end

feature {}
	out_buffer: STRING is "(0,0)"

	storage: NATIVE_ARRAY[INTEGER_8]

	make_storage is
		do
			storage := storage.calloc(basic_guikit_point_size)
		end

	basic_guikit_point (store: POINTER; x_, y_: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "guikit"
			feature_name: "basic_guikit_point"
			}"
		end

	basic_guikit_point_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "guikit"
			feature_name: "basic_guikit_point_size"
			}"
		end

	basic_guikit_point_get_x (store: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "guikit"
			feature_name: "basic_guikit_point_get_x"
			}"
		end

	basic_guikit_point_get_y (store: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "guikit"
			feature_name: "basic_guikit_point_get_y"
			}"
		end

end -- class POINT
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
