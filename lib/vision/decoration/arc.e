-- See the Copyright notice at the end of this file.
--
expanded class ARC

insert
	ANY
		redefine is_equal, default_create, out
		end

creation {ANY}
	default_create

feature {ANY}
	is_equal (other: like Current): BOOLEAN is
		do
			Result := other.x = x and then other.y = y and then other.width = width and then other.height = height and then other.angle1 = angle1 and then other.angle2 = angle2
		end

	out: STRING is
		do
			Result := out_buffer
			Result.clear_count
			Result.extend('(')
			x.append_in(Result)
			Result.extend(',')
			y.append_in(Result)
			Result.extend(' ')
			width.append_in(Result)
			Result.extend('x')
			height.append_in(Result)
			Result.extend(' ')
			(angle1.to_real_64 / 64.0).append_in(Result)
			Result.append(once "� ")
			(angle2.to_real_64 / 64.0).append_in(Result)
			Result.append(once "�)")
		end

feature {DECORATOR, ARC}
	set (x_, y_, width_, height_, angle1_, angle2_: INTEGER) is
		do
			if not storage.is_not_null then
				make_storage
			end
			basic_guikit_arc(to_external, x_, y_, width_, height_, angle1_, angle2_)
		end

	set_x (x_: INTEGER) is
		do
			set(x_, y, width, height, angle1, angle2)
		end

	set_y (y_: INTEGER) is
		do
			set(x, y_, width, height, angle1, angle2)
		end

	set_width (width_: INTEGER) is
		do
			set(x, y, width_, height, angle1, angle2)
		end

	set_height (height_: INTEGER) is
		do
			set(x, y, width, height_, angle1, angle2)
		end

	set_angle1 (angle1_: INTEGER) is
			-- angle in 64th of degree
		do
			set(x, y, width, height, angle1_, angle2)
		end

	set_angle2 (angle2_: INTEGER) is
			-- angle in 64th of degree
		do
			set(x, y, width, height, angle1, angle2_)
		end

	to_external: POINTER is
		do
			Result := storage.to_external
		end

	x: INTEGER is
		do
			--         Result := basic_guikit_arc_get_x(storage.to_external)
		end

	y: INTEGER is
		do
			--         Result := basic_guikit_arc_get_y(storage.to_external)
		end

	width: INTEGER is
		do
			--         Result := basic_guikit_arc_get_width(storage.to_external)
		end

	height: INTEGER is
		do
			--         Result := basic_guikit_arc_get_height(storage.to_external)
		end

	angle1: INTEGER is
			-- angle in 64th of degree
		do
			--         Result := basic_guikit_arc_get_angle1(storage.to_external)
		end

	angle2: INTEGER is
			-- angle in 64th of degree
		do
			--         Result := basic_guikit_arc_get_angle2(storage.to_external)
		end

feature {DECORATOR}
	export_data (store: like storage; index: INTEGER) is
		do
			store.copy_at(basic_guikit_arc_size * index, storage, basic_guikit_arc_size)
		end

feature {}
	default_create is
		do
			--io.put_string(once "new ARC!%N")
		end

	storage: NATIVE_ARRAY[INTEGER_8]

	out_buffer: STRING is ""

	make_storage is
		do
			storage := storage.calloc(basic_guikit_arc_size)
		end

	basic_guikit_arc (store: POINTER; x_, y_, width_, height_, angle1_, angle2_: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "guikit"
			feature_name: "basic_guikit_arc"
			}"
		end

	basic_guikit_arc_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "guikit"
			feature_name: "basic_guikit_arc_size"
			}"
		end

end -- class ARC
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
