-- See the Copyright notice at the end of this file.
--
class DECORATOR
	-- The most complex decorator

inherit
	DECORATION

creation {ANY}
	make

feature {LAYOUT}
	expose_paint is
		do
			not_yet_implemented
		end

feature {LAYOUT}
	set_geometry (x, y, w, h: INTEGER) is
		do
			not_yet_implemented
		end

feature {ANY}
	std_height: INTEGER is
		do
			not_yet_implemented
		end

	std_width: INTEGER is
		do
			not_yet_implemented
		end

	make (s: BOOLEAN) is
		do
			if s then
				not_yet_implemented
			end
			scaleable := s
			create dic_segments.make
			create dic_arcs.make
			create all_segments.make(0)
			create all_arcs.make(0)
			create dic_polygons.make
			create dic_pies.make
			create all_polygons.make(0)
			create all_pies.make(0)
		ensure
			scaleable = s
		end

	scaleable: BOOLEAN
			-- if True, the points are modified when the decorator is resized

feature {ANY}
	add_segment (x1, y1, x2, y2: INTEGER) is
		do
			do_add_segment(x1 + last_x, y1 + last_y, x2 + last_x, y2 + last_y, dic_segments, all_segments)
		end

	add_arc (x, y, w, h, angle1, angle2: INTEGER) is
		do
			do_add_arc(x + last_x, y + last_y, w, h, angle1, angle2, dic_arcs, all_arcs)
		end

	add_polygon (x1, y1, x2, y2: INTEGER) is
		do
			do_add_segment(x1 + last_x, y1 + last_y, x2 + last_x, y2 + last_y, dic_polygons, all_polygons)
		end

	add_pie (x, y, w, h, angle1, angle2: INTEGER) is
		do
			do_add_arc(x + last_x, y + last_y, w, h, angle1, angle2, dic_pies, all_pies)
		end

	paint (x, y: INTEGER) is
		do
			if widget /= default_pointer then
				do_paint_segments(x, y, False, all_segments)
				do_paint_segments(x, y, True, all_polygons)
				do_paint_arcs(x, y, False, all_arcs)
				do_paint_arcs(x, y, True, all_pies)
				last_x := x
				last_y := y
			end
		end

	resize (w, h: INTEGER) is
		do
			do_resize(w, h)
		end

feature {}
	-- Adding segments or polygons:
	-- In those algorithms we call "segment" any array of one or more points:
	-- indeed, of type FAST_ARRAY[POINT]
	show (dictionary: like dic_segments) is
		local
			p: POINT; i, j: ITERATOR[POINT]; n: ITERATOR[FAST_ARRAY[POINT]]; segs: FAST_ARRAY[FAST_ARRAY[POINT]]
			seg: FAST_ARRAY[POINT]
		do
			from
				i := dictionary.get_new_iterator_on_keys
				i.start
			until
				i.is_off
			loop
				p := i.item
				io.put_string(once "Anchor = ")
				io.put_string(p.out)
				io.put_new_line
				segs := dictionary.at(p)
				from
					n := segs.get_new_iterator
					n.start
				until
					n.is_off
				loop
					seg := n.item
					from
						j := seg.get_new_iterator
						j.start
					until
						j.is_off
					loop
						io.put_string(" ")
						io.put_string(j.item.out)
						j.next
					end
					n.next
				end
				io.put_new_line
				i.next
			end
		end

	do_add_segment (x1, y1, x2, y2: INTEGER; dictionary: like dic_segments; list: like all_segments) is
		local
			p1, p2: POINT; segs: FAST_ARRAY[FAST_ARRAY[POINT]]
		do
			debug
				io.put_string("--8<--------%N")
			end
			p1.set(x1, y1)
			p2.set(x2, y2)
			debug
				io.put_string(once "p1 = ")
				io.put_string(p1.out)
				io.put_string(once ", p2 = ")
				io.put_string(p2.out)
				io.put_new_line
				show(dictionary)
			end
			if dictionary.has(p1) then
				debug
					io.put_string("dictionary.has(p1)%N")
				end
				if dictionary.has(p2) and then exists_segment(p1, p2, dictionary) then
					-- nothing: the segment already exists
				else
					segs := dictionary.at(p1)
					try_add_point_to_segment(p2, segs, p1, dictionary, list)
				end
			else
				if dictionary.has(p2) then
					debug
						io.put_string("dictionary.has(p2)%N")
					end
					segs := dictionary.at(p2)
					try_add_point_to_segment(p1, segs, p2, dictionary, list)
				else
					make_segment(p1, p2, dictionary, list)
				end
			end
			debug
				io.put_string("-------->8--%N")
			end
		end

	exists_segment (p1, p2: POINT; dictionary: like dic_segments): BOOLEAN is
		require
			dictionary.has(p1)
			dictionary.has(p2)
		local
			d1, d2: FAST_ARRAY[FAST_ARRAY[POINT]]; i: INTEGER
		do
			from
				d1 := dictionary.at(p1)
				d2 := dictionary.at(p2)
				i := d1.lower
			until
				Result or else i > d1.upper
			loop
				Result := d2.fast_has(d1.item(i))
				i := i + 1
			end
		end

	try_add_point_to_segment (p: POINT; segs: FAST_ARRAY[FAST_ARRAY[POINT]]; anchor: POINT
		dictionary: like dic_segments; list: like all_segments) is
		require
		--segs.exists(agent {FAST_ARRAY[POINT]}.has(anchor))
			dictionary.has(anchor)
			-- redundant check!
			--not dictionary.has(p) -- this one is not True: the thing is, there must not be a segment between p and anchor
		local
			segs2: FAST_ARRAY[FAST_ARRAY[POINT]]; seg, seg2: FAST_ARRAY[POINT]; i, j, idx: INTEGER
			found, found2: BOOLEAN
		do
			-- This algorithm is a bit complicated. We know that `anchor' is a
			-- point of one of the segments in `segs'.
			-- Now we need to know if it is an _end_ of a segment. If it is, we
			-- just have to add the new point `p' at the end of the
			-- segment. Otherwise we have to create a brand new segment and add
			-- it to `segs'.
			from
				i := segs.lower
			until
				found or else i > segs.upper
			loop
				seg := segs.item(i)
				if anchor.is_equal(seg.first) then
					found := True
					debug
						io.put_string("add ")
						io.put_string(p.out)
						io.put_string(" first%N")
					end
					if p.is_equal(seg.last) then
						-- loop
					elseif dictionary.has(p) then
						segs2 := dictionary.at(p)
						from
							found2 := False
							j := segs2.lower
						until
							found2 or else j > segs2.upper
						loop
							seg2 := segs2.item(j)
							if seg /= seg2 then
								if p.is_equal(seg2.first) then
									found2 := True
									seg.add_first(p)
									join(seg, seg2, True, True)
									idx := list.first_index_of(seg2)
									list.remove(idx)
								elseif p.is_equal(seg2.last) then
									found2 := True
									seg.add_first(p)
									join(seg, seg2, True, False)
									idx := list.first_index_of(seg2)
									list.remove(idx)
								end
							end
							j := j + 1
						end
						if not found2 then
							seg.add_first(p)
						end
					else
						seg.add_first(p)
					end
				elseif anchor.is_equal(seg.last) then
					found := True
					debug
						io.put_string("add ")
						io.put_string(p.out)
						io.put_string(" last%N")
					end
					if p.is_equal(seg.first) then
						-- loop
					elseif dictionary.has(p) then
						segs2 := dictionary.at(p)
						from
							found2 := False
							j := segs2.lower
						until
							found2 or else j > segs2.upper
						loop
							seg2 := segs2.item(j)
							if seg /= seg2 then
								if p.is_equal(seg2.first) then
									found2 := True
									seg.add_last(p)
									join(seg, seg2, False, False)
									idx := list.first_index_of(seg2)
									list.remove(idx)
								elseif p.is_equal(seg2.last) then
									found2 := True
									seg.add_last(p)
									join(seg, seg2, False, True)
									idx := list.first_index_of(seg2)
									list.remove(idx)
								end
							end
							j := j + 1
						end
						if not found2 then
							seg.add_last(p)
						end
					else
						seg.add_last(p)
					end
				end
				i := i + 1
			end
			if not found then
				-- the anchor is at no end of segment: make a new segment
				make_segment_in(p, anchor, segs, list)
				dictionary.put(segs, p)
			end
		end

	make_segment_in (p1, p2: POINT; segs, list: FAST_ARRAY[FAST_ARRAY[POINT]]) is
		local
			seg: FAST_ARRAY[POINT]
		do
			debug
				io.put_string("new points%N")
			end
			create {FAST_ARRAY[POINT]} seg.make(2)
			seg.put(p1, 0)
			seg.put(p2, 1)
			segs.add_last(seg)
			list.add_last(seg)
		end

	make_segment (p1, p2: POINT; dictionary: like dic_segments; list: like all_segments) is
		local
			segs: FAST_ARRAY[FAST_ARRAY[POINT]]
		do
			create {FAST_ARRAY[FAST_ARRAY[POINT]]} segs.make(0)
			make_segment_in(p1, p2, segs, list)
			dictionary.put(segs, p1)
			dictionary.put(segs, p2)
		end

	join (seg1, seg2: FAST_ARRAY[POINT]; first, backwards: BOOLEAN) is
			-- Add seg2 to seg1.
			-- If `first', prepend it; otherwise append it.
			-- If `backwards', add it in reverse order.
		local
			i, d: INTEGER
		do
			from
				if backwards then
					check
						first implies seg2.last.is_equal(seg1.first)
						not first implies seg2.last.is_equal(seg1.last)
					end
					i := seg2.upper - 1
					d := -1
				else
					check
						first implies seg2.first.is_equal(seg1.first)
						not first implies seg2.first.is_equal(seg1.last)
					end
					i := seg2.lower + 1
					d := 1
				end
			until
				not seg2.valid_index(i)
			loop
				if first then
					seg1.add_first(seg2.item(i))
				else
					seg1.add_last(seg2.item(i))
				end
				i := i + d
			end
		end

feature {} -- Adding arcs and pies:
	do_add_arc (x, y, w, h, angle1, angle2: INTEGER; dictionary: like dic_arcs; list: like all_arcs) is
		local
			dic_arcs_by_size: HASHED_DICTIONARY[FAST_ARRAY[ARC], POINT]; arcs: FAST_ARRAY[ARC]; center: POINT
			size: POINT; arc: ARC
		do
			-- like dic_arcs.item
			-- like dic_arcs_by_size.item
			center.set(x, y)
			size.set(w, h)
			arc.set(x, y, w, h, angle1, angle2)
			if dictionary.has(center) then
				dic_arcs_by_size := dic_arcs.at(center)
				if dic_arcs_by_size.has(size) then
					-- merging arcs not yet implemented: for now we just add the
					-- arc and hope that the X server can merge adjacent and
					-- redundant arcs
					dic_arcs_by_size.at(size).add_last(arc)
					list.add_last(arc)
				else
					-- same center, but not same size
					create arcs.make(1)
					arcs.put(arc, 0)
					dic_arcs_by_size.put(arcs, size)
					list.add_last(arc)
				end
			else
				-- this arc has no registered center
				create dic_arcs_by_size.make
				create arcs.make(1)
				arcs.put(arc, 0)
				dic_arcs_by_size.put(arcs, size)
				dictionary.put(dic_arcs_by_size, center)
				list.add_last(arc)
			end
		end

feature {} -- Painting:
	last_x: INTEGER

	last_y: INTEGER

	do_paint_segments (x, y: INTEGER; fill: BOOLEAN; list: like all_segments) is
		local
			i, j: INTEGER; seg: FAST_ARRAY[POINT]; buffer: NATIVE_ARRAY[INTEGER_8]; buffer_size, size: INTEGER
		do
			from
				i := list.lower
			until
				i > list.upper
			loop
				seg := list.item(i)
				size := seg.count * basic_guikit_point_size
				if buffer_size = 0 then
					buffer := buffer.calloc(size)
					buffer_size := size
				elseif buffer_size /= size then
					buffer := buffer.realloc(buffer_size, size)
					buffer_size := size
				end
				from
					j := seg.lower
				until
					j > seg.upper
				loop
					seg.item(j).export_data(buffer, j)
					j := j + 1
				end
				if fill then
					basic_guikit_fillpolygon(widget, buffer.to_external, seg.count, x - last_x, y - last_y)
				else
					basic_guikit_drawlines(widget, buffer.to_external, seg.count, x - last_x, y - last_y)
				end
				i := i + 1
			end
		end

	do_paint_arcs (x, y: INTEGER; fill: BOOLEAN; list: like all_arcs) is
		local
			j: INTEGER; buffer: NATIVE_ARRAY[INTEGER_8]; buffer_size, size: INTEGER
		do
			size := list.count * basic_guikit_arc_size
			if buffer_size = 0 then
				buffer := buffer.calloc(size)
				buffer_size := size
			elseif buffer_size /= size then
				buffer := buffer.realloc(buffer_size, size)
				buffer_size := size
			end
			from
				j := list.lower
			until
				j > list.upper
			loop
				list.item(j).export_data(buffer, j)
				j := j + 1
			end
			if fill then
				basic_guikit_fillarcs(widget, buffer.to_external, list.count, x - last_x, y - last_y)
			else
				basic_guikit_drawarcs(widget, buffer.to_external, list.count, x - last_x, y - last_y)
			end
		end

feature {} -- Resizing:
	do_resize (w, h: INTEGER) is
		local
			scale_w, scale_h: REAL
		do
			if scaleable then
				scale_w := w.to_real_64 / width.to_real_64
				scale_h := h.to_real_64 / height.to_real_64
			end
			width := w
			height := h
		end

	do_scale_segments (scale_w, scale_h: REAL) is
		do
			-- not yet implemented
		end

	do_scale_arcs (scale_w, scale_h: REAL) is
		do
			-- not yet implemented
		end

	do_scale_polygons (scale_w, scale_h: REAL) is
		do
			-- not yet implemented
		end

	do_scale_pies (scale_w, scale_h: REAL) is
		do
			-- not yet implemented
		end

feature {}
	-- Structure:
	-- The structure is pretty complex.
	-- SEGMENTS:
	--  - The dictionary has points as keys. Each point links to every segment
	--    having it (as an end or not!)
	--  - The list allows fast painting of all segments
	--  - Polygons are filled segments
	-- ARCS:
	--  - The dictionary has centers as keys. Each point links to another
	--    dictionary; this one has (width, height) as keys. Those keys in turn
	--    point to every arc having this center and this size.
	--  - The list allows fast painting of all arcs
	--  - Pies are filled arcs
	dic_arcs: HASHED_DICTIONARY[HASHED_DICTIONARY[FAST_ARRAY[ARC], POINT], POINT]

	dic_segments: HASHED_DICTIONARY[FAST_ARRAY[FAST_ARRAY[POINT]], POINT]

	dic_pies: HASHED_DICTIONARY[HASHED_DICTIONARY[FAST_ARRAY[ARC], POINT], POINT]

	dic_polygons: HASHED_DICTIONARY[FAST_ARRAY[FAST_ARRAY[POINT]], POINT]

	all_arcs: FAST_ARRAY[ARC]

	all_segments: FAST_ARRAY[FAST_ARRAY[POINT]]

	all_pies: FAST_ARRAY[ARC]

	all_polygons: FAST_ARRAY[FAST_ARRAY[POINT]]

feature {} -- Basics:
	basic_guikit_drawlines (peer: POINTER; points: POINTER; count_points: INTEGER; dx, dy: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "guikit"
	 feature_name: "basic_guikit_drawlines"
	 }"
		end

	basic_guikit_fillpolygon (peer: POINTER; points: POINTER; count_points: INTEGER; dx, dy: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "guikit"
	 feature_name: "basic_guikit_fillpolygon"
	 }"
		end

	basic_guikit_drawarcs (peer: POINTER; arcs: POINTER; count_arcs: INTEGER; dx, dy: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "guikit"
	 feature_name: "basic_guikit_drawarcs"
	 }"
		end

	basic_guikit_fillarcs (peer: POINTER; arcs: POINTER; count_arcs: INTEGER; dx, dy: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "guikit"
	 feature_name: "basic_guikit_fillarcs"
	 }"
		end

	basic_guikit_point_size: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "guikit"
	 feature_name: "basic_guikit_point_size"
	 }"
		end

	basic_guikit_arc_size: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "guikit"
	 feature_name: "basic_guikit_arc_size"
	 }"
		end

end -- class DECORATOR
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
