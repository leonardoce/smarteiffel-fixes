-- See the Copyright notice at the end of this file.
--
class EVENT_CATCHER
	-- EVENT_CATCHER is the `JOB' that handle graphic interface
	-- relative events. The event_catcher wait for such events and
	-- emit signal relative to this event. Previously registred
	-- procedure (via vision.*_connect) are executed.
	--
	-- For more information about `JOB', see lib/sequencer and
	-- tutorial/sequencer directories.
	--Events without parameters:
	-- 2: KeyPress          = key_down
	-- 3: KeyRelease        = key_up
	-- 7: EnterNotify       = pointer_enter
	-- 8: LeaveNotify       = pointer_leave
	-- 9: FocusIn           = focus_in
	-- 10: FocusOut         = focus_out
	-- 11: KeymapNotify     = ???????????
	-- 12: Expose           = expose (expose_area need repaint)
	-- 13: GraphicsExpose   = ???????????
	-- 14: NoExpose         = ???????????
	-- 16: CreateNotify     = ???????????
	-- 17: DestroyNotify    = ???????????
	-- 18: UnmapNotify      = unmaped
	-- 19: MapNotify        = maped
	-- 20: MapRequest       = ??????????? find examples...
	-- 21: ReparentNotify   = ???????????
	-- 23: ConfigureRequest = ???????????
	-- 24: GravityNotify    =
	-- 25: ResizeRequest    = ??????????? 101 give information
	-- 26: CirculateNotify  =
	-- 27: CirculateRequest =
	-- 28: PropertyNotify   =
	-- 29: SelectionClear   =
	-- 30: SelectionRequest =
	-- 31: SelectionNotify  =
	-- 32: ColormapNotify   = ??????????? how many events
	-- 33: ClientMessage    =
	-- 34: MappingNotify    =
	-- 40: Button1Press     = when_left_down
	-- 41: Button2Press     = when_middle_down
	-- 42: Button3Press     = when_right_down
	-- 43: WheelUp          = when_wheel_up   --rotate up
	-- 44: WheelDown        = when_wheel_down --rotate down
	-- 50: Button1Release   = when_left_up
	-- 51: Button2Release   = when_middle_up
	-- 52: Button3Release   = when_right_up
	-- 60: Unobscured       = fully_visible
	-- 61: PartiallyObscured= partially_visible
	-- 62: FullyObscured    = not_visible
	-- 63: DeleteWindow     = when_close_requested
	--Events with parameters:
	-- 100: MotionNotify(x,y:INTEGER) = pointer_move
	-- 101: ConfigureNotify(x,y,width,height:INTEGER) = geometry_change
	-- 102: ConfigureNotify(x,y,width,height:INTEGER) = window move
	-- 103: ConfigureNotify(x,y,width,height:INTEGER) = window_resize
	-- 110: ExtensionDeviceEvent(event_number: INTEGER; win: SENSITIVE)
	
inherit
	JOB
	GRAPHIC
		--TODO: remove ? (needed for vision access)

creation {VISION}
	make

feature {}
	make (g_c: GRAPHIC_CONNECTION) is
		local
			ev: HASHED_DICTIONARY[SIGNAL_0, SENSITIVE]; i: INTEGER
		do
			graphic_connection := g_c
			create widget.make
			create event.make(65)
			from
				i := event.upper
			until
				i < event.lower
			loop
				create ev.make
				event.put(ev, i)
				i := i - 1
			end
			create pointer_move_event.make
			create geometry_change_event.make
			create extension_devices.make
		end

feature {LOOP_ITEM}
	--|*** TODO: have a specific EVENT_DESCRIPTOR instead of a specific GRAPHIC_CONNECTION?

	prepare (events: EVENTS_SET) is
		do
			basic_vision_flush
			events.expect(graphic_connection.event_can_read)
		end

	is_ready (events: EVENTS_SET): BOOLEAN is
		do
			Result := events.event_occurred(graphic_connection.event_can_read)
		end

feature {LOOP_ITEM, VISION}
	continue is
		local
			next_event: INTEGER; s: SENSITIVE; c_widget: POINTER
		do
			from
				next_event := basic_vision_next_event
				current_event_type := next_event
				suspend := False
			until
				next_event = -1 or else suspend
			loop
				if next_event = 40 and then preprocess_left_down_proc /= Void then
					preprocess_left_down_proc.call([])
				end
				c_widget := basic_vision_event_widget
				s := widget.reference_at(c_widget)
				if s /= Void then
					emit_event(s, next_event)
				elseif next_event > 3 and then not next_event.in_range(40, 59) and then next_event /= 100 then
					-- The event is not relative to widget from this loop
					-- and this type of event has to be propagated to the
					-- other events loop.
					s := search_widget(c_widget)
					if s /= Void then
						event_catcher_found.emit_event(s, next_event)
					else
						debug
							io.put_string("No widget for this event, type=")
							io.put_integer(next_event)
							io.put_new_line
						end
					end
				end
				next_event := basic_vision_next_event
				current_event_type := next_event
			end
		end

feature {LOOP_ITEM}
	done: BOOLEAN

	restart is
		do
			done := False
		end

feature {EVENT_CATCHER}
	emit_event (s: SENSITIVE; next_event: INTEGER) is
			-- Emit signal relative to event `next_event' for widget `s'.
		local
			sig0: SIGNAL_0; sig2: SIGNAL_2[INTEGER, INTEGER]; sig4: SIGNAL_4[INTEGER, INTEGER, INTEGER, INTEGER]
			win: WINDOW
			ext_device: EXTENSION_DEVICE
		do
			if next_event < 100 then
				sig0 := event.item(next_event).reference_at(s)
				if sig0 /= Void then
					sig0.emit
				end
			else
				inspect
					next_event
				when 100 then
					sig2 := pointer_move_event.reference_at(s)
					if sig2 /= Void then
						sig2.emit(basic_vision_pointer_x, basic_vision_pointer_y)
					end
				when 101 then
					sig4 := geometry_change_event.reference_at(s)
					if sig4 /= Void then
						sig4.emit(basic_vision_geometry_x, basic_vision_geometry_y, basic_vision_geometry_width, basic_vision_geometry_height)
					end
				when 102 then
					sig4 := geometry_change_event.reference_at(s)
					if sig4 /= Void then
						win ?= s
						check
							win /= Void
						end
						sig4.emit(basic_vision_geometry_x, basic_vision_geometry_y, win.width, win.height)
					end
				when 103 then
					sig4 := geometry_change_event.reference_at(s)
					if sig4 /= Void then
						win ?= s
						check
							win /= Void
						end
						sig4.emit(win.pos_x, win.pos_y, basic_vision_geometry_width, basic_vision_geometry_height)
					end
				when 110 then
					ext_device := extension_devices.reference_at(last_event_device_id)
					if ext_device /= Void then
						ext_device.handle_event(basic_vision_real_event, s)
					end
				end
			end
		end

feature {VISION}
	current_event_type: INTEGER

	current_event_native_type: INTEGER is
		require
			current_event_type = 110
		do
			Result := basic_vision_real_event
		end
	
	stop is
		do
			done := True
		end

	preprocess_left_down (p: PROCEDURE[TUPLE]) is
		do
			preprocess_left_down_proc := p
		end

	add_extension(extension_device: EXTENSION_DEVICE) is
		do
			extension_devices.put(extension_device, extension_device.device_id)
		end

feature {LOOP_STACK}
	break is
		do
			suspend := True
		end

feature {VISION, EVENT_CATCHER}
	widget: HASHED_DICTIONARY[SENSITIVE, POINTER]

feature {VISION}
	event: FAST_ARRAY[DICTIONARY[SIGNAL_0, SENSITIVE]]

	pointer_move_event: HASHED_DICTIONARY[SIGNAL_2[INTEGER, INTEGER], SENSITIVE]

	geometry_change_event: HASHED_DICTIONARY[SIGNAL_4[INTEGER, INTEGER, INTEGER, INTEGER], SENSITIVE]

feature {}
	extension_devices: HASHED_DICTIONARY[EXTENSION_DEVICE, INTEGER]

	preprocess_left_down_proc: PROCEDURE[TUPLE]

	graphic_connection: GRAPHIC_CONNECTION

	suspend: BOOLEAN

	event_catcher_found: EVENT_CATCHER

	search_widget (c_widget: POINTER): SENSITIVE is
			-- Widget `c_widget' doesn't exist in current loop. This
			-- function searches in the loop_stack to find the
			-- event_catcher that manages this widget. If found, the widget
			-- is retruned and `event_catcher_found` is set to the relative
			-- event_catcher.
		local
			stack: FAST_ARRAY[EVENT_CATCHER]; ls: VISION_LOOP_STACK; i: INTEGER
		do
			from
				ls ?= vision.loop_stack
				stack := ls.event_catcher_stack
				i := stack.upper
			until
				Result /= Void or else i < 0
			loop
				Result := stack.item(i).widget.reference_at(c_widget)
				i := i - 1
			end
			if Result /= Void then
				event_catcher_found := stack.item(i + 1)
			end
		ensure
			Result /= Void implies event_catcher_found.widget.has(c_widget)
			Result /= Void implies event_catcher_found.widget.at(c_widget) = Result
		end

	last_event_device_id: INTEGER is
		require
			current_event_type = 110 -- For extension device
		do
			Result := basic_vision_extension_device_id
		end

	basic_vision_next_event: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_next_event"
	 }"
		end

	basic_vision_real_event: INTEGER is -- The native (original) event code
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_real_event"
	 }"
		end

	basic_vision_extension_device_id: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_extension_device_id"
	 }"
		end

	basic_vision_flush is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_flush"
	 }"
		end

	basic_vision_event_widget: POINTER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_event_widget"
	 }"
		end

	basic_vision_pointer_x: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_pointer_x"
	 }"
		end

	basic_vision_pointer_y: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_pointer_y"
	 }"
		end

	basic_vision_geometry_x: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_geometry_x"
	 }"
		end

	basic_vision_geometry_y: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_geometry_y"
	 }"
		end

	basic_vision_geometry_width: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_geometry_width"
	 }"
		end

	basic_vision_geometry_height: INTEGER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins"
	 module_name: "vision"
	 feature_name: "basic_vision_geometry_height"
	 }"
		end

end -- class EVENT_CATCHER
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
