-- See the Copyright notice at the end of this file.
--
class HORIZONTAL_LINE
	-- `HORIZONTAL_LINE' is a widget you can use as separator. As it's
	-- a widget, position and size are managed by the layout
	-- associated with the container the line is put into.

inherit
	LINE
		redefine default_create
		end

creation {ANY}
	default_create

feature {}
	default_create is
		do
			width := 1
			height := 1
			thickness := 1
			x_expand_allowed := True
			std_width := 1
		end

feature {ANY}
	min_width: INTEGER is 1
			--TODO: change with thickness ?

	std_width: INTEGER

	std_height, min_height: INTEGER is
		do
			Result := thickness
		end

	set_thickness (thick: INTEGER) is
		do
			--TODO: influence width/position
			if thickness /= thick then
				thickness := thick
				if thick > height then
					--TODO: change this code
					height := thick
				end
				parent.child_requisition_changed
			end
		end

feature {LAYOUT}
	expose_paint is
		do
			if style /= Void then
				style.draw_line(parent.drawing_widget, pos_x, pos_y, pos_x + width - 1, pos_y)
			else
				renderer.draw_line(parent, pos_x, pos_y, pos_x + width - 1, pos_y, state)
			end
		end

end -- class HORIZONTAL_LINE
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
