-- See the Copyright notice at the end of this file.
--
deferred class ALIGNABLE
	-- Helper class to write alignable variant. See for example LABEL_ALIGNABLE
	-- Alignment is useful only if x_expand_allowed or y_expand_allowed
	-- is `True' and the space given to the object is bigger than it's
	-- standard size.

inherit
	WIDGET

feature {ANY}
	alignment: ALIGNMENT

	set_alignment (a: ALIGNMENT) is
			-- x_expand_allowed and y_expand_allowed are set to `True' if
			-- previous alignment was `Void'
		do
			if alignment = Void then
				set_x_expand(True)
				set_y_expand(True)
			end
			alignment := a
		end

feature {}
	adjust_pos is
		do
			if alignment /= Void then
				pos_x := pos_x + ((width - std_width) * alignment.x).force_to_integer_32
				pos_y := pos_y + ((height - std_height) * alignment.y).force_to_integer_32
			end
		end

end -- class ALIGNABLE
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
