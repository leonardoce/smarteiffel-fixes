-- See the Copyright notice at the end of this file.
--
class LABEL
	-- `LABEL' is a widget to draw (unicode) string.
	-- See class `TEXT_FIELD' if you want to allow the user to edit
	-- the string.

inherit
	WIDGET
	DRAWABLE_HANDLER
	GRAPHIC
		--TODO: remove ?

creation {ANY}
	make

feature {}
	make (s: UNICODE_STRING) is
			-- `s' is copied, future changes by the caller has no influence.
			-- Local copy is accessible as READ ONLY through `text'
		require
			s /= Void
		do
			create text.make(0)
			text.copy(s)
			font := renderer.font(state)
			std_width := font.text_width(s)
			std_height := font.height
			--min_width := 0
			min_height := std_height
			std_width := std_width.max(1)
			width := std_width
			height := std_height
		end

feature {ANY}
	width, height: INTEGER

	min_width: INTEGER

	min_height: INTEGER

	std_width: INTEGER

	std_height: INTEGER

	font: BASIC_FONT

	style: DRAW_STYLE

	text: UNICODE_STRING
			-- This access to the current text value for this label is READ ONLY.
			-- You have to never change this text, the label need it to
			-- refresh the screen.

	set_min_width (w: INTEGER) is
			-- Allow to chose the width your label may be shrinked to.
			-- Don't forget to set `x_shrink_allowed' to this label and
			-- upper containers.
			-- `set_text' may change this value if needed.
		require
			w <= std_width
		do
			if w /= min_width then
				min_width := w
				if parent /= Void then
					parent.child_requisition_changed
				end
			end
		end

	set_min_height (h: INTEGER) is
			-- Allow to chose the height your label may be shrinked to.
			-- Don't forget to set `y_shrink_allowed' to this label and
			-- upper containers.
		require
			h <= std_height
		do
			if h /= min_height then
				min_height := h
				if parent /= Void then
					parent.child_requisition_changed
				end
			end
		end

	set_text (s: UNICODE_STRING) is
			-- Allow to change the label's text.
			-- `s' is copied, future changes by the caller has no influence.
			-- Local copy is accessible as READ ONLY through `text'
		require
			s /= Void
			private_copy_is_read_only: not text_changed_from_outside(s)
		local
			old_width: INTEGER
		do
			old_width := width
			text.copy(s)
			std_width := font.text_width(s).max(1)
			if min_width /= 0 and then min_width > std_width then
				min_width := std_width
			end
			width := std_width
			if parent /= Void then
				if width /= old_width then
					parent.clear_area(pos_x, pos_y, old_width, height)
				end
				parent.child_requisition_changed
			end
		ensure
			private_copy_is_read_only: not text_changed_from_outside(text)
		end

	set_with_integer (i: INTEGER_64) is
			-- Set the label's text with value of `i' (as decimal).
		require
			private_copy_is_read_only: not text_changed_from_outside(once U"")
		do
			text.clear_count
			append_integer(i)
		ensure
			private_copy_is_read_only: not text_changed_from_outside(text)
		end

	append_integer (i: INTEGER_64) is
			-- Append the value of `i' (as decimal) to the label's text.
		require
			private_copy_is_read_only: not text_changed_from_outside(i.to_unicode_string)
		local
			old_width: INTEGER
		do
			old_width := width
			i.append_in_unicode(text)
			std_width := font.text_width(text)
			if min_width /= 0 and then min_width > std_width then
				min_width := std_width
			end
			width := std_width
			if parent /= Void then
				if width /= old_width then
					parent.clear_area(pos_x, pos_y, old_width, height)
				end
				parent.child_requisition_changed
			end
		ensure
			private_copy_is_read_only: not text_changed_from_outside(text)
		end

	set_style (s: DRAW_STYLE) is
			-- Change the style used to draw the line.
			-- NOTE: The screen is not updated. --TODO: change this ?
		do
			style := s
		ensure
			style = s
		end

	reset_default_style is
			-- The renderer will be used to draw the line.
			-- NOTE: The screen is not updated. --TODO: change this ?
		do
			style := Void
		end

	text_changed_from_outside (s: UNICODE_STRING): BOOLEAN is
			-- This function check that text didn't change.
			-- This function has not to be used externally, it's only
			-- used for assertion.
			-- `s' is the future value for the text.
		do
			if internal_text_copy = Void then
				create internal_text_copy.make(0)
				internal_text_copy.copy(s)
			else
				Result := not text.is_equal(internal_text_copy)
				if text /= s then
					internal_text_copy.copy(s)
				end
			end
		end

feature {LAYOUT}
	expose_paint is
		require
			private_copy_is_read_only: not text_changed_from_outside(text)
		do
			if style /= Void then
				--TODO: handle font
				style.draw_string(parent.drawing_widget, text, pos_x, pos_y)
			else
				renderer.draw_string(parent, text, pos_x, pos_y, state)
			end
		end

	set_geometry (x, y, w, h: INTEGER) is
		do
			pos_x := x
			pos_y := y
			width := w
			height := h
		end

feature {}
	internal_text_copy: UNICODE_STRING
			-- This attribute is only used if compiled with check (i.e. all
			-- mode except -boost and -no_check)

	resize (w, h: INTEGER) is
		do
			width := w
			height := h
		end

invariant
	text /= Void
	private_copy_is_read_only: not text_changed_from_outside(text)

end -- class LABEL
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
