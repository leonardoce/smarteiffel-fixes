-- See the Copyright notice at the end of this file.
--
class DRAW_STYLE
	-- DRAW_STYLE is low-level service who describe drawing attributes
	-- (color, line width, font...)

inherit
	GRAPHIC
		--TODO: remove? (needed for default_font)
		redefine default_create
		end
	UNICODE_STRING_HANDLER
		redefine default_create
		end
	DRAWABLE_HANDLER
		redefine default_create
		end
	DISPOSABLE
		redefine default_create
		end

creation {ANY}
	default_create

feature {}
	default_create is
		do
			style := basic_style_new
			font := default_font
			basic_style_set_font(style, font.font)
		end

feature {ANY}
	color: COLOR is
		do
			--TODO: store on eiffel side or get values from C side ?
			not_yet_implemented
		end

	set_color (c: COLOR) is
		require
			c /= Void
		do
			basic_style_set_color(style, c.storage)
		end

	line_width: INTEGER is
		do
			--TODO: store on eiffel side or get values from C side ?
			not_yet_implemented
		end

	set_line_width (w: INTEGER) is
		require
			w > 0
		do
			basic_style_set_line_width(style, w)
		end

feature {RECTANGLE}
	draw_rectangle (d: POINTER; x1, y1, w, h: INTEGER) is
		require
			w > 0
			h > 0
			d /= default_pointer
		do
			basic_draw_rectangle(d, style, x1, y1, w, h)
		end

feature {FILL_RECTANGLE}
	draw_fill_rectangle (d: POINTER; x1, y1, w, h: INTEGER) is
		require
			w > 0
			h > 0
			d /= default_pointer
		do
			basic_draw_fill_rectangle(d, style, x1, y1, w, h)
		end

feature {LINE}
	draw_line (d: POINTER; x1, y1, x2, y2: INTEGER) is
		require
			d /= default_pointer
		do
			basic_draw_line(d, style, x1, y1, x2, y2)
		end

feature {LABEL}
	draw_string (d: POINTER; s: UNICODE_STRING; x, y: INTEGER) is
		require
			s /= Void
			d /= default_pointer
		do
			--TODO: surrogate characters !
			--TODO: combining characters !
			basic_draw_text(d, style, x, y, font.base_line, s.storage.to_external, s.count)
		end

feature {}
	draw_point (d: POINTER; x1, y1: INTEGER) is
		require
			d /= default_pointer
		do
			basic_draw_point(d, style, x1, y1)
		end

	draw_arc (d: POINTER; x1, y1, w, h: INTEGER; angle1, angle2: REAL) is
			-- arc will be drawn inside the rectangle defined with x1, y1, w, h
		require
			w > 0
			h > 0
			d /= default_pointer
		do
			basic_draw_arc(d, style, x1, y1, w, h, angle1, angle2)
		end

	draw_arc_radius (d: POINTER; x, y, r1, r2: INTEGER; angle1, angle2: REAL) is
			-- arc will be drawn using (x, y) as center and r1/r2 as
			-- horizontal/vertical radius
		require
			r1 > 0
			r2 > 0
			d /= default_pointer
		do
			basic_draw_arc(d, style, x - r1, y - r2, 2 * r1, 2 * r2, angle1, angle2)
		end

	draw_fill_arc (d: POINTER; x1, y1, w, h: INTEGER; angle1, angle2: REAL) is
			-- arc will be drawn inside the rectangle defined with x1, y1, w, h
		require
			w > 0
			h > 0
			d /= default_pointer
		do
			basic_draw_fill_arc(d, style, x1, y1, w, h, angle1, angle2)
		end

	draw_fill_arc_radius (d: POINTER; x, y, r1, r2: INTEGER; angle1, angle2: REAL) is
			-- arc will be drawn using (x, y) as center and r1/r2 as
			-- horizontal/vertical radius
		require
			r1 > 0
			r2 > 0
			d /= default_pointer
		do
			basic_draw_arc(d, style, x - r1, y - r2, 2 * r1, 2 * r2, angle1, angle2)
		end

feature {}
	style: POINTER

	font: BASIC_FONT

	dispose is
		do
			basic_style_free(style)
		end

	basic_style_new: POINTER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "style"
	 feature_name: "basic_style_new"
	 }"
		end

	basic_style_set_color (sty: POINTER; color_: POINTER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "style"
	 feature_name: "basic_style_set_color"
	 }"
		end

	basic_style_set_line_width (sty: POINTER; w: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "style"
	 feature_name: "basic_style_set_line_width"
	 }"
		end

	basic_style_set_font (sty, f: POINTER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "style"
	 feature_name: "basic_style_set_font"
	 }"
		end

	basic_style_free (sty: POINTER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "style"
	 feature_name: "basic_style_free"
	 }"
		end

	basic_draw_point (draw, sty: POINTER; x1, y1: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "draw"
	 feature_name: "basic_draw_point"
	 }"
		end

	basic_draw_line (draw, sty: POINTER; x1, y1, x2, y2: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "draw"
	 feature_name: "basic_draw_line"
	 }"
		end

	basic_draw_rectangle (draw, sty: POINTER; x1, y1, w, h: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "draw"
	 feature_name: "basic_draw_rectangle"
	 }"
		end

	basic_draw_arc (draw, sty: POINTER; x1, y1, w, h: INTEGER; a1, a2: REAL) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "draw"
	 feature_name: "basic_draw_arc"
	 }"
		end

	basic_draw_fill_rectangle (draw, sty: POINTER; x1, y1, w, h: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "draw"
	 feature_name: "basic_draw_fill_rectangle"
	 }"
		end

	basic_draw_fill_arc (draw, sty: POINTER; x1, y1, w, h: INTEGER; a1, a2: REAL) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "draw"
	 feature_name: "basic_draw_fill_arc"
	 }"
		end

	basic_draw_text (draw, sty: POINTER; x, y, bl: INTEGER; text: POINTER; length: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "draw"
	 feature_name: "basic_draw_text"
	 }"
		end

end -- class DRAW_STYLE
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
