-- See the Copyright notice at the end of this file.
--
class CLASSIC_RENDERER

inherit
	RENDERER
		redefine default_create
		end
	GRAPHIC
		redefine default_create
		end

creation {ANY}
	default_create

feature {} --TODO: avoir un pen pour chaque couleur... de type draw_style
	pen: DRAW_KIT

	pixmap: PIXMAP

	default_create is
		do
			create pen
			create pixmap.make(1, 1)
		end

	draw_bevel (d: DRAWABLE; x1, y1, x2, y2, inset, outset: INTEGER) is
		do
			pen.set_drawable(d)
			pen.set_color(color_gradient.item(outset))
			pen.line(x1, y1, x2, y1)
			pen.line(x1, y1, x1, y2)
			pen.set_color(color_gradient.item(inset))
			pen.line(x1, y2, x2, y2)
			pen.line(x2, y1, x2, y2)
		end

	draw_box (d: DRAWABLE; x, y, width, height: INTEGER; intensity: INTEGER) is
		do
			pen.set_drawable(d)
			pen.set_color(color_gradient.item(intensity))
			pen.fill_rectangle(x, y, width, height)
		end

feature {ANY}
	draw_line (d: DRAWABLE; x1, y1, x2, y2, state: INTEGER) is
		do
			pen.set_drawable(d)
			if state = state_insensitive then
				pen.set_color(color_gradient.item(12))
			else
				pen.set_color(color_gradient.item(0))
			end
			pen.line(x1, y1, x2, y2)
		end

	draw_rectangle (d: DRAWABLE; x, y, width, height, state: INTEGER) is
		do
			pen.set_drawable(d)
			inspect
				state
			when state_insensitive then
				pen.set_color(color_gradient.item(11))
			when state_normal then
				pen.set_color(color_gradient.item(12))
			else
				pen.set_color(color_gradient.item(13))
			end
			pen.rectangle(x, y, width, height)
		end

	draw_fill_rectangle (d: DRAWABLE; x, y, width, height, state: INTEGER) is
		do
			pen.set_drawable(d)
			inspect
				state
			when state_insensitive then
				pen.set_color(color_gradient.item(11))
			when state_normal then
				pen.set_color(color_gradient.item(12))
			else
				pen.set_color(color_gradient.item(13))
			end
			pen.fill_rectangle(x, y, width, height)
		end

	draw_button (b: BUTTON) is
		local
			x, y, w, h: INTEGER
		do
			pixmap.make(b.width, b.height)
			x := 0
			y := 0
			w := b.width
			h := b.height
			if w > 2 then
				x := 1
				w := w - 2
			end
			if h > 2 then
				y := 1
				h := h - 2
			end
			inspect
				b.state
			when state_active then
				draw_box(pixmap, x, y, w, h, 11)
				draw_bevel(pixmap, 0, 0, b.width - 1, b.height - 1, 14, 4)
				draw_bevel(pixmap, 1, 1, b.width - 2, b.height - 2, 14, 4)
			when state_normal then
				draw_box(pixmap, x, y, w, h, 12)
				draw_bevel(pixmap, 0, 0, b.width - 1, b.height - 1, 4, 14)
				draw_bevel(pixmap, 1, 1, b.width - 2, b.height - 2, 4, 14)
			when state_prelight then
				draw_box(pixmap, x, y, w, h, 13)
				draw_bevel(pixmap, 0, 0, b.width - 1, b.height - 1, 4, 14)
				draw_bevel(pixmap, 1, 1, b.width - 2, b.height - 2, 4, 14)
			end
			b.set_background_pixmap(pixmap)
		end

	draw_check_button (b: CHECK_SPACE) is
		do
			inspect
				b.parent.state
			when state_active then
				draw_check(b.parent, b.pos_x, b.pos_y, b.width, b.height, True)
			when state_normal then
				draw_check(b.parent, b.pos_x, b.pos_y, b.width, b.height, False)
			end
		end

	draw_check (d: DRAWABLE; x, y, width, height: INTEGER; active: BOOLEAN) is
		local
			w, h: INTEGER
		do
			pen.set_drawable(d)
			pen.set_color(black_color)
			pen.rectangle(x, y, width, height)
			if active then
				pen.set_line_width(2)
				w := x + width - 1
				h := y + height - 1
				pen.line(x, y, w, h)
				pen.line(x, h, w, y)
				pen.set_line_width(1)
			end
		end

	draw_string (d: DRAWABLE; s: UNICODE_STRING; x, y, state: INTEGER) is
		do
			pen.set_drawable(d)
			if state = state_insensitive then
				pen.set_color(color_gradient.item(12))
			else
				pen.set_color(color_gradient.item(0))
			end
			pen.put_string(s, x, y)
		end

	font (state: INTEGER): BASIC_FONT is
		do
			Result := default_font
		end --TODO: feature {}

	color_gradient: FAST_ARRAY[COLOR] is
			-- 17 levels from black(0) to white(16)
		local
			i: INTEGER; color: COLOR
		once
			create Result.make(17)
			from
				i := Result.upper
				create color.like_rgb_8(255, 255, 255)
				Result.put(color, i)
				i := i - 1
			until
				i < Result.lower
			loop
				create color.like_rgb_8(i * 16, i * 16, i * 16)
				Result.put(color, i)
				i := i - 1
			end
		end

end -- class CLASSIC_RENDERER
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN,
--          Pierre-Nicolas CLAUSS
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
