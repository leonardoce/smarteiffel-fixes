-- See the Copyright notice at the end of this file.
--
class BASIC_RENDERER
	-- This renderer is designed for very simple and high speed rendering.
	-- It may be useful is the display goes through low bandwidth or high
	-- latency network.

inherit
	RENDERER
		redefine default_create
		end
	GRAPHIC
		redefine default_create
		end

creation {ANY}
	default_create

feature {} --avoir un pen pour chaque couleur... de type draw_style
	pen: DRAW_KIT

	pixmap: PIXMAP

	default_create is
		do
			create pen
			create pixmap.make(8, 8)
		end

	draw_box (d: DRAWABLE; x, y, width, height: INTEGER; intensity: INTEGER) is
		do
			pen.set_drawable(d)
			pen.set_color(color_gradient.item(intensity))
			pen.fill_rectangle(x, y, width, height)
		end

feature {ANY}
	draw_line (d: DRAWABLE; x1, y1, x2, y2, state: INTEGER) is
		do
			pen.set_drawable(d)
			if state = state_insensitive then
				pen.set_color(color_gradient.item(14))
			else
				pen.set_color(color_gradient.item(0))
			end
			pen.line(x1, y1, x2, y2)
		end

	draw_rectangle (d: DRAWABLE; x, y, width, height, state: INTEGER) is
		do
			pen.set_drawable(d)
			pen.set_color(box_color_for_state(state))
			pen.rectangle(x, y, width, height)
		end

	draw_fill_rectangle (d: DRAWABLE; x, y, width, height, state: INTEGER) is
		do
			pen.set_drawable(d)
			pen.set_color(box_color_for_state(state))
			pen.fill_rectangle(x, y, width, height)
		end

	draw_button (b: BUTTON) is
		do
			b.set_background_color(box_color_for_state(b.state))
			-- 	 inspect b.state
			-- 	 when state_active then draw_box(pixmap, 0, 0, 8, 8, 12)
			-- 	 when state_normal then draw_box(pixmap, 0, 0, 8, 8, 14)
			-- 	 when state_prelight then draw_box(pixmap, 0, 0, 8, 8, 15)
			-- 	 end
			-- 	 b.set_background_pixmap(pixmap)
		end

	draw_string (d: DRAWABLE; s: UNICODE_STRING; x, y, state: INTEGER) is
		do
			pen.set_drawable(d)
			if state = state_insensitive then
				pen.set_color(color_gradient.item(14))
			else
				pen.set_color(color_gradient.item(0))
			end
			pen.put_string(s, x, y)
		end

	draw_check_button (b: CHECK_SPACE) is
		do
			not_yet_implemented
			-- TODO
		end

	font (state: INTEGER): BASIC_FONT is
		do
			Result := default_font
		end

feature {}
	box_color_for_state (state: INTEGER): COLOR is
			-- What color should be used to draw a box (not a string!) in a given `state'
		local
			index: INTEGER
		do
			inspect
				state
			when state_active then
				index := 12
			when state_normal then
				index := 14
			when state_prelight then
				index := 15
			end
			Result := color_gradient.item(index)
		end

	color_gradient: FAST_ARRAY[COLOR] is
			-- 17 levels from black(0) to white(16)
		local
			i: INTEGER; color: COLOR
		once
			create Result.make(17)
			from
				i := Result.upper
				create color.like_rgb_8(255, 255, 255)
				Result.put(color, i)
				i := i - 1
			until
				i < Result.lower
			loop
				create color.like_rgb_8(i * 16, i * 16, i * 16)
				Result.put(color, i)
				i := i - 1
			end
		end

end -- class BASIC_RENDERER
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
