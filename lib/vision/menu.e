-- See the Copyright notice at the end of this file.
--
class MENU

insert
	COLOR_LIST

creation {ANY}
	make

feature {}
	make is
		do
			create actions.with_capacity(4)
			create elements.with_capacity(4)
			if shared_list_window.is_empty then
				create list_window
				list_window.set_background_color(grey_color)
			else
				list_window := shared_list_window.last
				shared_list_window.remove_last
			end
			activate_proc := agent run_action
		end

feature {ANY}
	open (x, y: INTEGER) is
		do
			list_window.open(elements, -1, activate_proc, x, y)
		end

	add_text_entry (text: UNICODE_STRING; action: PROCEDURE[TUPLE[INTEGER, INTEGER]]) is
		require
			text /= Void
			action /= Void
		local
			label: LABEL
		do
			create label.make(text)
			actions.add_last(action)
			elements.add_last(label)
		end

	add_entry (w: WIDGET; action: PROCEDURE[TUPLE[INTEGER, INTEGER]]) is
		require
			w /= Void
		do
			elements.add_last(w)
			actions.add_last(action)
		end

	add_sub_menu_enty (w: WIDGET; menu: MENU) is
		do
			not_yet_implemented
		end

	add_space is
		do
			elements.add_last(create {HORIZONTAL_LINE})
			actions.add_last(Void)
		end

feature {}
	elements: FAST_ARRAY[WIDGET]

	actions: FAST_ARRAY[PROCEDURE[TUPLE[INTEGER, INTEGER]]]

	activate_proc: PROCEDURE[TUPLE[INTEGER]]

	list_window: LIST_WINDOW

	shared_list_window: FAST_ARRAY[LIST_WINDOW] is
		once
			create Result.with_capacity(4)
		end

	run_action (i: INTEGER) is
		local
			x, y: INTEGER
		do
			if i >= 0 and then actions.item(i) /= Void then
				x := list_window.as_x_root(list_window.width - 1) + 1
				y := list_window.as_y_root(elements.item(i).pos_y)
				actions.item(i).call([x, y])
			end
		end

invariant
	actions.count = elements.count

end -- class MENU
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
