-- See the Copyright notice at the end of this file.
--
class BUTTON

inherit
	GRAPHIC
		undefine default_create
		end
	SUB_WINDOW
		undefine set_state
		redefine make, update_decoration, layout
		end
	WHEN_DOUBLE_CLICKED
		undefine default_create, set_state
		end

insert
	WHEN_MIDDLE_CLICKED
		undefine default_create
		end
	WHEN_RIGHT_CLICKED
		undefine default_create
		end

creation {ANY}
	make, with_label

feature {}
	make (p: like parent) is
		do
			Precursor(p)
			when_double_clicked_init
			when_middle_clicked_init
			when_right_clicked_init
			set_state_normal
			map
		end

	with_label (p: like parent; text: UNICODE_STRING) is
		local
			tmp: LABEL
		do
			make(p)
			create tmp.make(text)
			--tmp.set_alignment(center_alignment)
			child_attach(tmp)
			layout.set_border(5)
			layout.set_spacing(5)
		end

feature {ANY}
	layout: ROW_LAYOUT

	set_state (n: INTEGER) is
		do
			if state /= n then
				state := n
				renderer.draw_button(Current)
				clear_area(0, 0, width, height)
			end
		end

	update_decoration is
		do
			renderer.draw_button(Current)
		end
		-- border: BORDER
		--   set_border(b: like border) is
		--      do
		--         if border /= Void then
		--            layer_remove_child(1, border)
		--         end
		--         border := b
		--         if b /= Void then
		--            layer_add_child(1, b)
		--         end
		--      end

end -- class BUTTON
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
