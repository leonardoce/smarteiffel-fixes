-- See the Copyright notice at the end of this file.
--
class REPEAT_JOB

inherit
	GRAPHIC
	PERIODIC_JOB

creation {WHEN_LEFT_CLICKED}
	make

feature {}
	make is
		do
			period := 0.05
		end

feature {WHEN_LEFT_CLICKED}
	start (target: like clickable) is
		require
			target /= Void
		do
			done := False
			clickable := target
			next_time.update
			next_time := next_time + 0.8
			period := 0.25
			priority := 1
			vision.loop_stack.add_job(Current)
		end

	stop is
		do
			done := True
		end

feature {LOOP_ITEM}
	continue is
		do
			if clickable.left_is_down then
				clickable.left_click_signal.emit
				if period > 0.05 then
					period := period * 0.8
				end
			end
		end

	restart is
		do
			--TODO: check loop_stack status?
			done := False
			next_time.update
			next_time := next_time + 0.8
			period := 0.25
		end

	done: BOOLEAN

feature {}
	clickable: WHEN_LEFT_CLICKED

end -- class REPEAT_JOB
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
