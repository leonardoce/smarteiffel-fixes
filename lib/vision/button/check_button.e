-- See the Copyright notice at the end of this file.
--
class CHECK_BUTTON

inherit
	GRAPHIC
		undefine default_create
		end
	SUB_WINDOW
		redefine make, set_state, update_decoration, layout
		end

creation {ANY}
	make, with_label

feature {}
	make (p: like parent) is
		do
			Precursor(p)
			set_background_color(white_color)
			child_attach(create {CHECK_SPACE})
			set_state_normal
			when_left_down(agent switch_state)
			map
		end

	with_label (p: like parent; text: UNICODE_STRING) is
		local
			tmp: LABEL
		do
			make(p)
			create tmp.make(text)
			--tmp.set_alignment(center_alignment)
			child_attach(tmp)
			layout.set_border(5)
			layout.set_spacing(5)
		end

feature {ANY}
	layout: ROW_LAYOUT

	selected: BOOLEAN is
		do
			Result := is_state_active
		end

	switch_state is
		do
			if is_state_normal then
				set_state_active
			elseif is_state_active then
				set_state_normal
			end
		end

	set_state (n: INTEGER) is
		local
			w: WIDGET
		do
			if state /= n then
				state := n
				w := child.first
				clear_area(w.pos_x, w.pos_y, w.width, w.height)
				if when_value_change_signal /= Void then
					when_value_change_signal.emit(selected)
				end
			end
		end

	update_decoration is
		local
			w: WIDGET
		do
			w := child.first
			clear_area(w.pos_x, w.pos_y, w.width, w.height)
		end

	when_value_change_signal: SIGNAL_1[BOOLEAN]

	when_value_change (p: PROCEDURE[TUPLE[BOOLEAN]]) is
		do
			if when_value_change_signal = Void then
				create when_value_change_signal.make
			end
			when_value_change_signal.connect(p)
		end

end -- class CHECK_BUTTON
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
