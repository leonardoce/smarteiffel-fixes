-- See the Copyright notice at the end of this file.
--
class TEXT_CURSOR_JOB
	-- Draw blinking cursor in text area.

inherit
	PERIODIC_JOB
		redefine default_create
		end
	GRAPHIC
		redefine default_create
		end

creation {ANY}
	default_create

feature {}
	default_create is
		do
			create draw_kit
			priority := 100
			period := 0.5
			draw_cursor_proc := agent draw_cursor
		end

feature {ANY}
	start_in (text_field: TEXT_FIELD) is
		do
			check
				window = Void
			end
			window := text_field
			window.when_expose(draw_cursor_proc)
			vision.loop_stack.add_job(Current)
			next_time.update
			next_time := next_time + period
			done := False
			height := text_field.cursor_height
			move
		end

	stop is
		do
			if visible then
				visible := False
				update_cursor
			end
			if window /= Void then
				-- Double stop may occur when if button is down
				window.when_expose_signal.disconnect(draw_cursor_proc)
				window := Void
			end
			done := True
		end

	move is
		do
			if pos_x < window.width and then pos_y + height <= window.height then
				update_cursor
			end
			visible := True
			next_time.update
			next_time := next_time + period
			pos_x := window.cursor_pos_x
			pos_y := window.cursor_pos_y
		end

feature {LOOP_ITEM}
	done: BOOLEAN

	continue is
		do
			if window /= Void and then not done then
				visible := not visible
				update_cursor
			end
		end

	restart is
		do
			done := window /= Void
		end

feature {}
	draw_kit: DRAW_KIT

	window: TEXT_FIELD

	visible: BOOLEAN

	pos_x, pos_y, height: INTEGER

	draw_cursor_proc: PROCEDURE[TUPLE]

	update_cursor is
		do
			window.clear_area(pos_x, pos_y, 1, height)
		end

	draw_cursor is
		do
			check
				window /= Void
			end
			if visible then
				draw_kit.set_drawable(window)
				draw_kit.set_color(black_color)
				draw_kit.line(pos_x, pos_y, pos_x, pos_y + height - 1)
			end
		end

end -- class TEXT_CURSOR_JOB
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
