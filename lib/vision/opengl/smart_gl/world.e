-- See the Copyright notice at the end of this file.
--
class WORLD

inherit
	GL_WIDGET
	WHEN_LEFT_CLICKED
		undefine default_create, hash_code
		end

insert
	OPENGL
		undefine default_create
		end

creation {ANY}
	make

feature {ANY}
	init_gl is
		do
			create objects.with_capacity(1)
			create lights.with_capacity(1)
			select_myself_gl
			gl.shade_model(gl.const_smooth)
			smart_gl.set_clear_color(0.0, 0.0, 0.0, 0.0)
			smart_gl.set_clear_depth(1.0)
			gl.clear(gl.const_color_buffer_bit + gl.const_depth_buffer_bit)
			gl.depth_func(gl.const_lequal)
			gl.enable(gl.const_depth_test)
			gl.cull_face(gl.const_back)
			gl.front_face(gl.const_ccw)
			gl.hint(gl.const_perspective_correction_hint, gl.const_fastest)
			gl.blend_func(gl.const_src_alpha, gl.const_one_minus_src_alpha)
			when_left_clicked_init
		end

	redraw_gl is
		local
			i: INTEGER
		do
			select_myself_gl
			start_new_frame
			from
				i := lights.lower
			until
				i > lights.upper
			loop
				lights.item(i).render
				i := i + 1
			end
			from
				i := objects.lower
			until
				i > objects.upper
			loop
				objects.item(i).render
				i := i + 1
			end
		end

	resize_gl (w, h: INTEGER) is
		local
			ratio: REAL_64
		do
			select_myself_gl
			gl.viewport(0, 0, w, h)
			gl.matrix_mode(gl.const_projection)
			gl.load_identity
			ratio := w.to_real_64 / h.to_real_64
			glu.perspective(45, ratio, 0.1, 100)
			gl.matrix_mode(gl.const_modelview)
		end

	add_opaque_object (object: GL_OBJECT) is
		require
			not object.is_translucent
		do
			objects.add_first(object)
			object.set_parent(Current)
		end

	add_translucent_object (object: GL_OBJECT) is
		require
			object.is_translucent
		do
			objects.add_last(object)
			object.set_parent(Current)
		end

	camera: CAMERA

	set_camera (cam: like camera) is
		require
			cam /= Void
		do
			camera := cam
			camera.set_parent(Current)
		ensure
			camera = cam
		end

	add_light (light: LIGHT) is
		do
			lights.add_last(light)
			gl.enable(gl.const_lighting)
		end

	fog: FOG

	set_fog (f: like fog; atmospheric: BOOLEAN) is
		require
			f /= Void
		do
			fog := f
			gl.hint(gl.const_fog_hint, gl.const_fastest)
			fog.render
			gl.enable(gl.const_fog)
			if atmospheric then
				smart_gl.set_clear_color(fog.color.red, fog.color.green, fog.color.blue, fog.color.alpha)
			end
		ensure
			fog = f
		end

	get_object (name: STRING): GL_OBJECT is
		require
			name /= Void
		local
			i: INTEGER
		do
			
		end

feature {}
	start_new_frame is
		do
			gl.clear(gl.const_color_buffer_bit + gl.const_depth_buffer_bit)
			gl.load_identity
			if camera /= Void then
				camera.render
			end
		end

	objects: FAST_ARRAY[GL_OBJECT]

	lights: FAST_ARRAY[LIGHT]

end -- class WORLD
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
