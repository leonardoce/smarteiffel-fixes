-- See the Copyright notice at the end of this file.
--
class VERTEX

insert
	OPENGL

creation {ANY}
	make, make_with_color, make_with_color_object

feature {ANY}
	make (cx: like x; cy: like y; cz: like z) is
		do
			x := cx
			y := cy
			z := cz
		ensure
			x = cx
			y = cy
			z = cz
		end

	make_with_color (cx: like x; cy: like y; cz: like z; cr, cg, cb, ca: REAL) is
		local
			co: like color
		do
			create co.set_color(cr, cg, cb, ca)
			make_with_color_object(cx, cy, cz, co)
		end

	make_with_color_object (cx: like x; cy: like y; cz: like z; co: like color) is
		do
			make(cx, cy, cz)
			set_color(co)
		end

	set_color (co: like color) is
		require
			co /= Void
		do
			color := co
		ensure
			color = co
		end

	set_normal (cx: like nx; cy: like ny; cz: like nz) is
		local
			length: REAL
		do
			length := (cx * cx + cy * cy + cz * cz).sqrt
			nx := cx / length
			ny := cy / length
			nz := cz / length
			has_normal := True
		ensure
			has_normal = True
		end

	set_1d_texture_coord (cs: like s) is
		do
			s := cs
			--*** Use once to save memory
			texture_renderer := agent render_1d_texture
		ensure
			s = cs
		end

	set_2d_texture_coord (cs: like s; ct: like t) is
		do
			s := cs
			t := ct
			texture_renderer := agent render_2d_texture
		ensure
			s = cs
			t = ct
		end

	set_3d_texture_coord (cs: like s; ct: like t; cr: like r) is
		do
			s := cs
			t := ct
			r := cr
			texture_renderer := agent render_3d_texture
		ensure
			s = cs
			t = ct
			r = cr
		end

	set_4d_texture_coord (cs: like s; ct: like t; cr: like r; cq: like q) is
		do
			s := cs
			t := ct
			r := cr
			q := cq
			texture_renderer := agent render_4d_texture
		ensure
			s = cs
			t = ct
			r = cr
			q = cq
		end

	x, y, z, nx, ny, nz: REAL

	s, t, r, q: REAL

	color: GL_COLOR

	is_translucent: BOOLEAN is
		do
			Result := color /= Void and then color.alpha < 1
		end

	render is
		do
			if color /= Void then
				color.render
			end
			if texture_renderer /= Void then
				texture_renderer.call([])
			end
			if has_normal then
				smart_gl.normal(nx, ny, nz)
			end
			smart_gl.vertex(x, y, z)
		end

	has_normal: BOOLEAN

feature {}
	render_1d_texture is
		do
			gl.tex_coord1d(s)
		end

	render_2d_texture is
		do
			gl.tex_coord2d(s, t)
		end

	render_3d_texture is
		do
			gl.tex_coord3d(s, t, r)
		end

	render_4d_texture is
		do
			gl.tex_coord4d(s, t, r, q)
		end

	texture_renderer: PROCEDURE[TUPLE]

end -- class VERTEX
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
