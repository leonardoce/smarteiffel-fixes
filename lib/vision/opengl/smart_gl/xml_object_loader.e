-- See the Copyright notice at the end of this file.
--
class XML_OBJECT_LOADER[E_ -> LOADABLE_GL_OBJECT]

inherit
	OBJECT_LOADER[E_]

creation {ANY}
	make

feature {ANY}
	load_from_file (complete_file_name: STRING): E_ is
		require
			universal_texture_loader /= Void
		local
			tfr: TEXT_FILE_READ
			tree: XML_TREE
		do
			loaded_file := complete_file_name
			create tfr.connect_to(loaded_file)
			create object.make_at_origin
			create tree.make(tfr.url)
			parse(tree.root)
			Result := object
		end

	find_child (node: XML_COMPOSITE_NODE; name: UNICODE_STRING): XML_COMPOSITE_NODE is
		require
			node /= Void
			name /= Void
		local
			i: INTEGER
		do
			from
				i := 1
			until
				Result /= Void or else i > node.children_count
			loop
				if Result ?:= node.child(i) then
					Result ::= node.child(i)
					if not Result.name.is_equal(name) then
						Result := Void
					end
				end
				i := i + 1
			end
		end

	parse (node: XML_COMPOSITE_NODE) is
		require
			node /= Void and then node.name.is_equal(once U"object")
		local
			i: INTEGER; n: XML_COMPOSITE_NODE
		do
			from
				i := 1
			until
				i > node.children_count
			loop
				if n ?:= node.child(i) then
					n ::= node.child(i)
					if n.name.is_equal(once U"facet") then
						parse_facet(n)
					else
						parse_error
						i := node.children_count
					end
				else
					parse_error
					i := node.children_count
				end
				i := i + 1
			end
		end

	parse_facet (node: XML_COMPOSITE_NODE) is
		require
			node /= Void and then node.name.is_equal(once U"facet")
		local
			facet: FACET
			i: INTEGER
			vertex1, vertex2, vertex3: VERTEX
			n, texture: XML_COMPOSITE_NODE
			file, function: STRING
			coord: INTEGER
			s, t: STRING
		do
			from
				i := 1
			until
				i > node.children_count
			loop
				if n ?:= node.child(i) then
					n ::= node.child(i)
					if n.name.is_equal(once U"vertex") then
						if vertex1 = Void then
							vertex1 := parse_vertex(n)
						elseif vertex2 = Void then
							vertex2 := parse_vertex(n)
						elseif vertex3 = Void then
							vertex3 := parse_vertex(n)
						else
							parse_error
							i := node.children_count
						end
					else
						parse_error
						i := node.children_count
					end
				end
				i := i + 1
			end
			if vertex1 = Void or else vertex2 = Void or else vertex3 = Void then
				parse_error
			else
				create facet.make(vertex1, vertex2, vertex3)
				if facet.is_translucent then
					object.add_translucent_facet(facet)
				else
					object.add_opaque_facet(facet)
				end
				texture := find_child(node, once U"texture")
				if texture /= Void then
					file := texture.attribute_at(once U"file").to_string
					if file = Void then
						parse_error
					else
						facet.set_texture(universal_texture_loader.load_texture(file))
						from
							coord := 0
							i := 1
						until
							i > texture.children_count
						loop
							if n ?:= texture.child(i) then
								n ::= texture.child(i)
								if n.name.is_equal(once U"coord") then
									s := n.attribute_at(once U"s").to_string
									t := n.attribute_at(once U"t").to_string
									if s /= Void and then t /= Void then
										if coord = 0 then
											vertex1.set_2d_texture_coord(s.to_real, t.to_real)
										elseif coord = 1 then
											vertex2.set_2d_texture_coord(s.to_real, t.to_real)
										elseif coord = 2 then
											vertex3.set_2d_texture_coord(s.to_real, t.to_real)
										end
										coord := coord + 1
									end
								end
							end
							i := i + 1
						end
						function := texture.attribute_at(once U"function").to_string
						if function /= Void then
							inspect
								function
							when "blend" then
								facet.set_texture_composition_function(gl.const_blend)
							when "modulate" then
								facet.set_texture_composition_function(gl.const_modulate)
							when "replace" then
								facet.set_texture_composition_function(gl.const_replace)
							when "decal" then
								facet.set_texture_composition_function(gl.const_decal)
							end
						end
					end
				end
			end
		end

	parse_vertex (node: XML_COMPOSITE_NODE): VERTEX is
		require
			node /= Void and then node.name.is_equal(once U"vertex")
		local
			x, y, z, a: STRING
			color, normal: XML_COMPOSITE_NODE
		do
			x := node.attribute_at(once U"x").to_string
			y := node.attribute_at(once U"y").to_string
			z := node.attribute_at(once U"z").to_string
			if x /= Void and then y /= Void and then z /= Void then
				create Result.make(x.to_real, y.to_real, z.to_real)
				color := find_child(node, once U"color")
				if color /= Void then
					x := color.attribute_at(once U"r").to_string
					y := color.attribute_at(once U"g").to_string
					z := color.attribute_at(once U"b").to_string
					a := color.attribute_at(once U"a").to_string
					if x /= Void and then y /= Void and then z /= Void and then a /= Void then
						Result.set_color(create {GL_COLOR}.set_color(x.to_real, y.to_real, z.to_real, a.to_real))
					end
				end
				normal := find_child(node, once U"normal")
				if normal /= Void then
					x := normal.attribute_at(once U"x").to_string
					y := normal.attribute_at(once U"y").to_string
					z := normal.attribute_at(once U"z").to_string
					if x /= Void and then y /= Void and then z /= Void then
						Result.set_normal(x.to_real, y.to_real, z.to_real)
					end
				end
			end
		ensure
			Result /= Void
		end

	parse_error is
		do
			std_error.put_string("Error while loading file `")
			std_error.put_string(loaded_file)
			std_error.put_string("'%N")
		end

feature {}
	object: E_

	loaded_file: STRING

end -- class XML_OBJECT_LOADER
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
