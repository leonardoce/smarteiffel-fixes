-- See the Copyright notice at the end of this file.
--
class BMP_IMG

inherit
	IMG

creation {ANY}
	load_from_file

feature {ANY}
	load_from_file (file_name: STRING) is
		local
			dummy, offset, compression: INTEGER; header_size, image_size: INTEGER
		do
			create file.connect_to(file_name)
			dummy := read_integer_16_little_endian
			if dummy = 19778 then
				-- 19778 = 0x4D42 = 'BM'
				dummy := read_integer_32_little_endian -- read over the file_size field
				dummy := read_integer_16_little_endian -- read over the first reserved field
				dummy := read_integer_16_little_endian -- read over the second reserved field
				offset := read_integer_32_little_endian
				-- End of header structure: 14 bytes have been read so far
				header_size := read_integer_32_little_endian
				width := read_integer_32_little_endian
				height := read_integer_32_little_endian
				planes := read_integer_16_little_endian
				bits_per_pixel := read_integer_16_little_endian
				compression := read_integer_32_little_endian
				image_size := read_integer_32_little_endian -- read over the image_size field
				x_resolution := read_integer_32_little_endian
				y_resolution := read_integer_32_little_endian
				dummy := read_integer_32_little_endian -- read over the number_of_colors field
				dummy := read_integer_32_little_endian
				-- read over the important_colors field
				-- End of information structure: 54 bytes should have been read so far
				from
					offset := offset - header_size - 14
				variant
					offset
				until
					offset = 0
				loop
					dummy := read_byte
					offset := offset - 1
				end
				create pixels.make(width * height * 4)
				if image_size = 0 then
					image_size := width * height * (bits_per_pixel // 8)
				end
				inspect
					compression
				when 0 then
					load_without_compression(image_size)
				when 1 then
					load_8_bit_run_length_encoding(image_size)
				when 2 then
					load_4_bit_run_length_encoding(image_size)
				when 3 then
					load_rgb_bitmap_with_mask(image_size)
				end
				is_loaded := True
			end
			file.disconnect
		end

feature {}
	load_without_compression (size: INTEGER) is
		require
			size > 0
		local
			red, green, blue: REAL_32; data_left, i: INTEGER
		do
			from
				data_left := size
				i := pixels.lower
			variant
				data_left
			until
				data_left = 0
			loop
				blue := read_byte.to_real_32 / 255
				green := read_byte.to_real_32 / 255
				red := read_byte.to_real_32 / 255
				pixels.put(red, i)
				i := i + 1
				pixels.put(green, i)
				i := i + 1
				pixels.put(blue, i)
				i := i + 1
				pixels.put(1, i)
				i := i + 1
				data_left := data_left - 3
			end
		end

	load_8_bit_run_length_encoding (size: INTEGER) is
		require
			size > 0
		do
		end

	load_4_bit_run_length_encoding (size: INTEGER) is
		require
			size > 0
		do
		end

	load_rgb_bitmap_with_mask (size: INTEGER) is
		require
			size > 0
		do
		end

end -- class BMP_IMG
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
