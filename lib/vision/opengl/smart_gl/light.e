-- See the Copyright notice at the end of this file.
--
class LIGHT

insert
	OPENGL

creation {ANY}
	make, make_with_colors

feature {ANY}
	make (id: like light_id) is
		do
			create position.make(4)
			create direction.make(3)
			select_light(id)
		end

	make_with_colors (id: like light_id; a: like ambient; d: like diffuse; s: like specular) is
		do
			make(id)
			set_ambient(a)
			set_diffuse(d)
			set_specular(s)
		end

	set_ambient (a: like ambient) is
		require
			a /= Void
		do
			ambient := a
		ensure
			ambient = a
		end

	set_diffuse (d: like diffuse) is
		require
			d /= Void
		do
			diffuse := d
		ensure
			diffuse = d
		end

	set_specular (s: like ambient) is
		require
			s /= Void
		do
			specular := s
		ensure
			specular = s
		end

	light_id: INTEGER

	select_light (id: like light_id) is
		do
			light_id := id
			has_id := True
		ensure
			light_id = id
			has_id = True
		end

	ambient, diffuse, specular: GL_COLOR

	set_directional (x, y, z: REAL) is
		require
			position /= Void and then position.count >= 4
		do
			position.put(-x.force_to_real_32, 0)
			position.put(-y.force_to_real_32, 1)
			position.put(-z.force_to_real_32, 2)
			position.put(0, 3)
		ensure
			is_directional
		end

	set_spot_position (x, y, z: REAL) is
		require
			position /= Void and then position.count >= 4
		do
			position.put(x.force_to_real_32, 0)
			position.put(y.force_to_real_32, 1)
			position.put(z.force_to_real_32, 2)
			position.put(1, 3)
		ensure
			is_spot
		end

	set_spot_direction (x, y, z: REAL) is
		require
			direction /= Void and then direction.count >= 3
			is_spot
		do
			direction.put(x.force_to_real_32, 0)
			direction.put(y.force_to_real_32, 1)
			direction.put(z.force_to_real_32, 2)
		end

	is_directional: BOOLEAN is
		require
			position /= Void and then position.count >= 4
		do
			Result := position.item(3) = 0
		end

	is_spot: BOOLEAN is
		require
			position /= Void and then position.count >= 4
		do
			Result := position.item(3) /= 0
		end

	render is
		require
			has_id
		do
			gl.enable(light_id)
			if is_directional then
				render_directional
			else
				render_spot
			end
		end

	cutoff: REAL is
		require
			is_spot
			has_cutoff
		do
			Result := internal_cutoff.to_real_64
		ensure
			Result.in_range(0, 180)
		end

	set_cutoff (co: like cutoff) is
		require
			is_spot
			co.in_range(0, 180)
		do
			internal_cutoff := co.force_to_real_32
			has_cutoff := True
		ensure
			has_cutoff
		end

	exponent: REAL is
		require
			is_spot
			has_exponent
		do
			Result := internal_exponent.to_real_64
		end

	set_exponent (exp: like exponent) is
		require
			is_spot
		do
			internal_exponent := exp.force_to_real_32
			has_exponent := True
		ensure
			has_exponent
		end

	constant_attenuation: REAL is
		require
			is_spot
			has_constant_attenuation
		do
			Result := internal_constant_attenuation.to_real_64
		end

	set_constant_attenuation (att: like constant_attenuation) is
		require
			is_spot
		do
			internal_constant_attenuation := att.force_to_real_32
			has_constant_attenuation := True
		ensure
			has_constant_attenuation
		end

	linear_attenuation: REAL is
		require
			is_spot
			has_linear_attenuation
		do
			Result := internal_linear_attenuation.to_real_64
		end

	set_linear_attenuation (att: like linear_attenuation) is
		require
			is_spot
		do
			internal_linear_attenuation := att.force_to_real_32
			has_linear_attenuation := True
		ensure
			has_linear_attenuation
		end

	quadratic_attenuation: REAL is
		require
			is_spot
			has_quadratic_attenuation
		do
			Result := internal_quadratic_attenuation.to_real_64
		end

	set_quadratic_attenuation (att: like quadratic_attenuation) is
		require
			is_spot
		do
			internal_quadratic_attenuation := att.force_to_real_32
			has_quadratic_attenuation := True
		ensure
			has_quadratic_attenuation
		end

feature {}
	render_directional is
		require
			is_directional
			has_id
		do
			gl.lightfv(light_id, gl.const_position, position.to_external)
			render_all_colors
		end

	render_spot is
		require
			is_spot
			has_id
		do
			gl.lightfv(light_id, gl.const_position, position.to_external)
			gl.lightfv(light_id, gl.const_spot_direction, direction.to_external)
			if has_cutoff then
				gl.lightf(light_id, gl.const_spot_cutoff, cutoff.force_to_real_32)
			end
			if has_exponent then
				gl.lightf(light_id, gl.const_spot_exponent, exponent.force_to_real_32)
			end
			if has_constant_attenuation then
				gl.lightf(light_id, gl.const_constant_attenuation, constant_attenuation.force_to_real_32)
			end
			if has_linear_attenuation then
				gl.lightf(light_id, gl.const_linear_attenuation, linear_attenuation.force_to_real_32)
			end
			if has_quadratic_attenuation then
				gl.lightf(light_id, gl.const_quadratic_attenuation, quadratic_attenuation.force_to_real_32)
			end
			render_all_colors
		end

	render_all_colors is
		require
			has_id
		do
			if ambient /= Void then
				gl.lightfv(light_id, gl.const_ambient, ambient.color_array.to_external)
			end
			if diffuse /= Void then
				gl.lightfv(light_id, gl.const_diffuse, diffuse.color_array.to_external)
			end
			if specular /= Void then
				gl.lightfv(light_id, gl.const_specular, specular.color_array.to_external)
			end
		end

	position, direction: FAST_ARRAY[REAL_32]

	has_id, has_cutoff, has_exponent, has_constant_attenuation, has_linear_attenuation, has_quadratic_attenuation: BOOLEAN

	internal_cutoff, internal_exponent, internal_constant_attenuation, internal_linear_attenuation, internal_quadratic_attenuation: REAL_32

end -- class LIGHT
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
