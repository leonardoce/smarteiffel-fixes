-- See the Copyright notice at the end of this file.
--
class TEXTURE_2D

insert
	OPENGL

creation {ANY}
	make

feature {ANY}
	make (image: IMG) is
		require
			image /= Void and then image.is_loaded
		local
			dummy: INTEGER
		do
			gl.gen_textures(1, $texture_id)
			gl.bind_texture(gl.const_texture_2d, texture_id)
			gl.tex_parameteri(gl.const_texture_2d, gl.const_texture_mag_filter, gl.const_linear)
			gl.tex_parameteri(gl.const_texture_2d, gl.const_texture_min_filter, gl.const_linear_mipmap_nearest)
			dummy := glu.build_2d_mipmaps(gl.const_texture_2d, gl.const_rgba, image.width, image.height, gl.const_rgba, gl.const_float, image.pixels.to_external)
		ensure
			texture_id > 0
		end

	texture_id: INTEGER

end -- class TEXTURE_2D
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
