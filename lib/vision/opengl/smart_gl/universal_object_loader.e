-- See the Copyright notice at the end of this file.
--
class UNIVERSAL_OBJECT_LOADER[E_ -> LOADABLE_GL_OBJECT]

inherit
	UNIVERSAL_LOADER
		redefine make
		end

creation {ANY}
	make

feature {ANY}
	make is
		do
			Precursor
			create lookup_directories.make
			create loaders.make
		end

	load_object (file_name: STRING; universal_texture_loader: UNIVERSAL_TEXTURE_LOADER): E_ is
		local
			ft: FILE_TOOLS; bd: BASIC_DIRECTORY; found: BOOLEAN; complete_file_name, ext: STRING
		do
			from
				lookup_directories.new_search
			until
				lookup_directories.is_last_directory or else found
			loop
				bd.compute_file_path_with(lookup_directories.get_next_directory, file_name)
				complete_file_name := bd.last_entry
				if ft.is_readable(complete_file_name) then
					found := True
				end
			end
			if found then
				ext := extension(complete_file_name)
				if ext /= Void and then loaders.has(ext) then
					Result := loaders.at(ext).load(complete_file_name, universal_texture_loader)
					debug
						io.put_string("[O] Successfully loaded: ")
						io.put_string(file_name)
						io.put_new_line
					end
				end
			end
		end

	add_loader (ext: STRING; loader: OBJECT_LOADER[E_]) is
		require
			not loaders.has(ext)
		do
			loaders.put(loader, ext)
		end

feature {}
	loaders: HASHED_DICTIONARY[OBJECT_LOADER[E_], STRING]

end -- class UNIVERSAL_OBJECT_LOADER
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
