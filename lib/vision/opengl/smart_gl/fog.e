-- See the Copyright notice at the end of this file.
--
class FOG

insert
	OPENGL

creation {ANY}
	make_linear, make_exp, make_exp2

feature {ANY}
	make_linear (f_start, f_end: REAL) is
		do
			fog_start := f_start.force_to_real_32
			fog_end := f_end.force_to_real_32
			is_linear := True
			is_exp := False
			is_exp2 := False
		ensure
			is_linear
		end

	make_exp (f_start, f_end, f_density: REAL) is
		do
			fog_start := f_start.force_to_real_32
			fog_end := f_end.force_to_real_32
			fog_density := f_density.force_to_real_32
			is_linear := False
			is_exp := True
			is_exp2 := False
		ensure
			is_exp
		end

	make_exp2 (f_start, f_end, f_density: REAL) is
		do
			fog_start := f_start.force_to_real_32
			fog_end := f_end.force_to_real_32
			fog_density := f_density.force_to_real_32
			is_linear := False
			is_exp := False
			is_exp2 := True
		ensure
			is_exp2
		end

	is_linear, is_exp, is_exp2: BOOLEAN

	render is
		require
			color /= Void
		do
			gl.fogfv(gl.const_fog_color, color.color_array.to_external)
			gl.fogf(gl.const_fog_start, fog_start)
			gl.fogf(gl.const_fog_end, fog_end)
			if is_linear then
				gl.fogi(gl.const_fog_mode, gl.const_linear)
			else
				gl.fogf(gl.const_fog_density, fog_density)
				if is_exp then
					gl.fogi(gl.const_fog_mode, gl.const_exp)
				else
					gl.fogi(gl.const_fog_mode, gl.const_exp2)
				end
			end
		end

	color: GL_COLOR

	set_color (co: like color) is
		require
			co /= Void
		do
			color := co
		ensure
			color = co
		end

feature {}
	fog_start, fog_end, fog_density: REAL_32

invariant
	is_linear implies not is_exp and then not is_exp2
	is_exp implies not is_linear and then not is_exp2
	is_exp2 implies not is_linear and then not is_exp

end -- class FOG
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
