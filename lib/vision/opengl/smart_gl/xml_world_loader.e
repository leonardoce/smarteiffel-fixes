-- See the Copyright notice at the end of this file.
--
class XML_WORLD_LOADER[E_ -> LOADABLE_GL_OBJECT]

inherit
	WORLD_LOADER[E_]

creation {ANY}
	make

feature {ANY}
	load_from_file (world: WORLD; complete_file_name: STRING) is
		local
			tfr: TEXT_FILE_READ
			tree: XML_TREE
		do
			loaded_file := complete_file_name
			loaded_world := world
			current_light_id := gl.const_light0
			create tfr.connect_to(loaded_file)
			create tree.make(tfr.url)
			parse(tree.root)
		end

	parse_error is
		do
			std_error.put_string("Error while loading file `")
			std_error.put_string(loaded_file)
			std_error.put_string("'%N")
		end

	find_child (node: XML_COMPOSITE_NODE; name: UNICODE_STRING): XML_COMPOSITE_NODE is
		require
			node /= Void
			name /= Void
		local
			i: INTEGER
		do
			from
				i := 1
			until
				Result /= Void or else i > node.children_count
			loop
				if Result ?:= node.child(i) then
					Result ::= node.child(i)
					if not Result.name.is_equal(name) then
						Result := Void
					end
				end
				i := i + 1
			end
		end

	parse (node: XML_COMPOSITE_NODE) is
		require
			node /= Void and then node.name.is_equal(once U"world")
		local
			i: INTEGER; n: XML_COMPOSITE_NODE
		do
			from
				i := 1
			until
				i > node.children_count
			loop
				if n ?:= node.child(i) then
					n ::= node.child(i)
					if n.name.is_equal(once U"camera") then
						parse_camera(n)
					elseif n.name.is_equal(once U"light") then
						parse_light(n)
					elseif n.name.is_equal(once U"object") then
						parse_object(n)
					elseif n.name.is_equal(once U"fog") then
						parse_fog(n)
					else
						parse_error
						i := node.children_count
					end
				else
					parse_error
					i := node.children_count
				end
				i := i + 1
			end
		end

	parse_camera (node: XML_COMPOSITE_NODE) is
		require
			node /= Void and then node.name.is_equal(once U"camera")
		local
			camera: CAMERA
			position, orientation: XML_COMPOSITE_NODE
			x, y, z: STRING
		do
			position := find_child(node, once U"position")
			if position = Void then
				parse_error
			else
				x := position.attribute_at(once U"x").to_string
				y := position.attribute_at(once U"y").to_string
				z := position.attribute_at(once U"z").to_string
				if x = Void or else y = Void or else z = Void then
					parse_error
				else
					create camera.make_with_position(x.to_real, y.to_real, z.to_real)
					loaded_world.set_camera(camera)
					orientation := find_child(node, once U"orientation")
					if orientation  /= Void then
						x := orientation.attribute_at(once U"h").to_string
						y := orientation.attribute_at(once U"p").to_string
						z := orientation.attribute_at(once U"r").to_string
						if x /= Void and then y /= Void and then z /= Void then
							camera.set_orientation(x.to_real, y.to_real, z.to_real)
						end
					end
				end
			end
		end

	parse_light (node: XML_COMPOSITE_NODE) is
		require
			node /= Void and then node.name.is_equal(once U"light")
		local
			light: LIGHT
			type: UNICODE_STRING
			position, direction, color: XML_COMPOSITE_NODE
			x, y, z, a: STRING
		do
			create light.make(current_light_id)
			if current_light_id = gl.const_light0 then
				current_light_id := gl.const_light1
			elseif current_light_id = gl.const_light1 then
				current_light_id := gl.const_light2
			elseif current_light_id = gl.const_light2 then
				current_light_id := gl.const_light3
			elseif current_light_id = gl.const_light3 then
				current_light_id := gl.const_light4
			elseif current_light_id = gl.const_light4 then
				current_light_id := gl.const_light5
			elseif current_light_id = gl.const_light5 then
				current_light_id := gl.const_light6
			elseif current_light_id = gl.const_light6 then
				current_light_id := gl.const_light7
			else
				std_error.put_string("Warning: check number of lights in ")
				std_error.put_line(loaded_file)
				current_light_id := current_light_id + 1
			end
			type := node.attribute_at(once U"type")
			if type = Void or else type.is_equal(once U"spot") then
				light.set_spot_position(0, 0, 0)
				position := find_child(node, once U"position")
				if position /= Void then
					x := position.attribute_at(once U"x").to_string
					y := position.attribute_at(once U"y").to_string
					z := position.attribute_at(once U"z").to_string
					if x /= Void and then y /= Void and then z /= Void then
						light.set_spot_position(x.to_real, y.to_real, z.to_real)
					end
				end
				direction := find_child(node, once U"direction")
				if direction = Void then
					parse_error
				else
					x := direction.attribute_at(once U"x").to_string
					y := direction.attribute_at(once U"y").to_string
					z := direction.attribute_at(once U"z").to_string
					if x /= Void and then y /= Void and then z /= Void then
						light.set_spot_direction(x.to_real, y.to_real, z.to_real)
					end
				end
			elseif type /= Void and then type.is_equal(once U"directional") then
				direction := find_child(node, once U"direction")
				if direction = Void then
					parse_error
				else
					x := direction.attribute_at(once U"x").to_string
					y := direction.attribute_at(once U"y").to_string
					z := direction.attribute_at(once U"z").to_string
					if x /= Void and then y /= Void and then z /= Void then
						light.set_directional(x.to_real, y.to_real, z.to_real)
					end
				end
			else
				std_error.put_string("Warning: no light type in ")
				std_error.put_line(loaded_file)
			end
			color := find_child(node, once U"ambient")
			if color /= Void then
				x := color.attribute_at(once U"r").to_string
				y := color.attribute_at(once U"g").to_string
				z := color.attribute_at(once U"b").to_string
				a := color.attribute_at(once U"a").to_string
				if x /= Void and then y /= Void and then z /= Void and then a /= Void then
					light.set_ambient(create {GL_COLOR}.set_color(x.to_real, y.to_real, z.to_real, a.to_real))
				end
			end
			color := find_child(node, once U"diffuse")
			if color /= Void then
				x := color.attribute_at(once U"r").to_string
				y := color.attribute_at(once U"g").to_string
				z := color.attribute_at(once U"b").to_string
				a := color.attribute_at(once U"a").to_string
				if x /= Void and then y /= Void and then z /= Void and then a /= Void then
					light.set_diffuse(create {GL_COLOR}.set_color(x.to_real, y.to_real, z.to_real, a.to_real))
				end
			end
			color := find_child(node, once U"specular")
			if color /= Void then
				x := color.attribute_at(once U"r").to_string
				y := color.attribute_at(once U"g").to_string
				z := color.attribute_at(once U"b").to_string
				a := color.attribute_at(once U"a").to_string
				if x /= Void and then y /= Void and then z /= Void and then a /= Void then
					light.set_specular(create {GL_COLOR}.set_color(x.to_real, y.to_real, z.to_real, a.to_real))
				end
			end
			loaded_world.add_light(light)
		end

	parse_object (node: XML_COMPOSITE_NODE) is
		require
			node /= Void and then node.name.is_equal(once U"object")
		local
			file: UNICODE_STRING
			object: E_
			position, orientation: XML_COMPOSITE_NODE
			x, y, z: STRING
		do
			file := node.attribute_at(once U"file")
			if file = Void then
				parse_error
			else
				object := universal_object_loader.load_object(file.to_string, universal_texture_loader)
				if object.is_translucent then
					loaded_world.add_translucent_object(object)
				else
					loaded_world.add_opaque_object(object)
				end
				position := find_child(node, once U"position")
				if position /= Void then
					x := position.attribute_at(once U"x").to_string
					y := position.attribute_at(once U"y").to_string
					z := position.attribute_at(once U"z").to_string
					if x /= Void and then y /= Void and then z /= Void then
						object.set_position(x.to_real, y.to_real, z.to_real)
					end
				end
				orientation := find_child(node, once U"orientation")
				if orientation /= Void then
					x := position.attribute_at(once U"h").to_string
					y := position.attribute_at(once U"p").to_string
					z := position.attribute_at(once U"r").to_string
					if x /= Void and then y /= Void and then z /= Void then
						object.set_orientation(x.to_real, y.to_real, z.to_real)
					end
				end
			end
		end

	parse_fog (node: XML_COMPOSITE_NODE) is
		require
			node /= Void and then node.name.is_equal(once U"fog")
		local
			fog: FOG
			type: STRING
			f_start, f_end, f_density: STRING
			color: XML_COMPOSITE_NODE
			r, g, b, a: STRING
			atmospheric: STRING
		do
			type := node.attribute_at(once U"type").to_string
			inspect
				type
			when "linear" then
				f_start := node.attribute_at(once U"start").to_string
				f_end := node.attribute_at(once U"end").to_string
				if f_start /= Void and then f_end /= Void then
					create fog.make_linear(f_start.to_real, f_end.to_real)
				end
			when "exp" then
				f_start := node.attribute_at(once U"start").to_string
				f_end := node.attribute_at(once U"end").to_string
				f_density := node.attribute_at(once U"density").to_string
				if f_start /= Void and then f_end /= Void and then f_density /= Void then
					create fog.make_exp(f_start.to_real, f_end.to_real, f_density.to_real)
				end
			when "exp2" then
				f_start := node.attribute_at(once U"start").to_string
				f_end := node.attribute_at(once U"end").to_string
				f_density := node.attribute_at(once U"density").to_string
				if f_start /= Void and then f_end /= Void and then f_density /= Void then
					create fog.make_exp2(f_start.to_real, f_end.to_real, f_density.to_real)
				end
			else
				std_error.put_string("Warning: no fog type in ")
				std_error.put_line(loaded_file)
			end
			if fog /= Void then
				color := find_child(node, once U"color")
				if color /= Void then
					r := color.attribute_at(once U"r").to_string
					g := color.attribute_at(once U"g").to_string
					b := color.attribute_at(once U"b").to_string
					a := color.attribute_at(once U"a").to_string
					if r /= Void and then g /= Void and then b /= Void and then a /= Void then
						fog.set_color(create {GL_COLOR}.set_color(r.to_real, g.to_real, b.to_real, a.to_real))
					end
				end
				atmospheric := node.attribute_at(once U"atmospheric").to_string
				if atmospheric /= Void then
					loaded_world.set_fog(fog, atmospheric.to_boolean)
				else
					loaded_world.set_fog(fog, False)
				end
			end
		end

feature {}
	loaded_file: STRING

	loaded_world: WORLD

	current_light_id: INTEGER

end -- class XML_WORLD_LOADER
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
