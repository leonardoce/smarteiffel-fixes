-- See the Copyright notice at the end of this file.
--
deferred class GL_OBJECT

insert
	OPENGL

feature {ANY}
	make_at_origin is
		do
			make_with_position(0, 0, 0)
		end

	make_with_position (x: like position_x; y: like position_y; z: like position_z) is
		do
			set_position(x, y, z)
			set_scale(1, 1, 1)
			create facets.with_capacity(2)
			build
		end

	name: STRING

	set_name (n: like name) is
		require
			n /= Void
		do
			name := n
		ensure
			name = n
		end

	build is
		deferred
		end

	set_position (x: like position_x; y: like position_y; z: like position_z) is
		do
			set_position_x(x)
			set_position_y(y)
			set_position_z(z)
		end

	set_orientation (h: like heading; p: like pitch; r: like roll) is
		do
			set_heading(h)
			set_pitch(p)
			set_roll(r)
		end

	position_x, position_y, position_z: REAL

	heading, pitch, roll: REAL

	scale_x, scale_y, scale_z: REAL

	set_position_x (x: like position_x) is
		do
			position_x := x
		ensure
			position_x = x
		end

	set_position_y (y: like position_y) is
		do
			position_y := y
		ensure
			position_y = y
		end

	set_position_z (z: like position_z) is
		do
			position_z := z
		ensure
			position_z = z
		end

	set_heading (h: like heading) is
		do
			heading := h
			from
			until
				heading <= 180
			loop
				heading := heading - 360
			end
			from
			until
				heading >= -180
			loop
				heading := heading + 360
			end
		ensure
			heading.in_range(-180, 180)
		end

	set_pitch (p: like pitch) is
		do
			pitch := p
			from
			until
				pitch <= 180
			loop
				pitch := pitch - 360
			end
			from
			until
				pitch >= -180
			loop
				pitch := pitch + 360
			end
		ensure
			pitch.in_range(-180, 180)
		end

	set_roll (r: like roll) is
		do
			roll := r
			from
			until
				roll <= 180
			loop
				roll := roll - 360
			end
			from
			until
				roll >= -180
			loop
				roll := roll + 360
			end
		ensure
			roll.in_range(-180, 180)
		end

	set_scale (sx: like scale_x; sy: like scale_y; sz: like scale_z) is
		do
			set_scale_x(sx)
			set_scale_y(sy)
			set_scale_z(sz)
		end

	set_scale_x (sx: like scale_x) is
		do
			scale_x := sx
		ensure
			scale_x = sx
		end

	set_scale_y (sy: like scale_y) is
		do
			scale_y := sy
		ensure
			scale_y = sy
		end

	set_scale_z (sz: like scale_z) is
		do
			scale_z := sz
		ensure
			scale_z = sz
		end

	render is
		do
			smart_gl.begin_rendering
			smart_gl.translate(position_x, position_y, position_z)
			smart_gl.rotate(roll, 0, 0, 1)
			smart_gl.rotate(-pitch, 1, 0, 0)
			smart_gl.rotate(heading, 0, 1, 0)
			smart_gl.scale(scale_x, scale_y, scale_z)
			draw
			smart_gl.end_rendering
		end

	draw is
		local
			i: INTEGER
		do
			from
				i := facets.lower
			variant
				facets.upper - i
			until
				i > facets.upper
			loop
				facets.item(i).render
				i := i + 1
			end
		end

	set_parent (p: like parent) is
		require
			p /= Void
		do
			parent := p
		ensure
			parent = p
		end

	is_translucent: BOOLEAN

	add_opaque_facet (facet: FACET) is
		do
			facets.add_first(facet)
		end

	add_translucent_facet (facet: FACET) is
		do
			facets.add_last(facet)
			is_translucent := True
		ensure
			is_translucent = True
		end

feature {}
	parent: WORLD

	facets: FAST_ARRAY[FACET]

end -- class GL_OBJECT
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
