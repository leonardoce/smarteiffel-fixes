-- See the Copyright notice at the end of this file.
--
class FACET

insert
	OPENGL

creation {ANY}
	make

feature {ANY}
	make (v1, v2, v3: VERTEX) is
		require
			v1 /= Void
			v2 /= Void
			v3 /= Void
		do
			vertex1 := v1
			vertex2 := v2
			vertex3 := v3
			set_texture_composition_function(gl.const_modulate)
		ensure
			vertex1 = v1
			vertex2 = v2
			vertex3 = v3
		end

	render is
		do
			if texture /= Void then
				gl.enable(gl.const_texture_2d)
				gl.bind_texture(gl.const_texture_2d, texture.texture_id)
				gl.tex_envi(gl.const_texture_env, gl.const_texture_env_mode, texture_composition_function)
			else
				gl.disable(gl.const_texture_2d)
			end
			if is_translucent then
				gl.enable(gl.const_blend)
				gl.depth_mask(gl.const_false)
				gl.disable(gl.const_cull_face)
			else
				gl.disable(gl.const_blend)
				gl.enable(gl.const_cull_face)
				gl.depth_mask(gl.const_true)
			end
			smart_gl.begin_triangles
			vertex1.render
			vertex2.render
			vertex3.render
			smart_gl.end_primitive
			if is_translucent then
				gl.depth_mask(gl.const_true)
			end
		end

	set_texture (tex: like texture) is
		do
			texture := tex
		ensure
			texture = tex
		end

	vertex1, vertex2, vertex3: VERTEX

	set_texture_composition_function (function: like texture_composition_function) is
		do
			texture_composition_function := function
		ensure
			texture_composition_function = function
		end

	texture_composition_function: INTEGER

	is_translucent: BOOLEAN is
		do
			Result := vertex1.is_translucent or else vertex2.is_translucent or else vertex3.is_translucent
		end

	has_color: BOOLEAN is
		do
			Result := vertex1.color /= Void or else vertex2.color /= Void or else vertex3.color /= Void
		end

feature {}
	texture: TEXTURE_2D

end -- class FACET
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
