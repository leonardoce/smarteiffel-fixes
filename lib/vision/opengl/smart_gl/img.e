-- See the Copyright notice at the end of this file.
--
deferred class IMG

feature {ANY}
	load_from_file (file_name: STRING) is
		require
			is_loaded = False
			is_useable(file_name)
		deferred
		ensure
			is_loaded implies pixels /= Void
		end

	is_loaded: BOOLEAN

	width, height, planes, bits_per_pixel, x_resolution, y_resolution: INTEGER

	pixels: FAST_ARRAY[REAL_32]

feature {IMAGE}
	read_byte: INTEGER is
		do
			file.read_byte
			Result := file.last_byte
		end

	read_integer_16_little_endian: INTEGER is
		do
			file.read_integer_16_little_endian
			Result := file.last_integer_16
		end

	read_integer_16_big_endian: INTEGER is
		do
			file.read_integer_16_big_endian
			Result := file.last_integer_16
		end

	read_integer_32_little_endian: INTEGER is
		do
			file.read_integer_32_little_endian
			Result := file.last_integer_32
		end

	read_integer_32_big_endian: INTEGER is
		do
			file.read_integer_32_big_endian
			Result := file.last_integer_32
		end

feature {}
	file: BINARY_FILE_READ

	is_useable (file_name: STRING): BOOLEAN is
		local
			ft: FILE_TOOLS
		do
			if ft.file_exists(file_name) and then ft.is_readable(file_name) then
				Result := True
			end
		end

end -- class IMG
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
