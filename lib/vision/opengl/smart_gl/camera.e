-- See the Copyright notice at the end of this file.
--
class CAMERA

inherit
	GL_OBJECT
		redefine make_with_position, render, set_orientation, set_heading, set_pitch, set_roll
		end

creation {ANY}
	make_at_origin, make_with_position

feature {ANY}
	make_with_position (x: like position_x; y: like position_y; z: like position_z) is
		do
			set_position(x, y, z)
		end

	build is
		do
		end

	render is
		do
			smart_gl.begin_rendering
			smart_gl.rotate(-roll, 0, 0, 1)
			smart_gl.rotate(-pitch, 1, 0, 0)
			smart_gl.rotate(-heading, 0, 1, 0)
			smart_gl.translate(-position_x, -position_y, -position_z)
			smart_gl.end_rendering
		end

	direction_x, direction_y, direction_z: REAL

	set_orientation (h: like heading; p: like pitch; r: like roll) is
		do
			Precursor(h, p, r)
			update_direction
		end

	set_heading (h: like heading) is
		do
			Precursor(h)
			update_direction
		end

	set_pitch (p: like pitch) is
		do
			Precursor(p)
			update_direction
		end

	set_roll (r: like roll) is
		do
			Precursor(r)
			update_direction
		end

	turn_left (angle: REAL) is
		do
			set_heading(heading + angle)
		end

	turn_right (angle: REAL) is
		do
			turn_left(-angle)
		end

	head_up (angle: REAL) is
		do
			set_pitch(pitch + angle)
		end

	head_down (angle: REAL) is
		do
			head_up(-angle)
		end

	roll_left (angle: REAL) is
		do
			set_roll(roll + angle)
		end

	roll_right (angle: REAL) is
		do
			roll_left(-angle)
		end

	move_forward (distance: REAL) is
		do
			set_position(position_x + direction_x * distance, position_y + direction_y * distance, position_z + direction_z * distance)
		end

	move_backward (distance: REAL) is
		do
			move_forward(-distance)
		end

feature {}
	update_direction is
		local
			h, p, c: REAL
		do
			h := -heading * to_rad
			p := pitch * to_rad
			c := p.cos
			direction_x := h.sin * c
			direction_y := p.sin
			direction_z := -h.cos * c
		end

	to_rad: REAL is
		once
			Result := 3.1415926535 / 180
		end

end -- class CAMERA
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
