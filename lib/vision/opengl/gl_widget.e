-- See the Copyright notice at the end of this file.
--
deferred class GL_WIDGET

inherit
	DRAWABLE
	DISPOSABLE

insert
	GRAPHIC
		undefine default_create
		end
	OPENGL

feature {ANY}
	make (p: like parent) is
		do
			min_width := 1
			min_height := 1
			std_width := 1
			std_height := 1
			width := std_width
			height := std_height
			p.child_attach(Current)
			set_shrink(True)
			set_expand(True)
		end

	select_myself_gl is
		local
			ok: BOOLEAN
		do
			ok := basic_opengl_select(opengl_widget)
		end

	display_gl, expose_paint is
		do
			redraw_gl
			basic_opengl_swap_buffers(opengl_widget)
		end

	init_gl is
		deferred
		end

	redraw_gl is
		deferred
		end

	resize_gl (w, h: INTEGER) is
		deferred
		end

	dispose is
		require
			opengl_widget.is_not_null
		do
			basic_opengl_window_destroy(opengl_widget)
		end

	clear_without_expose is
		do
			gl.clear(gl.const_color_buffer_bit + gl.const_depth_buffer_bit)
		end

	resize (w, h: INTEGER) is
		do
			width := w
			height := h
			resize_gl(width, height)
		end

	set_geometry (x, y, w, h: INTEGER) is
		do
			if not has_been_init then
				opengl_widget := basic_opengl_window_create(x, y, width, height, parent.widget, False)
				has_been_init := True
				init_gl
			end
			if width /= w or else height /= h then
				width := w
				height := h
				basic_opengl_window_set_geometry(opengl_widget, x, y, width, height)
				resize_gl(width, height)
			end
		end

	hash_code: INTEGER is
		do
			Result := opengl_widget.hash_code
		end

	min_width, max_width: INTEGER

	min_height, max_height: INTEGER

	std_width, std_height: INTEGER

	width, height: INTEGER

feature {}
	has_been_init: BOOLEAN

	opengl_widget: POINTER

	basic_opengl_window_create (x, y, w, h: INTEGER; p: POINTER; decorate: BOOLEAN): POINTER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "opengl"
			feature_name: "basic_opengl_window_create"
			}"
		end

	basic_opengl_window_set_geometry (ogl_widget: POINTER; x, y, w, h: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "opengl"
			feature_name: "basic_opengl_window_set_geometry"
			}"
		end

	basic_opengl_select (ogl_widget: POINTER): BOOLEAN is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "opengl"
			feature_name: "basic_opengl_select"
			}"
		end

	basic_opengl_swap_buffers (ogl_widget: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "opengl"
			feature_name: "basic_opengl_swap_buffers"
			}"
		end

	basic_opengl_window_destroy (ogl_widget: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "opengl"
			feature_name: "basic_opengl_window_destroy"
			}"
		end

invariant
	(not has_been_init) implies opengl_widget.is_null
	has_been_init implies opengl_widget.is_not_null

end -- class GL_WIDGET
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
