-- See the Copyright notice at the end of this file.
--
class QUATERNION

creation {ANY}
	make_from_coordinates, make_from_angle_and_axis, make_from_euler_rotations

feature {ANY}
	zero: like Current is
		do
			create Result.make_from_coordinates(0, 0, 0, 0)
		end

	one: like Current is
		do
			create Result.make_from_coordinates(0, 0, 0, 1)
		end

	make_from_coordinates (nx: like x; ny: like y; nz: like z; nw: like w) is
		do
			set_x(nx)
			set_y(ny)
			set_z(nz)
			set_w(nw)
		end

	make_from_angle_and_axis (angle, axis_x, axis_y, axis_z: REAL) is
		local
			rad, sinus: REAL
		do
			if angle = 0 then
				make_from_coordinates(0, 0, 0, 1)
			else
				rad := angle * 3.1415926535 / 360 -- This is the half angle in radians
				sinus := rad.sin
				set_x(axis_x * sinus)
				set_y(axis_y * sinus)
				set_z(axis_z * sinus)
				set_w(rad.cos)
			end
		end

	make_from_euler_rotations (angle_x, angle_y, angle_z: REAL) is
		local
			tmp: QUATERNION
		do
			make_from_angle_and_axis(angle_x, 1, 0, 0)
			tmp := tmp_quaternion
			tmp.make_from_angle_and_axis(angle_y, 0, 1, 0)
			multiply(tmp)
			tmp.make_from_angle_and_axis(angle_z, 0, 0, 1)
			multiply(tmp)
		end

	optimized_to_matrix: FAST_ARRAY[REAL] is
		local
			dx, dy, dz: REAL; t1, t2, t3, t4, t5, t6, t7, t8, t9: REAL
		do
			create Result.make(16)
			dx := 2 * x
			dy := 2 * y
			dz := 2 * z
			t1 := dx * x
			t2 := dy * y
			t3 := dz * z
			t4 := dx * y
			t5 := dx * z
			t6 := dx * w
			t7 := dy * z
			t8 := dy * w
			t9 := dz * w
			-- First column
			Result.put(1 - t2 - t3, 0)
			Result.put(t4 + t9, 1)
			Result.put(t5 - t8, 2)
			Result.put(0, 3)
			-- Second column
			Result.put(t4 - t9, 4)
			Result.put(1 - t1 - t3, 5)
			Result.put(t7 + t6, 6)
			Result.put(0, 7)
			-- Third column
			Result.put(t5 + t8, 8)
			Result.put(t7 - t6, 9)
			Result.put(1 - t1 - t2, 10)
			Result.put(0, 11)
			-- Fourth column
			Result.put(0, 12)
			Result.put(0, 13)
			Result.put(0, 14)
			Result.put(1, 15)
		end

	multiply(other: like Current) is
			-- Multiply `Current' by `other'.
			-- See also `multiply_to'
		require
			other /= Void
		local
			nx: like x; ny: like y; nz: like z
		do
			nx := w * other.x + x * other.w + y * other.z - z * other.y
			ny := w * other.y + y * other.w + z * other.x - x * other.z
			nz := w * other.z + z * other.w + x * other.y - y * other.x
			make_from_coordinates(nx, ny, nz, w * other.w - x * other.x - y * other.y - z * other.z)
		end

	multiply_to (other, res: like Current) is
			-- Multiply the contents of `Current' and `other' and place the
			-- result  in `res'. (`Current' and `other' are not modified.)
			-- See also `multiply'
		require
			other /= Void
			res /= Void
			res /= Current
		do
			res.make_from_coordinates(w * other.x + x * other.w + y * other.z - z * other.y,
											  w * other.y + y * other.w + z * other.x - x * other.z,
											  w * other.z + z * other.w + x * other.y - y * other.x,
											  w * other.w - x * other.x - y * other.y - z * other.z)
		end
		
	infix "*" (other: like Current): like Current is
			-- See also `multiply' and `multiply_to'
		require
			other /= Void
		local
			nx: like x; ny: like y; nz: like z; nw: like w
		do
			nx := w * other.x + x * other.w + y * other.z - z * other.y
			ny := w * other.y + y * other.w + z * other.x - x * other.z
			nz := w * other.z + z * other.w + x * other.y - y * other.x
			nw := w * other.w - x * other.x - y * other.y - z * other.z
			create Result.make_from_coordinates(nx, ny, nz, nw)
		ensure
			Result /= Void
		end

	conjugate, inverse: like Current is
		do
			create Result.make_from_coordinates(-x, -y, -z, w)
		end

	magnitude: REAL is
		do
			Result := (x * x + y * y + z * z + w * w).sqrt
		end

	normalize is
		local
			mag: like magnitude
		do
			mag := magnitude
			x := x / mag
			y := y / mag
			z := z / mag
			w := w / mag
		ensure
			(magnitude - 1).abs < precision
		end

	x, y, z, w: REAL

	set_x (n: like x) is
		do
			x := n
		ensure
			x = n
		end

	set_y (n: like y) is
		do
			y := n
		ensure
			y = n
		end

	set_z (n: like z) is
		do
			z := n
		ensure
			z = n
		end

	set_w (n: like w) is
		do
			w := n
		ensure
			w = n
		end

feature {}
	precision: REAL is 0.000001

	tmp_quaternion: QUATERNION is
		once
			Result := zero
		end
	
end -- class QUATERNION
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
