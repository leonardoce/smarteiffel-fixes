-- See the Copyright notice at the end of this file.
--
class SMART_GL

insert
	OPENGL

creation {OPENGL}
	default_create

feature {ANY}
	set_clear_color (red, green, blue, alpha: REAL) is
		-- Specify  the  `red', `green', `blue', and `alpha' values used when the color buffers
		-- are cleared. The initial values are all 0.
		require
			not has_begun
			red.in_range(0.0, 1.0)
			green.in_range(0.0, 1.0)
			blue.in_range(0.0, 1.0)
			alpha.in_range(0.0, 1.0)
		do
			gl.clear_color(red.force_to_real_32, green.force_to_real_32, blue.force_to_real_32, alpha.force_to_real_32)
		end

	set_clear_depth (depth: REAL) is
			-- Specifies the depth value used when the depth buffer is cleared. The initial value is 1.
		require
			not has_begun
			depth.in_range(0.0, 1.0)
		do
			gl.clear_depth(depth)
		end

	translate (x, y, z: REAL) is
		require
			is_rendering
		do
			gl.translated(x, y, z)
		end

	rotate (angle, axis_x, axis_y, axis_z: REAL) is
		require
			is_rendering
		do
			gl.rotated(angle, axis_x, axis_y, axis_z)
		end

	scale (x, y, z: REAL) is
		require
			is_rendering
		do
			gl.scaled(x, y, z)
		end

	begin_rendering is
		do
			is_rendering := True
			gl.push_matrix
		ensure
			is_rendering
		end

	end_rendering is
		do
			gl.pop_matrix
			is_rendering := False
		ensure
			not is_rendering
		end

	begin_points is
		require
			is_rendering
			not has_begun
		do
			gl.begin(gl.const_points)
			has_begun := True
		ensure
			has_begun
		end

	begin_lines is
		require
			is_rendering
			not has_begun
		do
			gl.begin(gl.const_lines)
			has_begun := True
		ensure
			has_begun
		end

	begin_line_strip is
		require
			is_rendering
			not has_begun
		do
			gl.begin(gl.const_line_strip)
			has_begun := True
		ensure
			has_begun
		end

	begin_line_loop is
		require
			is_rendering
			not has_begun
		do
			gl.begin(gl.const_line_loop)
			has_begun := True
		ensure
			has_begun
		end

	begin_triangles is
		require
			is_rendering
			not has_begun
		do
			gl.begin(gl.const_triangles)
			has_begun := True
		ensure
			has_begun
		end

	begin_triangle_strip is
		require
			is_rendering
			not has_begun
		do
			gl.begin(gl.const_triangle_strip)
			has_begun := True
		ensure
			has_begun
		end

	begin_triangle_fan is
		require
			is_rendering
			not has_begun
		do
			gl.begin(gl.const_triangle_fan)
			has_begun := True
		ensure
			has_begun
		end

	begin_quads is
		require
			is_rendering
			not has_begun
		do
			gl.begin(gl.const_quads)
			has_begun := True
		ensure
			has_begun
		end

	begin_quad_strip is
		require
			is_rendering
			not has_begun
		do
			gl.begin(gl.const_quad_strip)
			has_begun := True
		ensure
			has_begun
		end

	begin_polygon is
		require
			is_rendering
			not has_begun
		do
			gl.begin(gl.const_polygon)
			has_begun := True
		ensure
			has_begun
		end

	end_primitive is
		require
			has_begun
		do
			gl.gl_end
			has_begun := False
		ensure
			not has_begun
		end

	vertex (x, y, z: REAL) is
		require
			is_rendering
			has_begun
		do
			gl.vertex3d(x, y, z)
		end

	normal (x, y, z: REAL) is
		require
			is_rendering
			has_begun
		do
			gl.normal3d(x, y, z)
		end

feature {}
	has_begun: BOOLEAN

	is_rendering: BOOLEAN

end -- class SMART_GL
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
