-- See the Copyright notice at the end of this file.
--
class GL

insert
	SINGLETON

creation {ANY}
	default_create

feature {ANY}
	const_false: INTEGER_8 is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FALSE"
		}"
		end

	const_true: INTEGER_8 is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TRUE"
		}"
		end

	const_byte: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BYTE"
		}"
		end

	const_unsigned_byte: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_BYTE"
		}"
		end

	const_short: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SHORT"
		}"
		end

	const_unsigned_short: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_SHORT"
		}"
		end

	const_int: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INT"
		}"
		end

	const_unsigned_int: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_INT"
		}"
		end

	const_float: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FLOAT"
		}"
		end

	const_2_bytes: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_2_BYTES"
		}"
		end

	const_3_bytes: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_3_BYTES"
		}"
		end

	const_4_bytes: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_4_BYTES"
		}"
		end

	const_double: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DOUBLE"
		}"
		end

	const_points: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POINTS"
		}"
		end

	const_lines: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINES"
		}"
		end

	const_line_loop: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE_LOOP"
		}"
		end

	const_line_strip: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE_STRIP"
		}"
		end

	const_triangles: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TRIANGLES"
		}"
		end

	const_triangle_strip: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TRIANGLE_STRIP"
		}"
		end

	const_triangle_fan: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TRIANGLE_FAN"
		}"
		end

	const_quads: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_QUADS"
		}"
		end

	const_quad_strip: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_QUAD_STRIP"
		}"
		end

	const_polygon: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POLYGON"
		}"
		end

	const_vertex_array: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_VERTEX_ARRAY"
		}"
		end

	const_normal_array: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NORMAL_ARRAY"
		}"
		end

	const_color_array: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_ARRAY"
		}"
		end

	const_index_array: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INDEX_ARRAY"
		}"
		end

	const_texture_coord_array: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_COORD_ARRAY"
		}"
		end

	const_edge_flag_array: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_EDGE_FLAG_ARRAY"
		}"
		end

	const_vertex_array_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_VERTEX_ARRAY_SIZE"
		}"
		end

	const_vertex_array_type: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_VERTEX_ARRAY_TYPE"
		}"
		end

	const_vertex_array_stride: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_VERTEX_ARRAY_STRIDE"
		}"
		end

	const_normal_array_type: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NORMAL_ARRAY_TYPE"
		}"
		end

	const_normal_array_stride: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NORMAL_ARRAY_STRIDE"
		}"
		end

	const_color_array_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_ARRAY_SIZE"
		}"
		end

	const_color_array_type: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_ARRAY_TYPE"
		}"
		end

	const_color_array_stride: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_ARRAY_STRIDE"
		}"
		end

	const_index_array_type: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INDEX_ARRAY_TYPE"
		}"
		end

	const_index_array_stride: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INDEX_ARRAY_STRIDE"
		}"
		end

	const_texture_coord_array_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_COORD_ARRAY_SIZE"
		}"
		end

	const_texture_coord_array_type: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_COORD_ARRAY_TYPE"
		}"
		end

	const_texture_coord_array_stride: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_COORD_ARRAY_STRIDE"
		}"
		end

	const_edge_flag_array_stride: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_EDGE_FLAG_ARRAY_STRIDE"
		}"
		end

	const_vertex_array_pointer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_VERTEX_ARRAY_POINTER"
		}"
		end

	const_normal_array_pointer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NORMAL_ARRAY_POINTER"
		}"
		end

	const_color_array_pointer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_ARRAY_POINTER"
		}"
		end

	const_index_array_pointer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INDEX_ARRAY_POINTER"
		}"
		end

	const_texture_coord_array_pointer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_COORD_ARRAY_POINTER"
		}"
		end

	const_edge_flag_array_pointer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_EDGE_FLAG_ARRAY_POINTER"
		}"
		end

	const_v2f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_V2F"
		}"
		end

	const_v3f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_V3F"
		}"
		end

	const_c4ub_v2f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_C4UB_V2F"
		}"
		end

	const_c4ub_v3f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_C4UB_V3F"
		}"
		end

	const_c3f_v3f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_C3F_V3F"
		}"
		end

	const_n3f_v3f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_N3F_V3F"
		}"
		end

	const_c4f_n3f_v3f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_C4F_N3F_V3F"
		}"
		end

	const_t2f_v3f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_T2F_V3F"
		}"
		end

	const_t4f_v4f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_T4F_V4F"
		}"
		end

	const_t2f_c4ub_v3f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_T2F_C4UB_V3F"
		}"
		end

	const_t2f_c3f_v3f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_T2F_C3F_V3F"
		}"
		end

	const_t2f_n3f_v3f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_T2F_N3F_V3F"
		}"
		end

	const_t2f_c4f_n3f_v3f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_T2F_C4F_N3F_V3F"
		}"
		end

	const_t4f_c4f_n3f_v4f: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_T4F_C4F_N3F_V4F"
		}"
		end

	const_matrix_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MATRIX_MODE"
		}"
		end

	const_modelview: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MODELVIEW"
		}"
		end

	const_projection: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PROJECTION"
		}"
		end

	const_texture: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE"
		}"
		end

	const_point_smooth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POINT_SMOOTH"
		}"
		end

	const_point_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POINT_SIZE"
		}"
		end

	const_point_size_granularity: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POINT_SIZE_GRANULARITY"
		}"
		end

	const_point_size_range: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POINT_SIZE_RANGE"
		}"
		end

	const_line_smooth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE_SMOOTH"
		}"
		end

	const_line_stipple: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE_STIPPLE"
		}"
		end

	const_line_stipple_pattern: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE_STIPPLE_PATTERN"
		}"
		end

	const_line_stipple_repeat: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE_STIPPLE_REPEAT"
		}"
		end

	const_line_width: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE_WIDTH"
		}"
		end

	const_line_width_granularity: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE_WIDTH_GRANULARITY"
		}"
		end

	const_line_width_range: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE_WIDTH_RANGE"
		}"
		end

	const_point: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POINT"
		}"
		end

	const_line: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE"
		}"
		end

	const_fill: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FILL"
		}"
		end

	const_cw: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CW"
		}"
		end

	const_ccw: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CCW"
		}"
		end

	const_front: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FRONT"
		}"
		end

	const_back: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BACK"
		}"
		end

	const_polygon_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POLYGON_MODE"
		}"
		end

	const_polygon_smooth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POLYGON_SMOOTH"
		}"
		end

	const_polygon_stipple: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POLYGON_STIPPLE"
		}"
		end

	const_edge_flag: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_EDGE_FLAG"
		}"
		end

	const_cull_face: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CULL_FACE"
		}"
		end

	const_cull_face_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CULL_FACE_MODE"
		}"
		end

	const_front_face: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FRONT_FACE"
		}"
		end

	const_polygon_offset_factor: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POLYGON_OFFSET_FACTOR"
		}"
		end

	const_polygon_offset_units: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POLYGON_OFFSET_UNITS"
		}"
		end

	const_polygon_offset_point: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POLYGON_OFFSET_POINT"
		}"
		end

	const_polygon_offset_line: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POLYGON_OFFSET_LINE"
		}"
		end

	const_polygon_offset_fill: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POLYGON_OFFSET_FILL"
		}"
		end

	const_compile: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COMPILE"
		}"
		end

	const_compile_and_execute: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COMPILE_AND_EXECUTE"
		}"
		end

	const_list_base: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIST_BASE"
		}"
		end

	const_list_index: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIST_INDEX"
		}"
		end

	const_list_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIST_MODE"
		}"
		end

	const_never: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NEVER"
		}"
		end

	const_less: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LESS"
		}"
		end

	const_equal: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_EQUAL"
		}"
		end

	const_lequal: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LEQUAL"
		}"
		end

	const_greater: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_GREATER"
		}"
		end

	const_notequal: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NOTEQUAL"
		}"
		end

	const_gequal: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_GEQUAL"
		}"
		end

	const_always: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALWAYS"
		}"
		end

	const_depth_test: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DEPTH_TEST"
		}"
		end

	const_depth_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DEPTH_BITS"
		}"
		end

	const_depth_clear_value: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DEPTH_CLEAR_VALUE"
		}"
		end

	const_depth_func: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DEPTH_FUNC"
		}"
		end

	const_depth_range: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DEPTH_RANGE"
		}"
		end

	const_depth_writemask: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DEPTH_WRITEMASK"
		}"
		end

	const_depth_component: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DEPTH_COMPONENT"
		}"
		end

	const_lighting: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHTING"
		}"
		end

	const_light0: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHT0"
		}"
		end

	const_light1: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHT1"
		}"
		end

	const_light2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHT2"
		}"
		end

	const_light3: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHT3"
		}"
		end

	const_light4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHT4"
		}"
		end

	const_light5: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHT5"
		}"
		end

	const_light6: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHT6"
		}"
		end

	const_light7: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHT7"
		}"
		end

	const_spot_exponent: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SPOT_EXPONENT"
		}"
		end

	const_spot_cutoff: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SPOT_CUTOFF"
		}"
		end

	const_constant_attenuation: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CONSTANT_ATTENUATION"
		}"
		end

	const_linear_attenuation: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINEAR_ATTENUATION"
		}"
		end

	const_quadratic_attenuation: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_QUADRATIC_ATTENUATION"
		}"
		end

	const_ambient: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_AMBIENT"
		}"
		end

	const_diffuse: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DIFFUSE"
		}"
		end

	const_specular: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SPECULAR"
		}"
		end

	const_shininess: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SHININESS"
		}"
		end

	const_emission: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_EMISSION"
		}"
		end

	const_position: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POSITION"
		}"
		end

	const_spot_direction: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SPOT_DIRECTION"
		}"
		end

	const_ambient_and_diffuse: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_AMBIENT_AND_DIFFUSE"
		}"
		end

	const_color_indexes: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_INDEXES"
		}"
		end

	const_light_model_two_side: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHT_MODEL_TWO_SIDE"
		}"
		end

	const_light_model_local_viewer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHT_MODEL_LOCAL_VIEWER"
		}"
		end

	const_light_model_ambient: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHT_MODEL_AMBIENT"
		}"
		end

	const_front_and_back: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FRONT_AND_BACK"
		}"
		end

	const_shade_model: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SHADE_MODEL"
		}"
		end

	const_flat: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FLAT"
		}"
		end

	const_smooth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SMOOTH"
		}"
		end

	const_color_material: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_MATERIAL"
		}"
		end

	const_color_material_face: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_MATERIAL_FACE"
		}"
		end

	const_color_material_parameter: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_MATERIAL_PARAMETER"
		}"
		end

	const_normalize: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NORMALIZE"
		}"
		end

	const_clip_plane0: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLIP_PLANE0"
		}"
		end

	const_clip_plane1: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLIP_PLANE1"
		}"
		end

	const_clip_plane2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLIP_PLANE2"
		}"
		end

	const_clip_plane3: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLIP_PLANE3"
		}"
		end

	const_clip_plane4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLIP_PLANE4"
		}"
		end

	const_clip_plane5: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLIP_PLANE5"
		}"
		end

	const_accum_red_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ACCUM_RED_BITS"
		}"
		end

	const_accum_green_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ACCUM_GREEN_BITS"
		}"
		end

	const_accum_blue_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ACCUM_BLUE_BITS"
		}"
		end

	const_accum_alpha_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ACCUM_ALPHA_BITS"
		}"
		end

	const_accum_clear_value: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ACCUM_CLEAR_VALUE"
		}"
		end

	const_accum: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ACCUM"
		}"
		end

	const_add: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ADD"
		}"
		end

	const_load: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LOAD"
		}"
		end

	const_mult: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MULT"
		}"
		end

	const_return: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RETURN"
		}"
		end

	const_alpha_test: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALPHA_TEST"
		}"
		end

	const_alpha_test_ref: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALPHA_TEST_REF"
		}"
		end

	const_alpha_test_func: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALPHA_TEST_FUNC"
		}"
		end

	const_blend: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BLEND"
		}"
		end

	const_blend_src: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BLEND_SRC"
		}"
		end

	const_blend_dst: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BLEND_DST"
		}"
		end

	const_zero: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ZERO"
		}"
		end

	const_one: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ONE"
		}"
		end

	const_src_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SRC_COLOR"
		}"
		end

	const_one_minus_src_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ONE_MINUS_SRC_COLOR"
		}"
		end

	const_src_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SRC_ALPHA"
		}"
		end

	const_one_minus_src_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ONE_MINUS_SRC_ALPHA"
		}"
		end

	const_dst_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DST_ALPHA"
		}"
		end

	const_one_minus_dst_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ONE_MINUS_DST_ALPHA"
		}"
		end

	const_dst_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DST_COLOR"
		}"
		end

	const_one_minus_dst_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ONE_MINUS_DST_COLOR"
		}"
		end

	const_src_alpha_saturate: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SRC_ALPHA_SATURATE"
		}"
		end

	const_feedback: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FEEDBACK"
		}"
		end

	const_render: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RENDER"
		}"
		end

	const_select: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SELECT"
		}"
		end

	const_point_token: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POINT_TOKEN"
		}"
		end

	const_line_token: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE_TOKEN"
		}"
		end

	const_line_reset_token: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE_RESET_TOKEN"
		}"
		end

	const_polygon_token: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POLYGON_TOKEN"
		}"
		end

	const_bitmap_token: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BITMAP_TOKEN"
		}"
		end

	const_draw_pixel_token: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DRAW_PIXEL_TOKEN"
		}"
		end

	const_copy_pixel_token: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COPY_PIXEL_TOKEN"
		}"
		end

	const_pass_through_token: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PASS_THROUGH_TOKEN"
		}"
		end

	const_feedback_buffer_pointer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FEEDBACK_BUFFER_POINTER"
		}"
		end

	const_feedback_buffer_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FEEDBACK_BUFFER_SIZE"
		}"
		end

	const_feedback_buffer_type: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FEEDBACK_BUFFER_TYPE"
		}"
		end

	const_selection_buffer_pointer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SELECTION_BUFFER_POINTER"
		}"
		end

	const_selection_buffer_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SELECTION_BUFFER_SIZE"
		}"
		end

	const_fog: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FOG"
		}"
		end

	const_fog_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FOG_MODE"
		}"
		end

	const_fog_density: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FOG_DENSITY"
		}"
		end

	const_fog_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FOG_COLOR"
		}"
		end

	const_fog_index: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FOG_INDEX"
		}"
		end

	const_fog_start: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FOG_START"
		}"
		end

	const_fog_end: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FOG_END"
		}"
		end

	const_linear: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINEAR"
		}"
		end

	const_exp: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_EXP"
		}"
		end

	const_exp2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_EXP2"
		}"
		end

	const_logic_op: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LOGIC_OP"
		}"
		end

	const_index_logic_op: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INDEX_LOGIC_OP"
		}"
		end

	const_color_logic_op: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_LOGIC_OP"
		}"
		end

	const_logic_op_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LOGIC_OP_MODE"
		}"
		end

	const_clear: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLEAR"
		}"
		end

	const_set: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SET"
		}"
		end

	const_copy: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COPY"
		}"
		end

	const_copy_inverted: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COPY_INVERTED"
		}"
		end

	const_noop: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NOOP"
		}"
		end

	const_invert: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INVERT"
		}"
		end

	const_and: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_AND"
		}"
		end

	const_nand: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NAND"
		}"
		end

	const_or: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_OR"
		}"
		end

	const_nor: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NOR"
		}"
		end

	const_xor: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_XOR"
		}"
		end

	const_equiv: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_EQUIV"
		}"
		end

	const_and_reverse: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_AND_REVERSE"
		}"
		end

	const_and_inverted: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_AND_INVERTED"
		}"
		end

	const_or_reverse: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_OR_REVERSE"
		}"
		end

	const_or_inverted: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_OR_INVERTED"
		}"
		end

	const_stencil_test: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STENCIL_TEST"
		}"
		end

	const_stencil_writemask: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STENCIL_WRITEMASK"
		}"
		end

	const_stencil_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STENCIL_BITS"
		}"
		end

	const_stencil_func: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STENCIL_FUNC"
		}"
		end

	const_stencil_value_mask: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STENCIL_VALUE_MASK"
		}"
		end

	const_stencil_ref: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STENCIL_REF"
		}"
		end

	const_stencil_fail: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STENCIL_FAIL"
		}"
		end

	const_stencil_pass_depth_pass: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STENCIL_PASS_DEPTH_PASS"
		}"
		end

	const_stencil_pass_depth_fail: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STENCIL_PASS_DEPTH_FAIL"
		}"
		end

	const_stencil_clear_value: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STENCIL_CLEAR_VALUE"
		}"
		end

	const_stencil_index: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STENCIL_INDEX"
		}"
		end

	const_keep: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_KEEP"
		}"
		end

	const_replace: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_REPLACE"
		}"
		end

	const_incr: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INCR"
		}"
		end

	const_decr: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DECR"
		}"
		end

	const_none: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NONE"
		}"
		end

	const_left: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LEFT"
		}"
		end

	const_right: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RIGHT"
		}"
		end

	const_front_left: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FRONT_LEFT"
		}"
		end

	const_front_right: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FRONT_RIGHT"
		}"
		end

	const_back_left: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BACK_LEFT"
		}"
		end

	const_back_right: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BACK_RIGHT"
		}"
		end

	const_aux0: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_AUX0"
		}"
		end

	const_aux1: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_AUX1"
		}"
		end

	const_aux2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_AUX2"
		}"
		end

	const_aux3: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_AUX3"
		}"
		end

	const_color_index: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_INDEX"
		}"
		end

	const_red: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RED"
		}"
		end

	const_green: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_GREEN"
		}"
		end

	const_blue: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BLUE"
		}"
		end

	const_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALPHA"
		}"
		end

	const_luminance: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LUMINANCE"
		}"
		end

	const_luminance_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LUMINANCE_ALPHA"
		}"
		end

	const_alpha_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALPHA_BITS"
		}"
		end

	const_red_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RED_BITS"
		}"
		end

	const_green_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_GREEN_BITS"
		}"
		end

	const_blue_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BLUE_BITS"
		}"
		end

	const_index_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INDEX_BITS"
		}"
		end

	const_subpixel_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SUBPIXEL_BITS"
		}"
		end

	const_aux_buffers: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_AUX_BUFFERS"
		}"
		end

	const_read_buffer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_READ_BUFFER"
		}"
		end

	const_draw_buffer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DRAW_BUFFER"
		}"
		end

	const_doublebuffer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DOUBLEBUFFER"
		}"
		end

	const_stereo: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STEREO"
		}"
		end

	const_bitmap: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BITMAP"
		}"
		end

	const_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR"
		}"
		end

	const_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DEPTH"
		}"
		end

	const_stencil: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STENCIL"
		}"
		end

	const_dither: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DITHER"
		}"
		end

	const_rgb: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGB"
		}"
		end

	const_rgba: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGBA"
		}"
		end

	const_max_list_nesting: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_LIST_NESTING"
		}"
		end

	const_max_attrib_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_ATTRIB_STACK_DEPTH"
		}"
		end

	const_max_modelview_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_MODELVIEW_STACK_DEPTH"
		}"
		end

	const_max_name_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_NAME_STACK_DEPTH"
		}"
		end

	const_max_projection_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_PROJECTION_STACK_DEPTH"
		}"
		end

	const_max_texture_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_TEXTURE_STACK_DEPTH"
		}"
		end

	const_max_eval_order: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_EVAL_ORDER"
		}"
		end

	const_max_lights: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_LIGHTS"
		}"
		end

	const_max_clip_planes: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_CLIP_PLANES"
		}"
		end

	const_max_texture_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_TEXTURE_SIZE"
		}"
		end

	const_max_pixel_map_table: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_PIXEL_MAP_TABLE"
		}"
		end

	const_max_viewport_dims: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_VIEWPORT_DIMS"
		}"
		end

	const_max_client_attrib_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_CLIENT_ATTRIB_STACK_DEPTH"
		}"
		end

	const_attrib_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ATTRIB_STACK_DEPTH"
		}"
		end

	const_client_attrib_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLIENT_ATTRIB_STACK_DEPTH"
		}"
		end

	const_color_clear_value: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_CLEAR_VALUE"
		}"
		end

	const_color_writemask: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_WRITEMASK"
		}"
		end

	const_current_index: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CURRENT_INDEX"
		}"
		end

	const_current_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CURRENT_COLOR"
		}"
		end

	const_current_normal: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CURRENT_NORMAL"
		}"
		end

	const_current_raster_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CURRENT_RASTER_COLOR"
		}"
		end

	const_current_raster_distance: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CURRENT_RASTER_DISTANCE"
		}"
		end

	const_current_raster_index: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CURRENT_RASTER_INDEX"
		}"
		end

	const_current_raster_position: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CURRENT_RASTER_POSITION"
		}"
		end

	const_current_raster_texture_coords: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CURRENT_RASTER_TEXTURE_COORDS"
		}"
		end

	const_current_raster_position_valid: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CURRENT_RASTER_POSITION_VALID"
		}"
		end

	const_current_texture_coords: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CURRENT_TEXTURE_COORDS"
		}"
		end

	const_index_clear_value: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INDEX_CLEAR_VALUE"
		}"
		end

	const_index_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INDEX_MODE"
		}"
		end

	const_index_writemask: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INDEX_WRITEMASK"
		}"
		end

	const_modelview_matrix: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MODELVIEW_MATRIX"
		}"
		end

	const_modelview_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MODELVIEW_STACK_DEPTH"
		}"
		end

	const_name_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NAME_STACK_DEPTH"
		}"
		end

	const_projection_matrix: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PROJECTION_MATRIX"
		}"
		end

	const_projection_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PROJECTION_STACK_DEPTH"
		}"
		end

	const_render_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RENDER_MODE"
		}"
		end

	const_rgba_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGBA_MODE"
		}"
		end

	const_texture_matrix: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_MATRIX"
		}"
		end

	const_texture_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_STACK_DEPTH"
		}"
		end

	const_viewport: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_VIEWPORT"
		}"
		end

	const_auto_normal: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_AUTO_NORMAL"
		}"
		end

	const_map1_color_4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP1_COLOR_4"
		}"
		end

	const_map1_index: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP1_INDEX"
		}"
		end

	const_map1_normal: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP1_NORMAL"
		}"
		end

	const_map1_texture_coord_1: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP1_TEXTURE_COORD_1"
		}"
		end

	const_map1_texture_coord_2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP1_TEXTURE_COORD_2"
		}"
		end

	const_map1_texture_coord_3: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP1_TEXTURE_COORD_3"
		}"
		end

	const_map1_texture_coord_4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP1_TEXTURE_COORD_4"
		}"
		end

	const_map1_vertex_3: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP1_VERTEX_3"
		}"
		end

	const_map1_vertex_4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP1_VERTEX_4"
		}"
		end

	const_map2_color_4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP2_COLOR_4"
		}"
		end

	const_map2_index: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP2_INDEX"
		}"
		end

	const_map2_normal: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP2_NORMAL"
		}"
		end

	const_map2_texture_coord_1: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP2_TEXTURE_COORD_1"
		}"
		end

	const_map2_texture_coord_2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP2_TEXTURE_COORD_2"
		}"
		end

	const_map2_texture_coord_3: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP2_TEXTURE_COORD_3"
		}"
		end

	const_map2_texture_coord_4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP2_TEXTURE_COORD_4"
		}"
		end

	const_map2_vertex_3: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP2_VERTEX_3"
		}"
		end

	const_map2_vertex_4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP2_VERTEX_4"
		}"
		end

	const_map1_grid_domain: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP1_GRID_DOMAIN"
		}"
		end

	const_map1_grid_segments: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP1_GRID_SEGMENTS"
		}"
		end

	const_map2_grid_domain: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP2_GRID_DOMAIN"
		}"
		end

	const_map2_grid_segments: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP2_GRID_SEGMENTS"
		}"
		end

	const_coeff: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COEFF"
		}"
		end

	const_domain: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DOMAIN"
		}"
		end

	const_order: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ORDER"
		}"
		end

	const_fog_hint: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FOG_HINT"
		}"
		end

	const_line_smooth_hint: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE_SMOOTH_HINT"
		}"
		end

	const_perspective_correction_hint: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PERSPECTIVE_CORRECTION_HINT"
		}"
		end

	const_point_smooth_hint: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POINT_SMOOTH_HINT"
		}"
		end

	const_polygon_smooth_hint: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POLYGON_SMOOTH_HINT"
		}"
		end

	const_dont_care: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DONT_CARE"
		}"
		end

	const_fastest: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FASTEST"
		}"
		end

	const_nicest: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NICEST"
		}"
		end

	const_scissor_test: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SCISSOR_TEST"
		}"
		end

	const_scissor_box: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SCISSOR_BOX"
		}"
		end

	const_map_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP_COLOR"
		}"
		end

	const_map_stencil: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAP_STENCIL"
		}"
		end

	const_index_shift: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INDEX_SHIFT"
		}"
		end

	const_index_offset: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INDEX_OFFSET"
		}"
		end

	const_red_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RED_SCALE"
		}"
		end

	const_red_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RED_BIAS"
		}"
		end

	const_green_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_GREEN_SCALE"
		}"
		end

	const_green_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_GREEN_BIAS"
		}"
		end

	const_blue_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BLUE_SCALE"
		}"
		end

	const_blue_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BLUE_BIAS"
		}"
		end

	const_alpha_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALPHA_SCALE"
		}"
		end

	const_alpha_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALPHA_BIAS"
		}"
		end

	const_depth_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DEPTH_SCALE"
		}"
		end

	const_depth_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DEPTH_BIAS"
		}"
		end

	const_pixel_map_s_to_s_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_S_TO_S_SIZE"
		}"
		end

	const_pixel_map_i_to_i_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_I_TO_I_SIZE"
		}"
		end

	const_pixel_map_i_to_r_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_I_TO_R_SIZE"
		}"
		end

	const_pixel_map_i_to_g_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_I_TO_G_SIZE"
		}"
		end

	const_pixel_map_i_to_b_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_I_TO_B_SIZE"
		}"
		end

	const_pixel_map_i_to_a_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_I_TO_A_SIZE"
		}"
		end

	const_pixel_map_r_to_r_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_R_TO_R_SIZE"
		}"
		end

	const_pixel_map_g_to_g_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_G_TO_G_SIZE"
		}"
		end

	const_pixel_map_b_to_b_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_B_TO_B_SIZE"
		}"
		end

	const_pixel_map_a_to_a_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_A_TO_A_SIZE"
		}"
		end

	const_pixel_map_s_to_s: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_S_TO_S"
		}"
		end

	const_pixel_map_i_to_i: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_I_TO_I"
		}"
		end

	const_pixel_map_i_to_r: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_I_TO_R"
		}"
		end

	const_pixel_map_i_to_g: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_I_TO_G"
		}"
		end

	const_pixel_map_i_to_b: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_I_TO_B"
		}"
		end

	const_pixel_map_i_to_a: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_I_TO_A"
		}"
		end

	const_pixel_map_r_to_r: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_R_TO_R"
		}"
		end

	const_pixel_map_g_to_g: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_G_TO_G"
		}"
		end

	const_pixel_map_b_to_b: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_B_TO_B"
		}"
		end

	const_pixel_map_a_to_a: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MAP_A_TO_A"
		}"
		end

	const_pack_alignment: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PACK_ALIGNMENT"
		}"
		end

	const_pack_lsb_first: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PACK_LSB_FIRST"
		}"
		end

	const_pack_row_length: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PACK_ROW_LENGTH"
		}"
		end

	const_pack_skip_pixels: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PACK_SKIP_PIXELS"
		}"
		end

	const_pack_skip_rows: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PACK_SKIP_ROWS"
		}"
		end

	const_pack_swap_bytes: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PACK_SWAP_BYTES"
		}"
		end

	const_unpack_alignment: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNPACK_ALIGNMENT"
		}"
		end

	const_unpack_lsb_first: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNPACK_LSB_FIRST"
		}"
		end

	const_unpack_row_length: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNPACK_ROW_LENGTH"
		}"
		end

	const_unpack_skip_pixels: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNPACK_SKIP_PIXELS"
		}"
		end

	const_unpack_skip_rows: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNPACK_SKIP_ROWS"
		}"
		end

	const_unpack_swap_bytes: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNPACK_SWAP_BYTES"
		}"
		end

	const_zoom_x: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ZOOM_X"
		}"
		end

	const_zoom_y: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ZOOM_Y"
		}"
		end

	const_texture_env: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_ENV"
		}"
		end

	const_texture_env_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_ENV_MODE"
		}"
		end

	const_texture_1d: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_1D"
		}"
		end

	const_texture_2d: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_2D"
		}"
		end

	const_texture_wrap_s: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_WRAP_S"
		}"
		end

	const_texture_wrap_t: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_WRAP_T"
		}"
		end

	const_texture_mag_filter: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_MAG_FILTER"
		}"
		end

	const_texture_min_filter: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_MIN_FILTER"
		}"
		end

	const_texture_env_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_ENV_COLOR"
		}"
		end

	const_texture_gen_s: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_GEN_S"
		}"
		end

	const_texture_gen_t: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_GEN_T"
		}"
		end

	const_texture_gen_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_GEN_MODE"
		}"
		end

	const_texture_border_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_BORDER_COLOR"
		}"
		end

	const_texture_width: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_WIDTH"
		}"
		end

	const_texture_height: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_HEIGHT"
		}"
		end

	const_texture_border: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_BORDER"
		}"
		end

	const_texture_components: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_COMPONENTS"
		}"
		end

	const_texture_red_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_RED_SIZE"
		}"
		end

	const_texture_green_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_GREEN_SIZE"
		}"
		end

	const_texture_blue_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_BLUE_SIZE"
		}"
		end

	const_texture_alpha_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_ALPHA_SIZE"
		}"
		end

	const_texture_luminance_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_LUMINANCE_SIZE"
		}"
		end

	const_texture_intensity_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_INTENSITY_SIZE"
		}"
		end

	const_nearest_mipmap_nearest: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NEAREST_MIPMAP_NEAREST"
		}"
		end

	const_nearest_mipmap_linear: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NEAREST_MIPMAP_LINEAR"
		}"
		end

	const_linear_mipmap_nearest: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINEAR_MIPMAP_NEAREST"
		}"
		end

	const_linear_mipmap_linear: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINEAR_MIPMAP_LINEAR"
		}"
		end

	const_object_linear: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_OBJECT_LINEAR"
		}"
		end

	const_object_plane: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_OBJECT_PLANE"
		}"
		end

	const_eye_linear: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_EYE_LINEAR"
		}"
		end

	const_eye_plane: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_EYE_PLANE"
		}"
		end

	const_sphere_map: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SPHERE_MAP"
		}"
		end

	const_decal: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DECAL"
		}"
		end

	const_modulate: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MODULATE"
		}"
		end

	const_nearest: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NEAREST"
		}"
		end

	const_repeat: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_REPEAT"
		}"
		end

	const_clamp: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLAMP"
		}"
		end

	const_s: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_S"
		}"
		end

	const_t: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_T"
		}"
		end

	const_r: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_R"
		}"
		end

	const_q: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_Q"
		}"
		end

	const_texture_gen_r: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_GEN_R"
		}"
		end

	const_texture_gen_q: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_GEN_Q"
		}"
		end

	const_vendor: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_VENDOR"
		}"
		end

	const_renderer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RENDERER"
		}"
		end

	const_version: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_VERSION"
		}"
		end

	const_extensions: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_EXTENSIONS"
		}"
		end

	const_no_error: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NO_ERROR"
		}"
		end

	const_invalid_value: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INVALID_VALUE"
		}"
		end

	const_invalid_enum: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INVALID_ENUM"
		}"
		end

	const_invalid_operation: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INVALID_OPERATION"
		}"
		end

	const_stack_overflow: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STACK_OVERFLOW"
		}"
		end

	const_stack_underflow: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STACK_UNDERFLOW"
		}"
		end

	const_out_of_memory: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_OUT_OF_MEMORY"
		}"
		end

	const_current_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CURRENT_BIT"
		}"
		end

	const_point_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POINT_BIT"
		}"
		end

	const_line_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LINE_BIT"
		}"
		end

	const_polygon_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POLYGON_BIT"
		}"
		end

	const_polygon_stipple_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POLYGON_STIPPLE_BIT"
		}"
		end

	const_pixel_mode_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PIXEL_MODE_BIT"
		}"
		end

	const_lighting_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHTING_BIT"
		}"
		end

	const_fog_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FOG_BIT"
		}"
		end

	const_depth_buffer_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DEPTH_BUFFER_BIT"
		}"
		end

	const_accum_buffer_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ACCUM_BUFFER_BIT"
		}"
		end

	const_stencil_buffer_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_STENCIL_BUFFER_BIT"
		}"
		end

	const_viewport_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_VIEWPORT_BIT"
		}"
		end

	const_transform_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TRANSFORM_BIT"
		}"
		end

	const_enable_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ENABLE_BIT"
		}"
		end

	const_color_buffer_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_BUFFER_BIT"
		}"
		end

	const_hint_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_HINT_BIT"
		}"
		end

	const_eval_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_EVAL_BIT"
		}"
		end

	const_list_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIST_BIT"
		}"
		end

	const_texture_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_BIT"
		}"
		end

	const_scissor_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SCISSOR_BIT"
		}"
		end

	const_all_attrib_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALL_ATTRIB_BITS"
		}"
		end

	const_texture_priority: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_PRIORITY"
		}"
		end

	const_texture_resident: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_RESIDENT"
		}"
		end

	const_texture_internal_format: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_INTERNAL_FORMAT"
		}"
		end

	const_alpha4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALPHA4"
		}"
		end

	const_alpha8: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALPHA8"
		}"
		end

	const_alpha12: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALPHA12"
		}"
		end

	const_alpha16: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALPHA16"
		}"
		end

	const_luminance4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LUMINANCE4"
		}"
		end

	const_luminance8: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LUMINANCE8"
		}"
		end

	const_luminance12: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LUMINANCE12"
		}"
		end

	const_luminance16: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LUMINANCE16"
		}"
		end

	const_luminance4_alpha4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LUMINANCE4_ALPHA4"
		}"
		end

	const_luminance6_alpha2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LUMINANCE6_ALPHA2"
		}"
		end

	const_luminance8_alpha8: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LUMINANCE8_ALPHA8"
		}"
		end

	const_luminance12_alpha4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LUMINANCE12_ALPHA4"
		}"
		end

	const_luminance12_alpha12: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LUMINANCE12_ALPHA12"
		}"
		end

	const_luminance16_alpha16: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LUMINANCE16_ALPHA16"
		}"
		end

	const_intensity: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INTENSITY"
		}"
		end

	const_intensity4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INTENSITY4"
		}"
		end

	const_intensity8: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INTENSITY8"
		}"
		end

	const_intensity12: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INTENSITY12"
		}"
		end

	const_intensity16: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INTENSITY16"
		}"
		end

	const_r3_g3_b2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_R3_G3_B2"
		}"
		end

	const_rgb4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGB4"
		}"
		end

	const_rgb5: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGB5"
		}"
		end

	const_rgb8: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGB8"
		}"
		end

	const_rgb10: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGB10"
		}"
		end

	const_rgb12: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGB12"
		}"
		end

	const_rgb16: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGB16"
		}"
		end

	const_rgba2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGBA2"
		}"
		end

	const_rgba4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGBA4"
		}"
		end

	const_rgb5_a1: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGB5_A1"
		}"
		end

	const_rgba8: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGBA8"
		}"
		end

	const_rgb10_a2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGB10_A2"
		}"
		end

	const_rgba12: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGBA12"
		}"
		end

	const_rgba16: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGBA16"
		}"
		end

	const_client_pixel_store_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLIENT_PIXEL_STORE_BIT"
		}"
		end

	const_client_vertex_array_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLIENT_VERTEX_ARRAY_BIT"
		}"
		end

	const_all_client_attrib_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALL_CLIENT_ATTRIB_BITS"
		}"
		end

	const_client_all_attrib_bits: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLIENT_ALL_ATTRIB_BITS"
		}"
		end

	const_rescale_normal: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RESCALE_NORMAL"
		}"
		end

	const_clamp_to_edge: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLAMP_TO_EDGE"
		}"
		end

	const_max_elements_vertices: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_ELEMENTS_VERTICES"
		}"
		end

	const_max_elements_indices: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_ELEMENTS_INDICES"
		}"
		end

	const_bgr: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BGR"
		}"
		end

	const_bgra: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BGRA"
		}"
		end

	const_unsigned_byte_3_3_2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_BYTE_3_3_2"
		}"
		end

	const_unsigned_byte_2_3_3_rev: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_BYTE_2_3_3_REV"
		}"
		end

	const_unsigned_short_5_6_5: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_SHORT_5_6_5"
		}"
		end

	const_unsigned_short_5_6_5_rev: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_SHORT_5_6_5_REV"
		}"
		end

	const_unsigned_short_4_4_4_4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_SHORT_4_4_4_4"
		}"
		end

	const_unsigned_short_4_4_4_4_rev: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_SHORT_4_4_4_4_REV"
		}"
		end

	const_unsigned_short_5_5_5_1: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_SHORT_5_5_5_1"
		}"
		end

	const_unsigned_short_1_5_5_5_rev: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_SHORT_1_5_5_5_REV"
		}"
		end

	const_unsigned_int_8_8_8_8: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_INT_8_8_8_8"
		}"
		end

	const_unsigned_int_8_8_8_8_rev: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_INT_8_8_8_8_REV"
		}"
		end

	const_unsigned_int_10_10_10_2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_INT_10_10_10_2"
		}"
		end

	const_unsigned_int_2_10_10_10_rev: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNSIGNED_INT_2_10_10_10_REV"
		}"
		end

	const_light_model_color_control: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_LIGHT_MODEL_COLOR_CONTROL"
		}"
		end

	const_single_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SINGLE_COLOR"
		}"
		end

	const_separate_specular_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SEPARATE_SPECULAR_COLOR"
		}"
		end

	const_texture_min_lod: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_MIN_LOD"
		}"
		end

	const_texture_max_lod: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_MAX_LOD"
		}"
		end

	const_texture_base_level: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_BASE_LEVEL"
		}"
		end

	const_texture_max_level: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_MAX_LEVEL"
		}"
		end

	const_smooth_point_size_range: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SMOOTH_POINT_SIZE_RANGE"
		}"
		end

	const_smooth_point_size_granularity: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SMOOTH_POINT_SIZE_GRANULARITY"
		}"
		end

	const_smooth_line_width_range: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SMOOTH_LINE_WIDTH_RANGE"
		}"
		end

	const_smooth_line_width_granularity: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SMOOTH_LINE_WIDTH_GRANULARITY"
		}"
		end

	const_aliased_point_size_range: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ALIASED_POINT_SIZE_RANGE"
		}"
		end

	const_aliased_line_width_range: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gl_const_aliased_line_width_rang"
		}"
		end

	const_pack_skip_images: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PACK_SKIP_IMAGES"
		}"
		end

	const_pack_image_height: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PACK_IMAGE_HEIGHT"
		}"
		end

	const_unpack_skip_images: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNPACK_SKIP_IMAGES"
		}"
		end

	const_unpack_image_height: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_UNPACK_IMAGE_HEIGHT"
		}"
		end

	const_texture_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_DEPTH"
		}"
		end

	const_texture_wrap_r: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_WRAP_R"
		}"
		end

	const_constant_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CONSTANT_COLOR"
		}"
		end

	const_one_minus_constant_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ONE_MINUS_CONSTANT_COLOR"
		}"
		end

	const_constant_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CONSTANT_ALPHA"
		}"
		end

	const_one_minus_constant_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ONE_MINUS_CONSTANT_ALPHA"
		}"
		end

	const_color_table: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_TABLE"
		}"
		end

	const_post_convolution_color_table: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_CONVOLUTION_COLOR_TABLE"
		}"
		end

	const_post_color_matrix_color_table: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_COLOR_MATRIX_COLOR_TABLE"
		}"
		end

	const_proxy_color_table: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PROXY_COLOR_TABLE"
		}"
		end

	const_proxy_post_convolution_color_table: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PROXY_POST_CONVOLUTION_COLOR_TABLE"
		}"
		end

	const_proxy_post_color_matrix_color_table: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE"
		}"
		end

	const_color_table_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_TABLE_SCALE"
		}"
		end

	const_color_table_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_TABLE_BIAS"
		}"
		end

	const_color_table_format: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_TABLE_FORMAT"
		}"
		end

	const_color_table_width: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_TABLE_WIDTH"
		}"
		end

	const_color_table_red_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_TABLE_RED_SIZE"
		}"
		end

	const_color_table_green_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_TABLE_GREEN_SIZE"
		}"
		end

	const_color_table_blue_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_TABLE_BLUE_SIZE"
		}"
		end

	const_color_table_alpha_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_TABLE_ALPHA_SIZE"
		}"
		end

	const_color_table_luminance_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_TABLE_LUMINANCE_SIZE"
		}"
		end

	const_color_table_intensity_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_TABLE_INTENSITY_SIZE"
		}"
		end

	const_convolution_border_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CONVOLUTION_BORDER_MODE"
		}"
		end

	const_convolution_filter_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CONVOLUTION_FILTER_SCALE"
		}"
		end

	const_convolution_filter_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CONVOLUTION_FILTER_BIAS"
		}"
		end

	const_reduce: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_REDUCE"
		}"
		end

	const_convolution_format: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CONVOLUTION_FORMAT"
		}"
		end

	const_convolution_width: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CONVOLUTION_WIDTH"
		}"
		end

	const_convolution_height: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CONVOLUTION_HEIGHT"
		}"
		end

	const_max_convolution_width: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_CONVOLUTION_WIDTH"
		}"
		end

	const_max_convolution_height: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_CONVOLUTION_HEIGHT"
		}"
		end

	const_post_convolution_red_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_CONVOLUTION_RED_SCALE"
		}"
		end

	const_post_convolution_green_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_CONVOLUTION_GREEN_SCALE"
		}"
		end

	const_post_convolution_blue_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_CONVOLUTION_BLUE_SCALE"
		}"
		end

	const_post_convolution_alpha_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_CONVOLUTION_ALPHA_SCALE"
		}"
		end

	const_post_convolution_red_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_CONVOLUTION_RED_BIAS"
		}"
		end

	const_post_convolution_green_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_CONVOLUTION_GREEN_BIAS"
		}"
		end

	const_post_convolution_blue_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_CONVOLUTION_BLUE_BIAS"
		}"
		end

	const_post_convolution_alpha_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_CONVOLUTION_ALPHA_BIAS"
		}"
		end

	const_constant_border: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CONSTANT_BORDER"
		}"
		end

	const_replicate_border: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_REPLICATE_BORDER"
		}"
		end

	const_convolution_border_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CONVOLUTION_BORDER_COLOR"
		}"
		end

	const_color_matrix: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_MATRIX"
		}"
		end

	const_color_matrix_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COLOR_MATRIX_STACK_DEPTH"
		}"
		end

	const_max_color_matrix_stack_depth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_COLOR_MATRIX_STACK_DEPTH"
		}"
		end

	const_post_color_matrix_red_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_COLOR_MATRIX_RED_SCALE"
		}"
		end

	const_post_color_matrix_green_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_COLOR_MATRIX_GREEN_SCALE"
		}"
		end

	const_post_color_matrix_blue_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_COLOR_MATRIX_BLUE_SCALE"
		}"
		end

	const_post_color_matrix_alpha_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_COLOR_MATRIX_ALPHA_SCALE"
		}"
		end

	const_post_color_matrix_red_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_COLOR_MATRIX_RED_BIAS"
		}"
		end

	const_post_color_matrix_green_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_COLOR_MATRIX_GREEN_BIAS"
		}"
		end

	const_post_color_matrix_blue_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_COLOR_MATRIX_BLUE_BIAS"
		}"
		end

	const_post_color_matrix_alpha_bias: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_POST_COLOR_MATRIX_ALPHA_BIAS"
		}"
		end

	const_histogram: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_HISTOGRAM"
		}"
		end

	const_proxy_histogram: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PROXY_HISTOGRAM"
		}"
		end

	const_histogram_width: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_HISTOGRAM_WIDTH"
		}"
		end

	const_histogram_format: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_HISTOGRAM_FORMAT"
		}"
		end

	const_histogram_red_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_HISTOGRAM_RED_SIZE"
		}"
		end

	const_histogram_green_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_HISTOGRAM_GREEN_SIZE"
		}"
		end

	const_histogram_blue_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_HISTOGRAM_BLUE_SIZE"
		}"
		end

	const_histogram_alpha_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_HISTOGRAM_ALPHA_SIZE"
		}"
		end

	const_histogram_luminance_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_HISTOGRAM_LUMINANCE_SIZE"
		}"
		end

	const_histogram_sink: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_HISTOGRAM_SINK"
		}"
		end

	const_minmax: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MINMAX"
		}"
		end

	const_minmax_format: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MINMAX_FORMAT"
		}"
		end

	const_minmax_sink: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MINMAX_SINK"
		}"
		end

	const_table_too_large: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TABLE_TOO_LARGE"
		}"
		end

	const_blend_equation: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BLEND_EQUATION"
		}"
		end

	const_min: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MIN"
		}"
		end

	const_max: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX"
		}"
		end

	const_func_add: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FUNC_ADD"
		}"
		end

	const_func_subtract: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FUNC_SUBTRACT"
		}"
		end

	const_func_reverse_subtract: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_FUNC_REVERSE_SUBTRACT"
		}"
		end

	const_blend_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_BLEND_COLOR"
		}"
		end

	const_texture0: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE0"
		}"
		end

	const_texture1: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE1"
		}"
		end

	const_texture2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE2"
		}"
		end

	const_texture3: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE3"
		}"
		end

	const_texture4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE4"
		}"
		end

	const_texture5: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE5"
		}"
		end

	const_texture6: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE6"
		}"
		end

	const_texture7: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE7"
		}"
		end

	const_texture8: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE8"
		}"
		end

	const_texture9: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE9"
		}"
		end

	const_texture10: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE10"
		}"
		end

	const_texture11: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE11"
		}"
		end

	const_texture12: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE12"
		}"
		end

	const_texture13: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE13"
		}"
		end

	const_texture14: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE14"
		}"
		end

	const_texture15: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE15"
		}"
		end

	const_texture16: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE16"
		}"
		end

	const_texture17: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE17"
		}"
		end

	const_texture18: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE18"
		}"
		end

	const_texture19: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE19"
		}"
		end

	const_texture20: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE20"
		}"
		end

	const_texture21: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE21"
		}"
		end

	const_texture22: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE22"
		}"
		end

	const_texture23: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE23"
		}"
		end

	const_texture24: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE24"
		}"
		end

	const_texture25: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE25"
		}"
		end

	const_texture26: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE26"
		}"
		end

	const_texture27: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE27"
		}"
		end

	const_texture28: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE28"
		}"
		end

	const_texture29: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE29"
		}"
		end

	const_texture30: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE30"
		}"
		end

	const_texture31: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE31"
		}"
		end

	const_active_texture: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ACTIVE_TEXTURE"
		}"
		end

	const_client_active_texture: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLIENT_ACTIVE_TEXTURE"
		}"
		end

	const_max_texture_units: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_TEXTURE_UNITS"
		}"
		end

	const_normal_map: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NORMAL_MAP"
		}"
		end

	const_reflection_map: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_REFLECTION_MAP"
		}"
		end

	const_texture_cube_map: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_CUBE_MAP"
		}"
		end

	const_texture_binding_cube_map: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_BINDING_CUBE_MAP"
		}"
		end

	const_texture_cube_map_positive_x: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_CUBE_MAP_POSITIVE_X"
		}"
		end

	const_texture_cube_map_negative_x: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_CUBE_MAP_NEGATIVE_X"
		}"
		end

	const_texture_cube_map_positive_y: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_CUBE_MAP_POSITIVE_Y"
		}"
		end

	const_texture_cube_map_negative_y: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_CUBE_MAP_NEGATIVE_Y"
		}"
		end

	const_texture_cube_map_positive_z: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_CUBE_MAP_POSITIVE_Z"
		}"
		end

	const_texture_cube_map_negative_z: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_CUBE_MAP_NEGATIVE_Z"
		}"
		end

	const_proxy_texture_cube_map: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PROXY_TEXTURE_CUBE_MAP"
		}"
		end

	const_max_cube_map_texture_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MAX_CUBE_MAP_TEXTURE_SIZE"
		}"
		end

	const_compressed_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COMPRESSED_ALPHA"
		}"
		end

	const_compressed_luminance: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COMPRESSED_LUMINANCE"
		}"
		end

	const_compressed_luminance_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COMPRESSED_LUMINANCE_ALPHA"
		}"
		end

	const_compressed_intensity: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COMPRESSED_INTENSITY"
		}"
		end

	const_compressed_rgb: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COMPRESSED_RGB"
		}"
		end

	const_compressed_rgba: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COMPRESSED_RGBA"
		}"
		end

	const_texture_compression_hint: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_COMPRESSION_HINT"
		}"
		end

	const_texture_compressed_image_size: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_COMPRESSED_IMAGE_SIZE"
		}"
		end

	const_texture_compressed: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TEXTURE_COMPRESSED"
		}"
		end

	const_num_compressed_texture_formats: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_NUM_COMPRESSED_TEXTURE_FORMATS"
		}"
		end

	const_compressed_texture_formats: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COMPRESSED_TEXTURE_FORMATS"
		}"
		end

	const_multisample: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MULTISAMPLE"
		}"
		end

	const_sample_alpha_to_coverage: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SAMPLE_ALPHA_TO_COVERAGE"
		}"
		end

	const_sample_alpha_to_one: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SAMPLE_ALPHA_TO_ONE"
		}"
		end

	const_sample_coverage: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SAMPLE_COVERAGE"
		}"
		end

	const_sample_buffers: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SAMPLE_BUFFERS"
		}"
		end

	const_samples: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SAMPLES"
		}"
		end

	const_sample_coverage_value: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SAMPLE_COVERAGE_VALUE"
		}"
		end

	const_sample_coverage_invert: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SAMPLE_COVERAGE_INVERT"
		}"
		end

	const_multisample_bit: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_MULTISAMPLE_BIT"
		}"
		end

	const_transpose_modelview_matrix: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TRANSPOSE_MODELVIEW_MATRIX"
		}"
		end

	const_transpose_projection_matrix: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TRANSPOSE_PROJECTION_MATRIX"
		}"
		end

	const_transpose_texture_matrix: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TRANSPOSE_TEXTURE_MATRIX"
		}"
		end

	const_transpose_color_matrix: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_TRANSPOSE_COLOR_MATRIX"
		}"
		end

	const_combine: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COMBINE"
		}"
		end

	const_combine_rgb: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COMBINE_RGB"
		}"
		end

	const_combine_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_COMBINE_ALPHA"
		}"
		end

	const_source0_rgb: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SOURCE0_RGB"
		}"
		end

	const_source1_rgb: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SOURCE1_RGB"
		}"
		end

	const_source2_rgb: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SOURCE2_RGB"
		}"
		end

	const_source0_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SOURCE0_ALPHA"
		}"
		end

	const_source1_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SOURCE1_ALPHA"
		}"
		end

	const_source2_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SOURCE2_ALPHA"
		}"
		end

	const_operand0_rgb: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_OPERAND0_RGB"
		}"
		end

	const_operand1_rgb: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_OPERAND1_RGB"
		}"
		end

	const_operand2_rgb: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_OPERAND2_RGB"
		}"
		end

	const_operand0_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_OPERAND0_ALPHA"
		}"
		end

	const_operand1_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_OPERAND1_ALPHA"
		}"
		end

	const_operand2_alpha: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_OPERAND2_ALPHA"
		}"
		end

	const_rgb_scale: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_RGB_SCALE"
		}"
		end

	const_add_signed: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_ADD_SIGNED"
		}"
		end

	const_interpolate: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_INTERPOLATE"
		}"
		end

	const_subtract: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_SUBTRACT"
		}"
		end

	const_constant: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CONSTANT"
		}"
		end

	const_primary_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PRIMARY_COLOR"
		}"
		end

	const_previous: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_PREVIOUS"
		}"
		end

	const_dot3_rgb: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DOT3_RGB"
		}"
		end

	const_dot3_rgba: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_DOT3_RGBA"
		}"
		end

	const_clamp_to_border: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GL_CLAMP_TO_BORDER"
		}"
		end

	clear_index (c: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glClearIndex"
		}"
		end

	clear_color (red, green, blue, alpha: REAL_32) is
		require
			red.in_range(0, 1)
			green.in_range(0, 1)
			blue.in_range(0, 1)
			alpha.in_range(0, 1)
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glClearColor"
		}"
		end

	clear (mask: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glClear"
		}"
		end

	index_mask (mask: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIndexMask"
		}"
		end

	color_mask (red, green, blue, alpha: INTEGER_8) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColorMask"
		}"
		end

	alpha_func (func: INTEGER; ref: REAL_32) is
		require
			ref.in_range(0, 1)
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glAlphaFunc"
		}"
		end

	blend_func (sfactor, dfactor: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glBlendFunc"
		}"
		end

	logic_op (opcode: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLogicOp"
		}"
		end

	cull_face (mode: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCullFace"
		}"
		end

	front_face (mode: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glFrontFace"
		}"
		end

	point_size (size: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPointSize"
		}"
		end

	line_width (width: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLineWidth"
		}"
		end

	line_stipple (factor: INTEGER; pattern: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLineStipple"
		}"
		end

	polygon_mode (face, mode: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPolygonMode"
		}"
		end

	polygon_offset (factor, units: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPolygonOffset"
		}"
		end

	polygon_stipple (mask: POINTER) is
		require
			mask.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPolygonStipple"
		}"
		end

	get_polygon_stipple (mask: POINTER) is
		require
			mask.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetPolygonStipple"
		}"
		end

	edge_flag (flag: INTEGER_8) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEdgeFlag"
		}"
		end

	edge_flagv (flag: POINTER) is
		require
			flag.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEdgeFlagv"
		}"
		end

	scissor (x, y, width, height: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glScissor"
		}"
		end

	clip_plane (plane: INTEGER; equation: POINTER) is
		require
			equation.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glClipPlane"
		}"
		end

	get_clip_plane (plane: INTEGER; equation: POINTER) is
		require
			equation.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetClipPlane"
		}"
		end

	draw_buffer (mode: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glDrawBuffer"
		}"
		end

	read_buffer (mode: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glReadBuffer"
		}"
		end

	enable (cap: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEnable"
		}"
		end

	disable (cap: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glDisable"
		}"
		end

	is_enabled (cap: INTEGER): BOOLEAN is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIsEnabled"
		}"
		end

	enable_client_state (cap: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEnableClientState"
		}"
		end

	disable_client_state (cap: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glDisableClientState"
		}"
		end

	get_booleanv (pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetBooleanv"
		}"
		end

	get_doublev (pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetDoublev"
		}"
		end

	get_floatv (pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetFloatv"
		}"
		end

	get_integerv (pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetIntegerv"
		}"
		end

	push_attrib (mask: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPushAttrib"
		}"
		end

	pop_attrib is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPopAttrib()"
		}"
		end

	push_client_attrib (mask: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPushClientAttrib"
		}"
		end

	pop_client_attrib is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPopClientAttrib()"
		}"
		end

	render_mode (mode: INTEGER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRenderMode"
		}"
		end

	get_error: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetError()"
		}"
		end

	get_string (name: INTEGER): POINTER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetString"
		}"
		ensure
			Result.is_not_null
		end

	finish is
			-- Blocks until notification is received from the graphic 
			-- hardware that the drawing is complete. Cost is higher 
			-- than "flush" because of round-trip communication.
			-- May be usefull when synchronisation is needed.
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glFinish()"
		}"
		end

	flush is
			-- Does not wait for the drawing to complete, just forces 
			-- the drawing to begin execution.
			--
			-- See also "finish".
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glFlush()"
		}"
		end

	hint (target, mode: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glHint"
		}"
		end

	clear_depth (depth: REAL_64) is
		require
			depth.in_range(0, 1)
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glClearDepth"
		}"
		end

	depth_func (func: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glDepthFunc"
		}"
		end

	depth_mask (flag: INTEGER_8) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glDepthMask"
		}"
		end

	depth_range (near_val, far_val: REAL_64) is
		require
			near_val.in_range(0, 1)
			far_val.in_range(0, 1)
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gl_depth_rang"
		}"
		end

	clear_accum (red, green, blue, alpha: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glClearAccum"
		}"
		end

	accum (op: INTEGER; value: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glAccum"
		}"
		end

	matrix_mode (mode: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMatrixMode"
		}"
		end

	ortho (left, right, bottom, top, near_val, far_val: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glOrtho"
		}"
		end

	frustum (left, right, bottom, top, near_val, far_fal: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glFrustum"
		}"
		end

	viewport (x, y, width, height: INTEGER) is
			-- `x' and `y' are lower left corner of the viewport rectangle, in pixels.
			-- `width' and `height' specify the size of the viewport.
			-- Coordinates in range [-1, 1] from the normalized device 
			-- are mapped to the range [x, x + width] or [y, y + height] 
			-- in window coordinates.
			--*** PH 12/07/2006: why not x + width - 1?
		require
			width > 0
			height > 0
			--*** not smart_gl.has_begun
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glViewport"
		}"
		end

	push_matrix is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPushMatrix()"
		}"
		end

	pop_matrix is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPopMatrix()"
		}"
		end

	load_identity is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLoadIdentity()"
		}"
		end

	load_matrixd (m: POINTER) is
		require
			m.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLoadMatrixd"
		}"
		end

	load_matrixf (m: POINTER) is
		require
			m.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLoadMatrixf"
		}"
		end

	mult_matrixd (m: POINTER) is
		require
			m.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultMatrixd"
		}"
		end

	mult_matrixf (m: POINTER) is
		require
			m.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultMatrixf"
		}"
		end

	rotated (angle, x, y, z: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRotated"
		}"
		end

	rotatef (angle, x, y, z: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRotatef"
		}"
		end

	scaled (x, y, z: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glScaled"
		}"
		end

	scalef (x, y, z: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glScalef"
		}"
		end

	translated (x, y, z: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTranslated"
		}"
		end

	translatef (x, y, z: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTranslatef"
		}"
		end

	is_list (list: INTEGER): BOOLEAN is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIsList"
		}"
		end

	delete_lists (list, range: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glDeleteLists"
		}"
		end

	gen_lists (range: INTEGER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGenLists"
		}"
		end

	new_list (list, mode: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glNewList"
		}"
		end

	end_list is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEndList()"
		}"
		end

	call_list (list: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCallList"
		}"
		end

	call_lists (n, type: INTEGER; lists: POINTER) is
		require
			lists.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCallLists"
		}"
		end

	list_base (base: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glListBase"
		}"
		end

	begin (mode: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glBegin"
		}"
		end

	gl_end is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEnd()"
		}"
		end

	vertex2d (x, y: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex2d"
		}"
		end

	vertex2f (x, y: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex2f"
		}"
		end

	vertex2i (x, y: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex2i"
		}"
		end

	vertex2s (x, y: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex2s"
		}"
		end

	vertex3d (x, y, z: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex3d"
		}"
		end

	vertex3f (x, y, z: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex3f"
		}"
		end

	vertex3i (x, y, z: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex3i"
		}"
		end

	vertex3s (x, y, z: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex3s"
		}"
		end

	vertex4d (x, y, z, w: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex4d"
		}"
		end

	vertex4f (x, y, z, w: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex4f"
		}"
		end

	vertex4i (x, y, z, w: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex4i"
		}"
		end

	vertex4s (x, y, z, w: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex4s"
		}"
		end

	vertex2dv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex2dv"
		}"
		end

	vertex2fv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex2fv"
		}"
		end

	vertex2iv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex2iv"
		}"
		end

	vertex2sv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex2sv"
		}"
		end

	vertex3dv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex3dv"
		}"
		end

	vertex3fv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex3fv"
		}"
		end

	vertex3iv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex3iv"
		}"
		end

	vertex3sv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex3sv"
		}"
		end

	vertex4dv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex4dv"
		}"
		end

	vertex4fv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex4fv"
		}"
		end

	vertex4iv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex4iv"
		}"
		end

	vertex4sv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertex4sv"
		}"
		end

	normal3b (nx, ny, nz: INTEGER_8) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glNormal3b"
		}"
		end

	normal3d (nx, ny, nz: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glNormal3d"
		}"
		end

	normal3f (nx, ny, nz: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glNormal3f"
		}"
		end

	normal3i (nx, ny, nz: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glNormal3i"
		}"
		end

	normal3s (nx, ny, nz: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glNormal3s"
		}"
		end

	normal3bv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glNormal3bv"
		}"
		end

	normal3dv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glNormal3dv"
		}"
		end

	normal3fv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glNormal3fv"
		}"
		end

	normal3iv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glNormal3iv"
		}"
		end

	normal3sv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glNormal3sv"
		}"
		end

	indexd (c: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIndexd"
		}"
		end

	indexf (c: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIndexf"
		}"
		end

	indexi (c: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIndexi"
		}"
		end

	indexs (c: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIndexs"
		}"
		end

	indexub (c: INTEGER_8) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIndexub"
		}"
		end

	indexdv (c: POINTER) is
		require
			c.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIndexdv"
		}"
		end

	indexfv (c: POINTER) is
		require
			c.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIndexfv"
		}"
		end

	indexiv (c: POINTER) is
		require
			c.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIndexiv"
		}"
		end

	indexsv (c: POINTER) is
		require
			c.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIndexsv"
		}"
		end

	indexubv (c: POINTER) is
		require
			c.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIndexubv"
		}"
		end

	color3b (red, green, blue: INTEGER_8) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3b"
		}"
		end

	color3d (red, green, blue: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3d"
		}"
		end

	color3f (red, green, blue: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3f"
		}"
		end

	color3i (red, green, blue: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3i"
		}"
		end

	color3s (red, green, blue: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3s"
		}"
		end

	color3ub (red, green, blue: INTEGER_8) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3ub"
		}"
		end

	color3ui (red, green, blue: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3ui"
		}"
		end

	color3us (red, green, blue: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3us"
		}"
		end

	color4b (red, green, blue, alpha: INTEGER_8) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4b"
		}"
		end

	color4d (red, green, blue, alpha: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4d"
		}"
		end

	color4f (red, green, blue, alpha: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4f"
		}"
		end

	color4i (red, green, blue, alpha: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4i"
		}"
		end

	color4s (red, green, blue, alpha: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4s"
		}"
		end

	color4ub (red, green, blue, alpha: INTEGER_8) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4ub"
		}"
		end

	color4ui (red, green, blue, alpha: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4ui"
		}"
		end

	color4us (red, green, blue, alpha: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4us"
		}"
		end

	color3bv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3bv"
		}"
		end

	color3dv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3dv"
		}"
		end

	color3fv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3fv"
		}"
		end

	color3iv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3iv"
		}"
		end

	color3sv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3sv"
		}"
		end

	color3ubv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3ubv"
		}"
		end

	color3uiv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3uiv"
		}"
		end

	color3usv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor3usv"
		}"
		end

	color4bv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4bv"
		}"
		end

	color4dv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4dv"
		}"
		end

	color4fv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4fv"
		}"
		end

	color4iv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4iv"
		}"
		end

	color4sv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4sv"
		}"
		end

	color4ubv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4ubv"
		}"
		end

	color4uiv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4uiv"
		}"
		end

	color4usv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColor4usv"
		}"
		end

	tex_coord1d (s: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord1d"
		}"
		end

	tex_coord1f (s: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord1f"
		}"
		end

	tex_coord1i (s: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord1i"
		}"
		end

	tex_coord1s (s: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord1s"
		}"
		end

	tex_coord2d (s, t: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord2d"
		}"
		end

	tex_coord2f (s, t: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord2f"
		}"
		end

	tex_coord2i (s, t: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord2i"
		}"
		end

	tex_coord2s (s, t: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord2s"
		}"
		end

	tex_coord3d (s, t, r: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord3d"
		}"
		end

	tex_coord3f (s, t, r: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord3f"
		}"
		end

	tex_coord3i (s, t, r: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord3i"
		}"
		end

	tex_coord3s (s, t, r: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord3s"
		}"
		end

	tex_coord4d (s, t, r, q: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord4d"
		}"
		end

	tex_coord4f (s, t, r, q: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord4f"
		}"
		end

	tex_coord4i (s, t, r, q: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord4i"
		}"
		end

	tex_coord4s (s, t, r, q: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord4s"
		}"
		end

	tex_coord1dv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord1dv"
		}"
		end

	tex_coord1fv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord1fv"
		}"
		end

	tex_coord1iv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord1iv"
		}"
		end

	tex_coord1sv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord1sv"
		}"
		end

	tex_coord2dv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord2dv"
		}"
		end

	tex_coord2fv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord2fv"
		}"
		end

	tex_coord2iv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord2iv"
		}"
		end

	tex_coord2sv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord2sv"
		}"
		end

	tex_coord3dv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord3dv"
		}"
		end

	tex_coord3fv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord3fv"
		}"
		end

	tex_coord3iv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord3iv"
		}"
		end

	tex_coord3sv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord3sv"
		}"
		end

	tex_coord4dv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord4dv"
		}"
		end

	tex_coord4fv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord4fv"
		}"
		end

	tex_coord4iv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord4iv"
		}"
		end

	tex_coord4sv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoord4sv"
		}"
		end

	raster_pos2d (x, y: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos2d"
		}"
		end

	raster_pos2f (x, y: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos2f"
		}"
		end

	raster_pos2i (x, y: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos2i"
		}"
		end

	raster_pos2s (x, y: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos2s"
		}"
		end

	raster_pos3d (x, y, z: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos3d"
		}"
		end

	raster_pos3f (x, y, z: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos3f"
		}"
		end

	raster_pos3i (x, y, z: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos3i"
		}"
		end

	raster_pos3s (x, y, z: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos3s"
		}"
		end

	raster_pos4d (x, y, z, w: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos4d"
		}"
		end

	raster_pos4f (x, y, z, w: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos4f"
		}"
		end

	raster_pos4i (x, y, z, w: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos4i"
		}"
		end

	raster_pos4s (x, y, z, w: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos4s"
		}"
		end

	raster_pos2dv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos2dv"
		}"
		end

	raster_pos2fv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos2fv"
		}"
		end

	raster_pos2iv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos2iv"
		}"
		end

	raster_pos2sv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos2sv"
		}"
		end

	raster_pos3dv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos3dv"
		}"
		end

	raster_pos3fv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos3fv"
		}"
		end

	raster_pos3iv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos3iv"
		}"
		end

	raster_pos3sv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos3sv"
		}"
		end

	raster_pos4dv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos4dv"
		}"
		end

	raster_pos4fv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos4fv"
		}"
		end

	raster_pos4iv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos4iv"
		}"
		end

	raster_pos4sv (v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRasterPos4sv"
		}"
		end

	rectd (x1, y1, x2, y2: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRectd"
		}"
		end

	rectf (x1, y1, x2, y2: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRectf"
		}"
		end

	recti (x1, y1, x2, y2: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRecti"
		}"
		end

	rects (x1, y1, x2, y2: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRects"
		}"
		end

	rectdv (v1, v2: POINTER) is
		require
			v1.is_not_null
			v2.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRectdv"
		}"
		end

	rectfv (v1, v2: POINTER) is
		require
			v1.is_not_null
			v2.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRectfv"
		}"
		end

	rectiv (v1, v2: POINTER) is
		require
			v1.is_not_null
			v2.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRectiv"
		}"
		end

	rectsv (v1, v2: POINTER) is
		require
			v1.is_not_null
			v2.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glRectsv"
		}"
		end

	vertex_pointer (size, type, strid: INTEGER; ptr: POINTER) is
		require
			ptr.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glVertexPointer"
		}"
		end

	normal_pointer (type, stride: INTEGER; ptr: POINTER) is
		require
			ptr.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glNormalPointer"
		}"
		end

	color_pointer (size, type, stride: INTEGER; ptr: POINTER) is
		require
			ptr.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColorPointer"
		}"
		end

	index_pointer (type, stride: INTEGER; ptr: POINTER) is
		require
			ptr.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIndexPointer"
		}"
		end

	tex_coord_pointer (size, type, stride: INTEGER; ptr: POINTER) is
		require
			ptr.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexCoordPointer"
		}"
		end

	edge_flag_pointer (stride: INTEGER; ptr: POINTER) is
		require
			ptr.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEdgeFlagPointer"
		}"
		end

	get_pointerv (pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetPointerv"
		}"
		end

	array_element (i: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glArrayElement"
		}"
		end

	draw_arrays (mode, first, count: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glDrawArrays"
		}"
		end

	draw_elements (mode, count, type: INTEGER; indices: POINTER) is
		require
			indices.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glDrawElements"
		}"
		end

	interleaved_arrays (format, stride: INTEGER; pointer: POINTER) is
		require
			pointer.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glInterleavedArrays"
		}"
		end

	shade_model (mode: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glShadeModel"
		}"
		end

	lightf (light, pname: INTEGER; param: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLightf"
		}"
		end

	lighti (light, pname, param: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLighti"
		}"
		end

	lightfv (light, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLightfv"
		}"
		end

	lightiv (light, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLightiv"
		}"
		end

	get_lightfv (light, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetLightfv"
		}"
		end

	get_lightiv (light, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetLightiv"
		}"
		end

	light_modelf (pname: INTEGER; param: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLightModelf"
		}"
		end

	light_modeli (pname, param: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLightModeli"
		}"
		end

	light_modelfv (pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLightModelfv"
		}"
		end

	light_modeliv (pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLightModeliv"
		}"
		end

	materialf (face, pname: INTEGER; param: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMaterialf"
		}"
		end

	materiali (face, pname, param: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMateriali"
		}"
		end

	materialfv (face, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMaterialfv"
		}"
		end

	materialiv (face, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMaterialiv"
		}"
		end

	get_materialfv (face, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetMaterialfv"
		}"
		end

	get_materialiv (face, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetMaterialiv"
		}"
		end

	color_material (face, mode: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColorMaterial"
		}"
		end

	pixel_zoom (xfactor, yfactor: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPixelZoom"
		}"
		end

	pixel_storef (pname: INTEGER; param: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPixelStoref"
		}"
		end

	pixel_storei (pname, param: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPixelStorei"
		}"
		end

	pixel_transferf (pname: INTEGER; param: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPixelTransferf"
		}"
		end

	pixel_transferi (pname, param: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPixelTransferi"
		}"
		end

	pixel_mapfv (map, mapsize: INTEGER; values: POINTER) is
		require
			values.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPixelMapfv"
		}"
		end

	pixel_mapuiv (map, mapsize: INTEGER; values: POINTER) is
		require
			values.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPixelMapuiv"
		}"
		end

	pixel_mapusv (map, mapsize: INTEGER; values: POINTER) is
		require
			values.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPixelMapusv"
		}"
		end

	get_pixel_mapfv (map: INTEGER; values: POINTER) is
		require
			values.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetPixelMapfv"
		}"
		end

	get_pixel_mapuiv (map: INTEGER; values: POINTER) is
		require
			values.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetPixelMapuiv"
		}"
		end

	get_pixel_mapusv (map: INTEGER; values: POINTER) is
		require
			values.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetPixelMapusv"
		}"
		end

	bitmap (width, height: INTEGER; xorig, yorig, xmove, ymove: REAL_32; bmap: POINTER) is
		require
			bmap.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glBitmap"
		}"
		end

	read_pixels (x, y, width, height, format, type: INTEGER; pixels: POINTER) is
		require
			pixels.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glReadPixels"
		}"
		end

	draw_pixels (width, height, format, type: INTEGER; pixels: POINTER) is
		require
			pixels.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glDrawPixels"
		}"
		end

	copy_pixels (x, y, width, height, type: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCopyPixels"
		}"
		end

	stencil_func (func, ref, mask: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glStencilFunc"
		}"
		end

	stencil_mask (mask: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glStencilMask"
		}"
		end

	stencil_op (fail, zfail, zpass: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glStencilOp"
		}"
		end

	clear_stencil (s: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glClearStencil"
		}"
		end

	tex_gend (coord, pname: INTEGER; param: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexGend"
		}"
		end

	tex_genf (coord, pname: INTEGER; param: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexGenf"
		}"
		end

	tex_geni (coord, pname, param: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexGeni"
		}"
		end

	tex_gendv (coord, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexGendv"
		}"
		end

	tex_genfv (coord, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexGenfv"
		}"
		end

	tex_geniv (coord, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexGeniv"
		}"
		end

	get_tex_gendv (coord, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetTexGendv"
		}"
		end

	get_tex_genfv (coord, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetTexGenfv"
		}"
		end

	get_tex_geniv (coord, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetTexGeniv"
		}"
		end

	tex_envf (target, pname: INTEGER; param: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexEnvf"
		}"
		end

	tex_envi (target, pname, param: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexEnvi"
		}"
		end

	tex_envfv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexEnvfv"
		}"
		end

	tex_enviv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexEnviv"
		}"
		end

	get_tex_envfv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetTexEnvfv"
		}"
		end

	get_tex_enviv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetTexEnviv"
		}"
		end

	tex_parameterf (target, pname: INTEGER; param: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexParameterf"
		}"
		end

	tex_parameteri (target, pname, param: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexParameteri"
		}"
		end

	tex_parameterfv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexParameterfv"
		}"
		end

	tex_parameteriv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexParameteriv"
		}"
		end

	get_tex_parameterfv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetTexParameterfv"
		}"
		end

	get_tex_parameteriv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetTexParameteriv"
		}"
		end

	get_tex_level_parameterfv (target, level, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetTexLevelParameterfv"
		}"
		end

	get_tex_level_parameteriv (target, level, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetTexLevelParameteriv"
		}"
		end

	tex_image_1d (target, level, internal_format, width, border, format, type: INTEGER; pixels: POINTER) is
		require
			pixels.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexImage1D"
		}"
		end

	tex_image_2d (target, level, internal_format, width, height, border, format, type: INTEGER; pixels: POINTER) is
		require
			pixels.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexImage2D"
		}"
		end

	get_tex_image (target, level, format, type: INTEGER; pixels: POINTER) is
		require
			pixels.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetTexImage"
		}"
		end

	gen_textures (n: INTEGER; textures: POINTER) is
		require
			textures.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGenTextures"
		}"
		end

	delete_textures (n: INTEGER; textures: POINTER) is
		require
			textures.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glDeleteTextures"
		}"
		end

	bind_texture (target, texture: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glBindTexture"
		}"
		end

	prioritize_textures (n: INTEGER; textures, priorities: POINTER) is
		require
			textures.is_not_null
			priorities.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPrioritizeTextures"
		}"
		end

	are_textures_resident (n: INTEGER; textures, residences: POINTER): BOOLEAN is
		require
			textures.is_not_null
			residences.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glAreTexturesResident"
		}"
		end

	is_texture (texture: INTEGER): BOOLEAN is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glIsTexture"
		}"
		end

	tex_sub_image_1d (target, level, xoffset, width, format, type: INTEGER; pixels: POINTER) is
		require
			pixels.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexSubImage1D"
		}"
		end

	tex_sub_image_2d (target, level, xoffset, yoffset, width, height, format, type: INTEGER; pixels: POINTER) is
		require
			pixels.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexSubImage2D"
		}"
		end

	copy_tex_image_1d (target, level, internal_format, x, y, width, border: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCopyTexImage1D"
		}"
		end

	copy_tex_image_2d (target, level, internal_format, x, y, width, height, border: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCopyTexImage2D"
		}"
		end

	copy_tex_sub_image_1d (target, level, xoffset, x, y, width: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCopyTexSubImage1D"
		}"
		end

	copy_tex_sub_image_2d (target, level, xoffset, yoffset, x, y, width, height: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCopyTexSubImage2D"
		}"
		end

	map1d (target: INTEGER; u1, u2: REAL_64; stride, order: INTEGER; points: POINTER) is
		require
			points.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMap1d"
		}"
		end

	map1f (target: INTEGER; u1, u2: REAL_32; stride, order: INTEGER; points: POINTER) is
		require
			points.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMap1f"
		}"
		end

	map2d (target: INTEGER; u1, u2: REAL_64; ustride, uorder: INTEGER; v1, v2: REAL_64; vstride, vorder: INTEGER
		pixels: POINTER) is
		require
			pixels.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMap2d"
		}"
		end

	map2f (target: INTEGER; u1, u2: REAL_32; ustride, uorder: INTEGER; v1, v2: REAL_32; vstride, vorder: INTEGER
		pixels: POINTER) is
		require
			pixels.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMap2f"
		}"
		end

	get_mapdv (target, query: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetMapdv"
		}"
		end

	get_mapfv (target, query: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetMapfv"
		}"
		end

	get_mapiv (target, query: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetMapiv"
		}"
		end

	eval_coord1d (u: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEvalCoord1d"
		}"
		end

	eval_coord1f (u: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEvalCoord1f"
		}"
		end

	eval_coord1dv (u: POINTER) is
		require
			u.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEvalCoord1dv"
		}"
		end

	eval_coord1fv (u: POINTER) is
		require
			u.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEvalCoord1fv"
		}"
		end

	eval_coord2d (u, v: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEvalCoord2d"
		}"
		end

	eval_coord2f (u, v: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEvalCoord2f"
		}"
		end

	eval_coord2dv (u: POINTER) is
		require
			u.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEvalCoord2dv"
		}"
		end

	eval_coord2fv (u: POINTER) is
		require
			u.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEvalCoord2fv"
		}"
		end

	map_grid1d (un: INTEGER; u1, u2: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMapGrid1d"
		}"
		end

	map_grid1f (un: INTEGER; u1, u2: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMapGrid1f"
		}"
		end

	map_grid2d (un: INTEGER; u1, u2: REAL_64; vn: INTEGER; v1, v2: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMapGrid2d"
		}"
		end

	map_grid2f (un: INTEGER; u1, u2: REAL_32; vn: INTEGER; v1, v2: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMapGrid2f"
		}"
		end

	eval_point1 (i: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEvalPoint1"
		}"
		end

	eval_point2 (i, j: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEvalPoint2"
		}"
		end

	eval_mesh1 (mode, i1, i2: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEvalMesh1"
		}"
		end

	eval_mesh2 (mode, i1, i2, j1, j2: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glEvalMesh2"
		}"
		end

	fogf (pname: INTEGER; param: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glFogf"
		}"
		end

	fogi (pname, param: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glFogi"
		}"
		end

	fogfv (pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glFogfv"
		}"
		end

	fogiv (pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glFogiv"
		}"
		end

	feedback_buffer (size, type: INTEGER; buffer: POINTER) is
		require
			buffer.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glFeedbackBuffer"
		}"
		end

	pass_through (token: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPassThrough"
		}"
		end

	select_buffer (size: INTEGER; buffer: POINTER) is
		require
			buffer.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glSelectBuffer"
		}"
		end

	init_names is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glInitNames()"
		}"
		end

	load_name (name: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLoadName"
		}"
		end

	push_name (name: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPushName"
		}"
		end

	pop_name is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glPopName()"
		}"
		end

	draw_range_elements (mode, start_element, end_element, count, type: INTEGER; indices: POINTER) is
		require
			indices.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glDrawRangeElements"
		}"
		end

	tex_image_3d (target, level, internal_format, width, height, depth, border, format, type: INTEGER; pixels: POINTER) is
		require
			pixels.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexImage3D"
		}"
		end

	tex_sub_image_3d (target, level, xoffset, yoffset, zoffset, width, height, depth, format, type: INTEGER
		pixels: POINTER) is
		require
			pixels.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glTexSubImage3D"
		}"
		end

	copy_tex_sub_image_3d (target, level, xoffset, yoffset, zoffset, x, y, width, height: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCopyTexSubImage3D"
		}"
		end

	color_table (target, internalformat, width, format, type: INTEGER; table: POINTER) is
		require
			table.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColorTable"
		}"
		end

	color_sub_table (target, start, count, format, type: INTEGER; table: POINTER) is
		require
			table.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColorSubTable"
		}"
		end

	color_table_parameteriv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColorTableParameteriv"
		}"
		end

	color_table_parameterfv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glColorTableParameterfv"
		}"
		end

	copy_color_sub_table (target, start, x, y, width: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCopyColorSubTable"
		}"
		end

	copy_color_table (target, internalformat, x, y, width: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCopyColorTable"
		}"
		end

	get_color_table (target, format, type: INTEGER; table: POINTER) is
		require
			table.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetColorTable"
		}"
		end

	get_color_table_parameterfv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetColorTableParameterfv"
		}"
		end

	get_color_table_parameteriv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetColorTableParameteriv"
		}"
		end

	blend_equation (mode: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glBlendEquation"
		}"
		end

	blend_color (red, green, blue, alpha: REAL_32) is
		require
			red.in_range(0, 1)
			green.in_range(0, 1)
			blue.in_range(0, 1)
			alpha.in_range(0, 1)
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glBlendColor"
		}"
		end

	histogram (target, width, internal_format: INTEGER; sink: BOOLEAN) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glHistogram"
		}"
		end

	reset_histogram (target: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glResetHistogram"
		}"
		end

	get_histogram (target: INTEGER; reset: BOOLEAN; format, type: INTEGER; values: POINTER) is
		require
			values.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetHistogram"
		}"
		end

	get_histogram_parameterfv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetHistogramParameterfv"
		}"
		end

	get_histogram_parameteriv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetHistogramParameteriv"
		}"
		end

	minmax (target, internalformat: INTEGER; sink: BOOLEAN) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMinmax"
		}"
		end

	reset_minmax (target: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glResetMinmax"
		}"
		end

	get_minmax (target: INTEGER; reset: BOOLEAN; format, types: INTEGER; values: POINTER) is
		require
			values.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetMinmax"
		}"
		end

	get_minmax_parameterfv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetMinmaxParameterfv"
		}"
		end

	get_minmax_parameteriv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetMinmaxParameteriv"
		}"
		end

	convolution_filter_1d (target, internalformat, width, format, type: INTEGER; image: POINTER) is
		require
			image.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glConvolutionFilter1D"
		}"
		end

	convolution_filter_2d (target, internalformat, width, height, format, type: INTEGER; image: POINTER) is
		require
			image.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glConvolutionFilter2D"
		}"
		end

	convolution_parameterf (target, pname: INTEGER; params: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glConvolutionParameterf"
		}"
		end

	convolution_parameterfv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glConvolutionParameterfv"
		}"
		end

	convolution_parameteri (target, pname, params: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glConvolutionParameteri"
		}"
		end

	convolution_parameteriv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glConvolutionParameteriv"
		}"
		end

	copy_convolution_filter_1d (target, internalformat, x, y, width: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCopyConvolutionFilter1D"
		}"
		end

	copy_convolution_filter_2d (target, internalformat, x, y, width, height: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCopyConvolutionFilter2D"
		}"
		end

	get_convolution_filter (target, format, type: INTEGER; image: POINTER) is
		require
			image.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetConvolutionFilter"
		}"
		end

	get_convolution_parameterfv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetConvolutionParameterfv"
		}"
		end

	get_convolution_parameteriv (target, pname: INTEGER; params: POINTER) is
		require
			params.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetConvolutionParameteriv"
		}"
		end

	separable_filter_2d (target, internalformat, width, height, format, type: INTEGER; row, column: POINTER) is
		require
			row.is_not_null
			column.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glSeparableFilter2D"
		}"
		end

	get_separable_filter (target, format, type: INTEGER; row, column, span: POINTER) is
		require
			row.is_not_null
			column.is_not_null
			span.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetSeparableFilter"
		}"
		end

	active_texture (texture: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glActiveTexture"
		}"
		end

	client_active_texture (texture: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glClientActiveTexture"
		}"
		end

	compressed_tex_image_1d (target, level, internalformat, width, border, image_size: INTEGER; data: POINTER) is
		require
			data.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCompressedTexImage1D"
		}"
		end

	compressed_tex_image_2d (target, level, internalformat, width, height, border, image_size: INTEGER; data: POINTER) is
		require
			data.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCompressedTexImage2D"
		}"
		end

	compressed_tex_image_3d (target, level, internalformat, width, height, depth, border, image_size: INTEGER
		data: POINTER) is
		require
			data.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCompressedTexImage3D"
		}"
		end

	compressed_tex_sub_image_1d (target, level, xoffset, width, format, image_size: INTEGER; data: POINTER) is
		require
			data.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCompressedTexSubImage1D"
		}"
		end

	compressed_tex_sub_image_2d (target, level, xoffset, yoffset, width, height, format, image_size: INTEGER
		data: POINTER) is
		require
			data.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCompressedTexSubImage2D"
		}"
		end

	compressed_tex_sub_image_3d (target, level, xoffset, yoffset, zoffset, width, height, depth, format
		image_size: INTEGER; data: POINTER) is
		require
			data.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glCompressedTexSubImage3D"
		}"
		end

	get_compressed_tex_image (target, lod: INTEGER; img: POINTER) is
		require
			img.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glGetCompressedTexImage"
		}"
		end

	multi_tex_coord1d (target: INTEGER; s: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1d"
		}"
		end

	multi_tex_coord1dv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1dv"
		}"
		end

	multi_tex_coord1f (target: INTEGER; s: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1f"
		}"
		end

	multi_tex_coord1fv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1fv"
		}"
		end

	multi_tex_coord1i (target, s: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1i"
		}"
		end

	multi_tex_coord1iv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1iv"
		}"
		end

	multi_tex_coord1s (target: INTEGER; s: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1s"
		}"
		end

	multi_tex_coord1sv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1sv"
		}"
		end

	multi_tex_coord2d (target: INTEGER; s, t: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2d"
		}"
		end

	multi_tex_coord2dv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2dv"
		}"
		end

	multi_tex_coord2f (target: INTEGER; s, t: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2f"
		}"
		end

	multi_tex_coord2fv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2fv"
		}"
		end

	multi_tex_coord2i (target, s, t: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2i"
		}"
		end

	multi_tex_coord2iv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2iv"
		}"
		end

	multi_tex_coord2s (target: INTEGER; s, t: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2s"
		}"
		end

	multi_tex_coord2sv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2sv"
		}"
		end

	multi_tex_coord3d (target: INTEGER; s, t, r: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3d"
		}"
		end

	multi_tex_coord3dv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3dv"
		}"
		end

	multi_tex_coord3f (target: INTEGER; s, t, r: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3f"
		}"
		end

	multi_tex_coord3fv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3fv"
		}"
		end

	multi_tex_coord3i (target, s, t, r: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3i"
		}"
		end

	multi_tex_coord3iv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3iv"
		}"
		end

	multi_tex_coord3s (target: INTEGER; s, t, r: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3s"
		}"
		end

	multi_tex_coord3sv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3sv"
		}"
		end

	multi_tex_coord4d (target: INTEGER; s, t, r, q: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4d"
		}"
		end

	multi_tex_coord4dv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4dv"
		}"
		end

	multi_tex_coord4f (target: INTEGER; s, t, r, q: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4f"
		}"
		end

	multi_tex_coord4fv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4fv"
		}"
		end

	multi_tex_coord4i (target, s, t, r, q: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4i"
		}"
		end

	multi_tex_coord4iv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4iv"
		}"
		end

	multi_tex_coord4s (target: INTEGER; s, t, r, q: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4s"
		}"
		end

	multi_tex_coord4sv (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4sv"
		}"
		end

	load_transpose_matrixd (m: POINTER) is
		require
			m.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLoadTransposeMatrixd"
		}"
		end

	load_transpose_matrixf (m: POINTER) is
		require
			m.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glLoadTransposeMatrixf"
		}"
		end

	mult_transpose_matrixd (m: POINTER) is
		require
			m.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultTransposeMatrixd"
		}"
		end

	mult_transpose_matrixf (m: POINTER) is
		require
			m.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultTransposeMatrixf"
		}"
		end

	sample_coverage (value: REAL_32; invert: BOOLEAN) is
		require
			value.in_range(0, 1)
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glSampleCoverage"
		}"
		end

	active_texture_arb (texture: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glActiveTextureARB"
		}"
		end

	client_active_texture_arb (texture: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glClientActiveTextureARB"
		}"
		end

	multi_tex_coord1d_arb (target: INTEGER; s: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1dARB"
		}"
		end

	multi_tex_coord1dv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1dvARB"
		}"
		end

	multi_tex_coord1f_arb (target: INTEGER; s: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1fARB"
		}"
		end

	multi_tex_coord1fv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1fvARB"
		}"
		end

	multi_tex_coord1i_arb (target, s: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1iARB"
		}"
		end

	multi_tex_coord1iv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1ivARB"
		}"
		end

	multi_tex_coord1s_arb (target: INTEGER; s: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1sARB"
		}"
		end

	multi_tex_coord1sv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord1svARB"
		}"
		end

	multi_tex_coord2d_arb (target: INTEGER; s, t: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2dARB"
		}"
		end

	multi_tex_coord2dv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2dvARB"
		}"
		end

	multi_tex_coord2f_arb (target: INTEGER; s, t: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2fARB"
		}"
		end

	multi_tex_coord2fv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2fvARB"
		}"
		end

	multi_tex_coord2i_arb (target, s, t: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2iARB"
		}"
		end

	multi_tex_coord2iv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2ivARB"
		}"
		end

	multi_tex_coord2s_arb (target: INTEGER; s, t: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2sARB"
		}"
		end

	multi_tex_coord2sv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord2svARB"
		}"
		end

	multi_tex_coord3d_arb (target: INTEGER; s, t, r: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3dARB"
		}"
		end

	multi_tex_coord3dv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3dvARB"
		}"
		end

	multi_tex_coord3f_arb (target: INTEGER; s, t, r: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3fARB"
		}"
		end

	multi_tex_coord3fv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3fvARB"
		}"
		end

	multi_tex_coord3i_arb (target, s, t, r: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3iARB"
		}"
		end

	multi_tex_coord3iv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3ivARB"
		}"
		end

	multi_tex_coord3s_arb (target: INTEGER; s, t, r: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3sARB"
		}"
		end

	multi_tex_coord3sv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord3svARB"
		}"
		end

	multi_tex_coord4d_arb (target: INTEGER; s, t, r, q: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4dARB"
		}"
		end

	multi_tex_coord4dv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4dvARB"
		}"
		end

	multi_tex_coord4f_arb (target: INTEGER; s, t, r, q: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4fARB"
		}"
		end

	multi_tex_coord4fv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4fvARB"
		}"
		end

	multi_tex_coord4i_arb (target, s, t, r, q: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4iARB"
		}"
		end

	multi_tex_coord4iv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4ivARB"
		}"
		end

	multi_tex_coord4s_arb (target: INTEGER; s, t, r, q: INTEGER_16) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4sARB"
		}"
		end

	multi_tex_coord4sv_arb (target: INTEGER; v: POINTER) is
		require
			v.is_not_null
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "glMultiTexCoord4svARB"
		}"
		end

end -- class GL
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
