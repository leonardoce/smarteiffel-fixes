-- See the Copyright notice at the end of this file.
--
class GLU

insert
	SINGLETON

creation {OPENGL}
	default_create

feature {ANY}
	const_version: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_VERSION"
		}"
		end

	const_extensions: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_EXTENSIONS"
		}"
		end

	const_invalid_enum: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_INVALID_ENUM"
		}"
		end

	const_invalid_value: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_INVALID_VALUE"
		}"
		end

	const_out_of_memory: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_OUT_OF_MEMORY"
		}"
		end

	const_invalid_operation: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_INVALID_OPERATION"
		}"
		end

	const_outline_polygon: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_OUTLINE_POLYGON"
		}"
		end

	const_outline_patch: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_OUTLINE_PATCH"
		}"
		end

	const_nurbs_error: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR"
		}"
		end

	const_error: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_ERROR"
		}"
		end

	const_nurbs_begin: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_BEGIN"
		}"
		end

	const_nurbs_begin_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_BEGIN_EXT"
		}"
		end

	const_nurbs_vertex: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_VERTEX"
		}"
		end

	const_nurbs_vertex_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_VERTEX_EXT"
		}"
		end

	const_nurbs_normal: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_NORMAL"
		}"
		end

	const_nurbs_normal_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_NORMAL_EXT"
		}"
		end

	const_nurbs_color: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_COLOR"
		}"
		end

	const_nurbs_color_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_COLOR_EXT"
		}"
		end

	const_nurbs_texture_coord: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_TEXTURE_COORD"
		}"
		end

	const_nurbs_tex_coord_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_TEX_COORD_EXT"
		}"
		end

	const_nurbs_end: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_END"
		}"
		end

	const_nurbs_end_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_END_EXT"
		}"
		end

	const_nurbs_begin_data: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_BEGIN_DATA"
		}"
		end

	const_nurbs_begin_data_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_BEGIN_DATA_EXT"
		}"
		end

	const_nurbs_vertex_data: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_VERTEX_DATA"
		}"
		end

	const_nurbs_vertex_data_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_VERTEX_DATA_EXT"
		}"
		end

	const_nurbs_normal_data: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_NORMAL_DATA"
		}"
		end

	const_nurbs_normal_data_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_NORMAL_DATA_EXT"
		}"
		end

	const_nurbs_color_data: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_COLOR_DATA"
		}"
		end

	const_nurbs_color_data_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_COLOR_DATA_EXT"
		}"
		end

	const_nurbs_texture_coord_data: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_TEXTURE_COORD_DATA"
		}"
		end

	const_nurbs_tex_coord_data_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_TEX_COORD_DATA_EXT"
		}"
		end

	const_nurbs_end_data: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_END_DATA"
		}"
		end

	const_nurbs_end_data_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_END_DATA_EXT"
		}"
		end

	const_nurbs_error1: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR1"
		}"
		end

	const_nurbs_error2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR2"
		}"
		end

	const_nurbs_error3: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR3"
		}"
		end

	const_nurbs_error4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR4"
		}"
		end

	const_nurbs_error5: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR5"
		}"
		end

	const_nurbs_error6: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR6"
		}"
		end

	const_nurbs_error7: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR7"
		}"
		end

	const_nurbs_error8: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR8"
		}"
		end

	const_nurbs_error9: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR9"
		}"
		end

	const_nurbs_error10: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR10"
		}"
		end

	const_nurbs_error11: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR11"
		}"
		end

	const_nurbs_error12: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR12"
		}"
		end

	const_nurbs_error13: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR13"
		}"
		end

	const_nurbs_error14: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR14"
		}"
		end

	const_nurbs_error15: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR15"
		}"
		end

	const_nurbs_error16: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR16"
		}"
		end

	const_nurbs_error17: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR17"
		}"
		end

	const_nurbs_error18: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR18"
		}"
		end

	const_nurbs_error19: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR19"
		}"
		end

	const_nurbs_error20: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR20"
		}"
		end

	const_nurbs_error21: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR21"
		}"
		end

	const_nurbs_error22: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR22"
		}"
		end

	const_nurbs_error23: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR23"
		}"
		end

	const_nurbs_error24: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR24"
		}"
		end

	const_nurbs_error25: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR25"
		}"
		end

	const_nurbs_error26: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR26"
		}"
		end

	const_nurbs_error27: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR27"
		}"
		end

	const_nurbs_error28: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR28"
		}"
		end

	const_nurbs_error29: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR29"
		}"
		end

	const_nurbs_error30: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR30"
		}"
		end

	const_nurbs_error31: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR31"
		}"
		end

	const_nurbs_error32: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR32"
		}"
		end

	const_nurbs_error33: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR33"
		}"
		end

	const_nurbs_error34: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR34"
		}"
		end

	const_nurbs_error35: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR35"
		}"
		end

	const_nurbs_error36: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR36"
		}"
		end

	const_nurbs_error37: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_ERROR37"
		}"
		end

	const_auto_load_matrix: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_AUTO_LOAD_MATRIX"
		}"
		end

	const_culling: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_CULLING"
		}"
		end

	const_sampling_tolerance: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_SAMPLING_TOLERANCE"
		}"
		end

	const_display_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_DISPLAY_MODE"
		}"
		end

	const_parametric_tolerance: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_PARAMETRIC_TOLERANCE"
		}"
		end

	const_sampling_method: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_SAMPLING_METHOD"
		}"
		end

	const_u_step: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_U_STEP"
		}"
		end

	const_v_step: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_V_STEP"
		}"
		end

	const_nurbs_mode: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_MODE"
		}"
		end

	const_nurbs_mode_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_MODE_EXT"
		}"
		end

	const_nurbs_tessellator: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_TESSELLATOR"
		}"
		end

	const_nurbs_tessellator_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_TESSELLATOR_EXT"
		}"
		end

	const_nurbs_renderer: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_RENDERER"
		}"
		end

	const_nurbs_renderer_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NURBS_RENDERER_EXT"
		}"
		end

	const_object_parametric_error: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_OBJECT_PARAMETRIC_ERROR"
		}"
		end

	const_object_parametric_error_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_OBJECT_PARAMETRIC_ERROR_EXT"
		}"
		end

	const_object_path_length: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_OBJECT_PATH_LENGTH"
		}"
		end

	const_object_path_length_ext: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_OBJECT_PATH_LENGTH_EXT"
		}"
		end

	const_path_length: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_PATH_LENGTH"
		}"
		end

	const_parametric_error: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_PARAMETRIC_ERROR"
		}"
		end

	const_domain_distance: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_DOMAIN_DISTANCE"
		}"
		end

	const_map1_trim_2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_MAP1_TRIM_2"
		}"
		end

	const_map1_trim_3: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_MAP1_TRIM_3"
		}"
		end

	const_point: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_POINT"
		}"
		end

	const_line: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_LINE"
		}"
		end

	const_fill: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_FILL"
		}"
		end

	const_silhouette: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_SILHOUETTE"
		}"
		end

	const_smooth: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_SMOOTH"
		}"
		end

	const_flat: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_FLAT"
		}"
		end

	const_none: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_NONE"
		}"
		end

	const_outside: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_OUTSIDE"
		}"
		end

	const_inside: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_INSIDE"
		}"
		end

	const_tess_begin: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_BEGIN"
		}"
		end

	const_begin: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_BEGIN"
		}"
		end

	const_tess_vertex: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_VERTEX"
		}"
		end

	const_vertex: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_VERTEX"
		}"
		end

	const_tess_end: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_END"
		}"
		end

	const_end: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_END"
		}"
		end

	const_tess_error: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_ERROR"
		}"
		end

	const_tess_edge_flag: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_EDGE_FLAG"
		}"
		end

	const_edge_flag: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_EDGE_FLAG"
		}"
		end

	const_tess_combine: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_COMBINE"
		}"
		end

	const_tess_begin_data: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_BEGIN_DATA"
		}"
		end

	const_tess_vertex_data: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_VERTEX_DATA"
		}"
		end

	const_tess_end_data: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_END_DATA"
		}"
		end

	const_tess_error_data: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_ERROR_DATA"
		}"
		end

	const_tess_edge_flag_data: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_EDGE_FLAG_DATA"
		}"
		end

	const_tess_combine_data: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_COMBINE_DATA"
		}"
		end

	const_cw: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_CW"
		}"
		end

	const_ccw: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_CCW"
		}"
		end

	const_interior: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_INTERIOR"
		}"
		end

	const_exterior: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_EXTERIOR"
		}"
		end

	const_unknown: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_UNKNOWN"
		}"
		end

	const_tess_winding_rule: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_WINDING_RULE"
		}"
		end

	const_tess_boundary_only: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_BOUNDARY_ONLY"
		}"
		end

	const_tess_tolerance: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_TOLERANCE"
		}"
		end

	const_tess_error1: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_ERROR1"
		}"
		end

	const_tess_error2: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_ERROR2"
		}"
		end

	const_tess_error3: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_ERROR3"
		}"
		end

	const_tess_error4: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_ERROR4"
		}"
		end

	const_tess_error5: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_ERROR5"
		}"
		end

	const_tess_error6: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_ERROR6"
		}"
		end

	const_tess_error7: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_ERROR7"
		}"
		end

	const_tess_error8: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_ERROR8"
		}"
		end

	const_tess_missing_begin_polygon: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_MISSING_BEGIN_POLYGON"
		}"
		end

	const_tess_missing_begin_contour: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_MISSING_BEGIN_CONTOUR"
		}"
		end

	const_tess_missing_end_polygon: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_MISSING_END_POLYGON"
		}"
		end

	const_tess_missing_end_contour: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_MISSING_END_CONTOUR"
		}"
		end

	const_tess_coord_too_large: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_COORD_TOO_LARGE"
		}"
		end

	const_tess_need_combine_callback: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_NEED_COMBINE_CALLBACK"
		}"
		end

	const_tess_winding_odd: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_WINDING_ODD"
		}"
		end

	const_tess_winding_nonzero: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_WINDING_NONZERO"
		}"
		end

	const_tess_winding_positive: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_WINDING_POSITIVE"
		}"
		end

	const_tess_winding_negative: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_WINDING_NEGATIVE"
		}"
		end

	const_tess_winding_abs_geq_two: INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_WINDING_ABS_GEQ_TWO"
		}"
		end

	const_tess_max_coord: REAL_64 is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "GLU_TESS_MAX_COORD"
		}"
		end

	begin_curve (nurb: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluBeginCurve"
		}"
		end

	begin_polygon (tess: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluBeginPolygon"
		}"
		end

	begin_surface (nurb: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluBeginSurface"
		}"
		end

	begin_trim (nurb: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluBeginTrim"
		}"
		end

	build_1d_mipmap_levels (target, internal_format, width, format, type, level, base, max: INTEGER; data: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluBuild1DMipmapLevels"
		}"
		end

	build_1d_mipmaps (target, internal_format, width, format, type: INTEGER; data: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluBuild1DMipmaps"
		}"
		end

	build_2d_mipmap_levels (target, internal_format, width, height, format, type, level, base, max: INTEGER
		data: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluBuild2DMipmapLevels"
		}"
		end

	build_2d_mipmaps (target, internal_format, width, height, format, type: INTEGER; data: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluBuild2DMipmaps"
		}"
		end

	build_3d_mipmap_levels (target, internal_format, width, height, depth, format, type, level, base, max: INTEGER
		data: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluBuild3DMipmapLevels"
		}"
		end

	build_3d_mipmaps (target, internal_format, width, height, depth, format, type: INTEGER; data: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluBuild3DMipmaps"
		}"
		end

	check_extension (ext_name, ext_string: POINTER): BOOLEAN is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluCheckExtension"
		}"
		end

	cylinder (quad: POINTER; base, top, height: REAL_64; slices, stacks: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluCylinder"
		}"
		end

	delete_nurbs_renderer (nurb: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluDeleteNurbsRenderer"
		}"
		end

	delete_quadric (quad: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluDeleteQuadric"
		}"
		end

	delete_tess (tess: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluDeleteTess"
		}"
		end

	disk (quad: POINTER; inner, outer: REAL_64; slices, loops: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluDisk"
		}"
		end

	end_curve (nurb: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluEndCurve"
		}"
		end

	end_polygon (tess: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluEndPolygon"
		}"
		end

	end_surface (nurb: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluEndSurface"
		}"
		end

	end_trim (nurb: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluEndTrim"
		}"
		end

	error_string (error: INTEGER): POINTER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluErrorString"
		}"
		end

	get_nurbs_property (nurb: POINTER; property: INTEGER; data: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluGetNurbsProperty"
		}"
		end

	get_string (name: INTEGER): POINTER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluGetString"
		}"
		end

	get_tess_property (tess: POINTER; which: INTEGER; data: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluGetTessProperty"
		}"
		end

	load_sampling_matrices (nurb, model, pers, view: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluLoadSamplingMatrices"
		}"
		end

	look_at (eye_x, eye_y, eye_z, center_x, center_y, center_z, up_x, up_y, up_z: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluLookAt"
		}"
		end

	new_nurbs_renderer: POINTER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluNewNurbsRenderer()"
		}"
		end

	new_quadric: POINTER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluNewQuadric()"
		}"
		end

	new_tess: POINTER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluNewTess()"
		}"
		end

	next_contour (tess: POINTER; type: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluNextContour"
		}"
		end

	nurbs_callback (nurb: POINTER; which: INTEGER; call_back_func: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluNurbsCallback"
		}"
		end

	nurbs_callback_data (nurb, user_data: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluNurbsCallbackData"
		}"
		end

	nurbs_callback_data_ext (nurb, user_data: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluNurbsCallbackDataEXT"
		}"
		end

	nurbs_curve (nurb: POINTER; knot_count: INTEGER; knots: POINTER; stride: INTEGER; control: POINTER
		order, type: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluNurbsCurve"
		}"
		end

	nurbs_property (nurb: POINTER; property: INTEGER; value: REAL_32) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluNurbsProperty"
		}"
		end

	nurbs_surface (nurb: POINTER; s_knot_count: INTEGER; s_knots: POINTER; t_knot_count: INTEGER; t_knots: POINTER
		s_stride, t_stride: INTEGER; control: POINTER; s_order, t_order, type: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluNurbsSurface"
		}"
		end

	ortho_2d (left, right, bottom, top: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluOrtho2D"
		}"
		end

	partial_disk (quad: POINTER; inner, outer: REAL_64; slices, loops: INTEGER; start, sweep: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluPartialDisk"
		}"
		end

	perspective (fovy, aspect, z_near, z_far: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluPerspective"
		}"
		end

	pick_matrix (x, y, del_x, del_y: REAL_64; viewport: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluPickMatrix"
		}"
		end

	project (obj_x, obj_y, obj_z: REAL_64; model, proj, view, win_x, win_y, win_z: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluProject"
		}"
		end

	pwl_curve (nurb: POINTER; count: INTEGER; data: POINTER; stride, type: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluPwlCurve"
		}"
		end

	quadric_callback (quad: POINTER; which: INTEGER; call_back_func: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluQuadricCallback"
		}"
		end

	quadric_draw_style (quad: POINTER; draw: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluQuadricDrawStyle"
		}"
		end

	quadric_normals (quad: POINTER; normal: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluQuadricNormals"
		}"
		end

	quadric_orientation (quad: POINTER; orientation: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluQuadricOrientation"
		}"
		end

	quadric_texture (quad: POINTER; texture: BOOLEAN) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluQuadricTexture"
		}"
		end

	scale_image (format, w_in, h_in, type_in: INTEGER; data_in: POINTER; w_out, h_out, type_out: INTEGER
		data_out: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluScaleImage"
		}"
		end

	sphere (quad: POINTER; radius: REAL_64; slices, stacks: INTEGER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluSphere"
		}"
		end

	tess_begin_contour (tess: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluTessBeginContour"
		}"
		end

	tess_begin_polygon (tess, data: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluTessBeginPolygon"
		}"
		end

	tess_callback (tess: POINTER; which: INTEGER; call_back_func: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluTessCallback"
		}"
		end

	tess_end_contour (tess: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluTessEndContour"
		}"
		end

	tess_end_polygon (tess: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluTessEndPolygon"
		}"
		end

	tess_normal (tess: POINTER; value_x, value_y, value_z: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluTessNormal"
		}"
		end

	tess_property (tess: POINTER; which: INTEGER; data: REAL_64) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluTessProperty"
		}"
		end

	tess_vertex (tess, location, data: POINTER) is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluTessVertex"
		}"
		end

	un_project (win_x, win_y, win_z: REAL_64; model, proj, view, obj_x, obj_y, obj_z: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluUnProject"
		}"
		end

	un_project4 (win_x, win_y, win_z, clip_w: REAL_64; model, proj, view: POINTER; near, far: REAL_64
		obj_x, obj_y, obj_z, obj_w: POINTER): INTEGER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision/opengl"
			module_name: "glu"
			feature_name: "gluUnProject4"
		}"
		end

end -- class GLU
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
