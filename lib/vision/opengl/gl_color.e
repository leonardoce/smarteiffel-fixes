-- See the Copyright notice at the end of this file.
--
class GL_COLOR

insert
	OPENGL

creation {ANY}
	set_color, set_from_collection

feature {ANY}
	set_from_collection (c: COLLECTION[REAL]) is
		require
			c.count >= 4
		do
			set_color(c.item(c.lower), c.item(c.lower + 1), c.item(c.lower + 2), c.item(c.lower + 3))
		end

	set_color (r: like red; g: like green; b: like blue; a: like alpha) is
		do
			if color_array = Void then
				create color_array.make(4)
			end
			set_red(r)
			set_green(g)
			set_blue(b)
			set_alpha(a)
		end

	set_red (r: like red) is
		require
			color_array /= Void and then color_array.count >= 4
			r.in_range(0, 1)
		do
			color_array.put(r.force_to_real_32, 0)
		end

	set_green (g: like green) is
		require
			color_array /= Void and then color_array.count >= 4
			g.in_range(0, 1)
		do
			color_array.put(g.force_to_real_32, 1)
		end

	set_blue (b: like blue) is
		require
			color_array /= Void and then color_array.count >= 4
			b.in_range(0, 1)
		do
			color_array.put(b.force_to_real_32, 2)
		end

	set_alpha (a: like alpha) is
		require
			color_array /= Void and then color_array.count >= 4
			a.in_range(0, 1)
		do
			color_array.put(a.force_to_real_32, 3)
		end

	red: REAL is
		require
			color_array /= Void and then color_array.count >= 4
		do
			Result := color_array.item(0).to_real_64
		ensure
			Result.in_range(0, 1)
		end

	green: REAL is
		require
			color_array /= Void and then color_array.count >= 4
		do
			Result := color_array.item(1).to_real_64
		ensure
			Result.in_range(0, 1)
		end

	blue: REAL is
		require
			color_array /= Void and then color_array.count >= 4
		do
			Result := color_array.item(2).to_real_64
		ensure
			Result.in_range(0, 1)
		end

	alpha: REAL is
		require
			color_array /= Void and then color_array.count >= 4
		do
			Result := color_array.item(3).to_real_64
		ensure
			Result.in_range(0, 1)
		end

	render is
		do
			gl.color4fv(color_array.to_external)
		end

	color_array: FAST_ARRAY[REAL_32]

end -- class GL_COLOR
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
