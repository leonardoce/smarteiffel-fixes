-- See the Copyright notice at the end of this file.
--
class DRAW_KIT
	-- DRAW_KIT is a tool to make some free drawing, you may consider
	-- it as a pen. You can draw points, lines, rectangles, arcs, words...
	-- Difference with DRAW_STYLE is that you pre-select the drawable
	-- to draw into and drawing functions automatically use this drawable.

inherit
	DRAW_STYLE

creation {ANY}
	default_create

feature {ANY}
	set_drawable (d: DRAWABLE) is
			-- Set the drawable to use in future drawing function calls.
		require
			d /= Void
		do
			drawable := d.drawing_widget
		ensure
			valid_drawable
		end

	point (x1, y1: INTEGER) is
		require
			valid_drawable
		do
			basic_draw_point(drawable, style, x1, y1)
		end

	line (x1, y1, x2, y2: INTEGER) is
			--TODO: explain how line width is used
		require
			valid_drawable
		do
			basic_draw_line(drawable, style, x1, y1, x2, y2)
		end

	rectangle (x1, y1, w, h: INTEGER) is
			--TODO: explain how line width is used
		require
			w > 0
			h > 0
			valid_drawable
		do
			basic_draw_rectangle(drawable, style, x1, y1, w, h)
		end

	arc (x1, y1, w, h: INTEGER; angle1, angle2: REAL) is
			--TODO: explain how line width is used
			-- arc will be drawn inside the rectangle defined with x1, y1, w, h
		require
			w > 0
			h > 0
			valid_drawable
		do
			basic_draw_arc(drawable, style, x1, y1, w, h, angle1, angle2)
		end

	arc_radius (x, y, r1, r2: INTEGER; angle1, angle2: REAL) is
			--TODO: explain how line width is used
			-- arc will be drawn using (x, y) as center and r1/r2 as
			-- horizontal/vertical radius
		require
			r1 > 0
			r2 > 0
			valid_drawable
		do
			basic_draw_arc(drawable, style, x - r1, y - r2, 2 * r1, 2 * r2, angle1, angle2)
		end

	fill_rectangle (x1, y1, w, h: INTEGER) is
		require
			w > 0
			h > 0
			valid_drawable
		do
			basic_draw_fill_rectangle(drawable, style, x1, y1, w, h)
		end

	fill_arc (x1, y1, w, h: INTEGER; angle1, angle2: REAL) is
			-- arc will be drawn inside the rectangle defined with x1, y1, w, h
		require
			w > 0
			h > 0
			valid_drawable
		do
			basic_draw_fill_arc(drawable, style, x1, y1, w, h, angle1, angle2)
		end

	fill_arc_radius (x, y, r1, r2: INTEGER; angle1, angle2: REAL) is
			-- arc will be drawn using (x, y) as center and r1/r2 as
			-- horizontal/vertical radius
		require
			r1 > 0
			r2 > 0
			valid_drawable
		do
			basic_draw_fill_arc(drawable, style, x - r1, y - r2, 2 * r1, 2 * r2, angle1, angle2)
		end

	put_string (s: UNICODE_STRING; x, y: INTEGER) is
		require
			s /= Void
			valid_drawable
		do
			--TODO: surrogate characters !
			--TODO: combining characters !
			basic_draw_text(drawable, style, x, y, font.base_line, s.storage.to_external, s.count)
		end

	valid_drawable: BOOLEAN is
		do
			Result := drawable /= default_pointer
		end

feature {}
	drawable: POINTER

end -- class DRAW_KIT
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
