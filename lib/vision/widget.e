-- See the Copyright notice at the end of this file.
--
deferred class WIDGET
	--
	-- WIDGET (i.e. WInDow GadGET) is the most abstract class for all graphical objects.
	--
	-- A WIDGET can have a fixed size or a WIDGET can be shrinked or expanded ( `x_shrink_allowed', 
	-- `x_expand_allowed', `y_shrink_allowed' and `y_expand_allowed').
	--
	-- The current WIDGET size is given by `width' and `height'.
	-- The minimum possible size is given by `min_width' and `min_height'.
	-- The needed space to be able to see the entire WIDGET is given by `std_width' and `std_height'.
	--
	-- Following properties are worth to be known:
	-- * When `x_shrink_allowed' is True, then `width' >= `min_width'. 
	-- * When `y_shrink_allowed' is True, then `height' >= `min_height'. 
	-- * When `x_shrink_allowed' is False, then `width' >= `std_width'
	-- * When `y_shrink_allowed' is False, then `height' >= `std_height'
	-- * When `x_expand_allowed' is True, then `width' can increase.
	-- * When `y_expand_allowed' is True, then `height' can increase.
	-- * When `x_expand_allowed' is False, then `width' <= `std_width'.
	-- * When `y_expand_allowed' is False, then `height' <= `std_height'.
	--

insert
	STATE

feature {ANY}
	parent: CONTAINER

	pos_x, pos_y: INTEGER

	x_shrink_allowed: BOOLEAN
	
	x_expand_allowed: BOOLEAN

	y_shrink_allowed: BOOLEAN

	y_expand_allowed: BOOLEAN

	min_width: INTEGER is
		deferred
		end

	min_height: INTEGER is
		deferred
		end

	std_width: INTEGER is
		deferred
		end

	std_height: INTEGER is
		deferred
		end

	width: INTEGER is
		deferred
		end

	height: INTEGER is
		deferred
		end

	valid_width (w: INTEGER): BOOLEAN is
		do
			if x_shrink_allowed then
				Result := w >= min_width
			else
				Result := w >= std_width
			end
			Result := Result and then (x_expand_allowed or else w <= std_width)
		end

	valid_height (h: INTEGER): BOOLEAN is
		do
			if y_shrink_allowed then
				Result := h >= min_height
			else
				Result := h >= std_height
			end
			Result := Result and then (y_expand_allowed or else h <= std_height)
		end

	area: RECT is
		do
			Result.make(pos_x, pos_y, width, height)
		end

	root_area: RECT is
		do
			Result.make(parent.as_x_root(pos_x), parent.as_y_root(pos_y), width, height)
		end

	set_x_shrink (b: BOOLEAN) is
		do
			if x_shrink_allowed /= b then
				x_shrink_allowed := b
				if not b and then width < std_width then
					resize(std_width, height)
				end
				if parent /= Void then
					parent.child_requisition_changed
				end
			end
		end

	set_x_expand (b: BOOLEAN) is
		do
			if x_expand_allowed /= b then
				x_expand_allowed := b
				if not b and then width > std_width then
					resize(std_width, height)
				end
				if parent /= Void then
					parent.child_requisition_changed
				end
			end
		end

	set_y_shrink (b: BOOLEAN) is
		do
			if y_shrink_allowed /= b then
				y_shrink_allowed := b
				if not b and then height < std_height then
					resize(width, std_height)
				end
				if parent /= Void then
					parent.child_requisition_changed
				end
			end
		end

	set_y_expand (b: BOOLEAN) is
		do
			if y_expand_allowed /= b then
				y_expand_allowed := b
				if not b and then height > std_height then
					resize(width, std_height)
				end
				if parent /= Void then
					parent.child_requisition_changed
				end
			end
		end

	set_shrink (b: BOOLEAN) is
			-- change both x and y shrink state
		do
			if x_shrink_allowed /= b and then y_shrink_allowed /= b then
				x_shrink_allowed := b
				y_shrink_allowed := b
				if not b then
					if width < std_width then
						if height < std_height then
							resize(std_width, std_height)
						else
							resize(std_width, height)
						end
					else
						if height < std_height then
							resize(width, std_height)
						end
					end
				end
				if parent /= Void then
					parent.child_requisition_changed
				end
			else
				set_x_shrink(b)
				set_y_shrink(b)
			end
		end

	set_expand (b: BOOLEAN) is
			-- change both x and y expand state
		do
			if x_expand_allowed /= b and then y_expand_allowed /= b then
				x_expand_allowed := b
				y_expand_allowed := b
				if not b then
					if width > std_width then
						if height > std_height then
							resize(std_width, std_height)
						else
							resize(std_width, height)
						end
					else
						if height > std_height then
							resize(width, std_height)
						end
					end
				end
				if parent /= Void then
					parent.child_requisition_changed
				end
			else
				set_x_expand(b)
				set_y_expand(b)
			end
		end

feature {LAYOUT}
	expose_paint is
			-- Apply paint with limited depth to the first window.
			-- Containers have to propagate, with special attention to windows where `expose_paint' do 
			-- nothing.
		deferred
		end

	set_geometry (x, y, w, h: INTEGER) is
		require
			x >= 0
			y >= 0
			w >= min_width
			h >= min_height
		deferred
		ensure
			width = w
			height = h
		end

feature {CONTAINER}
	set_parent (p: CONTAINER) is
		require
			p = Void implies parent /= Void
			p /= Void implies parent = Void
			p /= Void implies p.has_child(Current)
		do
			parent := p
		ensure
			parent = p
		end

feature {}
	computing_size: BOOLEAN is
		do
			-- used only for invariant checking
			-- needed because some classes have to do some work before
			-- they know the good size (determined by parent's layout).
		end

	resize (w, h: INTEGER) is
		require
			w >= min_width
			h >= min_height
		deferred
		ensure
			width = w
			height = h
		end

invariant
	width >= min_width or computing_size
	height >= min_height or computing_size
	std_width > 0
	std_height > 0
	;(not x_shrink_allowed implies width >= std_width) or computing_size
	;(not x_expand_allowed implies width <= std_width) or computing_size
	;(not y_shrink_allowed implies height >= std_height) or computing_size
	;(not y_expand_allowed implies height <= std_height) or computing_size

end -- class WIDGET
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
