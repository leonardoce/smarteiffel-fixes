-- See the Copyright notice at the end of this file.
--
class ALIGNMENT
	-- This class is used to describe widget position inside area
	-- choosen by the container's layout.
	-- x is in range 0.0 to 1.0 from left to right (0.5 is the middle)
	-- y is in range 0.0 to 1.0 from top to bottom (0.5 is the middle)
	-- Alignment is useful only if x_expand_allowed or y_expand_allowed
	-- is `True' and the space given to the object is bigger than it's
	-- standard size.

insert
	ANY
		redefine default_create
		end

creation {ANY}
	default_create, make

feature {ANY}
	x: REAL

	y: REAL

	make (x_pos, y_pos: REAL) is
		do
			x := x_pos
			y := y_pos
		end

	left is
		do
			x := 0.0
		end

	right is
		do
			x := 1.0
		end

	top is
		do
			y := 0.0
		end

	bottom is
		do
			y := 1.0
		end

	horizontal_center is
		do
			x := 0.5
		end

	vertical_center is
		do
			y := 0.5
		end

feature {}
	default_create is
		do
			x := 0.0
			y := 0.0
		end

invariant
	x.in_range(0.0, 1.0)
	y.in_range(0.0, 1.0)

end -- class ALIGNMENT
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
