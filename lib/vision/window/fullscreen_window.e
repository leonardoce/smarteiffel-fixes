-- See the Copyright notice at the end of this file.
--
class FULLSCREEN_WINDOW

inherit
	TOPLEVEL_WINDOW
		redefine default_create
		end

insert
	WHEN_CLOSE_REQUESTED
		redefine default_create
		end
	WHEN_UNMAPPED
		redefine default_create
		end
	SINGLETON
		redefine default_create
		end

creation {ANY}
	default_create, make_with_size

feature {}
	default_create is
		do
			make_with_size(vision.display_width, vision.display_height)
		end

	make_with_size (w, h: INTEGER) is
		local
			lo: COLUMN_LAYOUT; dummy: INTEGER
		do
			-- A call to vision must be done for this object to exist
			dummy := vision.display_width
			create lo
			width := w
			height := h
			std_width := width
			std_height := height
			min_width := 1
			min_height := 1
			max_width := w
			max_height := h
			widget := basic_fullscreen_window_create(width, height)
			container_init
			layout := lo
			lo.set_container(Current)
			vision.register(Current)
			default_init
			when_geometry_change(agent geometry_changed)
			create tmp_title.make(0)
			x_expand_allowed := False
			y_expand_allowed := False
			x_shrink_allowed := False
			y_shrink_allowed := False
			basic_window_set_requisition(widget, min_width, min_height, max_width, max_height)
			vision.root_window.child_attach(Current)
			debug
				when_expose(agent print_expose)
			end
		end

feature {ANY}
	destroy is
		do
			basic_fullscreen_window_destroy
		end

feature {}
	basic_fullscreen_window_create (w, h: INTEGER): POINTER is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "fullscreen_window"
			feature_name: "basic_fullscreen_window_create"
			}"
		end

	basic_fullscreen_window_destroy is
		external "plug_in"
		alias "{
			location: "${sys}/plugins/vision"
			module_name: "fullscreen_window"
			feature_name: "basic_fullscreen_window_destroy"
			}"
		end

invariant
	width.in_range(1, vision.display_width)
	height.in_range(1, vision.display_height)

end -- class FULLSCREEN_WINDOW
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
