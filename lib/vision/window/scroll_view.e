-- See the Copyright notice at the end of this file.
--
class SCROLL_VIEW
	-- A SCROLL_VIEW is a rectangular area in which you partially see
	-- a bigger SUB_WINDOW. Widgets to display are in the SUB_WINDOW,
	-- you are able to change the viewed part of this big SUB_WINDOW.
	-- Note: SCROLL_VIEW is not really a container (only one child
	-- of SUB_WINDOW type).

inherit
	SUB_WINDOW
		redefine make, child_attach, child_requisition_changed
		end

creation {ANY}
	make

feature {}
	make (p: like parent) is
		local
			default_layout: SCROLL_LAYOUT
		do
			create default_layout
			make_layout(p, default_layout)
			set_requisition(100, 100, 200, 200)
			create vertical_scroll_bar.make(Current)
			view := Void
			create horizontal_scroll_bar.make(Current)
			view := Void
		end

feature {ANY}
	view: SUB_WINDOW

	view_x, view_y: INTEGER

	vertical_scroll_bar, horizontal_scroll_bar: SUB_WINDOW

	move_view (x, y: INTEGER) is
		do
			view_x := x
			view_y := y
			view.set_geometry(x, y, view.width, view.height)
		end

	resize_contents (w, h: INTEGER) is
		require
			view.valid_width(w)
			view.valid_height(h)
		do
			view.set_geometry(view_x, view_y, w, h)
		end

	add_scrollbar is
		do
			--TODO: Left/right, up/down ? Both ? automatic/fixed ?
			--TODO: Resize scroll bars on resize. scroll bars in the layout ?
			--	 create vertical_scroll_bar.make(Current)
			--	 create horizontal_scroll_bar.make(Current)
			if view /= Void then
				not_yet_implemented
			end
		end

	child_attach (p: SUB_WINDOW) is
		do
			check
				view = Void
			end
			child.add_last(p)
			p.set_parent(Current)
			view := p
			--invalidate_layout --TODO ???
			--child_requisition_changed --TODO ???
		ensure then
			view /= Void
		end

	vertical_shift (i: INTEGER) is
			-- Image will go up if i<0
		do
			move_view(view_x, view_y + i)
		end

	horizontal_shift (i: INTEGER) is
			-- Image will go left if i<0
		do
			move_view(view_x + i, view_y)
		end

	shift (i, j: INTEGER) is
			-- Image will go left if i<0
			-- Image will go up if j<0
		do
			move_view(view_x + i, view_y + j)
		end

feature {WIDGET}
	child_requisition_changed is
		local
			h, v: BOOLEAN
		do
			if view.width > width then
				h := True
				if view.height > height - horizontal_scroll_bar.height then
					v := True
				end
			else
				if view.height > height then
					v := True
					if view.width > width - vertical_scroll_bar.width then
						h := True
					end
				end
			end
			--TODO: resize and map/unmap scroll bars
			if h then
				horizontal_scroll_bar.map
			end
			if v then
				vertical_scroll_bar.map
			end
			-- Here we did not need to go upper (above widget are not
			-- concerned with the change, the scroll_view is like a barrier).
			-- Now it's time to refresh the view (redo view layout and redraw)
			view.clear_area(0, 0, view.width, view.height)
			invalidate_layout
			dispatch
		end

end -- class SCROLL_VIEW
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
