-- See the Copyright notice at the end of this file.
--
deferred class WINDOW

inherit
	CONTAINER
		redefine set_parent, expose_paint, resize, set_geometry, dispatch, set_requisition, refresh, clear, clear_without_expose,
			as_x_root, as_y_root, area
		end

insert
	WHEN_LEFT_DOWN
		undefine default_create
		end
	WHEN_LEFT_UP
		undefine default_create
		end
	WHEN_MIDDLE_DOWN
		undefine default_create
		end
	WHEN_MIDDLE_UP
		undefine default_create
		end
	WHEN_RIGHT_DOWN
		undefine default_create
		end
	WHEN_RIGHT_UP
		undefine default_create
		end
	WHEN_WHEEL_UP
		undefine default_create
		end
	WHEN_WHEEL_DOWN
		undefine default_create
		end
	WHEN_POINTER_MOVE
		undefine default_create
		end
	WHEN_POINTER_ENTER
		undefine default_create
		end
	WHEN_POINTER_LEAVE
		undefine default_create
		end
	WHEN_KEY_DOWN
		undefine default_create
		end
	WHEN_KEY_UP
		undefine default_create
		end
	WHEN_MAPPED
		undefine default_create
		end
	WHEN_UNMAPPED
		undefine default_create
		end
	WHEN_GEOMETRY_CHANGE
		undefine default_create
		end
	WHEN_EXPOSE
		undefine default_create
		end

feature {WINDOW}
	default_init is
		do
			when_mapped(agent set_mapped(True))
			when_unmapped(agent set_mapped(False))
			--when_geometry_change(agent geometry_changed)
			when_expose(agent expose_event)
		end

feature {CONTAINER}
	set_parent (p: CONTAINER) is
		do
			parent := p
		end

feature {ANY}
	set_background_color (c: COLOR) is
			-- The color is copied, next changes to c won't change the background.
			-- Call clear_area (or refresh) to update the background from
			-- application, not needed from renderer
		require
			c /= Void
		do
			basic_window_set_bg_color(widget, c.storage)
			--TODO: check if expose event is generated
		end

	set_background_pixmap (p: PIXMAP) is
			-- Call this function again if you modify the pixmap (copy may
			-- have been done)
		require
			p /= Void
		do
			basic_window_set_bg_pixmap(widget, p.widget)
			--if mapped then -- problem due to asynchronous data
			-- Call clear_area to update the background from application,
			-- not needed from renderer
			--TODO: check if expose event is generated
			--if layout /= Void then --TODO: should be removed !
			--   paint(pos_x, pos_y)  --How could it be more efficient ? call clear ?
			--end
			--end
		end

	map is
		do
			if not layout_ready then
				-- ?????????
				dispatch
				--TODO: ????
			end
			basic_window_map(widget)
		end

	unmap is
		do
			basic_window_unmap(widget)
		end

	mapped: BOOLEAN -- Warning: this information is asynchronous

	expose_event is
		do
			--basic_window_clear(widget)
			drawing_widget := basic_window_get_drawing_widget(widget)
			if not layout_ready then
				dispatch
				--TODO: ???
			end
			layout.expose_paint
			--drawing_widget := default_pointer --TODO: only after all expose
			--agents have run
		end

	clear_without_expose is
		do
			basic_window_clear_no_expose(widget)
		end

	area: RECT is
		do
			Result.make(0, 0, width, height)
		end

	refresh, clear is
			-- clear and update entire object (sub_window(s) are not updated).
		do
			clear_area(0, 0, width, height)
		end

feature {WIDGET}
	as_x_root (x: INTEGER): INTEGER is
			--TODO: add basic conversion to speed up
		require else
			x >= 0
			x < width
		do
			Result := parent.as_x_root(pos_x) + x
		end

	as_y_root (y: INTEGER): INTEGER is
			--TODO: add basic conversion to speed up
		require else
			y >= 0
			y < height
		do
			Result := parent.as_y_root(pos_y) + y
		end

feature {LAYOUT}
	expose_paint is
		do
		end

	set_geometry (x, y, w, h: INTEGER) is
			-- Position may be negative (used for scrolling).
		require else
			w >= min_width
			h >= min_height
		do
			if pos_x /= x or else pos_y /= y then
				pos_x := x
				pos_y := y
				basic_window_set_geometry(widget, x, y, w, h)
				if width /= w or else height /= h then
					width := w
					height := h
					update_decoration
					refresh
					invalidate_layout
					dispatch
				end
			elseif width /= w or else height /= h then
				resize(w, h)
			end
		end

	set_requisition (min_w, min_h, std_w, std_h: INTEGER) is
		do
			if min_width /= min_w.max(1) then
				min_width := min_w.max(1)
				requisition_changed := True
			end
			if min_height /= min_h.max(1) then
				min_height := min_h.max(1)
				requisition_changed := True
			end
			if std_width /= std_w.max(1) then
				std_width := std_w.max(1)
				requisition_changed := True
			end
			if std_height /= std_h.max(1) then
				std_height := std_h.max(1)
				requisition_changed := True
			end
			invalidate_layout
			if requisition_changed then
				parent.child_requisition_changed
				requisition_changed := False
			else
				-- we are on top of hierarchy concerned with the change.
				-- Now we need to erase the area, redo layouts and redraw
				refresh
			end
			if not layout_ready then
				dispatch
			end
		end

feature {ROOT_WINDOW, LAYOUT}
	adjust_size is
		local
			w, h: INTEGER
		do
			w := width
			if w < std_width then
				if not x_shrink_allowed then
					w := std_width
				elseif w < min_width then
					w := min_width
				end
			elseif not x_expand_allowed then
				w := std_width
			end
			h := height
			if h < std_height then
				if not y_shrink_allowed then
					h := std_height
				elseif h < min_height then
					h := min_height
				end
			elseif not y_expand_allowed then
				h := std_height
			end
			resize(w, h)
		end

feature {}
	resize (w, h: INTEGER) is
		do
			if width /= w or else height /= h then
				width := w
				height := h
				basic_window_set_size(widget, pos_x, pos_y, w, h)
				update_decoration
				refresh
				child_requisition_changed
			end
		end

	paint_decorations (x, y: INTEGER) is
		do
			--         i: INTEGER
			--          from
			--             i := 0
			--          until
			--             i = decorations.count
			--          loop
			--             decorations.item(i).paint(x, y)
			--             i := i+1
			--          end
		end

	resize_decorations (w, h: INTEGER) is
		do
			--         i: INTEGER
			--          from
			--             i := 0
			--          until
			--             i = decorations.count
			--          loop
			--             decorations.item(i).resize(w, h)
			--             i := i+1
			--          end
		end

	set_mapped (b: BOOLEAN) is
		do
			mapped := b
		end

	geometry_changed (x, y, w, h: INTEGER) is
		do
			--TODO: remove ?
			pos_x := x
			pos_y := y
			width := w
			height := h
			invalidate_layout
		end

	dispatch is
		do
			layout.redo_layout(0, 0)
			layout_ready := True
		end

	basic_window_create (x, y, w, h: INTEGER; parent_win: POINTER; decorate: BOOLEAN): POINTER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_create"
	 }"
		end

	basic_window_set_geometry (win: POINTER; x, y, w, h: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_set_geometry"
	 }"
		end

	basic_window_set_position (win: POINTER; x, y: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_set_position"
	 }"
		end

	basic_window_set_size (win: POINTER; x, y, w, h: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_set_size"
	 }"
		end

	basic_window_set_bg_color (win, color: POINTER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_set_bg_color"
	 }"
		end

	basic_window_set_bg_pixmap (win, pixmap: POINTER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_set_bg_pixmap"
	 }"
		end

	basic_window_clear_no_expose (win: POINTER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_clear_no_expose"
	 }"
		end

	basic_window_set_kbd_focus (win: POINTER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_set_kbd_focus"
	 }"
		end

	basic_window_map (win: POINTER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_map"
	 }"
		end

	basic_window_unmap (win: POINTER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_unmap"
	 }"
		end

feature {}
	basic_window_get_drawing_widget (w: POINTER): POINTER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_get_drawing_widget"
	 }"
		end

invariant
	width >= 1
	height >= 1

end -- class WINDOW
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
