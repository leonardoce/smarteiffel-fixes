-- See the Copyright notice at the end of this file.
--
class LIST_WINDOW
	-- retractable list of widget like combo or menu

inherit
	TOPLEVEL_WINDOW
		export {WIDGET, MENU} as_x_root, as_y_root
		redefine default_create
		end

creation {COMBO, MENU}
	default_create

feature {}
	default_create is
		local
			lo: COLUMN_LAYOUT
		do
			width := 1
			height := 1
			std_width := width
			std_height := height
			min_width := 1
			min_height := 1
			widget := basic_window_create(0, 0, width, height, default_pointer, False)
			container_init
			create lo
			layout := lo
			layout.set_container(Current)
			vision.register(Current)
			default_init
			create tmp_title.make(0)
			vision.root_window.child_attach(Current)
			when_expose(agent private_expose)
			when_left_up(agent private_left_up)
			when_pointer_move(agent private_pointer_move)
			when_pointer_enter(agent private_pointer_enter)
			--TODO: fix grab problem
			lo.set_spacing(1)
			lo.set_border(1)
			create draw_kit
			set_background_color(white_color)
			vision.when_focus_out(agent cancel)
		end

feature {ANY}
	updater: PROCEDURE[TUPLE[INTEGER]]

	items: COLLECTION[WIDGET]

	active_item: INTEGER

	cancel_item: INTEGER

	close_time: INTEGER

	open (elements: COLLECTION[WIDGET]; default_item: like cancel_item; callback: like updater; x_root, y_root: INTEGER) is
		require
			elements /= Void
			elements.valid_index(default_item) or else default_item = elements.lower - 1
			callback /= Void
		local
			i: INTEGER; w: WIDGET
		do
			check
				child.is_empty
			end
			from
				i := elements.lower
			until
				i > elements.upper
			loop
				w := elements.item(i)
				if w.parent /= Void then
					w.set_parent(Void)
				end
				child.add_last(w)
				w.set_parent(Current)
				i := i + 1
			end
			child_requisition_changed
			items := elements
			cancel_item := default_item
			active_item := cancel_item
			updater := callback
			-- improve position to avoid out of screen area
			set_geometry(x_root, y_root, width, height)
			map
			vision.preprocess_left_down(agent private_left_down)
			active := True
		ensure
			updater = callback
			active_item = default_item
			cancel_item = default_item
		end

feature {}
	draw_kit: DRAW_KIT

	active: BOOLEAN

	continuous_update: BOOLEAN

	private_expose is
		local
			w: WIDGET
		do
			--|*** test coordinates
			if active_item >= items.lower then
				draw_kit.set_drawable(Current)
				draw_kit.set_color(black_color)
				w := items.item(active_item)
				draw_kit.rectangle(0, w.pos_y - 1, width, w.height + 2)
			end
		end

	private_left_down is
		do
			if root_area.include(vision.pointer_x_root, vision.pointer_y_root) then
				continuous_update := True
				private_pointer_move(vision.pointer_x, vision.pointer_y)
			else
				close_time := vision.event_time
				cancel
			end
		end

	private_left_up is
		do
			continuous_update := False
			private_pointer_move(vision.pointer_x, vision.pointer_y)
			if area.include(vision.pointer_x, vision.pointer_y) then
				close(active_item)
			end
		end

	private_pointer_enter is
		do
			if vision.is_left_down then
				continuous_update := True
				--TODO: pointer_move(vision.pointer_x, vision.pointer_y)
				--(needed if there is no grab...)
			end
		end

	private_pointer_move (x, y: INTEGER) is
		local
			i: INTEGER
		do
			if continuous_update then
				from
					i := items.upper
				until
					i <= items.lower or else y >= items.item(i).pos_y
				loop
					i := i - 1
				end
				set_active(i)
			end
		end

	set_active (i: like active_item) is
		require
			items.valid_index(i) or else i = items.lower - 1
		local
			w: WIDGET
		do
			if i /= active_item then
				if active_item >= items.lower then
					w := items.item(active_item)
					clear_area(0, w.pos_y - 1, width, w.height + 2)
				end
				active_item := i
				if i >= items.lower then
					w := items.item(i)
					clear_area(0, w.pos_y - 1, width, w.height + 2)
				end
			end
		ensure
			active_item = i
		end

	cancel is
		do
			if active then
				close(cancel_item)
			end
		end

	close (i: like active_item) is
		require
			items.valid_index(i) or else i = items.lower - 1
		do
			unmap
			if i >= items.lower then
				items.item(i).set_parent(Void)
			end
			updater.call([i])
			child.make(0)
			active := False
			continuous_update := False
			vision.preprocess_left_down(Void)
		end

end -- class LIST_WINDOW
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
