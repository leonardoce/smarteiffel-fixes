-- See the Copyright notice at the end of this file.
--
class ROOT_WINDOW

inherit
	WINDOW
		redefine last_child, child_requisition_changed, default_create, as_x_root, as_y_root
		end

creation {VISION}
	default_create

feature {ANY}
	last_child: TOPLEVEL_WINDOW is
		do
			Result := child.last
		end

feature {}
	default_create is
		do
			container_init
			width := vision.display_width
			height := vision.display_height
			min_width := width
			min_height := height
			std_width := width
			std_height := height
			create {ROOT_LAYOUT} layout
			layout.set_container(Current)
			widget := basic_window_root_id
			vision.register(Current)
		end

feature {WIDGET}
	child_requisition_changed is
		local
			req_w, req_h: INTEGER; max_w, max_h: INTEGER; i: INTEGER; c: TOPLEVEL_WINDOW
		do
			from
				i := child.upper
			until
				i < 0
			loop
				c := child.item(i)
				if c.x_shrink_allowed then
					req_w := c.min_width
				else
					req_w := c.std_width
				end
				if c.y_shrink_allowed then
					req_h := c.min_height
				else
					req_h := c.std_height
				end
				if c.x_expand_allowed then
					max_w := c.max_width
				else
					max_w := c.std_width
				end
				if c.y_expand_allowed then
					max_h := c.max_height
				else
					max_h := c.std_height
				end
				c.basic_window_set_requisition(c.widget, req_w, req_h, max_w, max_h)
				c.adjust_size
				--TODO: X code failure origin ?
				c.clear_area(0, 0, c.width, c.height)
				--TODO: clear already done if adjust_size resize the window...
				i := i - 1
				layout_ready := True
			end
		end

	as_x_root (x: INTEGER): INTEGER is
			--TODO: add basic conversion to speed up
		require else
			x >= 0
			x < width
		do
			Result := x
		end

	as_y_root (y: INTEGER): INTEGER is
			--TODO: add basic conversion to speed up
		require else
			y >= 0
			y < height
		do
			Result := y
		end

feature {}
	basic_window_root_id: POINTER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_root_id"
	 }"
		end

end -- class ROOT_WINDOW
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
