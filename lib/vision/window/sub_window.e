-- See the Copyright notice at the end of this file.
--
class SUB_WINDOW

inherit
	WINDOW
		export {LAYOUT, SCROLL_VIEW} set_geometry
		redefine make_layout
		end

creation {ANY}
	make, make_layout, sub_window_create

feature {}
	make (p: like parent) is
		local
			default_layout: ROW_LAYOUT
		do
			create default_layout
			make_layout(p, default_layout)
		end

	make_layout (p: like parent; lo: like layout) is
		do
			width := 1
			height := 1
			min_width := 1
			min_height := 1
			std_width := 1
			std_height := 1
			widget := basic_window_create(0, 0, width, height, p.widget, False)
			container_init
			p.child_attach(Current)
			vision.register(Current)
			default_init
			layout := lo
			lo.set_container(Current)
		end

	sub_window_create (x, y, w, h: INTEGER; parent_window: WINDOW) is
		require
			x.in_range(0, parent_window.width - 1)
			y.in_range(0, parent_window.height - 1)
			w.in_range(1, parent_window.width - x)
			h.in_range(1, parent_window.height - y)
		do
			--TODO: std_width, min_width...
			width := w
			height := h
			widget := basic_window_create(x, y, width, height, parent_window.widget, False)
			vision.register(Current)
		end

end -- class SUB_WINDOW
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
