-- See the Copyright notice at the end of this file.
--
class TOPLEVEL_WINDOW

inherit
	WINDOW
		redefine geometry_changed, default_create, set_geometry
		end

insert
	WHEN_CLOSE_REQUESTED
		redefine default_create
		end

creation {ANY}
	default_create, make

feature {}
	default_create is
		local
			default_layout: COLUMN_LAYOUT
		do
			create default_layout
			make(default_layout)
		end

	make (lo: LAYOUT) is
		require
			lo /= Void
			lo.container = Void
		do
			width := vision.display_width #// 2
			height := vision.display_height #// 2
			std_width := width
			std_height := height
			min_width := 1
			min_height := 1
			max_width := vision.display_width
			max_height := vision.display_height
			widget := basic_window_create(0, 0, width, height, default_pointer, True)
			container_init
			layout := lo
			lo.set_container(Current)
			vision.register(Current)
			default_init
			when_geometry_change(agent geometry_changed(?, ?, ?, ?))
			create tmp_title.make(0)
			x_expand_allowed := True
			y_expand_allowed := True
			x_shrink_allowed := True
			y_shrink_allowed := True
			basic_window_set_requisition(widget, min_width, min_height, max_width, max_height)
			vision.root_window.child_attach(Current)
			debug
				when_expose(agent print_expose)
			end
		end

	print_expose is
		do
			--TODO: remove memory leak
			io.put_string("expose : (" + vision.expose_area.x.to_string + "," + vision.expose_area.y.to_string + ") " + vision.expose_area.width.to_string + "x" + vision.expose_area.height.to_string + "%N")
		end

feature {LAYOUT}
	set_geometry (x, y, w, h: INTEGER) is
		do
			check
				--*** In effect, a "require then". Not pretty at, not inherited, but still better than having the
				--*** invariant break later on <FM-15/08/2007>
				w.in_range(1, vision.display_width)
				h.in_range(1, vision.display_height)
			end
			Precursor(x, y, w, h)
		end

feature {ANY}
	max_width: INTEGER

	max_height: INTEGER

	title: STRING is
		local
			p: POINTER
		do
			p := basic_window_get_title(widget)
			if p = default_pointer then
				tmp_title.clear_count
			else
				tmp_title.from_external_copy(p)
				basic_window_free_title(p)
			end
			Result := tmp_title
		end

	set_title (t: STRING) is
		require
			t /= Void
		do
			basic_window_set_title(widget, t.to_external)
		end

feature {ROOT_WINDOW}
	basic_window_set_requisition (w: POINTER; min_w, min_h, max_w, max_h: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_set_requisition"
	 }"
		end

feature {}
	tmp_title: STRING

	geometry_changed (x, y, w, h: INTEGER) is
		require
			w.in_range(1, vision.display_width)
			h.in_range(1, vision.display_height)
		do
			pos_x := x
			pos_y := y
			if width /= w or else height /= h then
				if valid_width(w) and then valid_height(h) then
					width := w
					height := h
					--TODO: resize_decorations(w, h)?
					clear_area(0, 0, width, height)
					invalidate_layout
					dispatch
				end
			end
		end

	basic_window_get_title (w: POINTER): POINTER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_get_title"
	 }"
		end

	basic_window_free_title (t: POINTER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_free_title"
	 }"
		end

	basic_window_set_title (w, t: POINTER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "window"
	 feature_name: "basic_window_set_title"
	 }"
		end

invariant
	width.in_range(1, vision.display_width)
	height.in_range(1, vision.display_height)

end -- class TOPLEVEL_WINDOW
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
