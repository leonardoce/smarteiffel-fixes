-- See the Copyright notice at the end of this file.
--
class FONT_MANAGER
	-- The font manager is singleton accessible via `font_manager'
	-- from `GRAPHIC'. This class help to find the font on the system best
	-- matching given whishes.
	-- All created fonts are accessible in `fonts' attribute, the
	-- default font on the system is the first one.

creation {VISION}
	init

feature {}
	init is
		local
			f: BASIC_FONT; tmp: STRING
		do
			create fonts.make(0)
			create tmp.from_external(basic_font_default_name)
			create f.make_from_id(basic_font_new(tmp.to_external))
			fonts.add_last(f)
		end

feature {ANY}
	fonts: FAST_ARRAY[BASIC_FONT]
			-- All allocated fonts, the first one is the default system font.

	default_font: BASIC_FONT is
			-- The default system font, always defined.
		do
			Result := fonts.first
		ensure
			Result /= Void
		end

	font_exist_by_name (font_name: STRING): BOOLEAN is
			-- It's recommended not to use this function.
			-- The `font_name' is the font name in the system syntax (ex: XLFD
			-- for X11).
		require
			font_name /= Void
		do
			Result := basic_font_exist(font_name.to_external)
		end

feature {}
	basic_font_default_name: POINTER is
			-- should return the font id and suppress folowing function
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "font"
	 feature_name: "basic_font_default_name"
	 }"
		end

	basic_font_new (font_name: POINTER): POINTER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "font"
	 feature_name: "basic_font_new"
	 }"
		end

	basic_font_exist (desc: POINTER): BOOLEAN is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "font"
	 feature_name: "basic_font_exist"
	 }"
		end

end -- class FONT_MANAGER
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
