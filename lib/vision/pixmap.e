-- See the Copyright notice at the end of this file.
--
class PIXMAP
	-- `PIXMAP' is `DRAWABLE' area like `WINDOW' but allow you to draw "off
	-- the screen" and the put it on the screen at any time.
	-- The pixmap is in display memory, not in the process' memory
	-- (see also `IMAGE').

inherit
	DRAWABLE
		--TODO: change to CONTAINER?
	SENSITIVE
	GRAPHIC
		--TODO: remove? (needed for expose_area acces from vision...)

creation {ANY}
	make

feature {ANY}
	make (width_, height_: INTEGER) is
			-- The pixmap is created with default_draw_kit color.
		require
			width_ > 0
			height_ > 0
		do
			if width /= width_ or else height /= height_ then
				if widget /= default_pointer then
					basic_pixmap_free(widget, drawing_widget)
				end
				width := width_
				height := height_
				std_width := width_
				std_height := height_
				if min_width > width_ then
					min_width := width_
				end
				if min_height > height_ then
					min_height := height_
				end
				widget := basic_pixmap_create(width_, height_, $drawing_widget)
			end
			vision.default_draw_kit.set_drawable(Current)
			vision.default_draw_kit.fill_rectangle(0, 0, width_, height_)
			if parent /= Void then
				parent.child_requisition_changed
			end
		end

feature {ANY}
	width: INTEGER

	height: INTEGER

	depth: INTEGER

	min_width: INTEGER

	min_height: INTEGER

	std_width: INTEGER

	std_height: INTEGER

	background_color: COLOR --TODO: used ? no style ?

	set_background_color (c: COLOR) is
			-- Define the color to use to clear the pixmap (pixmap is left
			-- unchanged).
			--TODO: use this color for clear
		require
			c /= Void
		do
			background_color := c
		end

	clear_without_expose is
		do
			vision.default_draw_kit.set_drawable(Current)
			if background_color = Void then
				vision.default_draw_kit.set_color(black_color)
			else
				vision.default_draw_kit.set_color(background_color)
			end
			vision.default_draw_kit.fill_rectangle(0, 0, std_width, std_height)
		end

feature {LAYOUT}
	expose_paint is
		local
			rect: RECT
		do
			rect := vision.expose_area.intersect_def(pos_x, pos_y, width, height)
			if rect.width > 0 then
				basic_pixmap_draw(parent.drawing_widget, drawing_widget, rect.x - pos_x, rect.y - pos_y, rect.x, rect.y, rect.width, rect.height)
			end
		end

	set_geometry (x, y, w, h: INTEGER) is
		do
			pos_x := x
			pos_y := y
			if width /= w or else height /= h then
				width := w
				height := h
				std_width := w.max(1)
				std_height := h.max(1)
				basic_pixmap_free(widget, drawing_widget)
				if w > 0 and then h > 0 then
					widget := basic_pixmap_create(w, h, $drawing_widget)
					vision.default_draw_kit.set_drawable(Current)
					vision.default_draw_kit.fill_rectangle(0, 0, w, h)
				else
					widget := default_pointer
				end
			end
		end

	resize (w, h: INTEGER) is
		do
			make(w, h)
		end

feature {}
	basic_pixmap_create (w, h: INTEGER; returned_drawing: POINTER): POINTER is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "pixmap"
	 feature_name: "basic_pixmap_create"
	 }"
		end

	basic_pixmap_free (pixmap, drawable_pixmap: POINTER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "pixmap"
	 feature_name: "basic_pixmap_free"
	 }"
		end

	basic_pixmap_draw (drawable_win: POINTER; drawable_pixmap: POINTER; src_x, src_y, dest_x, dest_y, w, h: INTEGER) is
		external "plug_in"
		alias "{
	 location: "${sys}/plugins/vision"
	 module_name: "pixmap"
	 feature_name: "basic_pixmap_draw"
	 }"
		end

end -- class PIXMAP
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
