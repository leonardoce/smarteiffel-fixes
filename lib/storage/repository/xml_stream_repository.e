-- See the Copyright notice at the end of this file.
--
class XML_STREAM_REPOSITORY [O_ -> STORABLE]
	--
	-- To be used via streams that commit and update the XML repository
	--
inherit
	XML_REPOSITORY_IMPL[O_]

create {ANY}
	connect_to

feature {ANY} -- Updating and committing
	commit is
		do
			write_to_stream(commit_stream)
		end

	is_commitable: BOOLEAN is
		do
			Result := commit_stream /= Void and then commit_stream.is_connected
		end

	update is
		do
			read_from_stream(update_stream)
		end

	is_updateable: BOOLEAN is
		do
			Result := update_stream /= Void and then update_stream.is_connected
		end

	is_connected: BOOLEAN is
		do
			Result := commit_stream /= Void and then commit_stream.is_connected
		end

feature {ANY} -- Creation
	connect_to (in_stream: like update_stream; out_stream: like commit_stream) is
			-- Connect to a repository with streams as physical store.
		do
			make
			update_stream := in_stream
			commit_stream := out_stream
		end

feature {}
	update_stream: INPUT_STREAM
	commit_stream: OUTPUT_STREAM

end -- class XML_STREAM_REPOSITORY
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
