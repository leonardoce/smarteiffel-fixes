-- See the Copyright notice at the end of this file.
--
expanded class CHARACTER_CONSTANTS
	--
	-- Platform-independent, universal, character constants.
	-- Intended to be used as ancestor for classes that need these constants.
	--

feature {ANY} -- Character names:
	Ctrl_a: CHARACTER is '%/1/'

	Ctrl_b: CHARACTER is '%/2/'

	Ctrl_c: CHARACTER is '%/3/'

	Ctrl_d: CHARACTER is '%/4/'

	Ctrl_e: CHARACTER is '%/5/'

	Ctrl_f: CHARACTER is '%/6/'

	Ctrl_g: CHARACTER is '%/7/'

	Ch_bs: CHARACTER is '%/8/'

	Ch_tab: CHARACTER is '%/9/'

	Ctrl_j: CHARACTER is '%/10/'

	Ctrl_k: CHARACTER is '%/11/'

	Ctrl_l: CHARACTER is '%/12/'

	Ctrl_m: CHARACTER is '%/13/'

	Ctrl_n: CHARACTER is '%/14/'

	Ctrl_o: CHARACTER is '%/15/'

	Ctrl_p: CHARACTER is '%/16/'

	Ctrl_q: CHARACTER is '%/17/'

	Ctrl_r: CHARACTER is '%/18/'

	Ctrl_s: CHARACTER is '%/19/'

	Ctrl_t: CHARACTER is '%/20/'

	Ctrl_u: CHARACTER is '%/21/'

	Ctrl_v: CHARACTER is '%/22/'

	Ctrl_w: CHARACTER is '%/23/'

	Ctrl_x: CHARACTER is '%/24/'

	Ctrl_y: CHARACTER is '%/25/'

	Ctrl_z: CHARACTER is '%/26/'

	Ch_del: CHARACTER is '%/127/'

end -- class CHARACTER_CONSTANTS
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
