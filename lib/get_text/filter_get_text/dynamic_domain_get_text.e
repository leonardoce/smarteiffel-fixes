class DYNAMIC_DOMAIN_GET_TEXT[E_]

inherit
	DOMAIN_GET_TEXT[E_]

creation {FILTER_GET_TEXT}
	make

feature {}
	make (domain_name: STRING; filtered_: like filtered) is
		do
			text_domain := domain_name
			connect_to(filtered_)
		end

feature {ANY}
	text_domain: STRING

end -- class DYNAMIC_DOMAIN_GET_TEXT
