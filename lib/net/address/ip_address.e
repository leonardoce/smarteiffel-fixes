-- See the Copyright notice at the end of this file.
--
class IP_ADDRESS
	-- The host represented by its address. Currently only IPv4 is supported.

inherit
	ADDRESS
		redefine out
		end

creation {ANY}
	make

feature {ANY}
	a, b, c, d: INTEGER
			-- The four parts of an IP address.
			-- Example: 192.168.0.1 will be represented:
			--   a = 192
			--   b = 168
			--   c = 0
			--   d = 1

	hash_code: INTEGER is
		do
			Result := a & 0x7f |<< 24 | (b |<< 16) | (c |<< 8) | d
		end

	ip: IP_ADDRESS is
		do
			Result := Current
		end

	out: STRING is
		do
			Result := a.out + "." + b.out + "." + c.out + "." + d.out
		end

feature {}
	make (ip_a, ip_b, ip_c, ip_d: INTEGER) is
		require
			ip_a.in_range(0, 255)
			ip_b.in_range(0, 255)
			ip_c.in_range(0, 255)
			ip_d.in_range(0, 255)
		do
			a := ip_a
			b := ip_b
			c := ip_c
			d := ip_d
		end

end -- class IP_ADDRESS
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
