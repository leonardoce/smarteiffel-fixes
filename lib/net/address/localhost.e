-- See the Copyright notice at the end of this file.
--
class LOCALHOST
	--
	-- The very machine on which the program runs
	--
	-- The name resolution part was originally written by Serge Romanchenko <se@sir.nensi.net>
	--

inherit
	HOST
		rename
			make as make_host
		export
			{} make_host;
			{ANY} last_error, last_error_number
		end

creation {ANY}
	make

feature {ANY}
	fqdn: STRING is
			-- Fully Qualified Domain Name
		do
			Result := once ""
			Result.copy(host_name)
			Result.extend('.')
			Result.append(domain_name)
		end

	host_name: STRING is
			-- Short host name
		local
			e: POINTER
		do
			e := net_gethostname
			if e /= default_pointer then
				Result := once ""
				Result.from_external_copy(e)
			end
		end

	domain_name: STRING is
			-- Full domain name (without the `host_name`)
		local
			e: POINTER
		do
			e := net_getdomainname
			if e /= default_pointer then
				Result := once ""
				Result.from_external_copy(e)
			end
		end

	set_host_name (a_host_name: STRING) is
		require
			a_host_name /= Void and then not a_host_name.is_empty
		do
			net_sethostname(a_host_name.to_external, a_host_name.count)
		end

	set_domain_name (a_domain_name: STRING) is
		require
			a_domain_name /= Void and then not a_domain_name.is_empty
		do
			net_setdomainname(a_domain_name.to_external, a_domain_name.count)
		end

feature {}
	make is
		do
			make_host(once "localhost")
		end

end -- class LOCALHOST
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
