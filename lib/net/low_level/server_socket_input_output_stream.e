-- See the Copyright notice at the end of this file.
--
class SERVER_SOCKET_INPUT_OUTPUT_STREAM

inherit
	SOCKET_INPUT_OUTPUT_STREAM
		redefine socket_disconnected
		end

creation {SOCKET_SERVER}
	connect_to

feature {ANY}
	disconnect is
		do
			detach
			server.unbind(socket)
		end

feature {}
	connect_to (a_server: SOCKET_SERVER; a_read_sync: BOOLEAN) is
		require
			a_server /= Void
		do
			server := a_server
			socket := a_server.bind
			make(a_read_sync)
		end

	socket: SOCKET

	server: SOCKET_SERVER

	socket_disconnected (a_socket: SOCKET) is
		do
			if server.has_socket(a_socket) then
				server.socket_disconnected(a_socket)
			end
			Precursor(a_socket)
		end

invariant
	server /= Void

end -- class SERVER_SOCKET_INPUT_OUTPUT_STREAM
--
-- ------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- This file is part of the SmartEiffel standard library.
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without
-- limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
-- the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial
-- portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
-- EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
-- AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
-- OR OTHER DEALINGS IN THE SOFTWARE.
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------
