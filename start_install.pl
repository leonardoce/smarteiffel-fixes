#!/usr/bin/env perl

# This is a little script to bootstrap "compile_to_c" from the germ files
# in order to start the official SmartEiffel installation process.
#
# I use Perl because this source code repository is managed by Git and, if
# you have Git, you have perl. 
use strict;
use warnings;

# Get the executable file bootstrap
sub get_exe_suffix {
	my $os = $^O;

	if ($os eq "mingw" || $os eq "msys" || $os =~ /win.*/i) {
		return ".exe";
	} else {
		return "";
	}
}

# Execute a command and dies if it hasn'n worked correctly
sub system_or_die {
	my $command = shift;

	print ($command);
	print ("\n");
	system ($command);
	if ($? != 0) {
		die ("Error executing: ${command}");
	}
}

# Compile an executable using GCC from one C file
sub compile_to_exe {
	my $exeName = shift;
	my $sourceName = shift;
	my $exeSuffix = get_exe_suffix();

	my $command = "gcc -o $exeName${exeSuffix} $sourceName";
	system_or_die ($command);
}

# This check the C compiler
sub check_c_compiler {
	print ("Checking Gcc compiler...\n");
	open (my $in, ">", "test_compiler.c") or die("Cannot write to test_compiler.c");
	print $in "int main(int argc, char **argv) { return 0; }\n";
	close $in;

	system ("gcc -o test_compiler.exe test_compiler.c");
	my $status = $?;
	unlink ("test_compiler.c");
	unlink ("test_compiler.exe");

	if ($status != 0) {
		die ("GNU C Compiler is NOT working. Please check the compiler configuration");
	} else {
		print ("GNU C Compiler is correctly working\n");
	}
}

# compile_to_c bootstrap
sub bootstrap_compile_to_c {
	print ("Bootstrapping compile_to_c from germ files...\n");
	compile_to_exe ("install/germ/compile_to_c", "work/germ/compile_to_c.c");
}

# boostrap install from germ
sub bootstrap_install {
	print ("Bootstrapping install from germ files...\n");
	compile_to_exe ("install_se", "work/germ/install.c");
}

check_c_compiler ();
bootstrap_compile_to_c ();
bootstrap_install ();
print ("Now you can start install with the \"install_se\" in this directory\n");
