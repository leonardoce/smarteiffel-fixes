-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_BASIC6
	-- Create pixmap and draw lines and display

inherit
	GRAPHIC

creation {ANY}
	make

feature {}
	win: TOPLEVEL_WINDOW

	pixmap: PIXMAP

	draw_kit: DRAW_KIT

	make is
		do
			create win.default_create
			win.map
			win.when_close_requested(agent vision.loop_stack.break)
			create draw_kit
			create pixmap.make(200, 100)
			draw_kit.set_drawable(pixmap)
			draw_kit.set_color(red_color)
			draw_kit.fill_rectangle(0, 0, 200, 100)
			draw_kit.set_color(yellow_color)
			draw_kit.line(0, 0, 99, 99)
			draw_kit.line(100, 99, 199, 0)
			win.child_attach(pixmap)
			vision.start
			io.put_string("The end%N")
		end

end -- class TEST_BASIC6
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
