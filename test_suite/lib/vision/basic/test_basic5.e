-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_BASIC5
	-- Create window and sub-windows with lines and rectangles

inherit
	GRAPHIC

creation {ANY}
	make

feature {}
	win: TOPLEVEL_WINDOW

	sub_win: SUB_WINDOW

	make is
		local
			draw_style: DRAW_STYLE; rectangle: RECTANGLE; h_line: HORIZONTAL_LINE; v_line: VERTICAL_LINE
			fill_rectangle: FILL_RECTANGLE
		do
			create win.default_create
			win.set_background_color(white_color)
			win.map
			win.when_close_requested(agent vision.loop_stack.break)
			create draw_style
			draw_style.set_color(red_color)
			create rectangle.make(20, 35, 20, 35)
			rectangle.set_x_expand(False)
			rectangle.set_y_expand(False)
			rectangle.set_style(draw_style)
			win.child_attach(rectangle)
			create sub_win.make(win)
			sub_win.set_x_expand(True)
			sub_win.set_y_expand(True)
			sub_win.map
			create h_line
			h_line.set_style(draw_style)
			sub_win.child_attach(h_line)
			create v_line
			v_line.set_style(draw_style)
			sub_win.child_attach(v_line)
			create fill_rectangle.make(20, 35, 20, 35)
			fill_rectangle.set_x_expand(False)
			fill_rectangle.set_y_expand(False)
			fill_rectangle.set_style(draw_style)
			sub_win.child_attach(fill_rectangle)
			create rectangle.make(20, 35, 20, 35)
			rectangle.set_x_expand(False)
			rectangle.set_y_expand(False)
			rectangle.set_style(draw_style)
			sub_win.child_attach(rectangle)
			vision.start
			io.put_string("The end%N")
		end

end -- class TEST_BASIC5
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
