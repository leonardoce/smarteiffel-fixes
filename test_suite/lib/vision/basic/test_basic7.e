-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_BASIC7
	-- Create window and lots of sub-windows

inherit
	GRAPHIC

creation {ANY}
	make

feature {}
	win: TOPLEVEL_WINDOW

	make is
		local
			red, green: INTEGER; sub_win: SUB_WINDOW; line: CONTAINER; row_layout: ROW_LAYOUT; color: COLOR
		do
			create win.default_create
			win.set_background_color(white_color)
			win.when_close_requested(agent vision.loop_stack.break)
			win.layout_pause
			from
				red := 0
			until
				red > 255
			loop
				from
					create row_layout
					create line.make_layout(win, row_layout)
					line.set_x_expand(True)
					line.set_y_expand(True)
					green := 0
				until
					green > 255
				loop
					create sub_win.make(line)
					sub_win.set_x_expand(True)
					sub_win.set_y_expand(True)
					create color.like_rgb_8(red, green, 128)
					sub_win.set_background_color(color)
					sub_win.map
					green := green + 10
				end
				red := red + 10
			end
			win.layout_continue
			win.map
			vision.start
			io.put_string("The end%N")
		end

end -- class TEST_BASIC7
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
