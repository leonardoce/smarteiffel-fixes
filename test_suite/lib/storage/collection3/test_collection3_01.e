-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_COLLECTION3_01
	-- tests ARRAY3
	--
	-- Jean - Philippe Caillaut Mai 1999
	--

creation {ANY}
	make

feature {ANY}
	a, b: ARRAY3[INTEGER]

	c: ARRAY[ARRAY[ARRAY[INTEGER]]]

	make is
		do
			--
			-- tests make-array3, count, count1, count2, count3,
			-- lower1, upper1, lower2, upper2, lower3, upper3
			--
			create a.make(1, 2, 3, 5, 6, 9)
			assert(a.count = 24)
			assert(a.count1 = 2)
			assert(a.count2 = 3)
			assert(a.count3 = 4)
			assert(a.lower1 = 1)
			assert(a.upper1 = 2)
			assert(a.lower2 = 3)
			assert(a.upper2 = 5)
			assert(a.lower3 = 6)
			assert(a.upper3 = 9) -- 10
			create a.make(0, 1, 0, 1, 0, 1)
			assert(a.count = 8)
			assert(a.count1 = 2)
			assert(a.count2 = 2)
			assert(a.count3 = 2)
			assert(a.lower1 = 0)
			assert(a.upper1 = 1)
			assert(a.lower2 = 0)
			assert(a.upper2 = 1)
			assert(a.lower3 = 0)
			assert(a.upper3 = 1) -- 20
			create b.from_collection({ARRAY[INTEGER] 1, << 0, 0, 0, 0, 0, 0, 0, 0 >> }, 0, 1, 0, 1, 0, 1)
			assert(b.count = 8)
			assert(b.count1 = 2)
			assert(b.count2 = 2)
			assert(b.count3 = 2)
			assert(b.lower1 = 0)
			assert(b.upper1 = 1)
			assert(b.lower2 = 0)
			assert(b.upper2 = 1)
			assert(b.lower3 = 0)
			assert(b.upper3 = 1) -- 30
			assert(a.is_equal(b))
			--
			-- tests occurrences and fast_occurrences
			--
			create a.from_collection({ARRAY[INTEGER] 1, << 1, 2, 2, 4, 5, 6, 7, 8, 9 >> }, 1, 3, 1, 3, 1, 1)
			assert(a.occurrences(2) = 2)
			assert(a.occurrences(3) = 0)
			assert(a.occurrences(9) = 1)
			assert(a.occurrences(4) = 1) -- 36
			assert(a.fast_occurrences(2) = 2)
			assert(a.fast_occurrences(3) = 0)
			assert(a.fast_occurrences(4) = 1)
			assert(a.fast_occurrences(9) = 1)
			-- 40
			--
			-- tests set_all_with
			--
			create a.make(0, 5, 0, 5, 1, 2)
			a.set_all_with(2)
			assert(a.occurrences(2) = 72)
			assert(a.item(0, 0, 1) = 2)
			assert(a.item(0, 5, 1) = 2)
			assert(a.item(5, 0, 2) = 2)
			assert(a.item(5, 5, 2) = 2)
			assert(a.item(2, 2, 1) = 2)
			-- 46
			--
			-- tests from_collection3
			--
			create a.make(0, 2, 0, 2, 1, 2)
			create b.make(2, 3, 4, 5, 3, 6)
			b.put(2, 2, 4, 3)
			a.from_collection3(b)
			assert(a.is_equal(b))
			create b.make(-2, 5, -1, 3, -4, 5)
			a.from_collection3(b)
			assert(a.is_equal(b))
			--
			-- tests force
			--
			create a.make(0, 1, 0, 1, 1, 2)
			a.force(1, 2, 2, 3)
			assert(a.count = 27)
			assert(a.count1 = 3)
			assert(a.count2 = 3)
			assert(a.count3 = 3)
			assert(a.upper1 = 2)
			assert(a.lower1 = 0)
			assert(a.upper2 = 2)
			assert(a.lower2 = 0)
			assert(a.upper3 = 3)
			assert(a.lower3 = 1)
			assert(a.item(0, 2, 2) = 0)
			assert(a.item(2, 0, 1) = 0)
			assert(a.item(2, 2, 3) = 1) -- 63
			a.force(1, 1, 1, 1)
			assert(a.count = 27)
			assert(a.count1 = 3)
			assert(a.count2 = 3)
			assert(a.count3 = 3)
			assert(a.upper1 = 2)
			assert(a.lower1 = 0)
			assert(a.upper2 = 2)
			assert(a.lower2 = 0)
			assert(a.upper3 = 3)
			assert(a.lower3 = 1)
			assert(a.occurrences(0) = 25)
			assert(a.item(1, 1, 1) = 1)
			-- 75
			--
			-- tests sub_collection3
			--
			create a.make(0, 1, 0, 1, 1, 2)
			b := a.sub_collection3(0, 1, 0, 1, 1, 2)
			assert(b.count = 8)
			assert(b.upper1 = 1)
			assert(b.lower1 = 0)
			assert(b.upper2 = 1)
			assert(b.lower2 = 0)
			assert(b.upper3 = 2)
			assert(b.lower3 = 1)
			assert(b.item(1, 1, 1) = 0) -- 83
			create a.from_collection({ARRAY[INTEGER] 1, << 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 >> }, -1, 1, -1, 1, -1, 0)
			b := a.sub_collection3(0, 1, 0, 1, -1, -1)
			assert(b.count = 4)
			assert(b.upper1 = 1)
			assert(b.lower1 = 0)
			assert(b.upper2 = 1)
			assert(b.lower2 = 0)
			assert(b.upper3 = -1)
			assert(b.lower3 = -1)
			assert(b.item(0, 0, -1) = 9)
			assert(b.item(1, 1, -1) = 17)
			-- 92
			--
			-- tests copy
			--
			create a.from_collection({ARRAY[INTEGER] 1, << 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 >> }, 0, 1, -1, 1, 0, 1)
			create b.make(0, 0, 0, 0, 1, 1)
			b.copy(a)
			assert(b.count = 12)
			assert(b.upper1 = 1)
			assert(b.lower1 = 0)
			assert(b.upper2 = 1)
			assert(b.lower2 = -1)
			assert(b.upper3 = 1)
			assert(b.lower3 = 0)
			assert(b.item(0, -1, 1) = 2) -- 100
			create b.make(-2, 3, -5, -2, 1, 2)
			b.copy(a)
			assert(b.count = 12)
			assert(b.upper1 = 1)
			assert(b.lower1 = 0)
			assert(b.upper2 = 1)
			assert(b.lower2 = -1)
			assert(b.upper3 = 1)
			assert(b.lower3 = 0)
			assert(b.item(0, -1, 1) = 2) -- 108
			create a.make(0, 2, 0, 2, 1, 2)
			create b.make(0, 1, 0, 1, 1, 2)
			b.copy(a)
			assert(b.count = 18)
			assert(b.upper1 = 2)
			assert(b.lower1 = 0)
			assert(b.upper2 = 2)
			assert(b.lower2 = 0)
			assert(b.upper3 = 2)
			assert(b.lower3 = 1)
			assert(b.item(1, 1, 1) = 0)
			-- 116
			--
			-- tests put and item
			--
			create a.make(0, 1, 0, 1, 0, 1)
			assert(a.item(0, 0, 0) = 0)
			a.put(1, 0, 0, 0)
			assert(a.item(0, 0, 0) = 1)
			-- 118
			--
			-- tests has and fast_has
			--
			create a.make(0, 1, 0, 1, 0, 1)
			assert(a.has(0))
			assert(a.fast_has(0))
			assert(not a.has(1))
			assert(not a.fast_has(1))
			a.put(2, 1, 1, 1)
			assert(a.has(2))
			assert(a.fast_has(2))
			-- 124
			--
			-- tests all_default
			--
			create a.make(0, 1, 0, 1, 0, 1)
			assert(a.all_default)
			a.put(1, 1, 1, 1)
			assert(not a.all_default)
			-- 126
			--
			-- tests 'replace_all' and 'fast_replace_all'
			--
			create a.make(0, 2, 0, 2, -1, 1)
			a.replace_all(0, 1)
			assert(a.occurrences(1) = 27)
			a.fast_replace_all(1, 0)
			assert(a.all_default)
			assert(a.occurrences(0) = 27)
			-- 129
			--
			-- tests swap
			--
			create a.from_collection({ARRAY[INTEGER] 1, << 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 >> }, 0, 1, -1, 1, -1, 0)
			a.swap(0, -1, -1, 1, 1, 0)
			assert(a.item(0, -1, -1) = 12)
			assert(a.item(1, 1, 0) = 1)
			-- 131
			--
			-- tests resize
			--
			create a.make(-2, 2, -1, 1, 3, 6)
			a.put(1, 1, 1, 3)
			a.resize(0, 1, 0, 2, 3, 4)
			assert(a.count = 12)
			assert(a.count1 = 2)
			assert(a.count2 = 3)
			assert(a.count3 = 2)
			assert(a.upper1 = 1)
			assert(a.lower1 = 0)
			assert(a.upper2 = 2)
			assert(a.lower2 = 0)
			assert(a.upper3 = 4)
			assert(a.lower3 = 3)
			assert(a.item(1, 1, 3) = 1)
			assert(not a.all_default) -- 143
			create a.make(0, 5, 1, 6, 0, 1)
			a.resize(1, 2, 3, 4, 0, 2)
			assert(a.count = 12)
			assert(a.count1 = 2)
			assert(a.count2 = 2)
			assert(a.count3 = 3)
			assert(a.upper1 = 2)
			assert(a.lower1 = 1)
			assert(a.upper2 = 4)
			assert(a.lower2 = 3)
			assert(a.upper3 = 2)
			assert(a.lower3 = 0)
			assert(a.all_default)
			-- 154
			--
			-- tests from_model
			--
			create c.make(0, 1)
			c.from_collection({ARRAY[ARRAY[ARRAY[INTEGER]]] 1, << {ARRAY[ARRAY[INTEGER]] 1, << {ARRAY[INTEGER] 1, << 1, 2 >> }, {ARRAY[INTEGER] 1, << 3, 4 >> } >> }, {ARRAY[ARRAY[INTEGER]] 1, << {ARRAY[INTEGER] 1, << 5, 6 >> }, {ARRAY[INTEGER] 1, << 7, 8 >> } >> }, {ARRAY[ARRAY[INTEGER]] 1, << {ARRAY[INTEGER] 1, << 9, 10 >> }, {ARRAY[INTEGER] 1, << 11, 12 >> } >> } >> })
			create b.from_model(c)
			assert(b.count = 12)
			assert(b.count1 = 3)
			assert(b.count2 = 2)
			assert(b.count3 = 2)
			assert(b.upper1 = 3)
			assert(b.lower1 = 1)
			assert(b.upper2 = 2)
			assert(b.lower2 = 1)
			assert(b.upper3 = 2)
			assert(b.lower3 = 1)
			assert(b.item(1, 2, 1) = 3) -- 166
		end

feature {}
	assert (bool: BOOLEAN) is
		do
			cpt := cpt + 1
			if not bool then
				std_output.put_string("TEST_COLLECTION3_01: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
			end
		end

	cpt: INTEGER

end -- class TEST_COLLECTION3_01
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
