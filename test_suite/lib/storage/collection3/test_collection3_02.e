-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_COLLECTION3_02

insert
	EIFFELTEST_TOOLS

creation {ANY}
	main

feature {ANY}
	main is
		local
			collection1: FAST_ARRAY3[INTEGER]; collection2: ARRAY3[INTEGER]; c2: COLLECTION3[INTEGER]
		do
			create collection1.make(4, 5, 3)
			collection1.put(6, 3, 4, 2)
			assert(collection1.count1 = 4)
			assert(collection1.count2 = 5)
			assert(collection1.count3 = 3)
			create collection2.make(0, 3, 0, 4, 0, 2)
			collection2.put(6, 3, 4, 2)
			assert(collection2.count1 = 4)
			assert(collection2.count2 = 5)
			assert(collection2.count3 = 3)
			c2 := collection2
			assert(not c2.same_dynamic_type(collection1))
			c2 := collection1
			assert(not c2.same_dynamic_type(collection2))
		end

end -- class TEST_COLLECTION3_02
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
