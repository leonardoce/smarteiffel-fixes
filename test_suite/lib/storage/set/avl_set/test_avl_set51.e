-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_AVL_SET51

insert
	EIFFELTEST_TOOLS

creation {ANY}
	make

feature {ANY}
	make is
		local
			s, s2: AVL_SET[INTEGER]; i: INTEGER
		do
			create s.make
			from
				s.clear_count
				i := 0
			until
				i = 100
			loop
				i := i + 1
				s.add(i)
			end
			assert(s.count = 100)
			assert(not s.has(0))
			assert(s.has(1))
			assert(s.has(51))
			assert(s.has(100))
			assert(not s.has(101))
			from
				i := 0
			until
				i >= 100
			loop
				i := i + 3
				s.remove(i)
			end
			assert(s.count = 67)
			assert(not s.has(3))
			assert(s.has(4))
			assert(s.has(5))
			assert(not s.has(6))
			assert(s.has(98))
			assert(not s.has(99))
			from
				i := 1
			until
				i >= 100
			loop
				i := i + 3
				s.remove(i)
			end
			i := s.count
			assert(s.count = 34)
			from
				i := -1
			until
				i >= 100
			loop
				i := i + 3
				s.remove(i)
			end
			assert(s.count = 1)
			assert(s.has(1))
			from
				i := 1
			until
				i = 100
			loop
				i := i + 3
				s.add(i)
			end
			assert(s.count = 34)
			from
				i := -1
			until
				i >= 100
			loop
				i := i + 3
				s.add(i)
			end
			create s2.make
			from
				i := 0
			until
				i >= 100
			loop
				i := i + 3
				s2.add(i)
			end
			s.union(s2)
			assert(s2.is_subset_of(s))
			assert(s.count = 102)
		end

end -- class TEST_AVL_SET51
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
