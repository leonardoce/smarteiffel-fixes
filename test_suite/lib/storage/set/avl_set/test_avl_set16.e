-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_AVL_SET16

insert
	EIFFELTEST_TOOLS

creation {ANY}
	make

feature {}
	make is
		local
			s1, s2: AVL_SET[INTEGER]
		do
			create s1.make
			create s2.make
			s1.add(15)
			s1.add(18)
			s2.add(10)
			s2.add(16)
			s1.union(s2)
			assert(s1.has(10))
			assert(s1.has(16))
			assert(s1.count = 4)
			s2.add(14)
			assert(s1.count = 4)
			assert(not s1.has(14))
			s2.clear_count
			s1.union(s2)
			assert(s1.count = 4)
			s2.add(10)
			s2.add(15)
			s2.add(-6)
			s1.union(s2)
			assert(s1.count = 5)
			assert(s1.has(-6))
			s1.union(s2)
			assert(s1.count = 5)
			s2.clear_count
			s1.union(s2)
			assert(s1.count = 5)
			s2.union(s1)
			assert(s2.count = s1.count)
			assert(s2.count = 5)
			assert(s2.is_equal(s1))
		end

end -- class TEST_AVL_SET16
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
