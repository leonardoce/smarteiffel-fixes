-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_HASHED_SET44

insert
	EIFFELTEST_TOOLS

creation {ANY}
	make

feature {ANY}
	make is
		local
			s1, s2: HASHED_SET[STRING]
		do
			create s1.make
			create s2.make
			assert(s1.is_disjoint_from(s2))
			s1.add("15")
			s1.add("18")
			assert(s1.is_disjoint_from(s2))
			s2.add("14")
			assert(s1.is_disjoint_from(s2))
			assert(s2.is_disjoint_from(s1))
			s2.add("18")
			assert(not s1.is_disjoint_from(s2))
			s1.minus(s2)
			s1.add("47")
			s1.add("44")
			assert(s1.is_disjoint_from(s2))
			s2.add("43")
			s2.add("45")
			assert(s2.is_disjoint_from(s1))
			assert(not s2.is_disjoint_from(s2))
			s2.add("44")
			s2.add("666")
			s2.add("777")
			assert(not s2.is_disjoint_from(s1))
			s1.clear_count
			assert(s1.is_disjoint_from(s2))
		end

end -- class TEST_HASHED_SET44
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
