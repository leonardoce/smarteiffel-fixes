-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_HASHED_SET13

insert
	EIFFELTEST_TOOLS

creation {ANY}
	make

feature {ANY}
	make is
		local
			s1, s2: HASHED_SET[INTEGER]
		do
			create s1.make
			create s2.make
			assert((s1 + s2).is_empty)
			s1.add(15)
			assert((s1 + s2).has(15))
			assert((s1 + s2).count = 1)
			s2.add(15)
			assert((s1 + s2).count = 1)
			assert((s1 + s2).has(15))
			s2.add(30)
			s2.add(40)
			s1.add(40)
			s1.add(555)
			assert((s1 + s2).count = 4)
			assert((s1 + s2).has(40))
			s1.clear_count
			assert((s1 + s2).count = 3)
		end

end -- class TEST_HASHED_SET13
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
