-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_HASHED_SET23

insert
	EIFFELTEST_TOOLS

creation {ANY}
	make

feature {ANY}
	make is
		local
			s, s2: HASHED_SET[INTEGER]; it: ITERATOR[INTEGER]; rem: INTEGER
		do
			create s.make
			it := s.get_new_iterator
			it.start
			assert(it.is_off)
			s.add(47)
			s.add(35)
			s.add(11)
			s.add(4333)
			s2 := s.twin
			it := s.get_new_iterator
			it.start
			assert(not it.is_off)
			assert(s.has(it.item))
			assert(s2.has(it.item))
			rem := it.item
			s2.remove(it.item)
			it.next
			assert(not it.is_off)
			assert(s.has(it.item))
			assert(s2.has(it.item))
			s2.remove(it.item)
			it.next
			assert(not it.is_off)
			assert(s.has(it.item))
			assert(s2.has(it.item))
			s2.remove(it.item)
			it.next
			assert(not it.is_off)
			assert(s.has(it.item))
			assert(s2.has(it.item))
			s2.remove(it.item)
			it.next
			assert(it.is_off)
			assert(s2.is_empty)
			it.start
			assert(not it.is_off)
			assert(it.item.is_equal(rem))
		end

end -- class TEST_HASHED_SET23
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
