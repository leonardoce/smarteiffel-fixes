-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_COLLECTION2_02

inherit
	AUX_COLLECTION2

creation {ANY}
	main

feature {}
	main is
		local
			view1, view2: STRING; collection1: FAST_ARRAY2[INTEGER]; collection2: ARRAY2[INTEGER]
		do
			create collection1.make(4, 5)
			collection1.put(6, 3, 4)
			assert(collection1.count1 = 4)
			assert(collection1.count2 = 5)
			create collection2.make(0, 3, 0, 4)
			collection2.put(6, 3, 4)
			assert(collection2.count1 = 4)
			assert(collection2.count2 = 5)
			view1 := collection2.out
			view2 := collection1.out
			c2 := collection2
			assert(not c2.same_dynamic_type(collection1))
			c2 := collection1
			assert(not c2.same_dynamic_type(collection2))
		end

end -- class TEST_COLLECTION2_02
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
