-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_COLLECTION2_01
	-- From the 97/98 M6 project.

creation {ANY}
	make

feature {ANY}
	a, b: ARRAY2[INTEGER]

	make is
		local
			i: INTEGER
		do
			--
			-- test make,array2,  count, count1, count2,lower1, upper1, lower2, upper2
			--
			create a.make(1, 2, 3, 4)
			assert(a.count = 4)
			assert(a.count1 = 2)
			assert(a.count2 = 2)
			assert(a.lower1 = 1)
			assert(a.upper1 = 2)
			assert(a.lower2 = 3)
			assert(a.upper2 = 4)
			assert(a.item(1, 3) = 0) -- 8
			create a.make(0, 2, 0, 2)
			assert(a.count = 9)
			assert(a.count1 = 3)
			assert(a.count2 = 3)
			assert(a.lower1 = 0)
			assert(a.upper1 = 2)
			assert(a.lower2 = 0)
			assert(a.upper2 = 2) -- 15
			create b.from_collection({ARRAY[INTEGER] 1, << 0, 0, 0, 0, 0, 0, 0, 0, 0 >> }, 0, 2, 0, 2)
			assert(b.count = 9)
			assert(a.count1 = 3)
			assert(a.count2 = 3)
			assert(b.lower1 = 0)
			assert(b.upper1 = 2)
			assert(b.lower2 = 0)
			assert(b.upper2 = 2) -- 22
			assert(a.is_equal(b))
			--
			-- test occurences and fast_occurrences
			--
			create a.from_collection({ARRAY[INTEGER] 1, << 1, 2, 2, 4, 5, 6, 7, 8, 9 >> }, 1, 3, 1, 3)
			assert(a.occurrences(2) = 2)
			assert(a.occurrences(3) = 0)
			assert(a.occurrences(4) = 1) -- 27
			assert(a.fast_occurrences(2) = 2)
			assert(a.fast_occurrences(3) = 0)
			assert(a.fast_occurrences(4) = 1)
			--
			-- test transpose
			--
			create a.from_collection({ARRAY[INTEGER] 1, << 1, 2, 3, 4, 5, 6 >> }, 0, 1, -1, 1)
			create b.from_collection({ARRAY[INTEGER] 1, << 1, 4, 2, 5, 3, 6 >> }, -1, 1, 0, 1)
			a.transpose
			assert(a.is_equal(b))
			--
			-- test set_all_with
			--
			create a.make(0, 5, 0, 5)
			a.set_all_with(2)
			assert(a.item(0, 0) = 2)
			assert(a.item(0, 5) = 2)
			assert(a.item(5, 0) = 2)
			assert(a.item(5, 5) = 2)
			assert(a.item(2, 2) = 2)
			-- 37
			--
			-- test from_collection
			--
			create a.make(0, 2, 0, 2)
			create b.make(1, 2, 3, 4)
			b.put(2, 2, 3)
			a.from_collection2(b)
			assert(a.is_equal(b))
			create b.make(-2, 5, -1, 3)
			a.from_collection2(b)
			assert(a.is_equal(b))
			--
			-- test force
			--
			create a.make(0, 1, 0, 1)
			a.force(1, 2, 2)
			assert(a.count = 9)
			assert(a.count1 = 3)
			assert(a.count2 = 3)
			assert(a.upper1 = 2)
			assert(a.lower1 = 0)
			assert(a.upper2 = 2)
			assert(a.lower2 = 0)
			assert(a.item(0, 2) = 0)
			assert(a.item(2, 0) = 0)
			assert(a.item(2, 2) = 1) -- 51
			a.force(1, 1, 1)
			assert(a.count = 9)
			assert(a.count1 = 3)
			assert(a.count2 = 3)
			assert(a.upper1 = 2)
			assert(a.lower1 = 0)
			assert(a.upper2 = 2)
			assert(a.lower2 = 0)
			assert(a.item(1, 1) = 1)
			-- 59
			--
			-- test sub_collection2
			--
			create a.make(0, 1, 0, 1)
			b := a.sub_collection2(0, 1, 0, 1)
			assert(b.count = 4)
			assert(b.upper1 = 1)
			assert(b.lower1 = 0)
			assert(b.upper2 = 1)
			assert(b.lower2 = 0)
			assert(b.item(1, 1) = 0) -- 65
			create a.from_collection({ARRAY[INTEGER] 1, << 1, 2, 3, 4, 5, 6, 7, 8, 9 >> }, -1, 1, -1, 1)
			b := a.sub_collection2(0, 1, 0, 1)
			assert(b.count = 4)
			assert(b.upper1 = 1)
			assert(b.lower1 = 0)
			assert(b.upper2 = 1)
			assert(b.lower2 = 0)
			assert(b.item(0, 0) = 5)
			assert(b.item(1, 1) = 9)
			-- 72
			--
			-- test copy
			--
			create a.from_collection({ARRAY[INTEGER] 1, << 1, 2, 3, 4, 5, 6 >> }, 0, 1, -1, 1)
			create b.make(0, 0, 0, 0)
			b.copy(a)
			assert(b.count = 6)
			assert(b.upper1 = 1)
			assert(b.lower1 = 0)
			assert(b.upper2 = 1)
			assert(b.lower2 = -1)
			assert(b.item(0, -1) = 1) -- 78
			create b.make(-2, 3, -5, -2)
			b.copy(a)
			assert(b.count = 6)
			assert(b.upper1 = 1)
			assert(b.lower1 = 0)
			assert(b.upper2 = 1)
			assert(b.lower2 = -1)
			assert(b.item(0, -1) = 1)
			create a.make(0, 2, 0, 2)
			create b.make(0, 1, 0, 1)
			b.copy(a)
			assert(b.count = 9)
			assert(b.upper1 = 2)
			assert(b.lower1 = 0)
			assert(b.upper2 = 2)
			assert(b.lower2 = 0)
			assert(b.item(1, 1) = 0)
			--
			-- test put and item
			--
			create a.make(0, 1, 0, 1)
			assert(a.item(0, 0) = 0)
			a.put(1, 0, 0)
			assert(a.item(0, 0) = 1)
			--
			-- test method 'transpose' of ARRAY2
			--
			create a.make(0, 9, 0, 4)
			from
				i := 0
			until
				i >= a.count
			loop
				a.put(i, i // 5, i \\ 5)
				i := i + 1
			end
			a.transpose
			from
				i := 0
			until
				i >= a.count
			loop
				assert(i = a.item(i \\ 5, i // 5))
				i := i + 1
			end
			--
			-- test has and fast_has
			--
			create a.make(0, 1, 0, 1)
			assert(a.has(0))
			assert(a.fast_has(0))
			assert(not a.has(1))
			assert(not a.fast_has(1))
			a.put(2, 1, 1)
			assert(a.has(2))
			assert(a.fast_has(2))
			--
			-- test all_default
			--
			create a.make(0, 1, 0, 1)
			assert(a.all_default)
			a.put(1, 1, 1)
			assert(not a.all_default)
			--
			-- test 'replace_all' and 'fast_replace_all'
			--
			create a.make(0, 2, 0, 2)
			a.replace_all(0, 1)
			assert(a.item(0, 0) = 1)
			assert(a.item(0, 2) = 1)
			assert(a.item(2, 0) = 1)
			assert(a.item(2, 2) = 1)
			assert(a.item(1, 1) = 1)
			a.fast_replace_all(1, 0)
			assert(a.all_default)
			--
			-- test swap
			--
			create a.from_collection({ARRAY[INTEGER] 1, << 1, 2, 3, 4, 5, 6 >> }, 0, 1, -1, 1)
			a.swap(0, -1, 1, 1)
			assert(a.item(0, -1) = 6)
			assert(a.item(1, 1) = 1)
			--
			-- test resize
			--
			create a.make(-2, 2, -1, 1)
			a.put(1, 1, 1)
			a.resize(0, 1, 0, 2)
			assert(a.count = 6)
			assert(a.count1 = 2)
			assert(a.count2 = 3)
			assert(a.upper1 = 1)
			assert(a.lower1 = 0)
			assert(a.upper2 = 2)
			assert(a.lower2 = 0)
			assert(not a.all_default)
			create a.make(0, 5, 1, 6)
			a.resize(1, 2, 3, 4)
			assert(a.count = 4)
			assert(a.count1 = 2)
			assert(a.count2 = 2)
			assert(a.upper1 = 2)
			assert(a.lower1 = 1)
			assert(a.upper2 = 4)
			assert(a.lower2 = 3)
			assert(a.all_default)
		end

feature {}
	assert (bool: BOOLEAN) is
		do
			cpt := cpt + 1
			if not bool then
				std_output.put_string("TEST_COLLECTION2_01: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
			end
		end

	cpt: INTEGER

end -- class TEST_COLLECTION2_01
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
