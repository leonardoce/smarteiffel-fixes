-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_COLLECTION2_14

inherit
	AUX_COLLECTION2

creation {ANY}
	make

feature {ANY}
	stockage: ARRAY2[INTEGER]

	copie: ARRAY2[INTEGER]

	make is
		do
			create copie.make(-5, 3, -2, 4)
			init
			copie.force(0, 0, 0)
			copie.force(0, -5, -2)
			copie.force(0, 3, 4)
			copie.force(10, 10, 10)
			assert(copie.item(3, 4) = 0)
			assert(copie.item(0, 0) = 0)
			assert(copie.item(-5, -2) = 0)
			assert(copie.item(10, 10) = 10)
		end

	init is
		local
			i, j, k: INTEGER
		do
			from
				i := copie.lower1
				k := 10
			until
				i > copie.upper1
			loop
				from
					j := copie.lower2
				until
					j > copie.upper2
				loop
					k := k + 1
					copie.put(k, i, j)
					j := j + 1
				end
				i := i + 1
			end
		end

end -- class TEST_COLLECTION2_14
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
