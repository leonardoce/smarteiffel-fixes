-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_DEEP_CLONE8

creation {ANY}
	make

feature {ANY}
	one: STRING is "1"

	two: STRING is "2"

	three: STRING is "3"

	make is
		local
			l1, l2: LINKED_LIST[AUX_DEEP_CLONE_B]; aux, aux2: AUX_DEEP_CLONE_B
		do
			aux.set_item(one)
			aux2 := aux.deep_twin
			assert(aux2.item.is_equal(one))
			assert(aux2.item /= one)
			create l1.make
			aux.set_item(one)
			l1.add_last(aux)
			aux.set_item(two)
			l1.add_last(aux)
			aux.set_item(three)
			l1.add_last(aux)
			assert(l1.item(1).item = one)
			assert(l1.item(2).item = two)
			assert(l1.item(3).item = three)
			l2 := l1.deep_twin
			assert(l2.item(1).item.is_equal(one))
			assert(l2.item(1).item /= one)
			assert(l2.item(2).item.is_equal(two))
			assert(l2.item(2).item /= two)
			assert(l2.item(3).item.is_equal(three))
			assert(l2.item(3).item /= three)
			--	 assert(l2.is_equal(l1));
			-- *** Because is equal of LINKED_LIST does not launch recursively
			--     is_equal between items .... waiting ELKS2000
		end

feature {}
	assert (b: BOOLEAN) is
		do
			cpt := cpt + 1
			if not b then
				std_output.put_string("TEST_DEEP_CLONE8: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
				crash
			end
		end

	cpt: INTEGER

end -- class TEST_DEEP_CLONE8
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
