-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_DEEP_EQUAL2
	-- From a bug report of Alain Le Guennec <Alain.Le_Guennec@irisa.fr>

creation {ANY}
	make

feature {ANY}
	c11, c12, c13: AUX_DEEP_EQUAL_CELL

	c21, c22, c23, c24: AUX_DEEP_EQUAL_CELL

	make is
		do
			create c11
			create c12
			create c13
			c11.set_item(1)
			c12.set_item(2)
			c13.set_item(3)
			c11.set_previous(c13)
			c12.set_previous(c11)
			c13.set_previous(c12)
			c11.set_next(c12)
			c12.set_next(c13)
			c13.set_next(c11)
			create c21
			create c22
			create c23
			create c24
			c21.set_item(1)
			c22.set_item(2)
			c23.set_item(3)
			c24.set_item(4)
			c21.set_previous(c23)
			c22.set_previous(c21)
			c23.set_previous(c22)
			c21.set_next(c22)
			c22.set_next(c23)
			c23.set_next(c24)
			assert(not c11.is_deep_equal(c21))
			assert(not c21.is_deep_equal(c11))
		end

	assert (b: BOOLEAN) is
		do
			cpt := cpt + 1
			if not b then
				std_output.put_string("TEST_DEEP_EQUAL2: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
				crash
			end
		end

	cpt: INTEGER

end -- class TEST_DEEP_EQUAL2
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
