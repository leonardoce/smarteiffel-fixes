-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_DEFAULT_VALUE1

creation {ANY}
	make, empty

feature {ANY}
	empty is
		do
		end

	integer_attribute: INTEGER

	reference_attribute: like Current

	real_32_attribute: REAL_32

	real_attribute: REAL

	boolean_attribute: BOOLEAN

	character_attribute: CHARACTER

	check_attribute is
		do
			assert(integer_attribute = 0)
			assert(reference_attribute = Void)
			assert(real_32_attribute = 0.0)
			assert(real_attribute = 0.0)
			assert(boolean_attribute = False)
			assert(character_attribute = '%U')
		end

	make is
		local
			integer_local: INTEGER; reference_local: like Current; real_32_local: REAL_32; real_local: REAL
			boolean_local: BOOLEAN; character_local: CHARACTER
		do
			assert(integer_local = 0)
			assert(reference_local = Void)
			assert(real_32_local = 0.0)
			assert(real_local = 0.0)
			assert(boolean_local = False)
			assert(character_local = '%U')
			check_attribute
			standard_twin.check_attribute
			twin.check_attribute
		end

	assert (b: BOOLEAN) is
		do
			cpt := cpt + 1
			if not b then
				std_output.put_string("TEST_DEFAULT_VALUE1: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
			else
				-- std_output.put_string("Yes%N");
			end
		end

	cpt: INTEGER

end -- class TEST_DEFAULT_VALUE1
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
