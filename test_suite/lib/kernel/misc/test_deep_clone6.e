-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_DEEP_CLONE6

creation {ANY}
	make

feature {ANY}
	foo: STRING is "foo"

	bar: STRING is "bar"

	make is
		local
			l1, l2: TWO_WAY_LINKED_LIST[STRING]
		do
			create l1.make
			l1.add_last(foo)
			l1.add_last(foo)
			l1.add_last(bar)
			l2 := l1.deep_twin
			assert(l2.is_equal_map(l1))
			assert(l2.item(1) /= foo)
			assert(l2.item(1).is_equal(foo))
			assert(l2.item(2) /= foo)
			assert(l2.item(2).is_equal(foo))
			assert(l2.item(1) = l2.item(2))
			assert(l2.item(3) /= bar)
			assert(l2.item(3).is_equal(bar))
		end

feature {}
	assert (b: BOOLEAN) is
		do
			cpt := cpt + 1
			if not b then
				std_output.put_string("TEST_DEEP_CLONE6: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
				crash
			end
		end

	cpt: INTEGER

end -- class TEST_DEEP_CLONE6
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
