-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_TUPLE05

creation {ANY}
	make

feature {}
	make is
		local
			tic: TUPLE[INTEGER, CHARACTER, REAL_32, REAL_64, STRING, TUPLE[INTEGER]]
		do
			tic := [{INTEGER_32 1}, 'a', {REAL_32 1.5}, 1.5, "foo", [3.to_integer_32]]
			--      **** BUG ??
			assert(6 = tic.count)
			assert(1 = tic.first)
			assert('a' = tic.second)
			assert(1.5 = tic.third)
			assert(1.5 = tic.fourth)
			assert(("foo").is_equal(tic.fifth))
			assert(3 = tic.item_6.first)
			assert(tic.item_6.count = 1)
			tic.set_item_6([{INTEGER_32 4}, 5])
			assert(tic.item_6.first = 4)
			assert(tic.item_6.count = 2)
			assert(([[1, 2, 3]]).count = 1)
			assert(([[1, 2, 3]]).first.count = 3)
			assert(([1, 2]).is_equal([1, 2]))
			assert(not ([1, 2]).is_equal([2, 2]))
			assert(not ([1, 2]).is_equal([1, 2, 3]))
		end

	assert (b: BOOLEAN) is
		do
			cpt := cpt + 1
			if not b then
				sedb_breakpoint
				std_output.put_string("TEST_TUPLE_05: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
			end
		end

	cpt: INTEGER

end -- class TEST_TUPLE05
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
