-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_CREATE_FROM_GENERATING_TYPE01

insert
	INTERNALS_HANDLER
	EIFFELTEST_TOOLS

creation {ANY}
	make

feature {ANY}
	make is
		local
			int: INTERNALS; i: INTEGER
		do
			int := ("").to_internals
			int := to_internals
			from
				i := 1
			until
				i > int.type_attribute_count
			loop
				if int.object_attribute(i) = Void then
				end
				i := i + 1
			end
			int := internals_from_generating_type("STRING")
			assert(int.type_generating_type.is_equal("STRING"))
			int := native_array_internals_from_generating_type("NATIVE_ARRAY[STRING]", 15)
			assert(int.type_generating_type.is_equal("NATIVE_ARRAY[STRING]"))
		end

	test1: NATIVE_ARRAY[STRING]

	capacity: INTEGER

end -- class TEST_CREATE_FROM_GENERATING_TYPE01
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
