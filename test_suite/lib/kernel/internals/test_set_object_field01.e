-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_SET_OBJECT_FIELD01
	-- *** This test breaks a precondition of set_object_attribute (object_can_be_modified). We'll improve this test
	-- once internals_from_generating_type from INTERNALS_HANDLER is working.

insert
	INTERNALS_HANDLER
	EIFFELTEST_TOOLS

creation {ANY}
	make

feature {ANY}
	make is
		local
			i_x, i_s: INTERNALS; i: TYPED_INTERNALS[AUX_SET_OBJECT_FIELD01]; a: AUX_SET_OBJECT_FIELD01
			j, s_index, x_index: INTEGER; s: STRING
		do
			create i.make_blank
			s := "Reponse"
			i_x := {INTEGER_32 42}.to_internals
			i_s := s.to_internals
			from
				j := 1
			until
				j > i.type_attribute_count
			loop
				assert(i.type_attribute_name(j).is_equal(s_x) or i.type_attribute_name(j).is_equal(s_s))
				if i.type_attribute_name(j).is_equal(s_x) then
					x_index := j
				else
					s_index := j
				end
				j := j + 1
			end
			i.set_object_attribute(i_s, s_index)
			i.set_object_attribute(i_x, x_index)
			i.set_object_can_be_retrieved
			a := i.object
			assert(a.s = s)
			assert(a.x = 42)
			create i.make_blank
			i.set_object_attribute(i_s, s_index)
			i.set_object_attribute(i_x, x_index)
			i.set_object_attribute(Void, s_index)
			i.set_object_can_be_retrieved
			a := i.object
			assert(a.s = Void)
		end

	s_x: STRING is "x"

	s_s: STRING is "s"

end -- class TEST_SET_OBJECT_FIELD01
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
