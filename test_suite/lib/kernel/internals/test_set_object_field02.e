-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_SET_OBJECT_FIELD02

insert
	INTERNALS_HANDLER
	EIFFELTEST_TOOLS

creation {}
	main

feature {}
	main is
		local
			int: INTERNALS; test_int: TYPED_INTERNALS[TEST_SET_OBJECT_FIELD02]; pos: INTEGER
		do
			alloc(15)
			int := to_internals
			from
				pos := int.type_attribute_count
			until
				pos < 1 or else int.type_attribute_name(pos).is_equal(once "storage")
			loop
				pos := pos - 1
			end
			int := int.object_attribute(pos)
			test_int ::= internals_from_generating_type(once "TEST_SET_OBJECT_FIELD02")
			test_int.set_object_attribute(int, pos)
			test_int.set_object_can_be_retrieved
			assert(test_int.object.capacity = capacity)
		end

	storage: NATIVE_ARRAY[CHARACTER]

	alloc (new_capacity: INTEGER) is
		do
			storage := storage.calloc(new_capacity)
			capacity := new_capacity
		end

feature {TEST_SET_OBJECT_FIELD02}
	capacity: INTEGER

end -- class TEST_SET_OBJECT_FIELD02
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
