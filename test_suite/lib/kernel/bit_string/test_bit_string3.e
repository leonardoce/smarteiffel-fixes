-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_BIT_STRING3
	-- Test 'to_string' and 'to_integer' of BIT_STRING.e

insert
	ANY
	PLATFORM

creation {ANY}
	make

feature {ANY}
	make is
		local
			b1, b2, b3, bs: BIT_STRING; bn: INTEGER_32; bn2, i: INTEGER
		do
			create b1.make(145)
			create b2.make(236)
			create b3.make(143)
			b3.copy(b1)
			assert(b1.is_equal(b3))
			assert(not b1.is_equal(b2))
			-- test 1 of 'to_string'
			create bs.make(4)
			bs.put_1(1)
			assert(bs.to_string.is_equal("1000"))
			-- test 2 of 'to_string'
			create bs.make(10)
			bs.put_1(1)
			bs.put_1(7)
			bs.put_1(10)
			assert(bs.to_string.is_equal("1000001001"))
			-- test 1 of 'to_integer' , comparaison with  'to_integer'
			-- of BIT_N
			bn := 0x00
			create bs.make(16)
			assert(bs.to_integer.is_equal(bn))
			-- test 2 of 'to_integer'
			bn := 0x001A9554
			create bs.make(22)
			bs.from_string("0110101001010101010100")
			bn2 := bs.to_integer
			assert(bs.to_integer.is_equal(bn))
			-- test 3 of 'to_integer'
			bn := 0x6A
			create bs.make(8)
			bs.from_string("01101010")
			assert(bs.to_integer.is_equal(bn))
			-- test 4 of 'to_integer' with size of Integer_bits
			create bs.make(Integer_bits)
			from
				i := 1
			until
				i > Integer_bits
			loop
				bs.put_1(i)
				bn := bn.bit_set((32 - i).to_integer_8)
				i := i + 1
			end
			assert(bs.to_integer.is_equal(bn))
			-- test 5 of 'to_integer'
			bn := Maximum_integer_32 - 1
			create bs.make(32)
			bs.from_string("01111111111111111111111111111110")
			assert(bs.to_integer.is_equal(bn))
			-- test 6 of 'to_integer'
			bn := 0xF7EFBBFE
			create bs.make(32)
			bs.from_string("11110111111011111011101111111110")
			assert(bs.to_integer.is_equal(bn))
		end

	assert (b: BOOLEAN) is
		do
			cpt := cpt + 1
			if not b then
				std_output.put_string("TEST_BIT_STRING3: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
				crash
			else
				-- std_output.put_string("Yes%N");
			end
		end

	cpt: INTEGER

end -- class TEST_BIT_STRING3
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
