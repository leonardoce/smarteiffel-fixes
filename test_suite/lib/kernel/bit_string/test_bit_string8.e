-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_BIT_STRING8

creation {ANY}
	make

feature {ANY}
	make is
		local
			b1, b2, b3, b4, b5, b6, b7, b8: BIT_STRING
		do
			-- 1
			create b1.make(35)
			b1.from_string("10011001100110011001100110011001100")
			b1.shift_right_by(4)
			create b2.make(35)
			b2.from_string("00001001100110011001100110011001100")
			assert(b1.is_equal(b2))
			-- 2
			create b3.make(35)
			b3.from_string("10011001100110011001100110011001100")
			b3.shift_left_by(4)
			create b4.make(35)
			b4.from_string("10011001100110011001100110011000000")
			assert(b3.is_equal(b4))
			-- 3
			create b5.make(35)
			b5.from_string("10011001100110011001100110011001100")
			b5.shift_by(2)
			create b6.make(35)
			b6.from_string("00100110011001100110011001100110011")
			assert(b5.is_equal(b6))
			-- 4
			create b7.make(35)
			b7.from_string("10011001100110011001100110011001100")
			b7.shift_by(-2)
			create b8.make(35)
			b8.from_string("01100110011001100110011001100110000")
			assert(b7.is_equal(b8))
			-- 5
			b1.from_string("00000000111000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000111111101100000000000000000000000000000000000000")
			b2.from_string("10000000000000000000000000011111110110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000")
			b1.shift_by(-74)
			assert(b1.is_equal(b2))
		end

	assert (b: BOOLEAN) is
		do
			cpt := cpt + 1
			if not b then
				std_output.put_string("TEST_BIT8: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
				crash
			else
				-- std_output.put_string("Yes%N");
			end
		end

	cpt: INTEGER

end -- class TEST_BIT_STRING8
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
