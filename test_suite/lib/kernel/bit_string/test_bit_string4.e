-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_BIT_STRING4

creation {ANY}
	make

feature {ANY}
	bs0, bs1, bs2, bs3, bs4: BIT_STRING

	s: STRING

	make is
		do
			-- rien a voir avec ce qui nous interresse
			create bs0.make(72)
			bs0.put_1(63)
			assert(bs0.item(63))
			bs0.put_1(64)
			assert(bs0.item(64))
			bs0.put_0(63)
			bs0.put_0(64)
			bs0.put_0(65)
			bs0.put_1(65)
			assert(bs0.item(65))
			--------------
			s := "1010101101010101010110001100100100111100010010001000000111101001"
			create bs1.make(s.count)
			create bs2.from_string(s)
			bs1.copy(bs2)
			assert(bs1.is_equal(bs2))
			create bs3.make(20)
			bs3.from_string("10100010100001010001")
			create bs4.make(20)
			bs4.from_string("11101010010010000101")
			assert(not bs3.is_equal(bs4)) -- erreur donc dans is_equal !!!!!!
		end

	assert (b: BOOLEAN) is
		do
			cpt := cpt + 1
			if not b then
				std_output.put_string("TEST_BIT_STRING4: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
				crash
			else
				-- std_output.put_string("Yes%N");
			end
		end

	cpt: INTEGER

end -- class TEST_BIT_STRING4
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
