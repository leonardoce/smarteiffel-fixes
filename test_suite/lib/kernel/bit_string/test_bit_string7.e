-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_BIT_STRING7

creation {ANY}
	make

feature {ANY}
	make is
		local
			b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16, b17, b18, b25, b26, b27
			b28: BIT_STRING
		do
			-- test de rotate_left_by
			-- 1
			create b1.make(35)
			b1.from_string("10011001100110011001100110011001100")
			b1.rotate_left_by(3)
			create b2.make(35)
			b2.from_string("11001100110011001100110011001100100")
			assert(b1.is_equal(b2))
			-- 2
			create b3.make(35)
			b3.from_string("10011001100110011001100110011001100")
			b3.rotate_left_by(3)
			create b4.make(35)
			b4.from_string("11001100110011001100110011001100100")
			assert(b3.is_equal(b4))
			-- 3
			create b5.make(100)
			b5.from_string("1001010100101000010101011101010010101000000101010101011010010010010101010010101001010101001010100101")
			b5.rotate_left_by(36)
			create b6.make(100)
			b6.from_string("1000000101010101011010010010010101010010101001010101001010100101100101010010100001010101110101001010")
			assert(b5.is_equal(b6))
			-- 4
			create b7.make(100)
			b7.from_string("1001010100101000010101011101010010101000000101010101011010010010010101010010101001010101001010100101")
			b7.rotate_left_by(136)
			assert(b7.is_equal(b6))
			-- 5
			create b8.make(100)
			b8.from_string("1010010010101000100010010111010100100010101110100101101001000100010010101100100100010100101001010001")
			b8.rotate_left_by(500)
			create b9.make(100)
			b9.from_string("1010010010101000100010010111010100100010101110100101101001000100010010101100100100010100101001010001")
			assert(b9.is_equal(b8))
			-- 6
			-- test de rotate_right_by
			create b10.make(35)
			b10.from_string("10011001100110011001100110011001100")
			b10.rotate_left_by(3)
			create b11.make(35)
			b11.from_string("11111011111111111111110111111111111") -- ############################## VAS PAS DU TOUT
			assert(not b10.is_equal(b11))
			-- 7
			create b12.make(35)
			b12.from_string("10011001100110011001100110011001100")
			b12.rotate_left_by(12)
			create b13.make(35)
			--  	10011001100110011001100110011001100
			--      10011001100110011001100100110011001
			b13.from_string("10011001100110011001100100110011001")
			assert(b12.is_equal(b13))
			-- 8
			create b14.make(100)
			b14.from_string("1001010100101000010101011101010010101000000101010101011010010010010101010010101001010101001010100101")
			b14.rotate_left_by(36)
			-- 1001010100101000010101011101010010101000000101010101011010010010010101010010101001010101001010100101
			-- 1000000101010101011010010010010101010010101001010101001010100101100101010010100001010101110101001010
			create b15.make(100)
			b15.from_string("1000000101010101011010010010010101010010101001010101001010100101100101010010100001010101110101001010")
			assert(b14.is_equal(b15))
			-- 9
			create b16.make(100)
			b16.from_string("1001010100101000010101011101010010101000000101010101011010010010010101010010101001010101001010100101")
			b16.rotate_left_by(136)
			assert(b16.is_equal(b15))
			-- 10
			create b17.make(100)
			b17.from_string("1010010010101000100010010111010100100010101110100101101001000100010010101100100100010100101001010001")
			b17.rotate_left_by(500)
			create b18.make(100)
			b18.from_string("1010010010101000100010010111010100100010101110100101101001000100010010101100100100010100101001010001")
			assert(b18.is_equal(b17))
			-- autres testes
			-- 11
			create b25.make(35)
			b25.from_string("10011001100110011001100110011001100")
			b25.rotate_by(1)
			create b26.make(35)
			b26.from_string("01001100110011001100110011001100110")
			assert(b25.is_equal(b26))
			-- 12
			create b27.make(35)
			b27.from_string("10011001100110011001100110011001100")
			b27.rotate_by(-1)
			create b28.make(35)
			b28.from_string("00110011001100110011001100110011001")
			assert(b27.is_equal(b28))
		end

	assert (b: BOOLEAN) is
		do
			cpt := cpt + 1
			if not b then
				std_output.put_string("TEST_BIT_STRING7: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
				crash
			else
				-- std_output.put_string("Yes%N");
			end
		end

	cpt: INTEGER

end -- class TEST_BIT_STRING7
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
