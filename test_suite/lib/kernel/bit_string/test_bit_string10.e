-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_BIT_STRING10

insert
	ANY
	PLATFORM

creation {ANY}
	make

feature {ANY}
	make is
		local
			b1, b2: BIT_STRING; i, j: INTEGER
		do
			from
				j := 1
			variant
				2 * Integer_bits + 3 - j
			until
				j > 2 * Integer_bits + 3
			loop
				create b1.make(j)
				create b2.make(j)
				from
					i := 1
				until
					i > j
				loop
					if i.is_even then
						b1.put_1(i)
					end
					i := i + 1
				end
				--            io.put_integer(b1.count);
				--            io.put_string(" ----%N");
				--            io.put_string(b1.out) ;
				--            io.put_new_line ;
				--            io.put_string(b2.out) ;
				--            io.put_new_line ;
				--            io.put_string((b1 implies b2).out) ;
				--            io.put_new_line ;
				assert((b1 implies b2).is_equal(not b1), "Test � la con de implies")
				b2.invert
				assert((b2 implies b1).is_equal(b1), "Test sur b2 invers�")
				b1.clear_all
				b2.clear_all
				b1 := b1 implies b2
				assert(b1.all_set, "0 x 0 => 1")
				b1 := b1 implies b2
				assert(b1.all_cleared, "0 x 1 => 1")
				b1 := b2 implies b1
				assert(b1.all_set, "1 x 0 => 1")
				b1.set_all
				b1 := b1 implies b1
				assert(b1.all_set, "1 x 1 => 1")
				j := j + 1
			end
		end

feature {}
	assert (b: BOOLEAN; s: STRING) is
		do
			cpt := cpt + 1
			if not b then
				std_output.put_string("TEST_BIT_STRING10: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string(" in " + s + "%N")
				crash
			else
				-- std_output.put_string("Yes%N");
			end
		end

	cpt: INTEGER

end -- class TEST_BIT_STRING10
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
