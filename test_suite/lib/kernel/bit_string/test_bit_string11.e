-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_BIT_STRING11

creation {ANY}
	make

feature {ANY}
	make is
		local
			b1, b2, b3, b4: BIT_STRING
		do
			create b1.from_string("1111111111111111111111111111111111000000000000000000000000000000000000000000011111111101010010101010101010101010000000000000000000000000000000")
			create b2.make(142)
			b3 := b1 and b2
			b4 := b2 and b1
			assert(b3.is_equal(b4))
			assert(b3.is_equal(b2))
			b1.from_string("1111011111111000010000000110000000000001001111111111111111111111111111111011111111111011111111111111111111111111111111111111101111110111111111111111111111111111111111111110111111111111111111111111111111111111111101111111111111111111111111111101111111111111111111101111111110111111111110111111111111111111111111111111111111111111111111111111111111111111111111111101111111111111111111111011111011111111111011101111111101111111111110111111111111111111011101011111111111111111110111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111110111111111111111111110011111011111111101111111110111111111111101111111111110111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111000111111111111111111111111111111111111111111111111111111111111111101111111111111110111110111111111111111111111011111111111111011111111110111111111111111111111111111111111111111011111111111111110111110111111111011111111111111111111111011111111110111111011111111111111111111111111111111111111111111111011101111111111111111111111111111101111111111111111111111111111111111111111111111111111111111111011111111111111111111111111111111111")
			b2 := b1.twin
			b2.set_all
			b3 := b1 and b2
			b4 := b2 and b1
			assert(b3.is_equal(b4))
			assert(b3.is_equal(b1))
			b2 := b1.twin
			b2.invert
			b3 := b1 and b2
			b4 := b2 and b1
			assert(not b3.is_equal(b2))
			b2.clear_all
			assert(b3.is_equal(b4))
			assert(b3.is_equal(b2))
			--------------------------------------------
			b1.from_string("101111101111010111111111110111111101101111111111111111111111111011111101111011111111111101111101111111011110111111101111111111111111111111111111101101011111111011001011111110111110110111111111111111111111101011111111111111111111111110111011110111111111111101111111110101111111111011111111110110111111111111011111111111111111011111111110101101101111111111111111111110111111111111111101111011011")
			b2 := b1.twin
			b2.clear_all
			b3 := b1 or b2
			b4 := b2 or b1
			assert(b3.is_equal(b4))
			b4 := b1.twin
			assert(b3.is_equal(b1))
			b2.set_all
			b3 := b1 or b2
			b4 := b2 or b1
			assert(b3.is_equal(b4))
			assert(b2.is_equal(b3))
			b2 := b1.twin
			b2.invert
			b3 := b1 or b2
			b4 := b2 or b1
			assert(b3.is_equal(b4))
			b2.set_all
			assert(b2.is_equal(b3))
			----------------------------------------------
			b1.from_string("0000100101001010101010101001010010101010010101001010101001010000010010010000010000000000011000010000000000101111111111111111111111111111110000010001000010000000010000000000000000100001000000001000000010100000010010000000001000000100000000000000010000000000000000000010000001000000000000001000000000000000100000000000111111111110111111111111111111111111111101111110101111111011101111011110110111111111111111100111111011111111111111111111111101111101111111011011110111111011011111111111101")
			b2 := b1.twin
			b2.clear_all
			b3 := b1 xor b2
			b4 := b2 xor b1
			assert(b3.is_equal(b4))
			assert(b1.is_equal(b3))
			b2.set_all
			b3 := b1 xor b2
			b4 := b2 xor b1
			assert(b3.is_equal(b4))
			b1.invert
			assert(b3.is_equal(b1))
			b3.invert
			b4 := b1 xor b3
			assert(b4.all_set)
		end

	assert (b: BOOLEAN) is
		do
			cpt := cpt + 1
			if not b then
				std_output.put_string("TEST_BIT11: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
				crash
			else
				-- std_output.put_string("Yes%N");
			end
		end

	cpt: INTEGER

end -- class TEST_BIT_STRING11
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
