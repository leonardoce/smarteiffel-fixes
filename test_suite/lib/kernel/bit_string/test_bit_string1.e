-- This file is part of SmartEiffel The GNU Eiffel Compiler Tools and Libraries.
-- See the Copyright notice at the end of this file.
--
class TEST_BIT_STRING1

insert
	ANY
	PLATFORM

creation {ANY}
	make

feature {ANY}
	make is
		local
			b1: BIT_STRING; b2: BIT_STRING; b3: BIT_STRING
		do
			create b1.make(2)
			create b2.make(Integer_bits)
			create b3.make(Integer_bits + 1)
			assert(b1.count = 2)
			assert(b1.all_cleared)
			assert(not b1.item(1))
			assert(not b1.item(2))
			b1.put(True, 1)
			assert(b1.item(1))
			assert(not b1.item(2))
			b1.put(False, 1)
			assert(not b1.item(1))
			assert(not b1.item(2))
			b1.put(True, 2)
			assert(not b1.item(1))
			assert(b1.item(2))
			b1.put(True, 1)
			assert(b1.all_set)
			assert(b2.count = Integer_bits)
			assert(b2.all_cleared)
			b2.put(True, Integer_bits)
			assert(b2.item(Integer_bits))
			assert(not b2.item(Integer_bits - 1))
			assert(b3.count = Integer_bits + 1)
			assert(b3.all_cleared)
			b3.put(True, Integer_bits)
			assert(b3.item(Integer_bits))
			assert(not b3.item(Integer_bits - 1))
			assert(not b3.item(Integer_bits + 1))
			b3.put(False, Integer_bits)
			b3.put(True, Integer_bits + 1)
			assert(not b3.item(Integer_bits))
			assert(b3.item(Integer_bits + 1))
			b3.set_all
			assert(b3.all_set)
			b1.clear_all
			b1.set_from_string("10", 1)
			assert(b1.item(1))
			assert(not b1.item(2))
			b2.clear_all
			b2.set_from_string("101", 30)
			assert(b2.item(30))
			assert(not b2.item(31))
			assert(b2.item(32))
		end

	assert (b: BOOLEAN) is
		do
			cpt := cpt + 1
			if not b then
				std_output.put_string("TEST_BIT1: ERROR Test # ")
				std_output.put_integer(cpt)
				std_output.put_string("%N")
				crash
			else
				-- std_output.put_string("Yes%N");
			end
		end

	cpt: INTEGER

end -- class TEST_BIT_STRING1
--
-- ------------------------------------------------------------------------------------------------------------------------------
-- Copyright notice below. Please read.
--
-- SmartEiffel is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License,
-- as published by the Free Software Foundation; either version 2, or (at your option) any later version.
-- SmartEiffel is distributed in the hope that it will be useful but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
-- received a copy of the GNU General Public License along with SmartEiffel; see the file COPYING. If not, write to the Free
-- Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- Copyright(C) 1994-2002: INRIA - LORIA (INRIA Lorraine) - ESIAL U.H.P.       - University of Nancy 1 - FRANCE
-- Copyright(C) 2003-2006: INRIA - LORIA (INRIA Lorraine) - I.U.T. Charlemagne - University of Nancy 2 - FRANCE
--
-- Authors: Dominique COLNET, Philippe RIBET, Cyril ADRIAN, Vincent CROIZIER, Frederic MERIZEN
--
-- http://SmartEiffel.loria.fr - SmartEiffel@loria.fr
-- ------------------------------------------------------------------------------------------------------------------------------
