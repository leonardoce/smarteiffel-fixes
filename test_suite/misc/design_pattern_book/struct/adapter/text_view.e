indexing
   description: "Pseudo-sophisticated text displayer and editor"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class TEXT_VIEW
creation 
   make
feature -- Creation
   make (x,y,w,h : INTEGER) is
      -- Creation and initialization
      do
	!!origin.make(x,y)
	width := w; height := h
      ensure
	 origin_set: origin.x = x and origin.y = y
	 dim_set: width = w and height = h
      end -- make
feature {ANY} -- Queries
   origin : POINT2D
   width, height : REAL
   is_empty : BOOLEAN
invariant origin_not_void: origin /= Void
end -- TEXT_VIEW

