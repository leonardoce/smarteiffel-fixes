indexing
   description: "Pseudo-class to manipulate SHAPEs"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class MANIPULATOR
creation 
   make
feature -- Creation
   make (s : like shape) is
      -- Creation and initialization
	require s_not_void: s /= Void
      do
	 shape := s
      end -- make
feature {ANY} -- Queries
   shape : SHAPE
end -- MANIPULATOR

