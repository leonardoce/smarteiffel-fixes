indexing
   description: "A point on a 2 dimentional space"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class POINT2D
inherit ANY redefine out end;
creation make
feature -- Creation
   make (new_x, new_y : like x) is
      -- Creation and initialization
      do
	 x := new_x; y := new_y
      ensure x = new_x and y = new_y
      end -- set, make
feature {ANY} -- Queries
   x, y : REAL -- Cartesian coordinates of the point
   is_zero : BOOLEAN is do Result := x = 0 and y = 0 end
   out : STRING is
        -- external representation of the point
      do
	 Result := x.out
	 Result.extend(',')
	 Result.append(y.out)
      end -- out
feature {ANY} -- Commands
   add (other: like Current) is
	 -- add 'other' to Current
      do
	 x := x+other.x; y := y+other.y
      end -- add
end -- POINT2D

