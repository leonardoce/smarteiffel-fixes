indexing
   description: "Abstraction of a graphical object with an editable shape"
   note: ""
   status: ""
   date: "$Date: 2004-09-18 10:13:28 +0200 (Sat, 18 Sep 2004) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class SHAPE
insert
	ANY
feature {ANY} -- Queries
   bottom_left, top_right : POINT2D is deferred end
   new_manipulator : MANIPULATOR is deferred end -- Factory Method
end -- SHAPE

