indexing
   description: "Adapter of a TEXT_VIEW to a SHAPE"
   note: ""
   status: ""
   date: "$Date: 2004-11-08 10:57:03 +0100 (Mon, 08 Nov 2004) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class TEXT_SHAPE
inherit
   SHAPE
insert
   TEXT_VIEW
      rename origin as bottom_left -- to allow a merge with SHAPE's
      export {NONE} width, height -- private inheritance: these features are hidden
      redefine make end
      -- is_empty is inherited without modification
creation make
feature -- Creation
   make (x,y,w,h : INTEGER) is
      -- Creation and initialization
      do
	!!bottom_left.make(x,y)
	width := w; height := h
	is_empty := True
      end -- make
feature {ANY} -- Queries
   top_right : POINT2D is
	 -- Returns the top right point of the shape
	 -- The Result could be cached if this method is to be called often
      do
	 !!Result.make(bottom_left.x+width, bottom_left.y+height)
      end -- top_right
   new_manipulator : MANIPULATOR is 
	 -- Factory Method
      do
	 !!Result.make(Current)
      end -- new_manipulator
end -- TEXT_SHAPE

