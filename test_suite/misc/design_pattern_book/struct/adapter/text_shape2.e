indexing
   description: "Adapter of a TEXT_VIEW to a SHAPE, object version"
   note: ""
   status: ""
   date: "$Date: 2004-09-18 10:13:28 +0200 (Sat, 18 Sep 2004) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class TEXT_SHAPE2
inherit
   SHAPE
creation make
feature -- Creation
   make (x,y,w,h : INTEGER) is
      -- Creation and initialization
      do
	!!text.make(x,y,w,h)
      end -- make
feature {ANY} -- Queries
   bottom_left : POINT2D is
      do
	 Result := text.origin.twin
      end -- bottom_left
   top_right : POINT2D is
      do
	 !!Result.make(text.origin.x+text.width, text.origin.y+text.height)
      end -- top_right
   is_empty : BOOLEAN is
	 -- Delagate the call to 'text' version
      do
	 Result := text.is_empty
      end -- is_empty
   new_manipulator : MANIPULATOR is 
	 -- Factory Method
      do
	 !!Result.make(Current)
      end -- new_manipulator
feature {NONE} -- Private
   text : TEXT_VIEW
invariant
   text_not_void: text /= Void
end -- TEXT_SHAPE2




