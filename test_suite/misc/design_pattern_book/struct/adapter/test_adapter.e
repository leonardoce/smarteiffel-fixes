indexing
   description: "Test class to exercise the Adapter pattern"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class TEST_ADAPTER
creation 
   make
feature -- Creation
   make is
      -- Creation and initialization
      local t : SHAPE
      do
	 !TEXT_SHAPE!t.make(1,2,3,4)
	 show_shape(t)
	 !TEXT_SHAPE2!t.make(1,2,3,4)
	 show_shape(t)
      end -- make
   show_shape (t : SHAPE) is
	-- show the bounding box of t
      do
	 print("Bottom left = ("); print(t.bottom_left.out); print(")%N")
	 print("Top right = ("); print(t.top_right.out); print(")%N")
      end -- show_shape
end
