indexing
   description: "Component of a user interface that can be displayed"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class VISUAL_COMPONENT
feature {ANY} -- Commands
   draw is deferred end -- draw on the screen
   resize is deferred end -- resize the component
end -- VISUAL_COMPONENT

