indexing
   description: "A VISUAL_COMPONENT decorated with a scroll bar"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class SCROLL_DECORATOR
inherit 
   DECORATOR
      redefine draw end;
creation 
   make
feature {ANY} -- Commands
   draw is
      -- draw on the screen
      do
	 component.draw
	 draw_scroll_bar
      end -- draw
feature {NONE} -- Private
   draw_scroll_bar is
	-- draw a scroll bar
      do
	 io.put_string("Drawing a scroll bar%N")
      end -- draw_border
end -- SCROLL_DECORATOR

