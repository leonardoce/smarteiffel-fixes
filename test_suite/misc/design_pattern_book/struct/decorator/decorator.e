indexing
   description: "Decorator of a VISUAL_COMPONENT"
   note: "All operations are by default delegated to the decorated"
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class DECORATOR
inherit VISUAL_COMPONENT
creation 
   make
feature -- Creation
   make (c : VISUAL_COMPONENT) is
      -- Creation and initialization
      require c_not_void: c /= Void
      do
	 component := c
      end -- make
feature {ANY} -- Commands
   draw is
      -- draw on the screen
      do
	 component.draw
      end -- draw
   resize is 
	-- resize the component
      do
	 component.resize
      end -- resize
feature {NONE} -- Private
   component : VISUAL_COMPONENT
invariant
   component_not_void: component /= Void
end -- DECORATOR

