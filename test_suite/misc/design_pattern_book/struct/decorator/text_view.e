indexing
   description: "Text that can be graphically viewed"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class TEXT_VIEW
inherit
   VISUAL_COMPONENT
creation 
   make
feature -- Creation
   make (new_text : STRING) is
      -- Creation and initialization
      require new_text_not_void: new_text /= Void
      do
	 text := new_text
      end -- make
feature {ANY} -- Commands
   draw is
	-- draw the text
      do
	 io.put_string(text); io.put_new_line
      end -- draw
   resize is do end -- do nothing
feature {NONE} -- Private
   text : STRING
invariant
   text_not_void: text /= Void
end -- TEXT_VIEW

