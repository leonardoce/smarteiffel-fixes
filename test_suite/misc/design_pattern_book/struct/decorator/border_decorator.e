indexing
   description: "A VISUAL_COMPONENT decorated with a border"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class BORDER_DECORATOR
inherit 
   DECORATOR
      rename make as decorator_make
      redefine draw end;
creation 
   make
feature -- Creation
   make (c : VISUAL_COMPONENT; border_width : INTEGER) is
      -- Creation and initialization
      require positive_width : border_width >= 0
      do
	 decorator_make(c)
	 width := border_width
      end -- make
feature {ANY} -- Commands
   draw is
      -- draw on the screen
      do
	 component.draw
	 draw_border
      end -- draw
feature {NONE} -- Private
   width : INTEGER
	-- width of the border around the component
   draw_border is
	-- draw a border
      do
	 io.put_string("Drawing a border of width ")
	 io.put_integer(width); io.put_new_line
      end -- draw_border
invariant
  positive_width : width >= 0
end -- BORDER_DECORATOR

