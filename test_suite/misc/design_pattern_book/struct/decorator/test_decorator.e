indexing
   description: "Example of the use of DECORATOR"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class TEST_DECORATOR
creation make
feature -- Creation
   make is
	 -- Creation and initialization
      local
	 t : TEXT_VIEW
	 s : SCROLL_DECORATOR
	 b : BORDER_DECORATOR
      do
	 !!t.make("Here is a text")
	 set_contents(t) -- Display text without decoration
	 !!s.make(t)
	 !!b.make(s,1)
	 set_contents(b) -- Redisplay text with decorations
      end -- make
feature {ANY} -- Commands
   set_contents (c : VISUAL_COMPONENT) is
	 -- Draw c in this window
      do
	 c.draw -- Simulates the drawing
      end -- set_contents
end
