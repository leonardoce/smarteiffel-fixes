indexing
   description: "A FACADE for a compilation subsystem"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class COMPILER
feature {ANY} -- Queries
   has_valid_abstract_syntax_tree : BOOLEAN is
	 -- Whether the input stream has been correctly parsed
      do
	 Result := ast /= Void
      end -- has_valid_abstract_syntax_tree
feature {ANY} -- Commands
   build_abstract_syntax_tree (in_stream: INPUT_STREAM) is
      -- Build an Abstract Syntax Tree by reading on `in_stream'
      require in_stream_not_void: in_stream /= Void
      do
	 !!scanner.make(in_stream)
	 !!builder.make
	 !!parser.make(scanner,builder)
      end -- build_abstract_syntax_tree
   generate_c_code (out_stream: OUTPUT_STREAM) is
	 -- Generate C code on `out' for the Abstract Syntax Tree
      require 
	 out_stream_not_void: out_stream /= Void
	 valid_abstract_syntax_tree: has_valid_abstract_syntax_tree
      local code_gen : C_CODE_GENERATOR
      do
	 !!code_gen.make(out_stream)
	 ast.traverse(code_gen)
      end -- generate_c_code
   print_statistics is
	 -- Print statistics on the number of each kind of constructs
	 -- in the Abstract Syntax Tree
      local stat_gen : STAT_GENERATOR
      do
	 if ast /= Void then	 
	    !!stat_gen; ast.traverse(stat_gen)
	 end -- if
      end -- print_statistics
   print_abstract_syntax_tree is
	 -- Print a representation of the Abstract Syntax Tree on STDOUT
      local printer : AST_PRINTER
      do
	 if ast /= Void then
	    !!printer; ast.traverse(printer)
	 end -- if
      end -- print_abstract_syntax_tree
feature {NONE}
   scanner : SCANNER
   parser : PARSER
   builder : NODE_BUILDER
   ast : PROGRAM_NODE is
	 -- Root of the Abstract Syntax Tree
      do
	 if builder /= Void then Result := builder.root_node end
      end -- ast
end -- COMPILER
