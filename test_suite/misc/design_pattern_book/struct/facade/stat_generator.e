indexing
   description: "A NODE_VISITOR reporting some stats on the program"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class STAT_GENERATOR
inherit NODE_VISITOR
feature {ANY} -- Queries
   operations, ifs, whiles, assignments, others : INTEGER -- Accumulates
	 -- occurrences of such program nodes as traversing is going
feature {ANY} -- Commands
   visit_binary_expression (node : BINARY_EXPRESSION) is 
	 -- Visit a BINARY_EXPRESSION
      do
	 operations := operations + 1
	 node.left.traverse(Current)
	 node.right.traverse(Current)
      end -- visit_binary_expression
   visit_variable_ref (node : VARIABLE_REF) is do end 
   visit_integer_value (node : INTEGER_VALUE) is do end
   visit_assignment (node : ASSIGNMENT) is 
	 -- Visit an ASSIGNMENT
      do
	 assignments := assignments + 1
	 node.right_hand_side.traverse(Current)
      end -- visit_assignment
   visit_print (node : PRINT) is 
	 -- Visit a PRINT
      do
	 others := others + 1
	 node.expression.traverse(Current)
      end -- visit_print
   visit_read (node : READ) is 
	 -- Visit a READ
      do
	 others := others + 1
      end -- visit_read
   visit_conditional (node : CONDITIONAL) is 
	 -- Visit a CONDITIONAL
      do
	 ifs := ifs + 1
	 node.condition.traverse(Current)
	 node.then_part.traverse(Current)
	 node.else_part.traverse(Current)
      end -- visit_conditional
   visit_while (node : WHILE) is 
	 -- Visit a WHILE
      do
	 whiles := whiles + 1
	 node.condition.traverse(Current)
	 node.loop_body.traverse(Current)
      end -- visit_while
   visit_block (node : BLOCK) is 
	 -- Visit a BLOCK
      local i : INTEGER
      do
	 from i := 1 until i > node.count
	 loop
	    node.item(i).traverse(Current)
	    i := i + 1
	 end -- loop
	 if node.is_top_level then
	    io.put_string("Number of 'if' statements: ")
	    io.put_integer(ifs); io.put_new_line
	    io.put_string("Number of 'while' statements: ")
	    io.put_integer(whiles); io.put_new_line
	    io.put_string("Number of ':=' statements: ")
	    io.put_integer(assignments); io.put_new_line
	    io.put_string("Number of other (read, print) statements: ")
	    io.put_integer(others); io.put_new_line
	    io.put_string("Number of binary operations: ")
	    io.put_integer(operations); io.put_new_line
	 end -- if
      end -- visit_block
end -- STAT_GENERATOR
