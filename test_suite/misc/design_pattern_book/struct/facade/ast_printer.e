indexing
   description: "A NODE_VISITOR to print the Abstract Syntax Tree"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class AST_PRINTER
inherit NODE_VISITOR
feature {ANY} -- Commands
   visit_binary_expression (node : BINARY_EXPRESSION) is 
	 -- print a representation of this node
      do
	 indentation; io.put_string(node.operator);io.put_new_line
	 indent
	 node.left.traverse(Current)
	 node.right.traverse(Current)
	 undent
      end -- visit_binary_expression
   visit_variable_ref (node : VARIABLE_REF) is 
	 -- print a representation of this node
      do
	 indentation; io.put_string(node.name);io.put_new_line
      end -- visit_variable_ref
   visit_integer_value (node : INTEGER_VALUE) is 
	 -- print a representation of this node
      do
	 indentation; io.put_integer(node.value);io.put_new_line
      end -- visit_integer_value
   visit_assignment (node : ASSIGNMENT) is 
	 -- print a representation of this node
      do
	 indentation; io.put_string("Assignment%N")
	 indent
	 node.variable.traverse(Current)
	 node.right_hand_side.traverse(Current)
         undent
      end -- visit_assignment
   visit_print (node : PRINT) is 
	 -- print a representation of this node
      do
	 indentation; io.put_string("Print%N")
	 indent
	 node.expression.traverse(Current)
	 undent
      end -- visit_print
   visit_read (node : READ) is 
	 -- print a representation of this node
      do
	 indentation; io.put_string("Read%N")
	 indent
	 node.variable.traverse(Current)
	 undent
      end -- visit_read
   visit_conditional (node : CONDITIONAL) is 
	 -- print a representation of this node
      do
	 indentation; io.put_string("If%N")
	 indent
	 node.condition.traverse(Current)
	 node.then_part.traverse(Current)
	 node.else_part.traverse(Current)
	 undent
      end -- visit_conditional
   visit_while (node : WHILE) is 
	 -- print a representation of this node
      do
	 indentation; io.put_string("While%N")
	 indent
	 node.condition.traverse(Current)
	 node.loop_body.traverse(Current)
	 undent
      end -- visit_while
   visit_block (node : BLOCK) is 
	 -- print a representation of this node
      local i : INTEGER
      do
	 from i := 1 until i > node.count
	 loop
	    node.item(i).traverse(Current)
	    i := i + 1
	 end -- loop
      end -- visit_block
feature {NONE} -- Private
   depth : INTEGER
   indent is
	-- increment depth
      do
	 depth := depth + 1
      end -- indent
   undent is
	-- decrement depth
      require depth > 0
      do
	 depth := depth - 1
      end -- undent
   indentation is
	-- Print a number of blnck characters according to depth
      local i: INTEGER
      do
	 from i := depth * 2 until i = 0 loop io.put_character(' '); i:=i-1 end
      end -- indentation
invariant
   depth_positive: depth >= 0
end -- AST_PRINTER
