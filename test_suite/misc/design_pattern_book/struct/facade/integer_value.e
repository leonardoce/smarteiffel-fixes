indexing
   description: ""
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class INTEGER_VALUE
inherit EXPRESSION
creation make
feature -- Creation
   make (v:INTEGER) is
      -- Creation and initialization
      do
	 value := v
      end -- make
feature {ANY} -- Queries
   value : INTEGER
feature {ANY} -- Commands
   traverse (visitor : NODE_VISITOR) is
	 -- let visitor visit the current node
      do
	 visitor.visit_integer_value (Current)
      end -- traverse
end -- INTEGER_VALUE

