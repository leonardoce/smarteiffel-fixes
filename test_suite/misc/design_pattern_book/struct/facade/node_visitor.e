indexing
   description: "A VISITOR for a PROGRAM_NODE"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class NODE_VISITOR
feature {ANY} -- Commands
   visit_binary_expression (node : BINARY_EXPRESSION) is require node_not_void: node /= Void deferred end
   visit_variable_ref (node : VARIABLE_REF) is require node_not_void: node /= Void deferred end
   visit_integer_value (node : INTEGER_VALUE) is require node_not_void: node /= Void deferred end
   visit_assignment (node : ASSIGNMENT) is require node_not_void: node /= Void deferred end
   visit_print (node : PRINT) is require node_not_void: node /= Void deferred end
   visit_read (node : READ) is require node_not_void: node /= Void deferred end
   visit_conditional (node : CONDITIONAL) is require node_not_void: node /= Void deferred end
   visit_while (node : WHILE) is require node_not_void: node /= Void deferred end
   visit_block (node : BLOCK) is require node_not_void: node /= Void deferred end
end -- NODE_VISITOR

