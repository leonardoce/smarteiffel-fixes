indexing
   description: "A NODE_VISITOR performing simple C code generation"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class C_CODE_GENERATOR
inherit NODE_VISITOR
creation make
feature -- Creation
   make (s: OUTPUT_STREAM) is
      -- Creation and initialization
      do
	 iostream := s
      end -- make
feature {ANY} -- Queries
   iostream : OUTPUT_STREAM
feature {ANY} -- Commands
   visit_binary_expression (node : BINARY_EXPRESSION) is 
	 -- Emit C code for this node
      do
	 iostream.put_character('(')
	 node.left.traverse(Current)
	 iostream.put_character(')')
         iostream.put_string(node.operator)
	 iostream.put_character('(')
	 node.right.traverse(Current)
	 iostream.put_character(')')
      end -- visit_binary_expression
   visit_variable_ref (node : VARIABLE_REF) is 
	 -- Emit C code for this node
      do
	 iostream.put_character('V') -- to avoid name clashes with C
	 iostream.put_string(node.name)
      end -- visit_variable_ref
   visit_integer_value (node : INTEGER_VALUE) is 
	 -- Emit C code for this node
      do
	 iostream.put_integer(node.value)
      end -- visit_integer_value
   visit_assignment (node : ASSIGNMENT) is 
	 -- Emit C code for this node
      do
	 indentation
	 node.variable.traverse(Current)
	 iostream.put_character('=')
	 node.right_hand_side.traverse(Current)
	 iostream.put_character(';'); iostream.put_new_line
      end -- visit_assignment
   visit_print (node : PRINT) is 
	 -- Emit C code for this node. PRINT is implemented with a printf()
      do
	 indentation; iostream.put_string("printf(%"%%d\n%",")
	 node.expression.traverse(Current)
	 iostream.put_character(')')
	 iostream.put_character(';'); iostream.put_new_line
      end -- visit_print
   visit_read (node : READ) is 
	 -- Emit C code for this node. READ is implemented with a scanf()
      do
	 indentation; iostream.put_string("scanf(%"%%d%",&")
	 node.variable.traverse(Current)
	 iostream.put_character(')')
	 iostream.put_character(';'); iostream.put_new_line
      end -- visit_read
   visit_conditional (node : CONDITIONAL) is 
	 -- Emit C code for this node
      do
	 indentation; iostream.put_string("if (")
	 node.condition.traverse(Current)
	 iostream.put_character(')')
	 node.then_part.traverse(Current)
         indentation; iostream.put_string("else")
	 node.else_part.traverse(Current)
      end -- visit_conditional
   visit_while (node : WHILE) is 
	 -- Emit C code for this node
      do
	 indentation; iostream.put_string("while (")
	 node.condition.traverse(Current)
	 indentation; iostream.put_character(')')
	 node.loop_body.traverse(Current)
      end -- visit_while
   visit_block (node : BLOCK) is 
	 -- Emit C code for this node
      local i : INTEGER
      do
	 if node.is_top_level then
	    emit_preambule (node)
	 end -- if
	 iostream.put_character('{'); iostream.put_new_line
	 indent
	 from i := 1 until i > node.count
	 loop
	    node.item(i).traverse(Current)
	    i := i + 1
	 end -- loop
	 dedent
         indentation; iostream.put_character('}'); iostream.put_new_line
	 if node.is_top_level then
	    emit_postambule (node)
	 end -- if
      end -- visit_block
   emit_preambule (node : BLOCK) is
	-- Emit code that precedes the main body
      local i : INTEGER
      do
	 iostream.put_string("#include <stdio.h>%Nint ")
	 -- list the variables used in the program
	 from i:=1 until i > node.var_table.count
	 loop
	    visit_variable_ref(node.var_table.item(i))
	    iostream.put_string(", ")
	    i := i + 1
	 end -- loop
	 iostream.put_string("_exit_code = 0;%N") -- Add one extra variable
	 iostream.put_string("void main ()") -- all the code is generated as
                                       --   a single procedure
      end -- emit_preambule
   emit_postambule (node : BLOCK) is
	-- Emit code that follows the main body
      do
      end -- emit_postambule
feature {NONE} -- Private
   depth : INTEGER
   indent is
	-- increment depth
      do
	 depth := depth + 1
      end -- indent
   dedent is
	-- decrement depth
      require depth > 0
      do
	 depth := depth - 1
      end -- dedent
   indentation is
	-- Print a number of blnck characters according to depth
      local i: INTEGER
      do
	 from i := depth * 2 until i = 0 
         loop
            iostream.put_character(' ')
            i:=i-1
         end -- loop
      end -- indentation
invariant
   depth_positive: depth >= 0
end -- C_CODE_GENERATOR

