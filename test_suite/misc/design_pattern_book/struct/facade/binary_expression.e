indexing
   description: "An EXPRESSION made of an operator and 2 operands"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class BINARY_EXPRESSION
inherit EXPRESSION
creation make
feature -- Creation
   make (op: STRING; arg1, arg2: EXPRESSION) is
      -- Creation and initialization
      require
	 op_not_void: op /= Void
	 arg1_not_void: arg1 /= Void
	 arg2_not_void: arg2 /= Void
      do
	 operator := op
	 left := arg1; right := arg2
      end -- make
feature {ANY} -- Queries
   left, right : EXPRESSION
   operator : STRING
feature {ANY} -- Commands
   traverse (visitor : NODE_VISITOR) is
	 -- let visitor visit the current node
      do
	 visitor.visit_binary_expression (Current)
      end -- traverse
end -- BINARY_EXPRESSION

