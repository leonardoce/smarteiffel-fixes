indexing
   description: ""
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class SCANNER
creation make
feature -- Creation
   make (stream : like input_stream) is
      -- Creation and initialization
      do
	 input_stream := stream
      end -- make
feature {ANY} -- Queries
   item : TOKEN is once !!Result.make("") end -- last token read
   end_of_input : BOOLEAN is do Result := input_stream.end_of_input end
feature {ANY} -- Commands
   next is
	 -- read the next token
      require not_end_of_input: not end_of_input
      do
	 input_stream.read_word
	 item.make(input_stream.last_string)
      end -- next
feature {NONE} -- Private
   input_stream : INPUT_STREAM
end -- SCANNER

