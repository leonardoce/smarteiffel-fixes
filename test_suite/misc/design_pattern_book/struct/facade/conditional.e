indexing
   description: "A PROGRAM_NODE describing an 'if' STATEMENT "
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class CONDITIONAL
inherit STATEMENT
creation make
feature -- Creation
   make  (cond: EXPRESSION; true_part, false_part: BLOCK) is
      -- Creation and initialization
      require 
	 cond_not_void: cond /= Void
	 true_part_not_void: true_part /= Void
	 false_part_not_void: false_part /= Void
      do
	 condition := cond
	 then_part := true_part; then_part.set_parent(Current)
         else_part := false_part; else_part.set_parent(Current)	 
      end -- make
feature {ANY} -- Queries
   condition : EXPRESSION
   then_part, else_part : BLOCK
feature {ANY} -- Commands
   traverse (visitor : NODE_VISITOR) is
	 -- let visitor visit the current node
      do
	 visitor.visit_conditional (Current)
      end -- traverse
end -- CONDITIONAL

