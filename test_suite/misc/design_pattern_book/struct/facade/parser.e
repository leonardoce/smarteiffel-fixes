indexing
   description: "A naive and straitforward parser"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class PARSER
creation make
feature -- Creation
   make (scanner : SCANNER; node_builder : NODE_BUILDER) is
      -- Creation and initialization
      require
	 scanner_not_void: scanner /= Void
	 node_builder_not_void: node_builder /= Void
      do
	 scan := scanner
	 builder := node_builder
	 parse_block
	 if not scan.end_of_input then 
	    syntax_error("Trailing garbage in input stream")
	 end -- if
      end -- make
feature {ANY} -- Commands
   parse_expression is
      -- parse a BIN_EXPRESSION
      local
	 left_arg, right_arg : EXPRESSION
	 op : STRING
      do
	 from parse_term 
	 until scan.end_of_input or else not scan.item.is_operator
	 loop
	    left_arg ?= builder.root_node -- remember left argument
	    op := scan.item.value -- remember operator name
	    parse_term -- parse right argument
	    right_arg ?= builder.root_node
	    builder.make_expression(op,left_arg,right_arg)
	 end -- loop
      end -- parse_expression
   parse_term is
      -- parse a terminal
      do
	 scan.next
	 if scan.item.is_integer then
	    builder.make_integer_value(scan.item.value.to_integer)
	    scan.next
	 elseif scan.item.is_left_parenthesis then
	    parse_expression
	    if not scan.item.is_right_parenthesis then
	       syntax_error("right_parenthesis expected")
	    end -- if
	    scan.next
	 elseif scan.item.is_identifier then
	    builder.make_variable_ref(scan.item.value)
	    scan.next
	 else
	    syntax_error("Integer, variable or ( expected")
	 end -- if
      end -- parse_term
   parse_statement is
	 -- parse a statement
      do
	 if scan.item.is_keyword_read then
	    parse_read
	 elseif scan.item.is_keyword_print then
	    parse_print
	 elseif scan.item.is_keyword_if then
	    parse_conditional
	 elseif scan.item.is_keyword_while then
	    parse_while
	 elseif scan.item.is_identifier then 
	    parse_assignment
	 else
	    syntax_error("Statement :=, read, print, if, while expected")
	 end -- if
      end -- parse_statement
   parse_read is
	 -- parse a read statement
      local target : VARIABLE_REF	
      do
	 scan.next
	 if scan.item.is_identifier then
	    builder.make_variable_ref(scan.item.value)
	    target ?= builder.root_node
	    builder.make_read_statement(target)
	 else
	    syntax_error("Identifier expected.")
	 end -- if
	 if not scan.end_of_input then scan.next end -- if
      end -- parse_read
   parse_print is
	 -- parse a print statement
      local arg : EXPRESSION
      do
	 parse_expression; arg ?= builder.root_node
	 builder.make_print_statement(arg)
      end -- parse_print
   parse_assignment is
	 -- parse an assigment statement
      local
	 target : VARIABLE_REF
	 arg : EXPRESSION
      do
         builder.make_variable_ref(scan.item.value)
	 target ?= builder.root_node
	 scan.next
	 if scan.item.is_assigment then
	    parse_expression; arg ?= builder.root_node
	    builder.make_assignment_statement(target,arg)
	 else
	    syntax_error(" assigment expected ( := )")
	 end -- if
      end -- parse_assignment
   parse_conditional is
	 -- parse an 'if' statement
      local 
	 cond : EXPRESSION
	 then_part, else_part : BLOCK
      do
	 parse_expression; cond ?= builder.root_node
	 if not scan.item.is_keyword_then then
	    syntax_error("Keyword 'then' expected")
	 end -- if
	 parse_block; then_part ?= builder.root_node
	 if scan.item.is_keyword_else then
	    parse_block; else_part ?= builder.root_node
	 end -- if
	 if scan.item.is_keyword_endif then
	    builder.make_conditional_statement(cond,then_part,else_part)
	    scan.next
	 else
	    syntax_error("Keyword 'endif' expected")
	 end -- if
      end -- parse_conditional
   parse_while is
	 -- parse a 'while' statement
      local
	 cond : EXPRESSION
	 body : BLOCK
      do
	 parse_expression; cond ?= builder.root_node
	 if not scan.item.is_keyword_do then
	    syntax_error("Keyword 'do' expected")
	 end -- if
	 parse_block; body ?= builder.root_node
	 if scan.item.is_keyword_end then
	    builder.make_while_statement(cond,body)
	    scan.next
	 else
	    syntax_error("Keyword 'end' expected")
	 end -- if
      end -- parse_while
   parse_block is
	 -- parse a block, that is a list of instructions
      local 
	 statements : LINKED_LIST[STATEMENT]
	 last : STATEMENT
      do
	 !!statements.make
	 from scan.next
	 until scan.end_of_input or else not scan.item.is_statement_keyword 
	 loop
	    parse_statement; last ?= builder.root_node
	    statements.add_last(last)
	 end -- loop
	 builder.make_block(statements)
      end -- parse_block
feature {NONE} -- Private
   scan : SCANNER
   builder : NODE_BUILDER
   syntax_error (msg:STRING) is
	 -- print a syntax error message and exit
      do
	 std_error.put_string("Syntax Error: ")
	 std_error.put_string(msg);std_error.put_new_line
	 std_error.put_string("Last token read: ")
	 std_error.put_string(scan.item.value); std_error.put_new_line
	 die_with_code(1)
      end -- syntax_error
end -- PARSER

