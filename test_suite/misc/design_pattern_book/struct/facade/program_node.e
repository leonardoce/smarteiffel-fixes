indexing
   description: "A node in an Abstract Syntax Tree"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class PROGRAM_NODE
feature {ANY} -- Queries
   var_table : DICTIONARY[VARIABLE_REF,STRING] is
	-- table storing the variables in the program
      once
			create {HASHED_DICTIONARY[VARIABLE_REF,STRING]} Result.make
      end -- var_table
feature {ANY} -- Commands
   traverse (visitor : NODE_VISITOR) is
      require visitor_not_void: visitor /= Void
      deferred
      end -- traverse
end -- PROGRAM_NODE

