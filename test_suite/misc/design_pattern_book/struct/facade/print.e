indexing
   description: "A PROGRAM_NODE representing a print STATEMENT"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class PRINT
inherit STATEMENT
creation make
feature -- Creation
   make (expr : EXPRESSION) is
      -- Creation and initialization
      require expr_not_void: expr /= Void
      do
	 expression := expr
      end -- make
feature {ANY} -- Queries
   expression : EXPRESSION
feature {ANY} -- Commands
   traverse (visitor : NODE_VISITOR) is
	 -- traverse this node
      do
	 visitor.visit_print(Current)
      end -- traverse
end -- PRINT

