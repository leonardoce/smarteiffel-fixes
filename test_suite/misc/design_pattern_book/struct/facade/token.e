indexing
   description: "Objects produced by a SCANNER and consumed par a PARSER"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class TOKEN
creation make
feature -- Creation
   make (s : STRING) is
      -- Creation and initialization
      require 
	 s_not_void: s /= Void
      do
	 value := s.twin
      end -- make
feature {ANY} -- Queries
   value : STRING 
	 -- A copy of the token string
   is_integer : BOOLEAN is
	 -- Whether it is an Integer value
      do
	 Result := value.is_integer
      end -- is_integer
   is_keyword : BOOLEAN is
	 -- Whether it is a keyword
      do
	 Result := is_keyword_read or is_keyword_print or is_keyword_if
	    or is_keyword_then or is_keyword_else or is_keyword_endif 
	    or is_keyword_while or is_keyword_do or is_keyword_end	    
      end -- is_keyword
   is_statement_keyword : BOOLEAN is
	 -- Whether it is a keyword starting a statement
      do
	 Result := is_keyword_read or is_keyword_print or is_keyword_if
	    or is_keyword_while or is_identifier  
      end -- is_statement_keyword
   is_operator : BOOLEAN is
	 -- Whether it is an operator
      do
	 Result := operators.has(value)
      end -- is_operator
   is_assigment : BOOLEAN is
	 -- Whether it is an assigment
      do
	 Result := value.is_equal(":=")
      end -- is_assigment
   is_left_parenthesis : BOOLEAN is
	 -- Whether it is a '('
      do
	 Result := value.is_equal("(")
      end -- is_left_parenthesis
   is_right_parenthesis : BOOLEAN is
	 -- Whether it is a ')'
      do
	 Result := value.is_equal(")")
      end -- is_right_parenthesis
   is_identifier : BOOLEAN is
	 -- Whether it is an identifier
      local i : INTEGER
      do
	 if value.item(1).is_letter and then not is_keyword then
	    Result := True
	    from i := 2 until not Result or i > value.count
	    loop
	       Result := value.item(i).is_letter_or_digit
		  or value.item(i) = '_'
	       i := i+1
	    end -- loop
	 end -- if
      end -- is_identifier
   is_keyword_read : BOOLEAN is do Result := value.same_as("read") end
   is_keyword_print : BOOLEAN is do Result := value.same_as("print") end
   is_keyword_if : BOOLEAN is do Result := value.same_as("if") end
   is_keyword_then : BOOLEAN is do Result := value.same_as("then") end
   is_keyword_else : BOOLEAN is do Result := value.same_as("else") end
   is_keyword_endif : BOOLEAN is do Result := value.same_as("endif") end
   is_keyword_while : BOOLEAN is do Result := value.same_as("while") end
   is_keyword_do : BOOLEAN is do Result := value.same_as("do") end
   is_keyword_end : BOOLEAN is do Result := value.same_as("end") end
feature {NONE} -- Private
   operators : ARRAY[STRING] is once 
      Result := <<"+","-","/","*","%%","<",">","=">> end
invariant
   value_not_void: value /= Void
end -- TOKEN

