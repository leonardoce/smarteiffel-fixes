indexing
   description: "A PROGRAM_NODE representing a list of STATEMENTs"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class BLOCK
inherit 
   STATEMENT undefine copy, is_equal, fill_tagged_out_memory end
   LINKED_LIST[STATEMENT]
creation make, make_links -- inherited from LINKED_LIST
feature {ANY} -- Creation
   make_links  (l : COLLECTION[STATEMENT]) is
	 -- Creation and initialization
      local i : INTEGER
      do
	 if l = Void then
	    make
	 else
	    from_collection(l)
	    from i := 1 until i > count
	    loop
	       item(i).set_parent(Current)
	       i := i + 1
	    end -- loop
	 end -- if
      end -- make_links
feature {ANY} -- Commands
   traverse (visitor : NODE_VISITOR) is
	 -- let visitor visit the current node
      do
	 visitor.visit_block (Current)
      end -- traverse
end -- BLOCK
