indexing
   description: "Exercise the FACADE pattern"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class TEST_FACADE

insert
	ARGUMENTS
	
creation make
feature -- Creation
   make is
      -- Read a program in the toy language on STDIN
      -- If "-stat" is given as the first argument, produces statistics on STDOUT
      -- else generates corresponding C code
      local
	 compiler : COMPILER -- The FACADE to the compilation subsystem
      do
	 !!compiler; compiler.build_abstract_syntax_tree(std_input)
	 if argument_count >= 1 and then argument(1).is_equal("-stat") then
	    compiler.print_statistics
	 else -- Generate C code
	    compiler.generate_c_code(std_output)
	 end -- if
      end -- make
end
