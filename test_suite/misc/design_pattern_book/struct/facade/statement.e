indexing
   description: "A PROGRAM_NODE representing a statement"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class STATEMENT
inherit PROGRAM_NODE
feature {ANY} -- Queries
   parent : STATEMENT
   is_top_level : BOOLEAN is do Result := parent = Void end
feature {ANY} -- Commands
   set_parent (new : like parent) is do parent := new end
end -- STATEMENT

