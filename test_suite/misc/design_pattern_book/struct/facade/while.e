indexing
   description: "A PROGRAM_NODE for the 'WHILE' STATEMENT"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class WHILE
inherit STATEMENT
creation make
feature -- Creation
   make (cond: EXPRESSION; body : BLOCK) is
      -- Creation and initialization
      require 
	 cond_not_void: cond /= Void
	 body_not_void: body /= Void
      do
	 condition := cond
	 loop_body := body; loop_body.set_parent(Current)
      end -- make
feature {ANY} -- Queries
   condition : EXPRESSION
   loop_body : BLOCK
feature {ANY} -- Commands
   traverse (visitor : NODE_VISITOR) is
	 -- let visitor visit the current node
      do
	 visitor.visit_while (Current)
      end -- traverse
end -- WHILE

