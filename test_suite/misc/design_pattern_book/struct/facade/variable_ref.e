indexing
   description: "An EXPRESSION constituted by a reference to a variable"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class VARIABLE_REF
inherit EXPRESSION
creation make
feature -- Creation
   make (n: STRING) is
      -- Creation and initialization
      require 
	 name_not_void: n /= Void
	 name_not_empty: not n.is_empty
      do
	 name := n
      end -- make
feature {ANY} -- Queries
   name : STRING
feature {ANY} -- Commands
   traverse (visitor : NODE_VISITOR) is
	 -- let visitor visit the current node
      do
	 visitor.visit_variable_ref (Current)
      end -- traverse
end -- VARIABLE_REF

