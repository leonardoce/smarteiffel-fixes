indexing
   description: ""
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class READ
inherit STATEMENT
creation make
feature -- Creation
   make (var : VARIABLE_REF) is
      -- Creation and initialization
      require var_not_void: var /= Void
      do
	 variable := var
      end -- make
feature {ANY} -- Queries
   variable : VARIABLE_REF
feature {ANY} -- Commands
   traverse (visitor : NODE_VISITOR) is
	 -- let visitor visit the current node
      do
	 visitor.visit_read (Current)
      end -- traverse
end -- READ
