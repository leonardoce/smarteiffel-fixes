indexing
   description: "Assignment Statement"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class ASSIGNMENT
inherit STATEMENT
creation make
feature -- Creation
   make (var : VARIABLE_REF; arg : EXPRESSION) is
      -- Creation and initialization
      require 
	 var_not_void: var /= Void
	 arg_not_void: arg /= Void
      do
	 variable := var
	 right_hand_side := arg
      end -- make
feature {ANY} -- Queries
   variable : VARIABLE_REF
   right_hand_side : EXPRESSION
feature {ANY} -- Commands
   traverse (visitor : NODE_VISITOR) is
	 -- let visitor visit the current node
      do
	 visitor.visit_assignment (Current)
      end -- traverse
end -- ASSIGNMENT

