indexing
   description: "A BUILDER for PROGRAM_NODEs"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class NODE_BUILDER
creation make
feature -- Creation
   make is
      -- Creation and initialization
      do
      end -- make
feature {ANY} -- Queries
   root_node : PROGRAM_NODE
feature {ANY} -- Commands
   make_expression (op: STRING; arg1, arg2: EXPRESSION) is
      -- Build a 'BINARY_EXPRESSION' PROGRAM_NODE
      do
	 !BINARY_EXPRESSION!root_node.make(op,arg1,arg2)
      end -- make_expression
   make_integer_value (value : INTEGER) is
      -- Build a 'INTEGER_VALUE' PROGRAM_NODE
      do
	 !INTEGER_VALUE!root_node.make(value)
      end -- make_integer 
   make_variable_ref (name: STRING) is
      -- Build a 'VARIABLE_REF' PROGRAM_NODE
      local var_ref : VARIABLE_REF
      do
	 if root_node /= Void and then root_node.var_table.has(name) then
            root_node := root_node.var_table.at(name) -- return previous ref
         else -- variable unknown so far
	    !!var_ref.make(name) -- make a new variable reference and 
	    root_node := var_ref
	    root_node.var_table.put(var_ref,name) -- insert it in lookup table
         end -- if
      end -- make_variable_ref
   make_print_statement (arg : EXPRESSION) is
	 -- Build a 'PRINT' PROGRAM_NODE
      do
	 !PRINT!root_node.make(arg)
      end -- make_print_statement
   make_read_statement (var_name : VARIABLE_REF) is
	 -- Build a 'READ' PROGRAM_NODE
      do
	 !READ!root_node.make(var_name)
      end -- make_read_statement
   make_assignment_statement (var_name : VARIABLE_REF; arg : EXPRESSION) is
	 -- Build an 'ASSIGNMENT' PROGRAM_NODE
      do
	 !ASSIGNMENT!root_node.make(var_name,arg)
      end -- make_assignment_statement
   make_conditional_statement (cond: EXPRESSION; then_part,else_part: BLOCK) is
	 -- Build a 'CONDITIONAL' PROGRAM_NODE
      do
	 !CONDITIONAL!root_node.make(cond,then_part,else_part)
      end -- make_conditional_statement
   make_while_statement (cond: EXPRESSION; body : BLOCK) is
	 -- Build a 'WHILE' PROGRAM_NODE
      do
	 !WHILE!root_node.make(cond,body)
      end -- make_while_statement
   make_block (l : COLLECTION[STATEMENT]) is
	 -- Build a 'BLOCK' PROGRAM_NODE from l
      do
	 !BLOCK!root_node.make_links(l)
      end -- make_block
end -- NODE_BUILDER

