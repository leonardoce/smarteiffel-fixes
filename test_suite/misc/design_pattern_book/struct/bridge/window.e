indexing
   description: "A Window abstraction for client applications"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class WINDOW
feature -- Creation
   make (a_contents : VIEW; window_imp : WINDOW_IMP) is
      -- Creation and initialization
      -- Simple case where the implementation is passed as a parameter
      require 
	 a_contents_not_void: a_contents /= Void
	 window_imp_not_void: window_imp /= Void
      do
	 contents := a_contents
	 imp := window_imp 
      end -- make
feature {ANY} -- Commands
   draw_contents is deferred end -- Draw the contents
   -- ...
   draw_text (text : STRING; p : POINT2D) is
	 -- Draw a text at 'p'
      do
	 imp.device_text(text,p.x,p.y)
      end -- draw_text
feature {NONE} -- Private
   imp : WINDOW_IMP -- interface to the underlying windowing system
   contents : VIEW -- the window's contents
invariant
   imp_not_void: imp /= Void
end -- WINDOW

