indexing
   description: "Application window that is able to draw its contents"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class APPLICATION_WINDOW
inherit
   WINDOW
creation make -- Creation routine (inherited)
feature {ANY} -- Commands
   draw_contents is 
	 -- Draw the contents
      do
	 contents.draw_on(Current)
      end  -- draw_contents
end -- APPLICATION_WINDOW

