indexing
   description: "Example of the use of the BRIDGE pattern"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class TEST_BRIDGE
creation make
feature -- Creation
   make is
	 -- Creation and initialization
      local
	 imp : WINDOW_IMP
      do
	 print("X-windows example -> ")
	 !XWINDOW_IMP!imp; window_example(imp)
	 print("PM windows example -> ")
	 !PMWINDOW_IMP!imp; window_example(imp)
      end -- make
feature {ANY} -- Commands
   window_example (imp : WINDOW_IMP) is
	 -- Creates 2 windows and make them draw their contents
      local 
	 aw : APPLICATION_WINDOW
	 iw : ICON_WINDOW
	 v : VIEW
      do
	 !!v
	 !!aw.make(v,imp) -- make an application Window
	 !!iw.make(v,imp) -- make an icon window
	 aw.draw_contents; iw.draw_contents -- draw both contents
      end -- window_example
end
