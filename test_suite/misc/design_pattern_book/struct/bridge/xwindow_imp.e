indexing
   description: ""
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class XWINDOW_IMP
inherit WINDOW_IMP
feature {ANY} -- Queries & Commands
   -- a lot of routines..., plus
   device_text(text: STRING; x,y: REAL) is 
      do
	 print("Printing: "); print(text)
	 print(" on an X-window at ("); io.put_real(x)
	 io.put_character(','); io.put_real(y); print(").%N")
      end -- device_text
   device_bitmap(bitmap: STRING; x,y: REAL) is
      do
	 print("Putting a bitmap "); print(bitmap)
	 print(" on an X-window at ("); io.put_real(x)
	 io.put_character(','); io.put_real(y); print(").%N")
      end -- device_bitmap
end -- XWINDOW_IMP

