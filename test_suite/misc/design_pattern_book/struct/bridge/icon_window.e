indexing
   description: ""
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class ICON_WINDOW
inherit
   WINDOW
creation make -- Creation routine inherited
feature {ANY} -- Commands
   draw_contents is 
	 -- Draw the contents
      do
	 imp.device_bitmap(bitmap_name,0.0,0.0)
      end  -- draw_contents
feature {NONE} -- Private
   bitmap_name : STRING is "Flashy Bitmap"
end -- ICON_WINDOW

