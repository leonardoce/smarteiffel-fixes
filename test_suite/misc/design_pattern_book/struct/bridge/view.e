indexing
   description: "Something drawable on a window"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class VIEW
feature {ANY} -- Commands
   draw_on (w : WINDOW) is
	 -- simulates that the view contents is being drawn on w
      require w_not_void: w /= Void
      local place : POINT2D
      do
	 !!place.make(2,5)
	 w.draw_text("A message",place)
      end -- draw_on
end -- VIEW

