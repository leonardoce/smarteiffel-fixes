indexing
   description: "Interface to the underlying windowing system"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class WINDOW_IMP
feature {ANY} -- Queries & Commands
   -- a lot of routines..., plus
   device_text(text: STRING; x,y: REAL) is deferred end
   device_bitmap(bitmap: STRING; x,y: REAL) is deferred end
end -- WINDOW_IMP
