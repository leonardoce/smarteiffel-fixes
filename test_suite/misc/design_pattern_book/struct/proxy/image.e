indexing
   description: "A GRAPHIC able to display resizable image files"
   note: ""
   status: "$State$"
   date: "$Date: 2004-09-18 10:36:05 +0200 (Sat, 18 Sep 2004) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class IMAGE
inherit GRAPHIC
creation make
feature -- Creation
   make (filename : STRING) is
	 -- Creation and initialization
      require filename_not_void: filename /= Void
      local f : TEXT_FILE_READ
      do
	 !!extent.make(0,0)
	 !!f.connect_to(filename)
	 load(f) -- load image from the file
      end -- make
feature {ANY} -- Queries
   extent : POINT2D -- implemented as an attribute
feature {ANY} -- Commands
   draw (at : POINT2D) is 
	 -- simulates drawing the image
      do
	 io.put_string(contents); io.put_string(" drawn at ")
	 io.put_string(at.out); io.put_new_line
      end -- draw
   resize (new_extent: POINT2D) is
	 -- resize the image
      do
	 extent := new_extent
      end -- resize
   load  (source: TEXT_FILE_READ) is
	 -- load from an input stream
      do
	 print("Loading an image. Please wait...%N")
	 extent.read(source)
	 source.read_line; contents := source.last_string.twin
      end -- load
   save (destination: TEXT_FILE_WRITE) is
	 -- save to an output stream
      do
	 print("Saving an Image...")
	 destination.put_string(extent.out); destination.put_character(' ')
	 destination.put_string(contents); destination.put_new_line
      end  -- save
feature {NONE} -- Private
   contents : STRING
end -- IMAGE

