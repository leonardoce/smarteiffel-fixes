indexing
   description: "A point on a 2 dimentional space"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class POINT2D
inherit ANY redefine out end;
creation make
feature -- Creation
   make (new_x, new_y : like x) is
      -- Creation and initialization
      do
	 x := new_x; y := new_y
      ensure x = new_x and y = new_y
      end -- set, make
feature {ANY} -- Queries
   x, y : REAL -- Cartesian coordinates of the point
   is_zero : BOOLEAN is do Result := x = 0 and y = 0 end
   out : STRING is
        -- external representation of the point
      do
	 Result := x.to_string
	 Result.extend(',')
	 Result.append(y.to_string)
      end -- out
feature {ANY} -- Commands
   add (other: like Current) is
	 -- add 'other' to Current
      do
	 x := x+other.x; y := y+other.y
      end -- add
   read (source: TEXT_FILE_READ) is
	 -- read form source
      require 
	 source_not_void: source /= Void
	 not_end_of_input: not source.end_of_input
      do
	 source.read_real; x := source.last_real
	 source.read_real; y := source.last_real
      end -- read
end -- POINT2D

