indexing
   description: "A proxy for an IMAGE. Load the image only when needed."
   note: ""
   status: "$State$"
   date: "$Date: 2004-09-18 10:36:05 +0200 (Sat, 18 Sep 2004) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class IMAGE_PROXY
inherit GRAPHIC
creation make
feature -- Creation
   make (filename : STRING) is
      -- Creation and initialization
      require filename_not_void: filename /= Void
      do
	 image_filename := filename.twin
	 !!cached_extent.make(0,0)
      end -- make
feature {ANY} -- Queries
   extent : POINT2D is
	 -- returns the cached extent if possible, or else load the image
      do
	 if cached_extent.is_zero then
	    cached_extent := get_image.extent
	 end -- if
	 Result := cached_extent
      end -- extent
feature {ANY} -- Commands
   draw (at : POINT2D) is 
	 -- simulates drawing the image
      do
	 get_image.draw(at)
      end -- draw
   resize (new_extent: POINT2D) is
	 -- resize the image
      do
	 get_image.resize(new_extent)
	 cached_extent := new_extent
      end -- resize
   load  (source: TEXT_FILE_READ) is
	 -- load from an input stream
      do
	 cached_extent.read(source)
	 source.read_line; image_filename := source.last_string.twin
      end -- load
   save (destination: TEXT_FILE_WRITE) is
	 -- save to an output stream
      do
	 print("Saving an Image_Proxy...")
	 destination.put_string(cached_extent.out)
	 destination.put_character(' ')
	 destination.put_string(image_filename); destination.put_new_line
      end  -- save
feature {NONE} -- Private
   get_image : IMAGE is
	 -- returns the cached image if possible, or else load it
      do
	 if actual_image = Void then
	    !!actual_image.make(image_filename)
	    cached_extent := actual_image.extent
	 end -- if
	 Result := actual_image
      end -- get_image
   image_filename : STRING -- the filename storing the actual image
   cached_extent : POINT2D -- a cache for the extent of the actual image
   actual_image : IMAGE -- loaded only when actually needed
invariant
   coherent_cache: actual_image /= Void 
                      implies cached_extent.is_equal(actual_image.extent)
end -- IMAGE_PROXY

