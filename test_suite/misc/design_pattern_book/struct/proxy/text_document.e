indexing
   description: ""
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class TEXT_DOCUMENT
inherit LINKED_LIST[GRAPHIC]
creation make
feature {ANY} -- Commands
   save (filename: STRING) is
	 -- save the current document to a file named 'filename'
      require filename_not_void: filename /= Void
      local 
	 f : TEXT_FILE_WRITE
	 i : INTEGER
      do
	 !!f.connect_to(filename)
	 from i := 1 until i > count 
	 loop
	    item(i).save(f)
	    i := i + 1
	 end -- loop
	 f.disconnect
	 print("Document saved.%N")
      end -- save
   draw is
	 -- draw the document in a diagonal (silly but simple).
      local
	 top_left : POINT2D
	 i : INTEGER
      do
	 !!top_left.make(0,0)
	 from i := 1 until i > count 
	 loop
	    item(i).draw(top_left)
	    top_left.add(item(i).extent)
	    i := i + 1
	 end -- loop
      end -- draw
end -- TEXT_DOCUMENT

