indexing
   description: ""
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class TEST_PROXY
creation make
feature -- Creation
   make is
      -- Creation and initialization
      local 
	 new_size : POINT2D
	 document : TEXT_DOCUMENT
	 g1, g2 : GRAPHIC
      do
	 !!document.make
	 !IMAGE!g1.make("Image1") -- Image1 is a small image: we include it directly
	 !IMAGE_PROXY!g2.make("Image2") -- Image2 is large, we use an image proxy 
	 document.add_last(g1) -- we insert both g1 and g2
	 document.add_last(g2) --   into the document
	 
	 document.save("A_document") -- Image2 is not yet accessed
	 document.draw -- Here we need the actual image
	 !!new_size.make(5,10); g1.resize(new_size) -- resize g1
	 !!new_size.make(50,100); g2.resize(new_size) -- resize g2
	 -- notice that the resize operation on g2 does not change 
	 -- the image itself, but just its proxy
	 document.save("A_document") -- Overwrite previous save
	 document.draw -- Redraw with new size
      end -- make
end

