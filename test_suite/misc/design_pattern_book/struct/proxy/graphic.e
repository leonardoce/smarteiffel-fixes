indexing
   description: "The abstract interface for graphic objetcs"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class GRAPHIC
feature {ANY} -- Queries
   extent : POINT2D is deferred end -- size of the graphic
feature {ANY} -- Commands
   draw (at : POINT2D) is deferred end -- draw itself starting at 'at'
   resize (new_extent: POINT2D) is
	 -- resize the graphic
      require new_extent_not_void: new_extent /= Void
      deferred
      ensure extent.is_equal(new_extent)
      end -- resize
   load (source: TEXT_FILE_READ) is
	 -- load from an input stream
      require 
	 source_not_void: source /= Void
	 not_end_of_input: not source.end_of_input
      deferred
      end  -- load
   save (destination: TEXT_FILE_WRITE) is
	 -- save to an output stream
      require destination_not_void: destination /= Void
      deferred
      end  -- save
end -- GRAPHIC

