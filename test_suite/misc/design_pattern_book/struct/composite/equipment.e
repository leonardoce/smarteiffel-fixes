indexing
   description: "Piece of electronic or computer equipment"
   note: ""
   status: ""
   date: "$Date: 2004-09-18 10:13:28 +0200 (Sat, 18 Sep 2004) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class EQUIPMENT
insert
	ANY
feature {ANY} -- Queries
   name : STRING
	 -- Name of this equipment
   power : REAL is deferred end
	 -- Electric power consumption
   net_price, discount_price : REAL is deferred end 
	 -- cost of this equipment
feature {ANY} -- Commands
   make (its_name : STRING) is
      -- Initialization
      require
	 its_name_not_void: its_name /= Void
      do
	 name := its_name
      end -- make
invariant
   name_not_void: name /= Void
   positive_power: power >= 0.0
   positive_price: discount_price >= 0.0
   real_discount: net_price >= discount_price
end -- EQUIPMENT

