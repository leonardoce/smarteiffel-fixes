indexing
   description: "Example of the use of a COMPOSITE"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class TEST_COMPOSITE
creation make
feature -- Creation
   make is
      -- Creation and assembly of a primitive PC whose price is displayed
      local
	 cabinet : CABINET -- Will hold a CHASSIS
	 chassis : CHASSIS -- Will contain a BUS and a FLOPPY_DISK
	 bus : BUS -- Will hold a CARD
	 e : EQUIPMENT -- Temporary for storing basic equipment
      do
	 !!cabinet.make("PC Cabinet")
	 !!chassis.make("PC Chassis")
	 cabinet.add(chassis)
	 !!bus.make("MCA Bus")
	 !CARD!e.make("16Mbs Token Ring"); bus.add(e)
	 chassis.add(bus)
	 !FLOPPY_DISK!e.make("3.5 Floppy"); chassis.add(e)
	 io.put_string("The net price is ")
	 io.put_real(cabinet.net_price); io.put_new_line
      end -- make
end
