indexing
   description: "Components having children and an optional parent"
   note: ""
   status: ""
   date: "$Date: 2005-12-20 15:46:40 +0100 (Tue, 20 Dec 2005) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class COMPOSITE [T]
feature {ANY} -- Queries
   parent : COMPOSITE [T] is do end
	 -- optional parent, default to Void. If an explicit reference
	 -- to the parent is needed, redefine as an attribute.
   has (child : T) : BOOLEAN is
	 -- does 'child' belong to the composite?
      require child_not_void: child /= Void
      do
	 Result := children.fast_has(child) -- identity comparison
      end -- has
feature {ANY} -- Commands
   add (new_child : T) is
         -- add `new_child' to the composite
      require new_child_not_void: new_child /= Void
      do
         children.add_last(new_child)
      ensure added: has(new_child)
      end -- add
   remove (child : T) is
         -- remove T from the composite
      require child_not_void: child /= Void
      do
         children.remove(children.fast_first_index_of(child))
      ensure removed: not has(child)
      end -- remove   
feature {NONE} -- Private
   children : COLLECTION [T] 
	 -- holding the parts.
invariant
   children_not_void: children /= Void
end -- COMPOSITE

