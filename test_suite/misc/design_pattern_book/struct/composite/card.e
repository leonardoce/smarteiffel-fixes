indexing
   description: "A card plugable in a bus"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class CARD
inherit EQUIPMENT
creation make -- inherited from EQUIPMENT
feature {ANY} -- Queries
   power : REAL is 1.0
   net_price : REAL is 70.0
   discount_price : REAL is 
	 -- No discount on such a single item
      do
	 Result := net_price
      end -- discount_price
end -- CARD

