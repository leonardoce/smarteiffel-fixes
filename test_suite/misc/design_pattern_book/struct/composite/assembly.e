indexing
   description: "Example of the use of a COMPOSITE"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class ASSEMBLY
creation 
   make
feature -- Creation
   make is
      -- Creation and initialization
      local
	 cabinet : CABINET
	 chassis : CHASSIS
	 bus : BUS
	 e : EQUIPMENT
      do
	 !!cabinet.make("PC Cabinet")
	 !!chassis.make_with_list("PC Chassis") -- use a list representation
	 cabinet.add(chassis)
	 !!bus.make("MCA Bus")
	 !CARD!e.make("16Mbs Token Ring"); e.set_basic_price(50); bus.add(e)
	 chassis.add(bus)
	 !FLOPPY_DISK!e.make("3.5 Floppy"); e.set_basic_price(20); chassis.add(e)
	 io.put_string("The net price is ")
	 io.put_real(chassis.net_price); io.put_new_line
      end -- make
end -- ASSEMBLY
