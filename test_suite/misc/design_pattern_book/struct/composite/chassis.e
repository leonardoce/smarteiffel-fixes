indexing
   description: "A computer chassis integrating other computer equipments"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class CHASSIS
inherit COMPOSITE_EQUIPMENT
creation make, make_with_list -- inherited creation methods
feature {ANY} -- Public queries
   length, width : REAL -- in centimeters
end -- CHASSIS
