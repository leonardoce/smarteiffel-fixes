indexing
   description: "Equipment that contains other equipment"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class COMPOSITE_EQUIPMENT
inherit
   COMPOSITE [EQUIPMENT] -- Handle parent and children management features
   EQUIPMENT
      redefine make end;
feature -- Creation
   make (its_name : STRING) is
      -- Creation and initialization of the children as an ARRAY
      do
	 name := its_name
	 !ARRAY[EQUIPMENT]!children.make(1,0)
      end -- make
   make_with_list  (its_name : STRING) is
      -- Creation and initialization of the children as a LINKED_LIST
      do
	 name := its_name
	 !LINKED_LIST[EQUIPMENT]!children.make
      end -- make_with_list
feature {ANY} -- Queries
   net_price : REAL is
	 -- Sum the net prices of the subequipments
      local
         i : INTEGER
      do
         from i := children.lower until i > children.upper
         loop
            Result := Result + children.item(i).net_price
            i := i + 1
         end -- loop
      end -- net_price
   discount_price : REAL is 
      do
	 Result := net_price * 0.9
      end -- discount_price
   power : REAL is 0.0 -- Should be a computation
invariant
   filiation: parent /= Void implies parent.has(Current)
end -- COMPOSITE_EQUIPMENT

