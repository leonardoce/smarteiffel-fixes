indexing
   description: "Interface of objects involved in a composition (COMPOSITE)"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class COMPONENT
feature {ANY} -- Queries
   is_leaf : BOOLEAN is deferred end
	 -- Whether the COMPONENT is a leaf, i.e. not a COMPOSITE
   composite : COMPONENT is
	 -- Return self if not a leaf. Serve as anchor for other features.
      require not_is_leaf: not is_leaf
      do
	 Result := Current
      ensure Result = Current
      end -- composite
feature {ANY} -- Commands
   
feature {NONE} -- Private
invariant
end -- COMPONENT

