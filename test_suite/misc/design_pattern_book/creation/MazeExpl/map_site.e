indexing
   description: "Abstract description of a component of a Maze"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class MAP_SITE
feature {ANY} -- Public Commands
   describe is deferred end -- Print a terse description of this MAP_SITE
   enter is deferred end -- Try to enter into this MAP_SITE
end -- MAP_SITE

