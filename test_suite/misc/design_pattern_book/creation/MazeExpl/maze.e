indexing
   description: "A Collection of ROOMs"
   note: "Use an array based implementation"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class MAZE
inherit
	ANY
creation make_empty
feature -- Creation
   make_empty is
      -- Creation and initialization
      do
	 !!store.make(1,0)
      end -- make_empty
feature {ANY} -- Public Queries
   describe is
	 -- Print a terse description of each ROOM of the MAZE
      local i : INTEGER
      do
	 print("Maze made of:%N")
	 from i := store.lower until i > store.upper
	 loop
	    if room(i) /= Void then 
	       room(i).describe; print(" with ")
	       room(i).north_side.describe;  print(" (N), ")
	       room(i).east_side.describe;  print(" (E), ")
	       room(i).south_side.describe;  print(" (S), ")
	       room(i).west_side.describe;  print(" (W).%N")
	    end -- if
	    i := i + 1
	 end -- loop
      end -- description
   room (i: INTEGER) : ROOM is
	 -- The ith ROOM if it exists in the MAZE, or else Void
      do
	 if store.valid_index(i) then Result := store.item(i) end
      end -- room
feature {ANY} -- Public Commands
   add_room (new_room : ROOM) is
	 -- add a new ROOM in the MAZE
      require new_room_not_void: new_room /= Void
      do
	 -- store it at position new_room.number, 
	 -- forcing a resize of `store' if needed
	 store.force(new_room,new_room.number)
      ensure stored: room(new_room.number) = new_room
      end -- add_room
feature {NONE} -- Private
   store : ARRAY[ROOM]
invariant
   store_not_void: store /= Void
end -- MAZE

