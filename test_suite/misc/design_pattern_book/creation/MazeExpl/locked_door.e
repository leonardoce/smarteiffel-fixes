indexing
   description: "A DOOR that might need a SPELL to be cast for %
	% removing its lock"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class LOCKED_DOOR
inherit DOOR
   redefine describe end
creation make
feature {ANY} -- Public Queries
   spell_needed : SPELL
feature {ANY} -- Public Commands
   describe is do print("a magical door") end -- describe
end -- LOCKED_DOOR

