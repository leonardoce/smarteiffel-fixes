indexing
   description: "Concrete subclass of MAP_SITE that defines relationships%
   % between components in the MAZE"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class ROOM
inherit
   MAP_SITE
creation make
feature -- Creation
   make (room_number: INTEGER) is
      -- Creation and initialization
      do
	 number := room_number
      end -- make
feature {ANY} -- Public Queries
   north_side, east_side, south_side, west_side : MAP_SITE
   number : INTEGER
feature {ANY} -- Public Commands
   enter is do print("You enter "); describe; print(".%N") end
   describe is do print("Room #"); io.put_integer(number) end
   set_north_side (map_site: MAP_SITE) is
      do
	 north_side := map_site
      ensure north_side = map_site
      end -- set_north_side
   set_east_side (map_site: MAP_SITE) is
      do
	 east_side := map_site
      ensure east_side = map_site
      end -- set_east_side
   set_south_side (map_site: MAP_SITE) is
      do
	 south_side := map_site
      ensure south_side = map_site
      end -- set_south_side
   set_west_side (map_site: MAP_SITE) is
      do
	 west_side := map_site
      ensure west_side = map_site
      end -- set_west_side
end -- ROOM

