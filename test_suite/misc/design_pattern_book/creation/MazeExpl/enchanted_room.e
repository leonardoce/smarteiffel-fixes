indexing
   description: "A ROOM that may contain a SPELL"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class ENCHANTED_ROOM
inherit
   ROOM
      rename make as make_without_spell redefine describe end;
creation make
feature -- Creation
   make (room_number: INTEGER; spell: SPELL) is
      -- Creation and initialization
      do
	 make_without_spell(room_number)
	 resident_spell := spell
      end -- make
feature {ANY} -- Public Queries
   resident_spell : SPELL -- protecting the ENCHANTED_ROOM
feature {ANY} -- Public Commands
   describe is do print("Enchanted room #"); io.put_integer(number) end
end -- ENCHANTED_ROOM

