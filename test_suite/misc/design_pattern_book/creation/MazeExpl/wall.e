indexing
   description: "Separation between two ROOMs"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class WALL
inherit
   MAP_SITE
feature {ANY} -- Public Commands
   describe is do print("a wall") end
   enter is do print("You hit the wall%N") end
end -- WALL

