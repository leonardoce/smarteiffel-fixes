indexing
   description: "Separation between two ROOMs, may be open or closed"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class DOOR
inherit
   MAP_SITE
creation make
feature -- Creation
   make (r1, r2 : ROOM) is
      -- Creation and initialization
      do
	 room1 := r1
	 room2 := r2
      ensure door_set: room1 = r1 and room2 = r2
      end -- make
feature {ANY} -- Public Queries
   room1, room2 : ROOM -- The ROOMs this DOOR is connecting
   other_side_from (room: ROOM) : ROOM is
      require adjacent_room: room = room1 or room = room2
      do
	 if room = room1 then
	    Result := room2
	 else
	    Result := room1
	 end -- if
      end -- other_side_from
   is_open : BOOLEAN
feature {ANY} -- Public Commands
   describe is 
      do 
	 if is_open then print("an open door") else print("a closed door") end -- if
      end -- describe
   enter is
      do
	 if not is_open then print("You hit the door%N") end
      end -- enter
end -- DOOR

