indexing
   description: "A magic spell, usefull to open a DOOR or to enchant %
	% an ENCHANTED_ROOM"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class SPELL
feature {ANY} -- Public Queries
   name : STRING
   set_name (new : STRING) is do name := new end
end -- SPELL
