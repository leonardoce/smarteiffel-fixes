indexing
   description: "Encapsulates compass rose directions"
   note: "Use as an imported module, i.e. expanded object"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
expanded class DIRECTION
feature
   north, east, south, west : INTEGER is unique
end -- DIRECTION
