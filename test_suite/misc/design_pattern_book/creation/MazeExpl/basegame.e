indexing
   description: "Illustrate the basic way of creating a MAZE"
   note: "Example of what should not be done."
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class BASEGAME
creation make
feature -- Creation
   make is
      -- Program entry point
      do
	 my_maze := create_maze
	 my_maze.describe
      end -- make
feature
   create_maze : MAZE is
      -- Create a new maze with 2 rooms connected through a door: [r1|r2]
      local
	 r1, r2 : ROOM
	 door : DOOR
	 wall : WALL
      do
	 !!Result.make_empty
	 !!r1.make(1); !!r2.make(2); !!door.make(r1,r2)
	 Result.add_room(r1); Result.add_room(r2)
	 -- Now set up r1
	 !!wall; r1.set_north_side(wall)
	 r1.set_east_side(door)
	 !!wall; r1.set_south_side(wall)
	 !!wall; r1.set_west_side(wall)
	 -- Now set up r2
	 !!wall; r2.set_north_side(wall)
	 !!wall; r2.set_east_side(wall)
	 !!wall; r2.set_south_side(wall)
	 r2.set_west_side(door)
      end -- create_maze
feature {NONE} -- Private
  my_maze : MAZE
end -- BASEGAME

