indexing
   description: "Factory to create the components of a MAZE"
   note: ""
   status: "$State$"
   date: "$Date: 2006-02-08 17:07:05 +0100 (Wed, 08 Feb 2006) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class MAZE_FACTORY
insert
   ABSTRACT_FACTORY
	rename new_product as new_maze end;
feature {ANY} -- Public
   new_maze : MAZE is 
      do 
	 !!Result.make_empty 
      end -- new_maze
   new_wall : WALL is
      do
	 !!Result
      ensure created: Result /= Void
      end -- new_wall
   new_room (number : INTEGER) : ROOM is
      do
	 !!Result.make(number)
      ensure created: Result /= Void
      end -- new_room
   new_door (r1, r2 : ROOM) : DOOR is
      do
	 !!Result.make(r1,r2)
      ensure created: Result /= Void
      end -- new_door
end -- MAZE_FACTORY

