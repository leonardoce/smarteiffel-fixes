indexing
   description: "Declare an interface for operations that create%
	% abstract product objects"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class ABSTRACT_FACTORY
feature {ANY} -- Public
   -- @@@ these comments, starting with -- @@@ do not appear in pretty versions
   -- @@@ new_product : ABSTRACT_PRODUCT is -- forces product to inherit
   -- @@@ from ABSTRACT_PRODUCT, which might not (?) be a good idea
   new_product : ANY is 
	 -- Create a new product instance
      deferred
      ensure new_created: Result /= Void
      end -- new_product
end -- ABSTRACT_FACTORY

