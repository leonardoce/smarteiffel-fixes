class TEST_ABSFACT

creation make

feature

   make is
      local
         g: GAME_WITH_ABSTRACT_FACTORY;
      do
         !!g.make;
      end;

end
