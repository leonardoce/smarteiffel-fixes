indexing
   description: "Factory to create the components of a MAZE"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class MAZE_FACTORY_SINGLETON
inherit
   MAZE_FACTORY
   SINGLETON
end -- MAZE_FACTORY_SINGLETON

