indexing
   description: "Access point to a MAZE_FACTORY Singleton"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class MAZE_FACTORY_ACCESSOR

inherit
   SINGLETON_ACCESSOR
	
feature {ANY} -- Public
   new_maze : MAZE is 
      do 
	 Result := singleton.new_maze
      ensure created: Result /= Void
      end -- new_maze
   new_wall : WALL is
      do
	 Result := singleton.new_wall
      ensure created: Result /= Void
      end -- new_wall
   new_room (number : INTEGER) : ROOM is
      do
	 Result := singleton.new_room(number)
      ensure created: Result /= Void
      end -- new_room
   new_door (r1, r2 : ROOM) : DOOR is
      do
	 Result := singleton.new_door(r1, r2)
      ensure created: Result /= Void
      end -- new_door
feature {NONE} -- private
   singleton : MAZE_FACTORY is
	 -- Return a unique instance of a MAZE_FACTORY
      local
	 maze_style : STRING; system: SYSTEM
      once
	 maze_style := system.get_environment_variable("MAZESTYLE")
	 if maze_style = Void then
	    !!Result
	 elseif maze_style.is_equal("enchanted") then
	    !ENCHANTED_MAZE_FACTORY!Result
	 else -- Default to MAZE_FACTORY 
	    !!Result
	 end -- if
      end -- singleton
end -- MAZE_FACTORY_ACCESSOR

