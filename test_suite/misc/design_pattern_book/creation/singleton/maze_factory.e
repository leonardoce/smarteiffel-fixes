indexing
   description: "Factory to create the components of a MAZE"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class MAZE_FACTORY
inherit
   ABSTRACT_FACTORY
	rename new_product as new_maze end;
   SINGLETON
feature {ANY} -- Public
   new_maze : MAZE is 
      do 
	 !!Result.make_empty 
      end -- new_maze
   new_wall : WALL is
      do
	 !!Result
      ensure created: Result /= Void
      end -- new_wall
   new_room (number : INTEGER) : ROOM is
      do
	 !!Result.make(number)
      ensure created: Result /= Void
      end -- new_room
   new_door (r1, r2 : ROOM) : DOOR is
      do
	 !!Result.make(r1,r2)
      ensure created: Result /= Void
      end -- new_door
end -- MAZE_FACTORY

