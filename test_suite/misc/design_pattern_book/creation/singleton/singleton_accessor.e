indexing
   description: "Specification of the abstract properties of the global %
   % acces point to a Singleton"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class SINGLETON_ACCESSOR
feature {NONE} -- Private
   singleton : SINGLETON is
	 -- Access to a unique instance.
	 -- Should be redefined as a once function in concrete subclasses
      deferred 
      end -- singleton
   is_real_singleton : BOOLEAN is
	 -- Do multiple calls to `singleton' return the same result?
      do
	 Result := singleton = singleton
      end -- is_real_singleton
invariant
   singleton_is_real_singleton: is_real_singleton
end -- SINGLETON_ACCESSOR


