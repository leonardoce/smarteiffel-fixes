indexing
   description: "Illustrate the way the Singleton DP can be %
	% used to create a MAZE"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class GAME_WITH_SINGLETON_FACTORY
creation make
feature -- Creation
   make is
      -- Program entry point
      local maze_factory : MAZE_FACTORY_ACCESSOR
      do
	 !!maze_factory
	 my_maze := create_maze (maze_factory)
	 my_maze.describe
      end -- make
feature
   create_maze (factory : MAZE_FACTORY_ACCESSOR) : MAZE is
      -- Create a new maze
      local
	 r1, r2: ROOM
	 door : DOOR
      do
	 Result := factory.new_maze
	 r1 := factory.new_room(1); r2 := factory.new_room(2)
	 door := factory.new_door(r1,r2)
	 Result.add_room(r1); Result.add_room(r2)
	 -- Now set up r1
	 r1.set_north_side(factory.new_wall)
	 r1.set_east_side(door)
	 r1.set_south_side(factory.new_wall)
	 r1.set_west_side(factory.new_wall)
	 -- Now set up r2
	 r2.set_north_side(factory.new_wall)
	 r2.set_east_side(factory.new_wall)
	 r2.set_south_side(factory.new_wall)
	 r2.set_west_side(door)
      end -- create_maze
feature {NONE} -- Private
  my_maze : MAZE
end -- GAME_WITH_SINGLETON_FACTORY

