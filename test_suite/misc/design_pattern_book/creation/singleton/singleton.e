indexing
   description: "Specification of the abstract properties of a Singleton"
   note: "Relies on proper evaluation of recursive once functions"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class SINGLETON
feature {NONE} -- Private
   frozen the_singleton : SINGLETON is
	 -- The unique instance of this class
      once
	 Result := Current
      end -- singleton
invariant
   only_one_instance:  Current = the_singleton
end -- SINGLETON
