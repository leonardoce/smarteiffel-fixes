indexing
   description: "Specification of an Abstract Interface for creating %
   % parts of a PRODUCT object"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class BUILDER
feature {ANY} -- Public Queries
   -- @@@ product : ABSTRACT_PRODUCT is -- forces product to inherit
   -- @@@ from ABSTRACT_PRODUCT, which might not (?) be a good idea
   product : ANY is
	 -- Returns the result of the build process
      deferred
      end -- product
feature {ANY} -- Public Commands
   build is
	 -- Start to build the product
      deferred
      ensure product_not_void: product /= Void
      end -- build
end -- BUILDER

