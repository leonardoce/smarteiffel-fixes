class TEST_BUILDER

creation make

feature

   make is
      local
         g: GAME_WITH_BUILDER;
      do
         !!g.make;
      end;

end
