indexing
   description: "Illustrate the way the BUILDER DP can be %
        % used to create a MAZE"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class GAME_WITH_BUILDER
creation make
feature -- Creation
   make is
      -- Program entry point
      local maze_builder : MAZE_BUILDER
      do
	 !STANDARD_MAZE_BUILDER!maze_builder
	 my_maze := create_maze (maze_builder)
	 my_maze.describe
      end -- make
feature
   create_maze (builder : MAZE_BUILDER) : MAZE is
      -- Create a new maze
      do
	 builder.build_maze
	 builder.build_room(1)
	 builder.build_room(2)
	 builder.build_door(1,2)	 
	 Result := builder.maze
      end -- create_maze
feature {NONE} -- Private
  my_maze : MAZE
end -- GAME_WITH_BUILDER




