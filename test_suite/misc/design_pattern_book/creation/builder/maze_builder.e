indexing
   description: "Definition of the Interface used to build a MAZE"
   note: ""
   status: "$State$"
   date: "$Date: 2006-02-10 09:47:13 +0100 (Fri, 10 Feb 2006) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class MAZE_BUILDER
insert
   BUILDER
	rename product as maze, build as build_maze end;
feature {ANY} -- Public Queries
   maze : MAZE is deferred end -- the MAZE being built
feature {ANY} -- Public Commands
   build_maze is
	-- Initialize the `maze'
      deferred
      end -- build_maze
   build_room (number : INTEGER) is
	-- Create a ROOM
      require room_not_yet_built: maze.room(number) = Void
      deferred
      ensure room_built: maze.room(number) /= Void
      end -- build_room
   build_door (n1, n2 : INTEGER) is
	 -- Build a DOOR between rooms numbered n1 and n2
      deferred
      end -- build_door
end -- MAZE_BUILDER

