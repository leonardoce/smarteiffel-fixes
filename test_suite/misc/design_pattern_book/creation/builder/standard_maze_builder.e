indexing
   description: "Simple subclass or MAZE_BUILDER"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class STANDARD_MAZE_BUILDER
inherit MAZE_BUILDER
feature {ANY} -- Public Queries
   maze : MAZE -- the MAZE being built
feature {ANY} -- Public Commands
   build_maze is 
	-- Initialize the `maze'
      do !!maze.make_empty end -- build_maze
   build_room (number : INTEGER) is 
	-- Create a ROOM and build the WALLs around it
      local
	 room : ROOM 
	 wall : WALL
      do 
	 !!room.make(number)
	 maze.add_room(room)
	 !!wall; room.set_north_side(wall)
	 !!wall; room.set_east_side(wall)
	 !!wall; room.set_south_side(wall)
	 !!wall; room.set_west_side(wall)
      end -- build_room
   build_door (n1, n2 : INTEGER) is
         -- Build a DOOR between rooms numbered n1 and n2
         -- and replace their adjoining wall
      local 
	 door : DOOR
	 r1, r2 : ROOM
      do
	 r1 := maze.room(n1); r2 := maze.room(n2)
	 !!door.make(r1,r2)
	 r1.set_east_side(door)
	 r2.set_west_side(door)
      end -- build_door
end -- STANDARD_MAZE_BUILDER

