indexing
   description: "Illustrate the way the Prototype DP can be %
   % used to create a MAZE"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class GAME_WITH_PROTOTYPE
inherit GAME_WITH_ABSTRACT_FACTORY
      redefine make end;
creation make
feature -- Creation
   make is
	 -- Program entry point
      local 
	 maze_prototype_factory : MAZE_PROTOTYPE_FACTORY
	 maze : MAZE; wall : WALL; room : ROOM; door : DOOR
      do
	 -- First make a standard MAZE by passing instances of
	 -- MAZE, WALL, ROOM and DOOR
	 !!maze.make_empty; !!wall; !!room.make(0); !!door.make(Void,Void)
	 !!maze_prototype_factory.make(maze,wall,room,door)
	 my_maze := create_maze (maze_prototype_factory)
	 my_maze.describe
	 -- Now make an enchanted MAZE by passing instances of
	 -- ENCHANTED_ROOM and LOCKED_DOOR
	 !ENCHANTED_ROOM!room.make(0,Void)
	 !LOCKED_DOOR!door.make(Void,Void)
	 !!maze_prototype_factory.make(maze,wall,room,door)
	 my_maze := create_maze (maze_prototype_factory)
	 my_maze.describe
      end -- make
end -- GAME_WITH_PROTOTYPE

