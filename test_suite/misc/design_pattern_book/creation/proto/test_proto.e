class TEST_PROTO

creation make

feature

   make is
      local
         g: GAME_WITH_PROTOTYPE;
      do
         !!g.make;
	 g := g.deep_twin; -- Added by D.Colnet;
      end;

end
