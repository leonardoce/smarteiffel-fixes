indexing
   description: "Factory to create the components of an enchanted MAZE"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class ENCHANTED_MAZE_FACTORY
inherit
   MAZE_FACTORY
	redefine new_room, new_door end; -- (co-variant redefinition)
feature {ANY} -- Public
   new_room (number : INTEGER) : ENCHANTED_ROOM is 
      -- Creates an ENCHANTED_ROOM
      do 
	 cast_a_spell
	 !!Result.make(number,last_spell_cast) 
      end -- new_room
   new_door (r1, r2 : ROOM) : LOCKED_DOOR is do !!Result.make(r1,r2) end
feature {NONE} -- Private
   last_spell_cast : SPELL
   cast_a_spell is do !!last_spell_cast end
end -- ENCHANTED_MAZE_FACTORY

