indexing
   description: "Ancestor of prototype factories in the Prototype DP"
   note: ""
   status: "$State$"
   date: "$Date: 2004-09-18 10:13:28 +0200 (Sat, 18 Sep 2004) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class PROTOTYPE_FACTORY
insert
	ANY
feature {ANY} -- Public Queries
   is_deep_cloning : BOOLEAN
	-- Whether deep cloning is enabled. Shallow cloning is the default.
feature {ANY} -- Public Commands
   select_deep_clone is
	-- Enable deep cloning of the prototypes
	do is_deep_cloning := True ensure is_deep_cloning end 
   select_shallow_cloning is
	-- Enable shallow cloning of the prototypes
	do is_deep_cloning := False ensure not is_deep_cloning end 
end -- PROTOTYPE_FACTORY

