indexing
   description: "Factory to create the components of a MAZE by cloning%
   % (or deep cloning) the prototypes in store."
   note: "Use clone and deep_clone inherited from GENERAL"
   status: "$State$"
   date: "$Date: 2004-09-18 10:13:28 +0200 (Sat, 18 Sep 2004) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class MAZE_PROTOTYPE_FACTORY
inherit
   MAZE_FACTORY
      redefine new_maze, new_wall, new_room, new_door end;
   PROTOTYPE_FACTORY
creation  make
feature -- Creation
   make (maze : MAZE; wall : WALL; room : ROOM; door : DOOR) is
	 -- Initialize the factory with prototypes of the objects
	 -- it will create.
      require
	 maze_not_void: maze /= Void;  wall_not_void: wall /= Void 
	 room_not_void: room /= Void;  door_not_void: door /= Void
      do
	 prototype_maze := maze
	 prototype_wall := wall
	 prototype_room := room
	 prototype_door := door
      end -- make
feature {ANY} -- Public
   new_maze : MAZE is 
      do
	 if is_deep_cloning then
	    Result := prototype_maze.deep_twin
	 else
	    Result := prototype_maze.twin
	 end -- if
	 Result.make_empty -- Re-initialize new_maze
      end -- new_maze
   new_wall : WALL is
      do
	 if is_deep_cloning then
	    Result := prototype_wall.deep_twin
	 else
	    Result := prototype_wall.twin
	 end -- if
      end -- new_wall
   new_room (number : INTEGER) : ROOM is
      do
	 if is_deep_cloning then
	    Result := prototype_room.deep_twin
	 else
	    Result := prototype_room.twin
	 end -- if
	 Result.make(number) -- Re-initialize new_room
      end -- new_room
   new_door (r1, r2 : ROOM) : DOOR is
      do
	 if is_deep_cloning then
	    Result := prototype_door.deep_twin
	 else
	    Result := prototype_door.twin
	 end -- if
	 Result.make(r1,r2) -- Re-initialize new_maze
      end -- new_door
feature {NONE} -- Private (prototype objects)
   prototype_maze : MAZE
   prototype_wall : WALL
   prototype_room : ROOM
   prototype_door : DOOR
invariant
   prototype_maze_not_void: prototype_maze /= Void
   prototype_wall_not_void: prototype_wall /= Void 
   prototype_room_not_void: prototype_room /= Void
   prototype_door_not_void: prototype_door /= Void
end -- MAZE_PROTOTYPE_FACTORY

