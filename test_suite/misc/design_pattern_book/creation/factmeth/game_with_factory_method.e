indexing
   description: "Illustrate the way the Factory Method DP can be %
        % used to create a MAZE"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class GAME_WITH_FACTORY_METHOD
creation make
feature -- Creation
   make is
      -- Program entry point
      do
	 my_maze := create_maze
	 my_maze.describe
      end -- make
feature -- Factory Methods
   create_maze : MAZE is
      -- Create a new maze
      local
	 r1, r2: ROOM
	 door : DOOR
      do
	 Result := new_maze
	 r1 := new_room(1); r2 := new_room(2)
	 door := new_door(r1,r2)
	 Result.add_room(r1); Result.add_room(r2)
	 -- Now set up r1
	 r1.set_north_side(new_wall)
	 r1.set_east_side(door)
	 r1.set_south_side(new_wall)
	 r1.set_west_side(new_wall)
	 -- Now set up r2
	 r2.set_north_side(new_wall)
	 r2.set_east_side(new_wall)
	 r2.set_south_side(new_wall)
	 r2.set_west_side(door)
      end -- create_maze
   new_maze : MAZE is 
      do 
	!!Result.make_empty 
      ensure created: Result /= Void
      end -- new_maze
   new_wall : WALL is
      do
	 !!Result
      ensure created: Result /= Void
      end -- new_wall
   new_room (number : INTEGER) : ROOM is
      do
	 !!Result.make(number)
      ensure created: Result /= Void
      end -- new_room
   new_door (r1, r2 : ROOM) : DOOR is
      do
	 !!Result.make(r1,r2)
      ensure created: Result /= Void
      end -- new_door
feature {NONE} -- Private
  my_maze : MAZE
end --  GAME_WITH_FACTORY_METHOD


