indexing
   description: "Illustrate the way Factory Methods may be redefined %
        % to create an enchanted MAZE"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class ENCHANTED_GAME
inherit 
   GAME_WITH_FACTORY_METHOD
        redefine new_room, new_door end; -- (co-variant redefinition)
creation make -- the `make' definition is inherited from GAME_WITH_FACTORY_METHOD
feature {ANY} -- Public
   new_room (number : INTEGER) : ENCHANTED_ROOM is 
      -- Creates an ENCHANTED_ROOM
      do 
	 cast_a_spell
	 !!Result.make(number,last_spell_cast) 
      end -- new_room
   new_door (r1, r2 : ROOM) : LOCKED_DOOR is do !!Result.make(r1,r2) end
feature {NONE} -- Private
   last_spell_cast : SPELL
   cast_a_spell is do !!last_spell_cast end
end -- ENCHANTED_GAME

