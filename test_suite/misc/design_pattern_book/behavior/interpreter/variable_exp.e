indexing
   description: "A variable"
   note: ""
   status: "$State$"
   date: "$Date: 2004-09-18 10:13:28 +0200 (Sat, 18 Sep 2004) $"
   revision: "$Revision$"
   author: "Michel Train"
class VARIABLE_EXP
inherit
   BOOLEAN_EXP
creation make
feature -- Creation
   make (a_name : STRING) is
	 -- Creation and initialization with a_name as the variable name
      require a_name_not_void: a_name /= Void
      do
	 name := a_name
      end -- make
feature {ANY} -- queries
   name : STRING -- The symbolic name of this boolean variable
   value (ctx : CONTEXT) : BOOLEAN is
	 -- Return truth value of this variable in the `ctx' context 
      do
	 Result := ctx.lookup_value(name)
      end -- value
   replacing (var : STRING; subexpression : BOOLEAN_EXP) : BOOLEAN_EXP is
	 -- Produces new expression with subexpression replacing var
      do
	 if var.is_equal(name) then
	    Result := subexpression.twin
	 else
	    !VARIABLE_EXP!Result.make(name)
	 end -- if
      end -- replacing
invariant
   name_not_void: name /= Void
end -- VARIABLE_EXP
