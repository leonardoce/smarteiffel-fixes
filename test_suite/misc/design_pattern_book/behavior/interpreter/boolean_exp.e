indexing
   description: "Root class of boolean expressions"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
deferred class BOOLEAN_EXP
feature {ANY}
   value (ctx : CONTEXT) : BOOLEAN is
	 -- Return truth value of Current in the `ctx' context
      require ctx_not_void: ctx /= Void
      deferred
      end -- value
   replacing (var : STRING; subexpression : BOOLEAN_EXP) : BOOLEAN_EXP is
	 -- produces new expression with subexpression replacing `var'
      require 
	 var_not_void: var /= Void
	 subexpression_not_void: subexpression /= Void
      deferred
      ensure Result_not_void: Result /= Void
      end -- replacing
end -- BOOLEAN_EXP
