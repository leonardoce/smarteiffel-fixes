indexing
   description: "And expression"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class AND_EXP
inherit
   BOOLEAN_EXP
creation make
feature {ANY}
   make (left_hand_side, right_hand_side : BOOLEAN_EXP) is
	 -- Creation and initialization
      require
	 left_hand_side_not_void: left_hand_side /= Void
	 right_hand_side_not_void: right_hand_side /= Void
      do
	 left := left_hand_side;  right := right_hand_side
      end -- make
feature {ANY} -- Queries
   left, right : BOOLEAN_EXP -- left hand side and right hand side in the expression
   value (ctx : CONTEXT) : BOOLEAN is
	 -- Return truth value of Current in the `ctx' context 
	 -- by evaluating the logical 'and' value of both sides
      do
	 Result := left.value(ctx) and right.value(ctx)
      end -- value
   replacing (var : STRING;  subexpression : BOOLEAN_EXP) : BOOLEAN_EXP is
	 -- Produces new expression with subexpression replacing var
      do
	 !AND_EXP!Result.make(left.replacing(var,subexpression), 
			      right.replacing(var,subexpression))
      end -- replacing
end -- AND_EXP

