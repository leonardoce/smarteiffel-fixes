indexing
   description: "Dictionary of symbols"
   note: "A wraping around a Hash Table"
   status: "$State$"
   date: "$Date: 2004-07-18 10:08:21 +0200 (Sun, 18 Jul 2004) $"
   revision: "$Revision$"
   author: "Michel Train"
class CONTEXT
creation make
feature  -- Creation
   make is
	 -- Creation and initialization 
      do
	 !!table.make
      end -- make
feature {ANY} -- queries
   is_defined (var : STRING) : BOOLEAN is
	 -- Whether variable `var' in known in this context
      require var_not_void: var /= Void
      do
	 Result := table.has(var)
      end -- is_defined
   lookup_value (var : STRING) : BOOLEAN is
	 -- Returns boolean value of variable
      require 
	 var_defined: var /= Void and then is_defined (var)
      do
	 Result := table.at(var)
      end -- lookup_value
   assign (var : STRING; val : BOOLEAN) is
	 -- Assign value `val' to variable name `var'
      require var_not_void: var /= Void
      do
	 table.put(val,var)
      end -- assign
feature {NONE}
   table : HASHED_DICTIONARY[BOOLEAN,STRING]
invariant
   table_not_void: table /= Void
end -- CONTEXT
