indexing
   description: "A test class"
   note: ""
   status: "$State$"
   date: "$Date: 2005-09-21 16:11:13 +0200 (Wed, 21 Sep 2005) $"
   revision: "$Revision$"
   author: "Michel Train"
class TEST_INTERPRETER
creation make
feature {ANY}
   make is	
      local
	 x, y : VARIABLE_EXP
	 ctx : CONTEXT
	 expr : AND_EXP
      do
	 !!x.make("x"); !!y.make("y") -- Build two variables
	 !!expr.make(x,y) -- Build an `and' expression between both
	 !!ctx.make -- Make an evaluation context
	 print("Setting x to true,"); ctx.assign("x",True)
	 print(" and setting y to false %N"); ctx.assign("y",False)
	 print("The value of the expression (x and y) is: ")
	 io.put_boolean(expr.value(ctx)); io.put_new_line
	 print("Result of replacing y with x in (x and y) is: ")
	 io.put_boolean(expr.replacing("y",x).value(ctx)); io.put_new_line
      end -- make
end
