indexing
   description: "Subject storing references to its observers itself"
   note: "Subclass should select a concrete implementation for observers"
   status: ""
   date: "$Date: 2005-12-20 15:46:40 +0100 (Tue, 20 Dec 2005) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class AUTONOMOUS_SUBJECT
inherit
   SUBJECT
feature -- Queries
   is_attached_to (o : OBSERVER) : BOOLEAN is
      -- Whether o belongs to the observers
      do
	 Result := observers.fast_has(o) -- identity comparison
      end -- is_attached_to
feature -- Commands
   attach (o : OBSERVER) is
	 -- add `o' to the observers
      do
	 observers.add_last(o)
      end -- attach
   detach (o : OBSERVER) is
	 -- remove `o' from the observers
      do
	 observers.remove(observers.fast_first_index_of(o))
      end -- detach
   notify is
	 -- Send an update message to each observer
      local
	 i : INTEGER
      do
	 from i := observers.lower until i > observers.upper
	 loop
	    observers.item(i).update(Current)
	    i := i + 1
	 end -- loop
      end -- notify
feature {NONE} -- Private
   observers : COLLECTION[OBSERVER]
	 -- the collection holding the observers. Can be instantiated 
	 -- with either make_array_of_observers or make_list_of_observers
   make_array_of_observers is
	 -- Factory method to instanciate observers as an ARRAY
      do
	 !ARRAY[OBSERVER]!observers.make(1,0)
      ensure observers_initialized: observers /= Void
      end -- make_array_of_observers
   make_list_of_observers is
	 -- Factory method to instanciate observers as a LINKED_LIST
      do
	 !LINKED_LIST[OBSERVER]!observers.make
      ensure observers_initialized: observers /= Void
      end -- make_list_of_observers
invariant
   observers_initialized: observers /= Void
end -- AUTONOMOUS_SUBJECT

