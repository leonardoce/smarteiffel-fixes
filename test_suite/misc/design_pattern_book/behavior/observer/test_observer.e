indexing
   description: "Example of the use of Analog and Digital clocks"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class TEST_OBSERVER
creation 
   make
feature -- Creation
   make is
      -- Creation and initialization
      local
	 clock_timer : CLOCK_TIMER
	 analog_clock : ANALOG_CLOCK
	 digital_clock : DIGITAL_CLOCK
	 i : INTEGER
      do
	 !!clock_timer.make(33333)
	 !!analog_clock.make(clock_timer)
	 !!digital_clock.make(clock_timer)
	 from i := 1 until i > 3
	 loop
	    clock_timer.tick -- Make the clock_timer tick
	    i := i + 1
	 end -- loop
      end -- make
end
