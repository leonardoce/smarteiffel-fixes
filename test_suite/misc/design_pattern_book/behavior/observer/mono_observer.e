indexing
   description: "Observer of only one Subject"
   note: "Uses a subject attribute whose type should be specialized %
   % in subclasses"
   status: ""
   date: "$Date: 2004-07-18 10:08:21 +0200 (Sun, 18 Jul 2004) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class MONO_OBSERVER
inherit
   OBSERVER
feature -- Creation
   make (s : like subject) is
	 -- Creation and initialization
      require s_not_void: s /= Void
      do
	 subject := s
	 subject.attach(Current)
      end -- make
feature {ANY} -- Queries
   subject : SUBJECT
feature {ANY} -- Commands
   update (s : like subject) is
	 -- take actions because 's' did change state
      do
	 check s_is_my_subject: s = subject end -- check
	 update_observer
      end -- update
   update_observer is deferred end 
      -- real action, no check needed on the subject identity
   detach is
	 -- detach from its subject
      do
	 subject.detach(Current)
	 subject := Void
      end -- detach
invariant
   subject_observer: subject /= Void implies subject.is_attached_to(Current)
end -- MONO_OBSERVER

