indexing
   description: "Subject using a CHANGE_MANAGER to reify its relationship%
   % with its observers itself"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class MANAGED_SUBJECT
inherit
   SUBJECT
   SINGLETON_ACCESSOR
      rename singleton as change_manager redefine change_manager end;
feature -- Queries
   is_attached_to (o : OBSERVER) : BOOLEAN is
      -- Whether o belongs to the observers
      do
	 Result := change_manager.is_registered(Current,o)
      end -- is_attached_to
feature -- Commands
   attach (o : OBSERVER) is
	 -- add `o' to the observers
      do
	 change_manager.register(Current,o)
      end -- attach
   detach (o : OBSERVER) is
	 -- remove `o' from the observers
      do
	 change_manager.unregister(Current,o)
      end -- detach
   notify is
	 -- Send an update message to each observer
      do
	 change_manager.notify(Current)
      end -- notify
feature {NONE} -- Private
    change_manager : CHANGE_MANAGER is
	 -- Singleton instance of a CHANGE_MANAGER
      once
	 !!Result.make
      end -- change_manager
invariant
   change_manager_initialized: change_manager /= Void
end -- MANAGED_SUBJECT

