indexing
   description: "Updating interface for objects that should be notified of%
   % changes in a Subject"
   note: "Could be made Generic, with Subject as the formal generic parameter"
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class OBSERVER
feature 
   update (s : SUBJECT) is 
	 -- update the observer's view on 's'
      require not_void: s /= Void
      deferred 
      end -- update
end -- OBSERVER


