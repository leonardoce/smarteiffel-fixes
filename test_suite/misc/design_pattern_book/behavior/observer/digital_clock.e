indexing
   description: "Display a digital clock"
   note: "Implemented as an Observer on a CLOCK_TIMER"
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class DIGITAL_CLOCK
inherit
   MONO_OBSERVER
      rename update_observer as draw redefine subject end;
creation make -- Inherited, with its signature automatically redefined (co-variantly)
feature {ANY} -- Commands
   draw is
	 -- Draw the clock according to the new status of subject
      do
	 io.put_string("Digital display of: ")
	 io.put_integer(subject.hour); io.put_character(':')
	 io.put_integer(subject.minute); io.put_character(':')
	 io.put_integer(subject.second); io.put_new_line
      end -- draw
feature {ANY} -- Queries
   subject : CLOCK_TIMER
      end -- DIGITAL_CLOCK

