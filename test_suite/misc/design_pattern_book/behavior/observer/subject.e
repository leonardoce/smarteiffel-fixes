indexing
   description: "Object observed by any number of Observers"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class SUBJECT
feature -- Queries
   is_attached_to (o : OBSERVER) : BOOLEAN is
      -- Whether o belongs to the observers
      require o_not_void: o /= Void
      deferred
      end -- is_attached_to
feature -- Commands
   attach (o : OBSERVER) is
	 -- add `o' to the observers
      require 
	 o_not_void: o /= Void
	 not_yet_attached: not is_attached_to(o)
      deferred
      ensure attached: is_attached_to(o)
      end -- attach
   detach (o : OBSERVER) is
	 -- remove `o' from the observers
      require o_not_void: o /= Void
      deferred
      ensure not_attached: not is_attached_to(o)
      end -- detach
   notify is
	 -- Send an update message to each observer
      deferred
      end -- notify
end -- SUBJECT

