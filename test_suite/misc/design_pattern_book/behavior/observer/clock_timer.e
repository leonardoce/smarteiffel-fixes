indexing
   description: "Concrete Subject for storing and maintaining the time of day"
   note: "It notifies its observers every second"
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class CLOCK_TIMER
inherit 
   AUTONOMOUS_SUBJECT
creation make
feature -- Creation
   make (initial_time : INTEGER) is
	 -- Creation and initialization with a time offset (in seconds)
      require positive_initial_time: initial_time >= 0
      do
	 time := initial_time
	 make_array_of_observers
      ensure 
	 time_initialized: initial_time\\86400 = 3600*hour + 60*minute + second
      end -- make
feature {ANY} -- Queries
   hour : INTEGER is
	 -- the current hour
      do
	 Result := (time//3600)\\24
      end -- hour
   minute : INTEGER is
	 -- the current minute
      do
	 Result := (time//60)\\60
      end -- minute
   second : INTEGER is
	 -- the current second
      do
	 Result := time\\60
      end -- second
feature {ANY} -- Commands
   tick is
	 -- Called by the internal timer to provide an accurate time base
	 -- Call `notify' to inform observers of the change
      do
	 time := time + tick_interval; notify
      end -- tick
feature {NONE} -- Private
   time : INTEGER -- Elapsed time since initialisation, in seconds
   tick_interval : INTEGER is 5 -- Number of seconds between ticks
invariant
   correct_time: time\\86400 = 3600*hour + 60*minute + second
end -- CLOCK_TIMER

