   myroutine is
	 -- The engine that make the automaton move between states
      do
	 if  state = s1 then
            if condition1 then
	       state := s1.t1; do_action1 -- Change state and do an action
            else 
	       state := s1.t2; do_action2 -- Change state and do an action
            end -- if
	 elseif state = s2 then
	    if condition2 then
	       state := s2.t1; do_action3 -- Change state and do an action
            else 
	       state := s2.t2; do_action4 -- Change state and do an action
            end -- if
	 end  -- if
      end -- myroutine
