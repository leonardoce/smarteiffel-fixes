indexing
   description: "Abstract ancestor of mailer states"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
deferred class MAILER_STATE
feature {MAILER} -- Commands for Mailer usage only
   make (m : MAILER) is
      require m_not_void: m /= Void
      do
	 mailer := m
      end -- make
   send (message : STRING) is 
      require message_not_void: message /= Void
      deferred 
      end -- send
   open is deferred end
   close is deferred end
   init_state is deferred end
   notify_connect is deferred end
   notify_close is deferred  end
   notify_received is deferred  end
feature {NONE}
   mailer : MAILER
invariant
   mailer_not_void: mailer /= Void
end -- MAILER_STATE
