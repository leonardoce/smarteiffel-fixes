indexing
   description: "Mailer with closed connection"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class CLOSED
inherit
   MAILER_STATE
creation make
feature{ANY}
   send (m : STRING) is
      do
	 print("Trying to send while closed: connecting%N")
	 mailer.set_pending_message(m)	
	 mailer.set_state(mailer.connecting)	
      end -- send
   open is
      do
	 print("Trying to open while closed : connecting%N")
	mailer.set_state(mailer.connecting)
      end -- open
   close is
      do
	print("Already closed%N") 
      end -- close
   init_state is
      do
	 print("Initializing closed: closing%N")
	 close_com
      end -- init_state
   notify_connect  is
      do
	print("Connect notification while closed%N") 
      end -- notify_connect
   notify_close  is
      do
	 print("Closing notification while closed%N") 
      end -- notify_close
   notify_received  is
      do
	print("Receive notification while closed%N")  
      end -- notify_received
feature {NONE}
   close_com is
      do
	 print("Closing  connection%N")
      end -- close_com

end -- CLOSED
