indexing
   description: "Waiting for connection aknowledge mailer state"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class CONNECTING
inherit
   MAILER_STATE
creation make
feature {ANY}
   send (m : STRING) is
      do
	 print("Trying to send while connecting%N")
	 mailer.set_pending_message(m)	
      end
   open is
      do
	 print("Receiving open request while waiting for connection%N")
      end
   close is
      do
	 print("Trying to close while connecting: closing%N")
	 mailer.set_state(mailer.closed)
      end
   init_state is
      do
	 print("Initializing connection%N")	 
	 init_com
      end
   notify_connect  is
      do
	 print("Connect notification while connecting: opened%N")
	 mailer.set_state(mailer.connected)
      end
   notify_close  is
      do
	 print("close notification while connecting: closing%N")
	 mailer.set_state(mailer.closed)
      end
   notify_received  is
      do
	 print("reception notification while connecting: closing%N")
	 mailer.set_state(mailer.closed)
      end
feature {NONE}
   init_com is
      do
	 print("Waiting for connection%N")
      end
end -- CONNECTING
