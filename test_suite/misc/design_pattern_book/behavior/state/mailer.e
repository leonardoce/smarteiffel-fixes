indexing
   description: "An example of an object with states"
   note: " "
   status: "$State$"
   date: "$Date: 2004-07-16 21:25:32 +0200 (Fri, 16 Jul 2004) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
class MAILER
creation make
feature {ANY} -- Creation
   make is
	 -- Creation and Initialization
      do
	 -- Creates incoming and outgoing message queues
	 !!incoming_queue.make; !!pending_queue.make
	 -- Create the 3 possible states of a Mailer
	 !!connecting.make(Current); !!connected.make(Current); !!closed.make(Current)
	 -- The initial state is 'closed'
	 state := closed
      end -- make
feature {ANY} -- Public Queries
   last_message : STRING -- Last message received
feature {ANY} -- Public Commands
   send (m : STRING) is
	 --  try to forward the message `m' to the network
      require valid_message: m /= Void
      do
	 print("Trying to send the message: ")
	 io.put_string(m); io.put_new_line
	 state.send(m)
      end -- send
   receive_next_message is
	 -- Try to get an incoming message
      do
	 io.put_string("Trying to receive a message%N")
	 if incoming_queue.is_empty then
	    last_message := Void
	 else
	    last_message := incoming_queue.first
	    incoming_queue.remove_first
	 end -- if
      end -- receive_next_message
   open is
	 -- try to open a network connection
      do
	 state.open
      end -- open
   close is
	 -- try to close the network connection
      do
	 state.close
      end -- close
feature {NETWORK} -- Only a NETWORK object may call these features
   notify_connect is
	 -- connection is open
      do
	 io.put_string("Connection is opened%N")
	 state.notify_connect
      end -- notify_connect
   notify_close is
	 -- connection has been closed
      do
	 io.put_string("Connection is closed%N")
	 state.notify_close 
      end -- notify_close
   notify_received is
	 -- a message just arrived
      do
	 io.put_string("a message has been received%N")
	 state.notify_received
      end -- notify_receive
feature {MAILER_STATE} -- Queries restricted to Mailer_State objects
   connecting : CONNECTING -- A Mailer_State
   connected : CONNECTED -- Another Mailer_State
   closed : CLOSED -- A third Mailer_State
   pending_message : STRING -- head of the pending_queue
   incomming_message : STRING -- head of the incoming_queue
feature {MAILER_STATE} -- Features that are called back by Mailer_State objects
   set_state(s : MAILER_STATE) is
	 -- select next state
      require valid_state: s /= Void
      do
	 state := s; s.init_state
      end -- set_state
   set_pending_message(m : STRING) is
	 -- register a message to send
      require valid_message: m /= Void
      do
	 pending_queue.add_last(m)
      end -- set_pending_message
   get_next_pending_message is
	 -- extract a message from sending queue and put it in pending_message
      do
	 if pending_queue.is_empty then
	    pending_message := Void
	 else
	    pending_message := pending_queue.first
	    pending_queue.remove_first
	 end -- if
      end -- get_next_pending_message
   set_incomming_message(m : STRING) is
	 -- register a message to send
      require valid_message: m /= Void
      do
	 incoming_queue.add_last(m)
      end -- set_incomming_message
   get_next_incomming_message is
	 -- extract a message from sending queue and put it in incomming_message
      require has_incomming_message: not incoming_queue.is_empty
      do
	 incomming_message := incoming_queue.first
	 incoming_queue.remove_first
      end -- get_next_incomming_message
feature {NONE}
   state : MAILER_STATE
   pending_queue : LINKED_LIST[STRING]
   incoming_queue : LINKED_LIST[STRING]
invariant
   valid_state: state /= Void
end -- MAILER
