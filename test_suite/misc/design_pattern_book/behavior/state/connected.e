indexing
   description: "Mailer state when connection has been successfully established"
   note: " "
   status: "$State$"
   date: "$Date: 2004-07-16 21:25:32 +0200 (Fri, 16 Jul 2004) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
class CONNECTED
inherit
   MAILER_STATE
creation make
feature {ANY}
   send (message : STRING) is
      do
	 print("Effective send while connected%N")
	 send_message(message)	 
      end -- send
   open is
      do
	 print("Already open%N")
      end -- open
   close is
      do
	 print("Trying to close while open: closing%N")
	 mailer.set_state(mailer.closed)
      end -- close
   init_state is
	 -- send pending messages
      do
	 print("Initializing connected state: sending pending messages%N")
	 from mailer.get_next_pending_message until mailer.pending_message = Void
	 loop
	    send_message(mailer.pending_message)
	    mailer.get_next_pending_message
	 end -- loop
      end -- init_state
   notify_connect is
      do
	 print("Connect notification received while connected%N")
      end -- notify_connect
   notify_close is
      do
	 print("Reception of close while open: closing%N")
	 mailer.set_state(mailer.closed)
      end -- notify_close
   notify_received is
      do
	 print("Reception of a message while open %N")
	 crash -- *** DEAD CODE ***
	 -- ***	 mailer.set_incoming_message("A brand new message")
      end -- notify_received
feature {NONE}
   send_message (m : STRING) is
      require valid_message: m /= Void
      do
	 print("Sending message "); print(m); io.put_new_line
      end -- send_message
end -- CONNECTED
