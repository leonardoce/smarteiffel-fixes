indexing
   description: "Mailer initial state"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"

deferred class MAILER_INIT

inherit
   MAILER_STATE

feature {ANY}
   send (m : STRING) is
      do
	 the_mailer.set_pending_message(m)
	 open
	 the_mailer.set_state(the_mailer.connecting)
	 the_mailer.send(m)
      end

   receive: STRING is
      deferred
      end

   notify_connect  is
      deferred
      end

   notify_close  is
      deferred
      end

   notify_received  is
      deferred
      end

feature {NONE}
   open is
      do
	 std_output.put_string("Starting connection%N")
      end

invariant

end -- MAILER_INIT
