indexing
   description: "Constantes for events"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
expanded class EVENT_CONST
feature
   unknown, close, click: INTEGER is unique
end -- EVENT_CONST
