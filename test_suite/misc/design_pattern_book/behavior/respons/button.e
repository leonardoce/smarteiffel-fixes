indexing
   description: "A simple button"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class BUTTON
inherit
   WIDGET
      redefine handle end;
feature {ANY}
  handle (event : EVENT) is
      do
	 if event.is_mouse_click then
	    handled := True
	    print("Button clicked!%N")
	 else
	    default_handle(event)
	 end -- if
      end -- handle
end -- BUTTON
