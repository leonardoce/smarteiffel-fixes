indexing
   description: "Abstract widget class"
   note: "co variance version"
   status: "$State$"
   date: "$Date: 2004-07-18 10:08:21 +0200 (Sun, 18 Jul 2004) $"
   revision: "$Revision$"
   author: "Michel Train"
class WIDGET
inherit
   HANDLER [EVENT]
      rename next_handler as container
      end
feature {ANY}  -- Queries
   container : WIDGET 
	 -- Co-variant redefinition of the responsibility chain link
feature {ANY} -- Commands
   set_container (new_container : WIDGET) is
	 -- Set a new container WIDGET
      require new_container_not_void: new_container /= Void
      do
	 container := new_container
      ensure container = new_container
      end -- set_container
end -- WIDGET
