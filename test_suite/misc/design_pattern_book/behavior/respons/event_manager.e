indexing
   description: "A simple event dispatcher"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class EVENT_MANAGER
creation make
feature -- Creation
   make, set_focus (focus_widget : WIDGET) is
      -- Creation and initialization with 'focus_widget' as the focus
      require focus_widget_not_void: focus_widget /= Void
      do
	 focus := focus_widget
      end -- make, set_focus 
feature {ANY} -- Queries
   focus : WIDGET -- The Widget that currently has the focus
feature {ANY} -- Commands
   dispatch (event : EVENT) is
      -- Ask the current head of the responsability chain to handle the event
      do
	 focus.handle(event)
	 if focus.handled then
	    print("Event dispatched%N")
	 else
	    print("Event discarded%N")
	 end -- if
      end -- dispatch
invariant
   defined_focus: focus /= Void
end --EVENT_MANAGER
