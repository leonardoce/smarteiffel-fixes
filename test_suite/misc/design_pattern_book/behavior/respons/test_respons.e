indexing
   description: "Application of responsability chain"
   note: " "
   status: "$State$"
   date: "$Date: 2004-11-08 10:57:03 +0100 (Mon, 08 Nov 2004) $"
   revision: "$Revision$"
   author: "Michel Train"
class TEST_RESPONS
creation make
feature -- Creation
   make is
	 -- Exercise the 'Responsability Chain' pattern
      local
	 button : BUTTON
	 window : WINDOW
	 manager : EVENT_MANAGER
      do
	 !!button; !!window; window.add(button) -- Creates a Winow containing a Button
	 !!manager.make(button) -- 'button' is supposed to have the focus
	 loop_on_events(manager)
      end -- make
   loop_on_events(event_manager : EVENT_MANAGER) is
	 -- Event loop
      require
	 valid_manager : event_manager /= Void
      do
	 from get_next_event until last_event = Void
	 loop
	    event_manager.dispatch(last_event)
	    get_next_event
	 end -- loop
	 print("Event stream closed%N")
      end --  loop_on_events
   last_event : EVENT
	 -- last EVENT detected
feature {NONE} -- internal routines
   get_next_event is
	 -- Simulates getting a new EVENT from an event stream
      do
	 event_index := event_index + 1
	 if event_code.valid_index(event_index) then
	    !!last_event.make(event_code.item(event_index))
	 else
	    last_event := Void
	 end -- if
      end -- get_next_event
   event_index : INTEGER
	 -- Current rank in the event_code list
   event_code : ARRAY[INTEGER] is
	 -- A constant ARRAY holding 4 event codes: click, until, click, and close
      once
	 Result := {ARRAY[INTEGER] 1, <<ec.click,ec.unknown,ec.click,ec.close>> }
      end -- event_code
   ec : EVENT_CONST
end
	
