indexing
   description: "The window subclass of widget"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class WINDOW
inherit
   WIDGET
      redefine handle end;
feature {ANY}
   add (a_widget : WIDGET) is
      -- Appends a_widget as a direct contained element
      require valid_widget : a_widget /= Void
      do
	 a_widget.set_container(Current)
      end -- add
   handle (event : EVENT) is
      do
	 if event.is_close then
	    handled := True
	    print("Window closed!%N");
	 else
	    default_handle(event)
	 end -- if
      end -- handle
end -- WINDOW
