indexing
   description: "Generic HANDLER: just forward the request to next handler"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
deferred class HANDLER [REQUEST] -- REQUEST is a generic type parameter
feature {ANY}
   default_handle, handle (r : REQUEST) is
	 -- Forwards the request 'r' to the next handler in the chain
      do
	 if next_handler /= Void then
	    next_handler.handle(r)
	    handled := next_handler.handled
	 else
	    handled := False
	 end -- if
      end -- default_handle, handle
   handled : BOOLEAN -- Whether last request has successfully been handled
feature {NONE}
   next_handler : HANDLER [REQUEST] is deferred end
	 -- Implement the responsibility chain link
end -- HANDLER
