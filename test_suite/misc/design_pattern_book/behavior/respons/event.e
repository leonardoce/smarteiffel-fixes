indexing
   description: "The description of a graphical event"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class EVENT
creation make
feature
   make (a_type : INTEGER) is
      -- build the event acording to a_type
      do
	 type := a_type
      end
   type : INTEGER
   is_mouse_click : BOOLEAN is
      -- is this event a mouse click
      do
	 Result := (type = event_const.click)
      end
   is_close : BOOLEAN is
      -- is this event a click
      do
	 Result := (type = event_const.close)
      end
feature {NONE}
   event_const : EVENT_CONST
end -- EVENT
