indexing
   description: "Base class for regular expressions"
   note: "Suppose each token separeted by white space"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class REGULAR_EXPRESSION
   --@@@ action : default behavior is forwarding
   --@@@ handler : the next in chain
feature {ANY}
   parse : BOOLEAN is
      local
	 got : STRING
      do
	 got := token(input)
	 if selector_list.has_key(got) then
	    selector_list.elem(got).parse(before)
	 else
	    before.append(got)
	 end
      end
      end
   token (input : STREAM) : STRING is
	 -- read next token from input stream
      do
	 Result := input.get_word
      end
      end
	
feature {NONE}
   selector_list : MAP[STRING,NTERM]
end -- REGULAR_EXPRESSION
