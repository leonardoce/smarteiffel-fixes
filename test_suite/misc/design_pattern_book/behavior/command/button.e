
indexing
   description: "Button with command list to specify behavior"
   note: "Commands list is managed by base class INVOKER"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class BUTTON
inherit WIDGET
creation set_command
feature {ANY} -- Creation
   set_command (new_command : COMMAND) is
      -- Register a new COMMAND
      require new_command_not_void: new_command /= Void
      do
	 associated_command := new_command
      ensure associated_command = new_command
      end -- set_command
feature {ANY} -- Queries
   associated_command : COMMAND
	 -- Command that must be executed when the BUTTON is pressed
feature {ANY} -- Commands
   activate is
      -- Execute the associated_command 
      do
	 associated_command.execute
      end -- activate
invariant
   associated_command_not_void: associated_command /= Void
end -- BUTTON
