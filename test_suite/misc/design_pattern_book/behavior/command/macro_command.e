indexing
   description: ""
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class MACRO_COMMAND
inherit
   COMPOSITE [COMMAND]
   COMMAND
creation make
feature {ANY} -- Creation
   make is
      -- Creation and initialization
      do
	 !LINKED_LIST[COMMAND]!children.make
      end -- make
feature {ANY} -- Commands
   execute is
	 -- Execute each command of the list in sequence
      local i : INTEGER
      do
	 from i := children.lower until i > children.upper
	 loop
	    children.item(i).execute
	    i := i + 1
	 end -- loop
      end -- execute
end -- MACRO_COMMAND

