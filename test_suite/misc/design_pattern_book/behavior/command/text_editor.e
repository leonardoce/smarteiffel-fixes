indexing
   description: "Text_editor as client example"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class TEXT_EDITOR
feature {ANY}
   save is
      do
         print("Text editor is requested to save its buffer.%N")
      end -- save
   quit is
      do
         print("Bye ...%N")
      end -- quit
end -- TEXT_EDITOR
