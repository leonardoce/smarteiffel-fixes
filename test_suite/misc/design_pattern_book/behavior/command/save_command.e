indexing
   description: "Save : a command subclass"
   note: "Specifies execute feature"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class SAVE_COMMAND
inherit COMMAND
creation make
feature {ANY}
   make (editor : TEXT_EDITOR) is
      -- registers client
      require 
	 editor_not_void : editor /= Void
      do
	 client := editor
      ensure 
	 client_registered : client = editor
      end -- make
   execute is
      -- calls client's save routine
      do
	 client.save
      end -- execute
feature {NONE}
   client : TEXT_EDITOR
invariant
   client_not_void : client /= Void 
end -- SAVE_COMMAND
