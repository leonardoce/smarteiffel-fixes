indexing
   description: "Abstact base command class"
   note: "Defines an execute feature"
   status: "$State$"
   date: "$Date: 2004-09-18 10:13:28 +0200 (Sat, 18 Sep 2004) $"
   revision: "$Revision$"
   author: "Michel Train"
deferred class COMMAND
insert
	ANY
feature {ANY}
   execute is
	 -- Should be defined in subclasses to perform the relevant action
      deferred
      end -- execute
end -- COMMAND
