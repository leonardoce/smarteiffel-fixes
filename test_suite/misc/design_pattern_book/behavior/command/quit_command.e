indexing
   description: "QUIT : a command subclass"
   note: "Specifies execute feature"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class QUIT_COMMAND
inherit COMMAND
creation make
feature {ANY}
   make (editor : TEXT_EDITOR) is
      require 
	 editor_not_void : editor /= Void
      do
	 client := editor
      ensure 
	 has_client : client = editor
      end -- make
   execute is
      -- calls client's QUIT routine
      do
	 client.quit
      end -- execute
feature {NONE}
   client : TEXT_EDITOR
invariant 
   client_not_void : client /= Void
end -- QUIT_COMMAND
