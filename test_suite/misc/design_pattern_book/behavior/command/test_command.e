indexing
   description: "Exemple root class : an interactive application"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class TEST_COMMAND
creation make
feature {ANY}
   make is
      local
	 editor : TEXT_EDITOR
	 save_button, quit_button, save_and_quit_button : BUTTON
	 save_command : SAVE_COMMAND ; quit_command : QUIT_COMMAND
	 save_and_quit_command : MACRO_COMMAND
      do
	 -- initializes application components
	 !!editor -- Create the editor
	 !!save_command.make(editor); !!quit_command.make(editor) -- Create the commands
	 !!save_and_quit_command.make -- Creates the macro-command
	 save_and_quit_command.add(save_command ) -- First command of the macro
	 save_and_quit_command.add(quit_command ) -- Last command of the macro
	 -- now create 3 buttons: SAVE, QUIT and SAVE&QUIT
	 !!save_button.set_command(save_command)
	 !!quit_button.set_command(quit_command)
	 !!save_and_quit_button.set_command(save_and_quit_command)
	 -- Simulate pressing the buttons
	 print("Pressing button SAVE: "); save_button.activate
	 print("Pressing button QUIT: "); quit_button.activate
	 print("Pressing button SAVE&QUIT: "); save_and_quit_button.activate
      end -- make
end
