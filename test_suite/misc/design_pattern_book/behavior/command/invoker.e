indexing
   description: "Base class of command bearers"
   note: "Manages list of commands"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
deferred class INVOKER
   --@@@ action : default behavior is forwarding
   --@@@ handler : the next in chain
feature {ANY}
   add_command (command : COMMAND) is
      require not_void : command /= Void
      do
	 command_list.append(command)
      ensure is_last : command_list.last = command
      end
feature {NONE}
   command_list : LIST[COMMAND]
end -- INVOKER
