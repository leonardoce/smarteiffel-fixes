indexing
   description: "Abstract director base class"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
deferred class DIALOG_DIRECTOR
inherit MEDIATOR[WIDGET]
   rename activate as show_dialog end
feature {ANY} -- Queries
   window : WINDOW
   ok, cancel : BUTTON
   entry_list : LIST_BOX
   selected_entry : ENTRY_FIELD
feature {ANY} -- Commands
   show_dialog is
	 -- Template Method to open a dialog window made of
	 -- a list box, an entry field and 2 buttons labeled "OK" and "CANCEL"
      do
	 !!window.make(Current)
	 !!ok.make(Current); ok.set_label("OK"); window.add(ok)
	 !!cancel.make(Current); cancel.set_label("CANCEL"); window.add(cancel)
	 !!entry_list.make(Current); window.add(entry_list)
	 !!selected_entry.make(Current); window.add(selected_entry)
	 populate_entry_list -- to be customized in subclasses
	 window.open
      end -- show_dialog
feature {NONE} -- Private
   populate_entry_list is
	 -- Fill the entry_list contents.
	 -- Primitive operation of the `show_dialog' Template Method above.
      require entry_list_not_void: entry_list /= Void
      deferred
      end -- populate_entry_list
end -- DIALOG_DIRECTOR

