indexing
   description: "A concrete director"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class FONT_DIALOG_DIRECTOR
inherit
   DIALOG_DIRECTOR
creation make
feature -- Creation
   make (palette : PALETTE) is
	 -- Initialize this Director with palette as its client
      require palette_not_void: palette /= Void
      do
	 client := palette
      end -- make
feature {ANY} -- Commands
   changed (w : WIDGET) is 
	 -- propagates change notification according to the modified widget
      do
	 if w = entry_list then
	    selected_entry.set_value(entry_list.get_selection)
	 elseif w = ok then
	    client.set_font(selected_entry.value)
	    window.close
	 elseif w = cancel then
	    window.close
	 end -- if
      end -- widget_changed
feature {NONE} -- Private
   client : PALETTE -- The client for which this director works
   populate_entry_list is
	 -- Fill the entry_list contents with the client PALETTE contents
      do
	 entry_list.set_list(client.font_names)
      end -- populate_entry_list
end -- FONT_DIALOG_DIRECTOR
