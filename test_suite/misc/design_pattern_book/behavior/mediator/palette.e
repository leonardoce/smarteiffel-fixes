indexing
   description: "A selection box"
   note: ""
   status: "$State$"
   date: "$Date: 2004-11-08 10:57:03 +0100 (Mon, 08 Nov 2004) $"
   revision: "$Revision$"
   author: "Michel Train"
class PALETTE
creation make
feature -- Creation
   make is
      -- initialize possible fonts
      do
	 font_names := {ARRAY[STRING] 1, <<"regular","bold","oblique">> }
      end -- end
feature {ANY} -- Queries
   font_names : ARRAY[STRING] -- The available fonts
   selected_font : STRING -- the currently selected font
feature {ANY} -- Commands
   set_font (a_font : STRING) is
      -- select current font
      require
	 valid_font : a_font /= Void
      do
	 selected_font := a_font
      ensure
	 selected_font = a_font
      end -- set_font
end -- PALETTE
