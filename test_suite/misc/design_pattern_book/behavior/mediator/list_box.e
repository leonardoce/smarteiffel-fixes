indexing
   description: "A selection box"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class LIST_BOX
inherit
   WIDGET
creation make -- inherited from WIDGET
feature {ANY} -- Queries
   available_choices : COLLECTION[STRING] 
	 -- The STRINGs available in the LIST_BOX
   current_choice : INTEGER
	 -- The index of the currently selected STRING in the available_choices
   get_selection : STRING is
	 -- return the selected item if any, or Void if none
      require has_model: available_choices /= Void
      do
	 if available_choices.valid_index(current_choice)  then
	    Result := available_choices.item(current_choice)
	 end -- if
      end -- get_selection
feature {ANY} -- Commands
   set_list (new : like available_choices) is
	 -- Assign list_box content
      require new_not_void: new /= Void
      do
	 available_choices := new
      end -- set_list
   handle_mouse (ev : MOUSE_EVENT) is
	 -- registers mouse position and notifies change
      do
	 print("List Box receives an event%N")
	 current_choice := ev.pos
	 changed
      end -- handle_mouse
end -- LIST_BOX
