indexing
   description: "A button"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class BUTTON
inherit
   WIDGET
creation make -- inherited from WIDGET
feature {ANY} -- Queries
   label : STRING -- The label on the button
feature {ANY} -- Commands
   set_label(new : STRING) is do label := new end -- set_label
   handle_mouse (ev : MOUSE_EVENT) is
	 -- React to mouse event
      do
	 print("Button "); print(label); print(" pressed%N")
	 changed
      end -- handle_mouse
end -- BUTTON
