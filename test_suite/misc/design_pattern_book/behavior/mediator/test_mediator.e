indexing
   description: "Exemple root class : an interactive application"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision: 2 $"
   author: "Michel Train"
class TEST_MEDIATOR
creation make
feature {ANY}
   make is
	 -- initialize application components and simulates the occurrence of 2
	 -- mouse events: one on the entry_list and then one on the OK button.
      local
	 director : FONT_DIALOG_DIRECTOR
	 palette : PALETTE
      do
	 !!palette.make
	 !!director.make(palette)
	 director.show_dialog -- Display the PALETTE window
	 -- simulates the occurrence of an event on the font_list
	 director.entry_list.handle_mouse(new_event(2))
	 -- simulates the occurrence of another event on the OK button
	 director.ok.handle_mouse(new_event(1))
      end -- make
feature {NONE} -- Private
   new_event(code: INTEGER) : MOUSE_EVENT is
	 -- Factory method to create new mouse events
      do
	 !!Result.make(code)
      ensure Result_not_void: Result /= Void
      end -- new_event
end
