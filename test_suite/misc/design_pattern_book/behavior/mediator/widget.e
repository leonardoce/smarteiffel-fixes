indexing
   description: "Abstract widget class, build with its corresponding director"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
deferred class WIDGET
feature {ANY}
   make, make_widget (dir : DIALOG_DIRECTOR) is
      -- registers director
      require dir_not_void: dir /= Void
      do
	 director := dir
      ensure director_set: director = dir
      end -- make
   changed is
      -- calls back director on change notification
      do
	 director.changed(Current)
      end -- changed
   handle_mouse (ev : MOUSE_EVENT) is
      -- reaction to mouse event is to be defined in sub classes
      deferred
      end -- handle_mouse
feature {NONE}
   director : DIALOG_DIRECTOR
invariant
   has_director: director /= Void
end -- WIDGET
