indexing
   description: "A window"
   note: ""
   status: "$State$"
   date: "$Date: 2004-07-15 15:14:53 +0200 (Thu, 15 Jul 2004) $"
   revision: "$Revision$"
   author: "Michel Train"
class WINDOW
inherit
   WIDGET 
      redefine make end
creation make
feature {ANY}
   make (dir : DIALOG_DIRECTOR) is
	 -- initialize child list
      do
	 make_widget(dir)
	 !!items.make -- initialize the list of contained widgets
      end -- make
   handle_mouse (ev : MOUSE_EVENT) is
      	 -- React to mouse event
      do
	 print("Window receives an event%N")
	 curpos := ev.pos
	 changed
      end -- handle_mouse
   add (w : WIDGET) is
	 -- add a child widget to the window
      do
	 items.add_last(w)
      end -- add
   open is
	 -- Simulate opening the window
      do
	 print("Opening window%N")
      end -- open
   close is
	 -- Simulate closing the window
      do
	 print("Closing window%N")
      end -- close
feature {NONE}
   -- *** s_list : LIST[STRING]
   -- *** Because the new compiler now checks all...
   -- *** (Dom. july 15th 2004) ***
   curpos : INTEGER
   items : LINKED_LIST[WIDGET]
invariant
   items_not_void : items /= Void
end -- WINDOW
