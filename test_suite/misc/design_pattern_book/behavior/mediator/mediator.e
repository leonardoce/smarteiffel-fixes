indexing
   description: "Abstract director base class"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class MEDIATOR [COLLEAGUE] -- COLLEAGUE is a generic formal parameter
feature {ANY}
   activate is
	 -- Activate the Mediator's Colleagues
      deferred
      end -- activate
   changed (source : COLLEAGUE) is 
	 -- Take action to inform dependant of the `source' change
      require source_not_void: source /= Void
      deferred
      end -- changed
end -- MEDIATOR
