indexing
   description: "An entry field"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class ENTRY_FIELD
inherit
   WIDGET
creation make
feature {ANY} -- Queries
   value : STRING -- the content of the ENTRY_FIELD
feature {ANY} -- Commands
   set_value (v : STRING) is
      -- Assign `v' to `value'
      do
	 print("The value of this Entry_Field becomes: ")
	 io.put_string(v); io.put_new_line
	 value := v
      end -- set_value
   handle_mouse (ev : MOUSE_EVENT) is
      -- propagates change
      do
	 print("Entry Field receives an event%N")
	 changed
      end -- handle_mouse
end -- ENTRY_FIELD
