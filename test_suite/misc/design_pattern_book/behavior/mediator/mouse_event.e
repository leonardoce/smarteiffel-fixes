indexing
   description: "Description of a user mouse input"
   note: "adapted to our example"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class MOUSE_EVENT
creation
   make
feature
   make (a_pos : INTEGER) is
      -- registers mouse event pos ( should be a POINT in a real case )
      do
	 pos := a_pos
      ensure
	 pos = a_pos
      end
   pos : INTEGER
invariant
   valid_pos : pos > 0
end -- MOUSE_EVENT
