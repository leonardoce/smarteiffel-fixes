indexing
   description: "Computer equipment to handle floppy disks"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class FLOPPY_DISK
inherit EQUIPMENT
creation make -- inherited from EQUIPMENT
feature {ANY} -- Queries
   power : REAL is 10.0
   net_price : REAL is 20.0
   discount_price : REAL is 
	 -- No discount on such a single item
      do
	 Result := net_price
      end -- discount_price
feature {ANY} -- For Visitors
   accept (v : EQUIPMENT_VISITOR) is
	 -- Makes v visit the current piece of equipment.
      do
	 v.visit_floppy_disk (Current)
      end -- accept
end -- FLOPPY_DISK

