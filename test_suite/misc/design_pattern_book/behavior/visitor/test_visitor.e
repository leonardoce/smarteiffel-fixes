indexing
	description:
		"Example of the use of a VISITOR"
	note:
		""
	status:
		""
	date:
		"$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
	revision:
		"$Revision$"
	author:
		"Jean-Marc Jezequel"
class TEST_VISITOR

creation {ANY}
	make

feature {ANY} -- Creation
	make is
			-- Creation and initialization
		local
			e: EQUIPMENT; visitor1: PRICING_VISITOR; visitor2: INVENTORY_VISITOR
		do
			e := new_equipments
			-- Display the various component names in e with an INVENTORY_VISITOR
			io.put_string("Equipment made of: ")
			create visitor2
			e.accept(visitor2)
			-- Compute the price of e with a PRICING_VISITOR
			create visitor1
			e.accept(visitor1)
			io.put_string("Its price is ")
			io.put_real(visitor1.total)
			io.put_new_line
		end

feature {}
	new_equipments: EQUIPMENT is
			-- Factory Method to create pieces of EQUIPMENT
		local
			cabinet: CABINET; chassis: CHASSIS; bus: BUS; e: EQUIPMENT
		do
			create cabinet.make("PC Cabinet")
			create chassis.make_with_list("PC Chassis") -- use a list representation
			cabinet.add(chassis)
			create bus.make("MCA Bus")
			create {CARD} e.make("16Mbs Token Ring")
			bus.add(e)
			chassis.add(bus)
			create {FLOPPY_DISK} e.make("3.5 Floppy")
			chassis.add(e)
			Result := cabinet
		end

end -- class TEST_VISITOR
