indexing
   description: "Interface of a Visitor for pieces of electronic or computer equipment"
   note: "Does nothing. Interesting routine should be redefined"
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class EQUIPMENT_VISITOR
feature {ANY} -- Commands
   visit_bus (b : BUS) is deferred end
   visit_cabinet (c : CABINET) is deferred end
   visit_card (c : CARD) is deferred end
   visit_chassis (c : CHASSIS) is deferred end
   visit_floppy_disk (fd : FLOPPY_DISK) is deferred end
end -- EQUIPMENT_VISITOR

