indexing
   description: "Cabinet for a PC"
   note: ""
   status: ""
   date: "$Date: 2004-11-08 10:57:03 +0100 (Mon, 08 Nov 2004) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class CABINET
inherit COMPOSITE_EQUIPMENT
creation 
   make
feature {ANY} -- For Visitors
   accept (v : EQUIPMENT_VISITOR) is
	 -- Makes v visit the current piece of equipment.
      do
	 v.visit_cabinet (Current)
      end -- accept
end -- CABINET

