indexing
   description: "Visitor to compute the price for pieces of electronic or computer equipment"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class PRICING_VISITOR
inherit EQUIPMENT_VISITOR
feature {ANY} -- Queries
   total : REAL 
	 -- Sum of the net_prices of simple equipments 
feature {ANY} -- Commands
   visit_bus (b : BUS) is do b.visit_children(Current) end
   visit_cabinet (c : CABINET) is do c.visit_children(Current) end
   visit_card (c : CARD) is do  total := total + c.net_price end
   visit_chassis (c : CHASSIS) is do c.visit_children(Current) end
   visit_floppy_disk (fd : FLOPPY_DISK) is do total := total + fd.net_price end
end -- PRICING_VISITOR

