indexing
   description: "Visitor to display the names of pieces of electronic or computer equipment"
   note: ""
   status: ""
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class INVENTORY_VISITOR
inherit EQUIPMENT_VISITOR
feature {ANY} -- Commands
   visit_bus (b : BUS) is
      do
	 io.put_string("%N   - ")
	 b.visit_children(Current)
	 io.put_string("plugged into a "); io.put_string(b.name) 
      end -- visit_bus
   visit_cabinet (c : CABINET) is
      do
	 io.put_string(c.name); io.put_string(" containing ")
	 c.visit_children(Current)
	 io.put_new_line
      end -- visit_cabinet
   visit_card (c : CARD) is do io.put_string(c.name); io.put_string(", ") end
   visit_chassis (c : CHASSIS) is
      do
	 io.put_string("a "); io.put_string(c.name)
	 io.put_string(" made of:")
	 c.visit_children(Current)
      end -- visit_chassis
   visit_floppy_disk (fd : FLOPPY_DISK) is 
      do
	 io.put_string("%N   - "); io.put_string(fd.name)
      end -- visit_floppy_disk
end -- INVENTORY_VISITOR

