indexing
   description: "A computer back-end bus"
   note: ""
   status: ""
   date: "$Date: 2004-11-08 10:57:03 +0100 (Mon, 08 Nov 2004) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class BUS
inherit COMPOSITE_EQUIPMENT
creation 
   make
feature {ANY} -- For Visitors
   accept (v : EQUIPMENT_VISITOR) is
	 -- Makes v visit the current piece of equipment.
      do
	 v.visit_bus (Current)
      end -- accept
end -- BUS

