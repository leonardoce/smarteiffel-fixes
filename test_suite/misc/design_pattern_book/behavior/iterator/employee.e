indexing
   description: "Employee : a concrete item example"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class EMPLOYEE
creation make_from_number
feature -- Creation
   make_from_number (his_number : INTEGER) is
	 -- Create an employee from its his_number
      do
	 number := his_number
      end -- make_from_number
feature {ANY}
   number : INTEGER -- An employee record number
   print_it is
	 -- Print a short description of this employee
      do
	 std_output.put_string("Employee number ")
	 std_output.put_integer(number)
	 std_output.put_new_line
      end -- print_it
end -- EMPLOYEE
