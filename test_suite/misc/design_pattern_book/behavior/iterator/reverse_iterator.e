indexing
   description: "A reverse generic ITERATOR for an indexed COLLECTION"
   note: "Applicable for all descendant of SmartEiffel COLLECTION class"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class REVERSE_ITERATOR[E]
inherit COLLECTION_ITERATOR[E]
      redefine exhausted, is_done, start, first, next end;
creation make
feature {ANY} -- Queries
   exhausted, is_done : BOOLEAN  is
	 -- Whether all elements have been visited
      do
	 Result := index < target_collection.lower
      end -- exhausted, is_done
feature {ANY} -- Commands
   start, first is
	 -- Go to the aggregate first position: start the iteration
      do
	 index := target_collection.upper
      end -- start, first
   next is
	 -- Advance to the next element
      do
	 index := index - 1
      end -- next
end -- REVERSE_ITERATOR

