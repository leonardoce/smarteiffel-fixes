
indexing
   description: "A basic generic list of printable items"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
-- class PRLIST[E -> PRINTABLE]
class PRLIST[E]
   --@@@ count : items count
   --@@@ get : returns the n'th item
feature {ANY}
   count : INTEGER is
      do
	 Result := nb_items;
      end
   get (rank : INTEGER) : like item is
      do
	 -- access implementation
      end
   print_items(iter : LIST_ITERATOR) is
      do
	 from iter.first
	 until iter.is_done
	 loop
	    iter.current_item.print_it
	    iter.next
	 end
      end

feature {NONE}
   nb_items : INTEGER;
end -- PRLIST

