indexing
   description: "Root class of the application"
   note: "Testing purpose"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class TEST_ITERATOR
creation make
feature {ANY}
   make is
	 -- builds context of test and run it
      local
	 forward : COLLECTION_ITERATOR[EMPLOYEE]
	 backward : REVERSE_ITERATOR[EMPLOYEE]
      do
	 build_employees_list(5) -- Makes a list of employees
	 !!forward.make(employees) -- Create a (forward) iterator on this list
	 !!backward.make(employees) -- Create a backward iterator on this list
	 print("From first to last:%N")
	 print_employees(forward)
	 print("From last to first:%N")
	 print_employees(backward)
      end -- make
   print_employees (iter : ITERATOR[EMPLOYEE]) is
	 -- demonstrate the use of iterators
      do
	 from iter.start until iter.is_done
	 loop
	    iter.item.print_it
	    iter.next
	 end -- loop 
      end -- print_employees
feature {NONE}
   employees : LINKED_LIST[EMPLOYEE]
   build_employees_list (nb : INTEGER) is
	 -- build a list with 'nb' members
      local
	 i : INTEGER
	 emp : EMPLOYEE
      do
	 !!employees.make
	 from i := 1 until i > nb
	 loop
	    !!emp.make_from_number(i)
	    employees.add_last(emp)
	    i := i + 1
	 end -- loop 
      end -- build_employees_list
end
