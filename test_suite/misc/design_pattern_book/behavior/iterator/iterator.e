indexing
   description: "Definition of the ITERATOR interface"
   note: "The Intent of the iterator is to provide a way to access the elements of an aggregate object sequentially without exposing its underlying representation."
   status: "$State$"
   date: "$Date: 2004-07-18 10:08:21 +0200 (Sun, 18 Jul 2004) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
deferred class ITERATOR[E]
feature {ANY} -- Queries
   is_done : BOOLEAN  is
	 -- Whether all elements have been visited
      do
			crash -- *** Dead code *** (Dom. july 18th2004) ***
      end -- exhausted, is_done
   item : E is
	 -- The current element in the iteration
      require not_exhausted: not is_done
      deferred
      ensure 
	 -- If not robust, we cannot change the aggregate while traversing it
	 robustness: is_robust or else aggregate_state = old(aggregate_state)
      end -- item
   is_robust : BOOLEAN is 
	 -- Whether we can insert and delete items in the collection
	 -- without interfering with the traversal
      do -- False by default
      end -- is_robust
feature {ANY} -- Commands
   start is
	 -- Go to the aggregate first position: start the iteration
      deferred
      end -- start, first
   next is
	 -- Advance to the next element
      require not_exhausted: not is_done
      deferred
      end -- next
feature {NONE} -- Private
   aggregate_state : INTEGER is
	 -- Encoding of the aggregate abstract state that should not
	 -- change if the Iterator is not robust
      do
			crash -- *** Dead code *** (Dom. july 18th2004) ***
      end -- aggregate_state
end -- ITERATOR

