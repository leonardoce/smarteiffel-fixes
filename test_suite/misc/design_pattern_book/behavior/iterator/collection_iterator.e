indexing
   description: "A generic ITERATOR for an indexed COLLECTION"
   note: "Applicable for all descendant of SmartEiffel COLLECTION class"
   status: "$State$"
   date: "$Date: 2004-07-18 10:08:21 +0200 (Sun, 18 Jul 2004) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class COLLECTION_ITERATOR[E]
inherit ITERATOR[E] redefine aggregate_state, is_done end
creation make
feature -- Creation
   make (target: like target_collection) is
	 -- Creation and initialization
      require target_not_void: target /= Void
      do
	 target_collection := target
      end -- make
feature {ANY} -- Queries
   exhausted, is_done : BOOLEAN  is
	 -- Whether all elements have been visited
      do
	 Result := index > target_collection.upper
      end -- exhausted, is_done
   item : E is
	 -- The current element in the iteration
      do
	 Result := target_collection.item(index)
      end -- item
feature {ANY} -- Commands
   start, first is
	 -- Go to the aggregate first position: start the iteration
      do
	 index := target_collection.lower
      end -- start, first
   next is
	 -- Advance to the next element
      do
	 index := index + 1
      end -- next
feature {NONE}
   index : INTEGER -- The cursor on the target collection
   target_collection : COLLECTION[E] -- The target collection itself
   aggregate_state : INTEGER is
	 -- Encoding of the aggregate abstract state that should not
	 -- change if the Iterator is not robust
      do
	 Result := target_collection.count -- Gross simplification
      end -- aggregate_state
invariant
   target_collection_not_void: target_collection /= Void
end -- COLLECTION_ITERATOR

