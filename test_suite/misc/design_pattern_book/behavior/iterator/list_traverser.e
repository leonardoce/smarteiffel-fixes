indexing
   description: "Internal iterator "
   note: "Uses an external iterator"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
deferred class LIST_TRAVERSER
   --@@@ traverse : applying process to all items
   --@@@ process : treatment to be redefined
feature {ANY}
   make (a_list : LIST[E]) is
      do
	 !!iter.make(a_list);
      end
   traverse : BOLEAN is
      local
	 l_result : BOOLEAN
      do
	 from
	    l_result := True;
	    Result := False;
	    iter.first;
	 until
	    l_result = False or iter.is_done
	 loop
	    l_result := process(iter.current_item)
	    iter.next
	 end
      end

feature {NONE}
   iter : LIST_ITERATOR;
end -- LIST_TRAVERSER

