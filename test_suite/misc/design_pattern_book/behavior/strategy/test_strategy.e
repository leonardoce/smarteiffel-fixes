indexing
   description: "Exemple root class : an interactive application"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
class TEST_STRATEGY
creation make
feature {ANY}
   make is
      local
	 printer : PRINTING_DEVICE
	 display : DRAWING_DEVICE
	 figure : FIGURE
      do
	 !!printer; !!display
	 !!figure.make_default -- Make it a square 
	 figure.set_device(display); figure.draw -- Draw it on the Display
	 figure.move(3,5); figure.draw -- Move it and redraw it
	 figure.set_device(printer); figure.draw -- Now print it
      end -- make
end
