indexing
   description: "A postscript device"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
class PRINTING_DEVICE
inherit
   DEVICE
feature {ANY} -- Commands
   move_to (x,y : INTEGER) is
      do
	 io.put_integer(x); io.put_character(' '); io.put_integer(y)
	 io.put_string(" move%N")
      end -- move_to
   draw_line (p1, p2 : POINT) is
      do
	 io.put_integer(p1.x); io.put_character(' '); io.put_integer(p1.y)
	 io.put_character(' ')
	 io.put_integer(p2.x); io.put_character(' '); io.put_integer(p2.y)
	 io.put_string(" stroke%N")
      end -- draw_line
   draw_point (p1 : POINT) is
      do
	 io.put_integer(p1.x); io.put_character(' '); io.put_integer(p1.y)
	 io.put_string(" stroke%N")
      end -- draw_point
end -- PRINTING_DEVICE
