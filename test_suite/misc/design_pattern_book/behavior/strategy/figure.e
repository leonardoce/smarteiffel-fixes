indexing
   description: "A simple polygon figure"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
class FIGURE
creation make_default, make
feature {ANY} -- Creation
   make_default is
	 -- build a default square
      do
	 !!points.make
	 points.add_last(new_point(1,1))
	 points.add_last(new_point(1,5))
	 points.add_last(new_point(5,1))
	 points.add_last(new_point(5,5))
      end -- make_default
   make (pts : COLLECTION[POINT]) is
	 -- make figure from COLLECTION of points 
      require pts_not_void: pts /= Void
      local index : INTEGER
      do
	 !!points.make
	 from index := pts.lower until index > pts.upper
	 loop
	    points.add_last(pts.item(index))
	    index := index + 1
	 end -- loop
      end -- make
feature {ANY} -- Queries
   device : DEVICE -- Where the figure must be drawn
feature {ANY} -- Commands
   set_device (d : DEVICE) is
	 -- define current figure device
      require device_not_void : d /= Void
      do
	 device := d
      ensure
	 device = d
      end -- set_device
   move (dx, dy : INTEGER) is
	 -- translate coordinates from vector
      local index : INTEGER
      do
	 from index := points.lower
	 until index > points.upper
	 loop
	    points.item(index).move(dx,dy)
	    index := index + 1
	 end -- loop
      end -- move
   draw is
	 -- produce output suitable to describe figure to current device
      require device_set: device /= Void
      local
	 index : INTEGER
	 p : POINT
      do
	 if points.count > 1 then
	    p := points.item(points.lower)
	    from index := points.lower + 1
	    until index > points.upper
	    loop
	       device.draw_line(p,points.item(index))
	       p := points.item(index)
	       index := index + 1
	    end -- loop
	    device.draw_line(p,points.item(points.lower))
	 elseif points.count = 1 then
	    device.draw_point(points.item(points.lower))
	 end -- if
      end -- draw		       
feature {NONE} -- Private
   new_point (x,y : INTEGER) : POINT is
	 -- Factory method to create a new POINT
      do
	 !!Result.make(x,y)
      ensure created: Result /= Void
      end -- new_point
   points : LINKED_LIST[POINT]
invariant
   initialized : points /= Void
end -- FIGURE
