indexing
   description: "A point"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class POINT
   --@@@ move : translate point from vector
creation
   make_default, make

feature {ANY}
   x : INTEGER
   y : INTEGER

   make_default is
      do
	 x := 1
	 y := 1
      end
   make (ax , ay : INTEGER) is
      do
	 x := ax
	 y := ay
      end

   move (dx , dy : INTEGER) is
      do
	 x := x + dx
	 y := y + dy
      end
end -- POINT
