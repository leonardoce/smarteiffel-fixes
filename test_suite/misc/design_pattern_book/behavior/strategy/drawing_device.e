indexing
   description: "A display"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
class DRAWING_DEVICE
inherit
   DEVICE
feature {ANY} -- Commands
   move_to (x,y : INTEGER) is
      do
	 io.put_string("Moving to ")
	 io.put_integer(x); io.put_character(' '); io.put_integer(y)
	 io.put_new_line
      end -- move_to
   draw_line (p1, p2 : POINT) is
      do
	 io.put_string("Drawing a line from ")
	 io.put_integer(p1.x); io.put_character(' '); io.put_integer(p1.y)
	 io.put_string(" to ")
	 io.put_integer(p2.x); io.put_character(' '); io.put_integer(p2.y)
	 io.put_new_line
      end -- draw_line
   draw_point (p1 : POINT) is
      do
	 io.put_string("Drawing a point at ")
	 io.put_integer(p1.x); io.put_character(' '); io.put_integer(p1.y)
	 io.put_new_line
      end -- draw_point
end -- DRAWING_DEVICE
