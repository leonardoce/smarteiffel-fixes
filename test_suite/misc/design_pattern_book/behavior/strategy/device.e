indexing
   description: "AN abstract device class providing drawing primitives"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
deferred class DEVICE
feature {ANY}
   move_to (x,y : INTEGER) is
	 -- set current position
      deferred
      end -- move_to
   draw_line (p1, p2 : POINT) is
	 -- emit commands to draw a line from p1 to p2
      deferred
      end -- draw_line
   draw_point (p1 : POINT) is
	 -- emit commands to draw a point
      deferred
      end -- draw_point
end -- DEVICE
