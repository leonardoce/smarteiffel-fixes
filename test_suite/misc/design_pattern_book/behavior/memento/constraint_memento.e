indexing
   description: "A memento class dedicated to CONSTRAINT_SOLVER"
   note: ""
   status: "$State"
   date: "$Date: 2004-11-08 10:57:03 +0100 (Mon, 08 Nov 2004) $"
   revision: "$Revision$"
   author: "Michel Train"
class CONSTRAINT_MEMENTO
inherit MEMENTO
creation make
feature {MEMORIZABLE, CONSTRAINT_SOLVER}
   make (cs : CONSTRAINT_SOLVER) is
	 -- register `cs' state variables
      local i, top : INTEGER
      do
	 !!graphics.make; !!positions.make
	 from i := cs.constraint_list.lower; top := cs.constraint_list.upper
	 until i > top
	 loop
	    append_constraint(cs.constraint_list.item(i))
	    i := i + 1
	 end -- loop
      end -- make
   graphics : LINKED_LIST[GRAPHIC]
   positions : LINKED_LIST[POINT]
feature {NONE} 
   append_constraint (co : CONSTRAINT) is
      require valid_constraint: co /= Void
      do
	 append_graphic(co.first)
	 append_graphic(co.second)
      end -- append_constraint
   append_graphic (g : GRAPHIC) is
      require valid_graphic: g /= Void
      local point : POINT
      do
	 if not graphics.fast_has(g) then
	    graphics.add_last(g)
	    !!point.make(g.pos.x, g.pos.y)
	    positions.add_last(point)
	 end -- if
      ensure graphics.fast_has(g)
      end -- append_graphic
invariant
   graphics_initialized: graphics /= Void
   positions_initialized: positions /= Void
end -- CONSTRAINT_MEMENTO
