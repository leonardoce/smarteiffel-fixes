indexing
   description: "A constraint solver class example. Computes links according to graphic positions"
   note: ""
   status: "$State$"
   date: "$Date: 2004-07-16 09:45:03 +0200 (Fri, 16 Jul 2004) $"
   revision: "$Revision$"
   author: "Michel Train"
class CONSTRAINT_SOLVER
inherit
   MEMORIZABLE [CONSTRAINT_MEMENTO]
creation make
feature {ANY}
   make is
	 -- Creation and initialization
      do
	 !!constraint_list.make
      end -- make
   solve (target : GRAPHIC) is
	 -- naive implementation of contraint solving
      local
	 i, up : INTEGER
	 constraint : CONSTRAINT
      do
	 print("Solving constraints%N")
	 from i := constraint_list.lower; up := constraint_list.upper until i > up
	 loop
	    constraint := constraint_list.item(i)
	    i := i + 1
	    if constraint.is_about(target) then
	       constraint.repairs_from(target)
	    end -- if
	 end -- loop
      end -- solve
   add_constraint (g1, g2 : GRAPHIC) is
	 -- defines a new constraint
      require
	 valid_origin : g1 /= Void
	 valid_extremity : g2 /= Void
      local
	 constraint : CONSTRAINT
      do
	 !!constraint.make(g1,g2)
	 constraint_list.add_last(constraint)
      end -- add_constraint
   remove_constraint(g1, g2 : GRAPHIC) is
      local
	 constraint : CONSTRAINT
	 index, up : INTEGER
      do
	 from index := constraint_list.lower; up := constraint_list.upper until index > up
	 loop
		 crash -- *** Dead code ? (Dom. july 16th 2004) ***
	    -- *** constraint := constraint_list(index)
	    if constraint.refers_to(g1,g2) then
			 crash -- *** Dead code ? (Dom. july 16th 2004) ***
			 -- ***	       constraint_list.remove(constraint)
			 
	    end -- if
	    index := index + 1
	 end -- loop
      end -- remove_constraint
   new_memento : CONSTRAINT_MEMENTO is 
	 -- Factory Method to produce a snapshot of the Current state
      do
	 !!Result.make(Current)
      end -- new_memento
   set_from_memento (m : like new_memento) is
	 -- Return to the state described in the MEMENTO `m'
      local i, top : INTEGER
      do
	 from i := m.graphics.lower; top := m.graphics.upper
	 variant increasing_i: top - i
	 until i > top
	 loop
	    m.graphics.item(i).move_to(m.positions.item(i))
	    i := i + 1
	 end -- loop
      end -- set_from_memento
feature {CONSTRAINT_MEMENTO} -- Only a CONSTRAINT_MEMENTO can access to the following
   constraint_list : LINKED_LIST[CONSTRAINT]
      end -- CONSTRAINT_SOLVER
