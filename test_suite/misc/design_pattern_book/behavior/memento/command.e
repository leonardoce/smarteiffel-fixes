
indexing
   description: "Abstact base command class"
   note: "Defines an execute feature"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
deferred class COMMAND
   --@@@ execute : redefined in subclasses to perform action
feature {ANY}
   execute is
	  deferred
	  end
   unexecute is
	  deferred
	  end
end -- COMMAND
