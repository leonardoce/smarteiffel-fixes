indexing
   description: "Any graphic class"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class GRAPHIC
creation make
feature {ANY}
   make is
      -- Creation and initialization
      do
	 !!pos.make(0,0)
      end -- make
feature {ANY} -- Queries
   pos : POINT -- Current position
feature {ANY} -- Commands
   move (delta : POINT) is
      -- simulate a move specified by a vector
      require delta_not_void: delta /= Void
      do
	 pos.translater(delta.x,delta.y)
      end -- move
   move_to (new_pos : POINT) is
      -- simulate a move specified by a new position
      require new_pos_not_void: new_pos /= Void
      do
	 pos.copy(new_pos)
      end -- move_to
   print_position(name : STRING) is
      require name_not_void: name /= Void
      do
	 print("Position of "); print(name)
	 print(" is: x =  "); io.put_integer(pos.x)
	 print(" y = "); io.put_integer(pos.y); io.put_new_line
      end -- print_position
end -- GRAPHIC
