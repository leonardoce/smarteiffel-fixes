indexing
   description: "Deferred class defining memento protocol"
   note: "Visibility of interface should be set in sub classes"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision"
   author: "Michel Train"
deferred class MEMORIZABLE [T->MEMENTO]
   -- The generic formal parameter T is contrained to conform to MEMENTO
feature {ANY}
   new_memento : T is
	 -- Factory Method to produce a snapshot of the Current state
      deferred
      ensure Result_not_void: Result /= Void
      end -- new_memento
   set_from_memento (m : like new_memento) is
	 -- Return to the state described in the MEMENTO `m'
      require m_not_void: m /= Void
      deferred
      end -- set_from_memento
end -- MEMORIZABLE
