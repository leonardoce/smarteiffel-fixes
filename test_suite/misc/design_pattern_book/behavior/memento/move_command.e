indexing
   description: "A moving command class"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class MOVE_COMMAND
inherit COMMAND
creation make
feature {ANY}
   make (target_graphic : GRAPHIC; delta : POINT; cs : CONSTRAINT_SOLVER) is
	 -- Creation and initialization
      require target_graphic_not_void: target_graphic /= Void
      do
	 target := target_graphic
	 vector := delta
	 constraint_solver := cs
      ensure
	 target_not_void: target /= Void
	 constraint_solver_not_void: constraint_solver /= Void
      end -- make
   execute is
	 -- Make this command execute itself
      do
	 print("Now moving...%N")
	 -- First, save the CONSTRAINT_SOLVER state
	 saved_state := constraint_solver.new_memento
	 -- Actually move
	 target.move(vector)
	 -- And finally, solve the constraints
	 constraint_solver.solve(target)
      end -- execute
   unexecute is
	 -- Undo previous command execution
      do
	 print("Undoing last move%N")
	 constraint_solver.set_from_memento(saved_state) -- Restore to previous state
      end -- unexecute
feature {NONE}
   target : GRAPHIC
   vector : POINT
   saved_state : CONSTRAINT_MEMENTO
   constraint_solver : CONSTRAINT_SOLVER
      end -- MOVE_COMMAND
