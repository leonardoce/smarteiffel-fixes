indexing
   description: "an element of NxN"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class POINT
creation
   make
feature{ANY}
   x, y : INTEGER;
   make (cx : INTEGER; cy : INTEGER) is
      do
	 x := cx; 
	 y := cy;
      end; -- make
   translater(dx, dy : INTEGER) is
      do
	 x := x + dx;
	 y := y + dy;
      end;  -- translater
end -- POINT
