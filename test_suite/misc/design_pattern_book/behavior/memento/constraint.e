indexing
   description: "A constraint class example defined by two graphics"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class CONSTRAINT
creation make
feature
   make (g1, g2 : GRAPHIC) is
	 -- Creates a constraint between the two graphics
      require
	 valid_origin: g1 /= Void
	 valid_extremity: g2 /= Void     
      do
	 first := g1; second := g2
	 dx := g2.pos.x - g1.pos.x; dy := g2.pos.y - g1.pos.y
      end -- make
feature {ANY} -- Queries
   first, second : GRAPHIC -- The two GRAPHICs linked by the constraint
   refers_to (g1, g2 : GRAPHIC) : BOOLEAN is
	 -- is constraint between g1 and g2 ?
      do
	 Result := (((g1 = first) and (g2 = second)) or ((g2 = first) and (g1 = second)))
      end -- refers_to
   is_about (g1 : GRAPHIC) : BOOLEAN is
	 -- is g1 part of this constraint ?
      do
	 Result := ((g1 = first) or (g1 = second))
      end -- is_about
   satisfied : BOOLEAN  is
	 -- Whether the constraint is satisfied or not
      do
	 Result := ((second.pos.x - first.pos.x) = dx)  and
	    ((second.pos.y - first.pos.y) = dy)
      end -- satisfied
feature {ANY} -- Commands
   repairs_from (g1 : GRAPHIC)  is
	 -- move other part to satisfy constraint
      do
	 if g1 = first then repair_from_origin else repair_from_end end -- if
      end -- repair_from
feature {NONE} -- Private
   repair_from_origin is
	 -- move second to satisfy constraint
      local destination_point : POINT
      do
	 !!destination_point.make(first.pos.x + dx, first.pos.y + dy)
	 second.move_to(destination_point)
      end -- repair_from_origin
   repair_from_end is
	 -- moves first to satisfy constraint
      local detination_point : POINT
      do
	 !!detination_point.make(second.pos.x - dx, second.pos.y - dy)
	 first.move(detination_point)
      end -- repair_from_end
   dx, dy : INTEGER
end -- CONSTRAINT

