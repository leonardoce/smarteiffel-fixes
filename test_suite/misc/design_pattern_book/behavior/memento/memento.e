indexing
   description: "Abstract memento class"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
deferred class MEMENTO
feature {MEMORIZABLE} -- Only MEMORIZABLE subclasses can create & access MEMENTOs
   make (m : MEMORIZABLE [like Current]) is
	 -- Creation and initialization of a Memento of `m' state
      require m_not_void: m /= Void
      deferred
      end -- make
end -- MEMENTO
