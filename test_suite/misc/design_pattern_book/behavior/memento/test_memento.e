indexing
   description: "Exemple root class : an interactive application"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class TEST_MEMENTO
creation make
feature {ANY} -- Creation
   make is
      local
	 cs : CONSTRAINT_SOLVER
	 move : MOVE_COMMAND
	 p0, p1 : POINT
      do
	 !!t1.make; !!t2.make -- Make the graphics at 0,0
	 !!p0.make(1,2); t2.move(p0) -- Move t2 to 1,2
	 !!cs.make; cs.add_constraint(t1,t2) -- Set up the constraints
	 !!p1.make(10,10) -- Initialize a moving vector
	 !!move.make(t1,p1,cs) -- Make the MOVE command
	 print("Position before moving%N"); print_position
	 move.execute
	 print("Position after moving%N"); print_position
	 move.unexecute
	 print("Position after undoing%N"); print_position
      end -- make
feature {ANY} -- Queries
   t1, t2 : GRAPHIC
feature {ANY} -- Commands
   print_position is
	 -- Print position of both GRAPHICs
      do
	 t1.print_position("t1"); t2.print_position("t2")
      end -- print_position
end
