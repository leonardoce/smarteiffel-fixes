indexing
   description: "A transition description in the automaton"
   note: " "
   status: "$State$"
   date: "$Date: 2004-07-18 10:08:21 +0200 (Sun, 18 Jul 2004) $"
   revision: "$Revision$"
   author: "Michel Train"
class TRANSITION
creation make
feature {ANY} -- Creation
   make (m : STRING; next_state : STATE) is
	 -- Creation and Initialization
      require next_state_not_void: next_state /= Void
      do
	 next := next_state; trigger := m
	 !!actions.make; !!guards.make
      ensure
	 has_next: next = next_state
	 has_trigger: trigger = m
      end -- make
feature {ANY} -- Public Queries
   next : STATE
   trigger : STRING
   actions : LINKED_LIST[ACTION]
   guards : LINKED_LIST[GUARD]
feature {ANY} -- Public Commands
   add_guard (g : GUARD) is
      require valid_guard: g /= Void
      do
	 guards.add_last(g)
      end -- add_guard
   remove_guard (g : GUARD) is
      require valid_guard: g /= Void
      do
			crash -- *** dead code *** (Dom. july 18th 2004) ***
			-- *** guards.remove(g)
      end -- remove_guard
   add_action (a : ACTION) is
      require valid_action: a /= Void
      do
	actions.add_last(a) 
      end -- add_action
   remove_action (a : ACTION) is
      require valid_action: a /= Void
      do
			crash -- *** dead code *** (Dom. july 18th 2004) ***
			-- *** actions.remove(a) 
      end -- remove_action
   fire (a_session : SESSION) is
	 -- Execute the set of actions associated to this transition
      local
	 index : INTEGER
      do
	 from index := actions.lower until index > actions.upper
	 loop
	    actions.item(index).execute(a_session)
	    index := index + 1
	 end -- loop
      end -- fire
   passes (a_session : SESSION) : BOOLEAN is
	 -- Whether all uards are evaluated to True
      local
	 index : INTEGER
      do
	 print("Testing transition guards%N")
	 from Result := True; index := guards.lower
	 until not Result  or index > guards.upper
	 loop
	    Result := Result and guards.item(index).passes(a_session)
	    index := index + 1
	 end -- loop
	 print("Result = "); io.put_boolean(Result); io.put_new_line
      end -- passes
end -- TRANSITION
