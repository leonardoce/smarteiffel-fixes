indexing
   description: "Exemple root class : an interactive application"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
class TEST_STATE2
creation make
feature {ANY}
   make is
      -- Creation and initialization
      do
	 !!net.make;  !!mailer.make(net)
	 simulate_a_session
      end -- make
feature {NONE}
   mailer : MAILER
   net : NETWORK
   simulate_a_session is
	 -- issues various commands to the mailer and the network
	 -- to simulate a session
      do
	 mailer.send("Hello world") -- Sending while the mailer is not connected
	 net.notify_connect
	 mailer.receive_next_message
	 if mailer.last_message /= Void then
	    print("Reception of : "); print(mailer.last_message); io.put_new_line
	 else
	    print("Sorry, no message for you%N")
	 end -- if
	 mailer.send("Second message") -- Sending while the mailer is connected
	 mailer.close
      end -- simulate_a_session
invariant
   mailer_not_void: mailer /= Void
   net_not_void: net /= Void
end
