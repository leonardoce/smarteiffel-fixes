indexing
   description: "A simple class that uses Mailer"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"

class PROCESSOR

creation
   make
feature
   make (m : MAILER) is
      require
	 mailer_not_void : m /= Void
      do
	 mailer := m
      ensure
	 valid_mailer : mailer = m
      end

   run is
      local
	 mess : STRING
      do
	 mailer.send("First message")
	 mess := mailer.receive
	 if mess = "" then
	    mailer.send("No news good news")
	 end
      end
	 
feature {NONE}
   mailer : MAILER
invariant
   valid_mailer: mailer /= Void
end -- PROCESSOR
