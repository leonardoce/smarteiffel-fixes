indexing
   description: "A communication provider using abstract states"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class MAILER
inherit NETWORK_CLIENT
creation make
feature {ANY} -- Creation
   make (net : NETWORK) is
	 -- Creation and Initialization
      require
	 valid_network : net /= Void
      do 
	 -- Creates incoming and outgoing message queues
	 !!incoming_queue.make; !!pending_queue.make
	 !!com.make(net,Current); !!session.make(configure_automaton)
      end -- make
feature {ANY} -- Public Queries
   last_message : STRING -- Last message received
feature {ANY} -- Public Commands
   send (m : STRING) is
	 -- try to forward the message `m' to the network
      require valid_message: m /= Void
      do
	 pending_queue.add_last(m); session.fire("send")
      end -- send
   receive_next_message is
	 -- Try to get an incoming message
      do
	 if incoming_queue.is_empty then
	    last_message := Void
	    session.fire("receive")
	 else
	    last_message := incoming_queue.first
	    incoming_queue.remove_first
	 end -- if
      end -- receive_next_message
   open is do session.fire("open") end -- open
   close is do session.fire("close") end -- close
   com : CONNECTION
feature {NETWORK} -- Only a NETWORK object may call these features
   notify_connect is do session.fire("notify_connect") end -- notify_connect
   notify_close is do session.fire("notify_close") end -- notify_close
feature {ACTION}
   pending_queue : LINKED_LIST[STRING]
   incoming_queue : LINKED_LIST[STRING]
feature {NONE}
   session : SESSION
   configure_automaton : STATE is
	 -- Configure the states, guards and transitions of the automaton
      local
	 idle_state, connecting, connected : STATE
	 flush : FLUSH_ACTION
	 read : READ_ACTION
	 open_net : OPEN_ACTION
	 query_open : QUERY_OPEN
	 close_net : CLOSE_ACTION
	 query_close : QUERY_CLOSE
	 tr1, tr2, tr3, tr4, tr5, tr6, tr7, tr8 : TRANSITION
      do
	 -- first create the states
         !!idle_state.make("idle"); !!connecting.make("connecting")
	 !!connected.make("connected")
	 -- selection of initial state
	 Result := idle_state

	 -- Then create the actions
	 !!flush.make(Current); !!read.make(Current)
	 !!open_net.make(Current); !!close_net.make(Current)
	 !!query_open.make(Current); !!query_close.make(Current)
	 -- And finally create transitions
	 -- NB: transitions to current state without action are only declared for clarity

	 -- Transitions starting from the state: idle_state
	 !!tr1.make("send",idle_state); idle_state.add_transition(tr1)
	 tr1.add_action(query_open)
	 !!tr2.make("receive",idle_state); idle_state.add_transition(tr2)
	 tr2.add_action(query_open)
	 !!tr3.make("open",connecting); idle_state.add_transition(tr3)
	 tr3.add_action(open_net)

	 -- Transitions starting from the state: connecting
	 !!tr4.make("notify_connect",connected)
	 tr4.add_action(flush); connecting.add_transition(tr4)

	 -- Transitions starting from the state: connected 
	 !!tr5.make("send",connected); tr5.add_action(flush)
	 connected.add_transition(tr5)
	 !!tr6.make("receive",connected)
	 tr6.add_action(read); connected.add_transition(tr6)
	 !!tr7.make("notify_close",idle_state); connected.add_transition(tr7)
	 !!tr8.make("close",idle_state); connected.add_transition(tr8)
	 tr8.add_action(close_net)
      end -- configure_automaton
end -- MAILER
