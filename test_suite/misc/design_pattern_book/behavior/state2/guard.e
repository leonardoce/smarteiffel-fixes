indexing
   description: "Abstract class used to associate a condition to "
   note: "This class is an example of pattern command as action"
   status: "$State$"
   date: "$Date: 2005-09-21 16:11:13 +0200 (Wed, 21 Sep 2005) $"
   revision: "$Revision$"
   author: "Michel Train"

deferred class GUARD

feature
   passes ( a_session : SESSION) : BOOLEAN is 
      do
	 Result := True
      end

end -- GUARD
