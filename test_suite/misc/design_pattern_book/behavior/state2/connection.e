indexing
   description: "A fictive communication channel"
   note: " "
   status: "$State$"
   date: "$Date: 2005-09-21 16:11:13 +0200 (Wed, 21 Sep 2005) $"
   revision: "$Revision$"
   author: "Michel Train"

class CONNECTION

creation
   make
feature
   make (net : NETWORK; a_client : NETWORK_CLIENT) is
      -- declares client to network
      require
	 valid_network : net /= Void
	 valid_client: a_client /= Void
      do
	 media := net;
	 client := a_client;
      end
	 
   send (m : STRING) is
      do
	 std_output.put_string("sending ")
	 std_output.put_string(m)
	 std_output.put_string("%N")
      end

   read : STRING is
      do
	 !!Result.copy("A message from network")
      end

   open is
      do
	 media.connect(client);
	 opened := True
      end

   close is
      do
	 media.disconnect(client);
	 opened := False
      end
   

   opened : BOOLEAN
   closed : BOOLEAN is
      do
	 Result := not opened
      end

feature {NONE}
   media : NETWORK
   client : NETWORK_CLIENT
invariant
   valid_media : media /= Void
   valid_client : client /= Void
end -- CONNECTION
