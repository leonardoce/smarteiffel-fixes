indexing
   description: "Simulates a network connection"
   note: " "
   status: "$State$"
   date: "$Date: 2005-12-20 15:46:40 +0100 (Tue, 20 Dec 2005) $"
   revision: "$Revision$"
   author: "Michel Train"
class NETWORK
creation make
feature
   make is
      -- initialize client list
      do
	 !!client_list.make
      end
   connect (nc : NETWORK_CLIENT) is
      -- register the object attached to the connection
      require
	 valid_client : nc /= Void
      do
	 client_list.add_last(nc)
      end
   disconnect (nc : NETWORK_CLIENT) is
      -- unregister the object attached to the connection
      require
	 valid_client : nc /= Void
      do
	 if client_list.has(nc) then
	    client_list.remove(client_list.first_index_of(nc))
	 end -- if
      end
   notify_connect is
      -- simulates connection notification from peer
      local
	 i : INTEGER
      do
	 from i := client_list.lower until i > client_list.upper
	 loop
	    client_list.item(i).notify_connect
	    i := i + 1
	 end
      end
   notify_close is
      -- simulates close notification from peer
      local
	 i : INTEGER
      do
	 from i := client_list.lower until i > client_list.upper
	 loop
	    client_list.item(i).notify_close
	    i := i + 1
	 end
      end
   client_list : LINKED_LIST[NETWORK_CLIENT]
end -- NETWORK
