indexing
   description: "Concrete service class described by an automaton"
   note: "Carries the description of automaton and provides action support"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"

class SERVICE

creation
   make

feature
   make is
      do
      end

   
invariant

end -- SERVICE
