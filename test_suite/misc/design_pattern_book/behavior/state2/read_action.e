indexing
   description: "Getting input from network"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class READ_ACTION
inherit
   ACTION
creation make
feature
   make (m : MAILER) is
      do
	 mailer := m
      end
   execute (a_session : SESSION) is 
      do
	 mailer.incoming_queue.add_last(mailer.com.read)
      end
feature {NONE}
   mailer : MAILER
end -- READ_ACTION
