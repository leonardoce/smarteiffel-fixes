indexing
   description: "Asks the mailer to open network connection"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class QUERY_OPEN
inherit ACTION
creation make
feature {ANY} -- Creation
   make (m : MAILER) is
	 -- Creation and Initialization
      do
	 mailer := m
      end -- make
   execute (a_session : SESSION) is
	 -- ask the mailer to open
      do
	 mailer.open
      end -- execute
feature {NONE}
   mailer : MAILER
end -- QUERY_OPEN
