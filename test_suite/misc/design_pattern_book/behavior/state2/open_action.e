indexing
   description: "Open network connection"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class OPEN_ACTION
inherit ACTION
creation make
feature {ANY} -- Creation
   make (m : MAILER) is
	 -- Creation and Initialization
      do
	 mailer := m
      end -- make
   execute (a_session : SESSION) is
	 -- execute an 'open' action
      do
	 mailer.com.open
      end -- execute
feature {NONE}
   mailer : MAILER
end -- OPEN_ACTION
