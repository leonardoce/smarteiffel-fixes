indexing
   description: "Asks the mailer to close communication"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"

class QUERY_CLOSE
inherit
   ACTION
creation make
feature
   make (m : MAILER) is
      do
	 mailer := m
      end
   execute (a_session : SESSION) is 
      do
	 mailer.close
      end
feature {NONE}
   mailer : MAILER
end -- QUERY_CLOSE
