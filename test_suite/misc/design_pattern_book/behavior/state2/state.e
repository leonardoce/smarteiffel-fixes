indexing
   description: "An abstract state"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
class STATE
creation make
feature {ANY} -- Creation
   make (a_name : STRING) is
	 -- initialize state with a name
      require valid_name: a_name /= Void
      do
	 !!transitions.make
	 name := a_name
      end -- make
feature {ANY} -- Public Queries
   name : STRING
   firable (mess : STRING; a_session : SESSION) : LINKED_LIST[TRANSITION] is
	 -- returns list of firable transitions
      require valid_message: mess /= Void
      local
	 trans : TRANSITION
	 i : INTEGER
      do
	 !!Result.make
	 from i := transitions.lower until i > transitions.upper
	 loop
	    trans := transitions.item(i)
	    print("    State: testing transition # "); io.put_integer(i)
	    print(" with mess =  "); print(trans.trigger); io.put_new_line
	    if trans.trigger.is_equal(mess) and then trans.passes(a_session) then
	       Result.add_last(trans)
	    end -- if
	    i := i + 1
	 end -- loop
      end -- firable
feature {ANY} -- Public Commands
   add_transition (tr : TRANSITION) is
	 -- add a new transition from this state
      require valid_transition: tr /= Void
      do
	 transitions.add_last(tr)
      end -- add_transition
feature {NONE}
   transitions : LINKED_LIST[TRANSITION]
end -- STATE
