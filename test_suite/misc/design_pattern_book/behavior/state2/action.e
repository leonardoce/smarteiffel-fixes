indexing
   description: "An abstract external action associated to a transition"
   note: "This class is an example of the command pattern"
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"

deferred class ACTION

feature
   execute (a_session : SESSION) is 
      deferred
      end
	 
invariant

end -- ACTION
