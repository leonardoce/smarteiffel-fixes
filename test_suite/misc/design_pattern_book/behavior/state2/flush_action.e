indexing
   description: "Action of sending to the network pending messages"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"

class FLUSH_ACTION
inherit
   ACTION
creation make
feature
   make (m : MAILER) is
      do
	 mailer := m
      end
   execute (a_session : SESSION) is 
      local
	 queue : LINKED_LIST[STRING]
	 com : CONNECTION
	 index : INTEGER
      do
	 queue := mailer.pending_queue
	 com := mailer.com
	 from index := queue.lower
	 until index > queue.upper
	 loop
	    com.send(queue.item(index))
	    queue.remove(index)
	    index := index + 1
	 end
      end


feature {NONE}
   mailer : MAILER

end -- FLUSH_ACTION
