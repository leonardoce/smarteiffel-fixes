indexing
   description: "Abstract allowing to be notified from a network"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
class NETWORK_CLIENT
feature {NETWORK} -- Only a NETWORK object may call these features
   notify_connect is deferred end -- notify_connect
   notify_close is deferred end -- notify_close
end -- NETWORK_CLIENT
