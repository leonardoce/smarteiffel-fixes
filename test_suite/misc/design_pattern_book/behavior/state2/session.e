indexing
   description: "A mailer session, that is, an execution of the automaton"
   note: "Session is responsible for keeping track of current state."
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
class SESSION
creation make
feature {ANY} -- Creation
   make (st : STATE) is
	 -- Creation and Initialization
      require 
	 valid_initial_state: st /= Void
      do
	 current_state := st
      end -- make
feature {ANY} -- Public Queries: the execution context
   current_state : STATE
feature {ANY} -- Public Commands
   fire (mess : STRING) is
	 -- fires the transition triggered by mess
      require
	 valid_message : mess /= Void
      local
	 possible_transitions : LINKED_LIST[TRANSITION]
	 selected_transition : TRANSITION
      do
	 print("Reception of "); print(mess)
	 print(" for state: "); print(current_state.name); io.put_new_line
	 possible_transitions := current_state.firable(mess,Current)
	 print("  Possible transitions: ")
	 io.put_integer(possible_transitions.count); io.put_new_line
	 if possible_transitions.count > 0 then
	    selected_transition := select_transition(possible_transitions)
	    if (selected_transition /= Void) then
	       print("triggering actions and moving to state: ")
	       io.put_string(selected_transition.next.name); io.put_new_line
	       current_state := selected_transition.next
	       selected_transition.fire(Current)
	    end -- if
	 end -- if
      end -- fire
feature {NONE} -- internal routines
   select_transition (possible : LINKED_LIST[TRANSITION]) : TRANSITION is
	 -- select one of the firable transitions
      do
	 Result := possible.first -- here simply take the first one
      end -- select_transition
invariant
   current_state_defined: current_state /= Void
end -- SESSION
