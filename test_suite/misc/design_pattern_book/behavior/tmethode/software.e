indexing
   description: "An abstract software package"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
deferred class SOFTWARE
feature {ANY} -- Creation (useful for concrete subclasses)
   make (source_file: STRING) is
	 -- Create and initialize the SOFTWARE with the 'source_file' archive
      require source_file_not_void: source_file /= Void
      do
	 source := source_file
      end -- make
feature {ANY} -- Queries
   source : STRING -- Where is stored the source of the software
   completed : BOOLEAN -- Whether the install was successful
feature {ANY} -- Commands
   install (destination_dir: STRING) is
      -- Template Method for installing a SOFTWARE
      require destination_dir_not_void: destination_dir /= Void
      do
	 print("Starting generic software installation.%N")
	 completed := False
	 if not is_valid(source) then
	    io.put_string(source); print(": invalid source file%N")
	 else
	    prepare_directory(destination_dir)
	    if completed then
	       install_files(source,destination_dir)
	       if completed then
		  build(destination_dir)
		  if completed then
		     print("Software installation completed.%N")
		  end -- if				
	       end -- if
	    end -- if
	 end -- if
      end -- install
   run (command_name : STRING) is
	 -- run this software under the name `command_name'
      require installation_completed: completed
      deferred
      end -- run
feature {NONE} -- Hooks for customizing the `install' Template Method 
   is_valid (filename : STRING) : BOOLEAN is
	 -- Whether file whose name is 'filename' is valid
      require filename_not_void: filename /= Void
      deferred
      end -- is_valid
   prepare_directory (directory : STRING) is
	 -- check for directory existence, creating it if needed
      require directory_not_void: directory /= Void
      deferred
      ensure ready_directory: completed implies is_valid(directory)
      end -- prepare_directory 
   install_files(source_file, destination_dir: STRING) is
	 -- Unpack the source into the destination_dir
      require
	 valid_source_file: is_valid(source_file)
	 valid_destination_dir: is_valid(destination_dir)
      deferred  
      end -- install_files
   build (destination_dir : STRING) is
      -- Finish the installation by calling whatever command is appropriate
      require
	 valid_destination_dir: is_valid(destination_dir)
      deferred
      end -- build
end -- SOFTWARE
