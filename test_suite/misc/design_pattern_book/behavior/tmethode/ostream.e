indexing
   description: "An out stream output facility"
   note: " "
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
expanded class OSTREAM
feature
   output (args : ARRAY[ANY])  is
      require
	 args_not_void : args /= Void
      local
	 i , top : INTEGER
      do
	 from
	    i := args.lower
	    top := args.upper
	 variant
	    decreasing : top - i
	 until
	    i > top
	 loop
	    args.item(i).print_on(std_output)
	    i := i + 1
	 end
      end
end -- OSTREAM
