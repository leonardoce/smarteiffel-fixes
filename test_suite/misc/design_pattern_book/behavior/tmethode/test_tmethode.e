indexing
   description: "Exemple root class: installation of a sofware"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train","Jean-Marc Jezequel"
class TEST_TMETHODE
creation make
feature {ANY}
   make is
      local
	 soft : SOFTWARE
      do
	 !UNIXSOFT!soft.make("mysoft.tar")
	 soft.install(".")
	 if soft.completed then
	    soft.run("mysoft")
	 end -- if
      end -- make
end
