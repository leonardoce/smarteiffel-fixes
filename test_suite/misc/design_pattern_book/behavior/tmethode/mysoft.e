indexing
   description: "An example software package"
   note: ""
   status: "$State$"
   date: "$Date: 2002-08-21 16:00:00 +0200 (Wed, 21 Aug 2002) $"
   revision: "$Revision$"
   author: "Michel Train"
class MYSOFT
   --@@@ install : copy files, compile and setup
   --@@@ run : execute
inherit
   SOFTWARE
      redefine install_files, setup end
creation
   make
feature
   make is
      -- initialized launch command
      do
	 launch_command := "go"
      end

   install_files(from_dir : STRING) : BOOLEAN is 
      -- uncompress files
      local
	 comm,res : STRING
	 cout : OSTREAM
      do
	 cout.output(<<"Uncompressing mysoft.tgz%N">>)
	 !!comm.copy("cd ")
	 comm.append(from_dir)
	 comm.append(" ; gnutar zxvf mysoft.tgz")
	 system(comm)
	 res := get_environment_variable("status")
	 if res /= Void and res = "0" then
	    Result := True
	 end
	 cout.output(<<"Command completed with status ",Result,"%N">>)
      end
   setup (to_dir : STRING) : BOOLEAN is
      -- copy resources files
      local
	 comm , res : STRING
      do
	 !!comm.copy("cd ")
	 comm.append(origin)
	 comm.append("; cp mysoft.res ")
	 comm.append(to_dir)
	 system(comm)
	 res := get_environment_variable("status")
	 if res /= Void and res = "0" then
	    Result := True
	 end
      end
end -- MYSOFT
