indexing
   description: "A SOFTWARE specialized for a Unix installation"
   note: ""
   status: "$State$"
   date: "$Date: 2005-05-19 18:37:32 +0200 (Thu, 19 May 2005) $"
   revision: "$Revision$"
   author: "Jean-Marc Jezequel"
class UNIXSOFT
inherit SOFTWARE
creation make
feature {ANY} -- Commands
   run (command_name : STRING) is
	 -- run this software under the name command_name
      do
	 print("Running "); print(command_name); io.put_new_line
      end -- run
feature {NONE} -- Queries
   is_valid (filename : STRING) : BOOLEAN is
	 -- Whether file whose name is 'filename' can be read
      do
	 Result := (create {FILE_TOOLS}).is_readable(filename)
      end -- is_readable
   prepare_directory (directory : STRING) is
	 -- check for directory existence, creating it if needed
      do
	 if is_valid(directory) then
	    completed := True
	    print("Directory "); print(directory); 
	    print(" already existing. Reusing it.%N") 
	 else
	    print("Creating destination directory "); print(directory)
	    launch_system_command({ARRAY[STRING] 1, <<"mkdir ",directory>> })
	    get_last_system_command_status
	    if completed then
	       print("Done.%N")
	    else
	       print("Failed.%N")
	    end -- if
	 end -- if
      end -- prepare_directory
   install_files(source_file, destination_dir: STRING) is
	 -- Unpack the source_file into the destination_dir
      do
	 print("Untaring "); print(source_file)
	 print(" into "); print(destination_dir); io.put_new_line
	 launch_system_command({ARRAY[STRING] 1, <<"tar xvf ",source>> })
	 get_last_system_command_status
	 if completed then
	    print("Done.%N")
	 else
	    print("Failed.%N")
	 end -- if
      end -- install_files
   build (destination_dir : STRING) is
      -- Finish the installation by calling whatever command is appropriate
      do
	 print("Running make in "); print(destination_dir); io.put_new_line
	 launch_system_command({ARRAY[STRING] 1, <<"cd ",destination_dir,"; make">> })
	 get_last_system_command_status
	 if completed then
	    print("Done.%N")
	 else
	    print("Failed.%N")
	 end -- if
      end -- build
feature {NONE} -- Internals
   get_last_system_command_status is
      -- Set the boolean variable 'completed' according 
      -- to the status of the last system command
      do
	 completed := True -- Simulate actual call
      end -- get_last_system_command_status
   launch_system_command (arg : ARRAY[STRING]) is
	 -- Concatenate the strings in the array and use the result
	 -- as a command for the underlying system
      require arg_not_void: arg /= Void
      local 
	 command : STRING
	 i : INTEGER
      do
	 !!command.make(80)
	 from i := arg.lower until i> arg.upper 
	 loop 
	    command.append_string(arg.item(i))
	    i := i + 1
	 end -- loop
	 print("   -->"); print(command); io.put_new_line -- Simulate execution
      end -- launch_system_command
end -- UNIXSOFT

